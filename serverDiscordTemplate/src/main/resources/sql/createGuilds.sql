CREATE DATABASE <DB_NAME>;

CREATE TABLE GUILDS (
ID_GUILD BIGINT UNSIGNED NOT NULL PRIMARY KEY,
PAY_LEVEL TINYINT DEFAULT 0,
PAY_END DATE,
PREFIX VARCHAR(3),
LOCALE VARCHAR(2)
);

-------------------------------
--PAY_LEVEL 0 se utente FREE, altri valori utenti paganti (fino a un massimo di 127)