package it.bitsplitters.bottemplate.util;

import static it.bitsplitters.bottemplate.Config.RESOURCE_NAME;
import static it.bitsplitters.util.ResourceBundleUtil.customControl;
import static java.util.ResourceBundle.getBundle;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class ResourceBundleUtil {

	private static Map<Locale, ResourceBundle> guildsRB = new HashMap<>();
	
	public static ResourceBundle getResourceBundle(Locale locale) {
		
		if(guildsRB.containsKey(locale))
			return guildsRB.get(locale);
		
		ResourceBundle bundle = getBundle(RESOURCE_NAME, locale, customControl);
		
		guildsRB.put(locale, bundle);
		
		return bundle;
	}
}
