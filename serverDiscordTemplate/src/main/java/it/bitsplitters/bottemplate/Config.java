package it.bitsplitters.bottemplate;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import it.bitsplitters.command.GeneralCommand;
import it.bitsplitters.setting.BotConfig;

/**
 * Where all configurations reside.
 * @author AVurro
 *
 */
public class Config {
	
	private static final Logger logger = LogManager.getLogger();
	
	/** where the configurations reside */
	private static final Properties configProperties = new Properties();
	
	/** Discord token, used to communicate with Discord */
	private static final String botToken = "bot.token";
	
	/** CoC Token, used to interact with Clash of Clans API */
	private static final String cocAPI = "coc.api";
	
	/** DB username */
	private static final String dbUsername = "db.username";
	
	/** DB password */
	private static final String dbPassword = "db.password";
	
	/** DB url */
	private static final String dbUrl = "db.url";

	/** JOOQ context, used to interact with DB */
	private static DSLContext context;
    
	/** Server Discord configuration, how the bot interacts with the server */
	public static final BotConfig botConfig;

	/** Manager that verifies if a specific user can use the command invoked */
	public static final BotCommandsManager commandsManager;
	
	/** Guilds Manager */
	public static final GuildsManager guildsManager;
	
	/** Bot Commands, the core of the bot */
	public static final GeneralCommand[] commands;
	
	/** resource bundle name */
	public static final String RESOURCE_NAME = "resource";
	
    static {
    	
    	commands = new GeneralCommand[] {};
    	commandsManager = new BotCommandsManager();
    	guildsManager = new GuildsManager();
    	botConfig = new BotConfig() {
    		@Override public boolean isCaseSensitive() { return false; }
    		@Override public List<Long> adminIDs() { return Arrays.asList(0L); }
    		@Override public String resourceBundle() { return ""; }
    	};
    	
		try(FileInputStream file = new FileInputStream("./config.properties")) {
		    configProperties.load(file);

			BasicDataSource ds = new BasicDataSource();
			ds.setUrl(configProperties.getProperty(dbUrl));
			ds.setDriverClassName("org.mariadb.jdbc.Driver");
			ds.setUsername(configProperties.getProperty(dbUsername));
			ds.setPassword(configProperties.getProperty(dbPassword));
			ds.setMinIdle(5);
			ds.setMaxIdle(10);
			ds.setMaxOpenPreparedStatements(10);
			context = DSL.using(ds, SQLDialect.MYSQL);
			
		} catch (IOException e) { logger.atError().log(e.getMessage()); }
    }
    
    public static DSLContext jooq() {
    	return context;
    }
	
	public static String getBottoken() {
		return configProperties.getProperty(botToken);
	}

	public static String getCocapi() {
		return configProperties.getProperty(cocAPI);
	}


	private Config() {}
}
