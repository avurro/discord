package it.bitsplitters.bottemplate;

import static it.bitsplitters.bottemplate.Config.botConfig;
import static it.bitsplitters.bottemplate.Config.commands;
import static it.bitsplitters.bottemplate.Config.commandsManager;
import static it.bitsplitters.bottemplate.Config.getBottoken;
import static it.bitsplitters.bottemplate.Config.guildsManager;
import static it.bitsplitters.util.BotUtil.unauthorizedEmbed;

import java.util.Optional;

import javax.security.auth.login.LoginException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.BotManager;
import it.bitsplitters.command.exceptions.CommandPermissionException;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
public class MessageListener extends ListenerAdapter
{
	
	private static final Logger logger = LogManager.getLogger();
	
	private BotManager botManager;
	
	public MessageListener() {
	    botManager = new BotManager(botConfig, commandsManager, null, guildsManager, commands);
	}
	/**
     * This is the method where the program starts.
     */
    public static void main(String[] args)
    {
        //We construct a builder for a BOT account. If we wanted to use a CLIENT account
        // we would use AccountType.CLIENT
        try
        {
            JDA jda = JDABuilder.createDefault(getBottoken()) // The token of the account that is logging in.
                    .addEventListeners(new MessageListener())   // An instance of a class that will handle events.
                    .build();
            jda.awaitReady(); // Blocking guarantees that JDA will be completely loaded.
            logger.atInfo().log("Finished Building JDA!");
            
        }
        catch (LoginException e)
        {
            //If anything goes wrong in terms of authentication, this is the exception that will represent it
        	logger.atError().log(e.getMessage());
        }
        catch (InterruptedException e)
        {
            //Due to the fact that awaitReady is a blocking method, one which waits until JDA is fully loaded,
            // the waiting can be interrupted. This is the exception that would fire in that situation.
            //As a note: in this extremely simplified example this will never occur. In fact, this will never occur unless
            // you use awaitReady in a thread that has the possibility of being interrupted (async thread usage and interrupts)
        	logger.atError().log(e.getMessage());
        }
    }
    
    @Override
    public void onGuildLeave(GuildLeaveEvent event) {
    	logger.atInfo().log("Server uscito! {}", event.getGuild().getName());
    	guildsManager.guildLeave(event.getGuild().getIdLong());
    	
    }
    
    @Override
    public void onGuildJoin(GuildJoinEvent event) {
    	logger.atInfo().log("Nuovo server! {}", event.getGuild().getName());
    	guildsManager.guildJoin(event.getGuild().getIdLong());
    }
    
    /**
     * @param event
     *          An event containing information about a {@link net.dv8tion.jda.api.entities.Message Message} that was
     *          sent in a channel.
     */
    @Override
    public void onMessageReceived(MessageReceivedEvent event)
    {

        //Event specific information
        User author = event.getAuthor();                //The user that sent the message
        Message message = event.getMessage();           //The message that was received.

        String msg = message.getContentDisplay();       //This returns a human readable version of the Message. Similar to
                                                        // what you would see in the client.

        if(author.isBot()) //This boolean is useful to determine if the User that
        	return;  	   //sent the Message is a BOT or not!
		
    	if (event.isFromType(ChannelType.TEXT)){         //If this message was sent to a Guild TextChannel
        
            //Because we now know that this message was sent in a Guild, we can do guild specific things
            // Note, if you don't check the ChannelType before using these methods, they might return null due
            // the message possibly not being from a Guild!

            Guild guild = event.getGuild();             //The Guild that this message was sent in. (note, in the API, Guilds are Servers)
            TextChannel textChannel = event.getTextChannel(); //The TextChannel that this message was sent to.
            Member member = event.getMember();          //This Member that sent the message. Contains Guild specific information about the User!

            String name = message.isWebhookMessage()?
                          author.getName():             //If this is a Webhook message, then there is no Member associated
                          member.getEffectiveName();    //This will either use the Member's nickname if they have one,
                                                        //otherwise it will default to their username. (User#getName())

            logger.atInfo().log("({})[{}]<{}>: {}", guild.getName(), textChannel.getName(), name, msg);
            
            Optional<Exception> optError = botManager.findAndRunGuildCommand(event);
            if(optError.isPresent()) {
            	if(optError.get().getMessage() != null) {
            		if(optError.get() instanceof CommandPermissionException) {
            			unauthorizedEmbed(event, optError.get().getMessage());
            		}else {
            			event.getChannel().sendMessage(optError.get().getMessage()).queue();
            		}
            	}else
            		optError.get().printStackTrace();
            }
        }
        
    }
}
