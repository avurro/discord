package it.bitsplitters.bottemplate;

import static net.dv8tion.jda.api.Permission.ADMINISTRATOR;

import it.bitsplitters.CommandsManager;
import it.bitsplitters.command.Command;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class BotCommandsManager implements CommandsManager {

	@Override
	public boolean isCommandRunnable(Command command, MessageReceivedEvent event) {
		
		if(command.permissionLevel().isAdmin())
			return event.getMember().getPermissions().contains(ADMINISTRATOR);
		
		if(command.permissionLevel().isPublic()) 
			return true;
		
		return false;
	}

}
