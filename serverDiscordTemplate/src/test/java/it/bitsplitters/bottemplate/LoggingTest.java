package it.bitsplitters.bottemplate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

public class LoggingTest{

	private static final Logger logger = LogManager.getLogger();
	
	@Test public void testLogging() { 
		logger.atError().log("-------------testing logger-------------");
	}
}
