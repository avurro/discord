package it.bitsplitter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;

import it.bitsplitters.clashapi.ClanMember;
import it.bitsplitters.clashapi.ClashAPI;
import it.bitsplitters.clashapi.ClashAPIAsync;
import it.bitsplitters.clashapi.ClashWrapper;
import it.bitsplitters.clashapi.Player;
import it.bitsplitters.clashapi.PlayerToken;
import it.bitsplitters.clashapi.exception.ClashException;

public class Test {

	private static void asyncTest() {
//		ClashAPIAsync api = ClashWrapper.getAPIInstanceAsync(() ->"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6ImE4NzhiYTNkLTczMGUtNGYxYi1hNzEzLWUwYjE1YWNhMjBjMCIsImlhdCI6MTYxNDE5MzEwMiwic3ViIjoiZGV2ZWxvcGVyLzM1MmEyNDc0LTQ4ZTQtZjhiNi0zYTI0LTY0ZTFkMTZlMGYyNiIsInNjb3BlcyI6WyJjbGFzaCJdLCJsaW1pdHMiOlt7InRpZXIiOiJkZXZlbG9wZXIvc2lsdmVyIiwidHlwZSI6InRocm90dGxpbmcifSx7ImNpZHJzIjpbIjkzLjM3LjEzMy4xNzciXSwidHlwZSI6ImNsaWVudCJ9XX0.nFodxu3vN8_J1ifIiuDRyKs_k-xCVOOJCXVsPFLPA8Q5CbGubeVvEu5gZTG8_cmMqeUhoRVd381tZyDzBVQ46w");
//		try {
//			//invalid
//			//ok
//			PlayerToken playerToken = api.verifyToken("#V0LJVU2J", "zdzt7x3a")
//			.whenComplete((result, ex) -> {
//				
//				if(ex != null) {
//					ex.toString();
//				}
//				
//				System.out.println(result.getStatus());
//			}) .get();
//			System.out.println(playerToken.getStatus());
//			
//		} catch (CompletionException e) {
//			e.getCause().printStackTrace();
//		} catch (URISyntaxException e) {
//			e.printStackTrace();
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		} catch (ExecutionException e) {
//			e.printStackTrace();
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}
	}
	
	public static void main(String[] args) {
		asyncTest();
		ClashAPI api = ClashWrapper.getAPIInstance("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6ImE4NzhiYTNkLTczMGUtNGYxYi1hNzEzLWUwYjE1YWNhMjBjMCIsImlhdCI6MTYxNDE5MzEwMiwic3ViIjoiZGV2ZWxvcGVyLzM1MmEyNDc0LTQ4ZTQtZjhiNi0zYTI0LTY0ZTFkMTZlMGYyNiIsInNjb3BlcyI6WyJjbGFzaCJdLCJsaW1pdHMiOlt7InRpZXIiOiJkZXZlbG9wZXIvc2lsdmVyIiwidHlwZSI6InRocm90dGxpbmcifSx7ImNpZHJzIjpbIjkzLjM3LjEzMy4xNzciXSwidHlwZSI6ImNsaWVudCJ9XX0.nFodxu3vN8_J1ifIiuDRyKs_k-xCVOOJCXVsPFLPA8Q5CbGubeVvEu5gZTG8_cmMqeUhoRVd381tZyDzBVQ46w");
		
		try {
			Player requestPlayer = api.requestPlayer("#2CG8CC902");
			System.out.println(requestPlayer.getTrophies());
		} catch (IOException | ClashException e1) {
			e1.printStackTrace();
		}
		try {
			for (ClanMember member : api.requestClanMembers("#CJC2G8L")) {
				System.out.println(member.toString());
			};
		} catch (IOException | ClashException e) {
			e.printStackTrace();
		}
//		ArrayList<String> tig = new ArrayList<>();
//		tig.add("#2cqrrr08q");
//		tig.add("#R2G0JJJC");
//		tig.add("#2P9PV9J8P");
//		tig.add("#22JG0GRCY");
//		tig.add("#RQCVLCUY"); 
//		tig.add("#YRY9JYVP"); 
//		tig.add("#R2GUYY9P"); 
//		tig.add("#82V22PGYC");
//		tig.add("#R8CV8YQ9"); 
//		tig.add("#JG22VPVR"); 
//		tig.add("#R8GG9RR0");
//		tig.add("#2JVPLGJVY");
//		tig.add("#20JPV08V"); 
//		tig.add("#22Q80QLJ9");
//		tig.add("#2P8P09LL"); 
//		tig.add("#2U8LRG00R");
//		tig.add("#Y29CLGYP9");
//		tig.add("#C9CRUR2Q"); 
//		tig.add("#Y0089RLL"); 
//		tig.add("#Q892VQCP"); 
//		tig.add("#LJ99UV8Y"); 
//		tig.add("#JLC82V8P"); 
//		tig.add("#20Q2P908J");
//		tig.add("#VQLYOLCP"); 
//		tig.add("#QRGGUP8Y"); 
//		tig.add("#QRPU8VUP"); 
//		tig.add("#2GORJ8PP2");
//		tig.add("#80YV0GQJY");
//		long start = System.currentTimeMillis();
//		System.out.println(LocalDateTime.now());
//		for (String str : tig) {
//			
//			try {
//				Player 
//				player = api.requestPlayer("#V0LJVU2J","trophies");
////				player = api.requestPlayer(str);
//				
//			} catch (IOException | ClashException e) {
//				System.out.println(str);
//				e.printStackTrace();
//			}
//		}
//
//		long end = System.currentTimeMillis();
//		System.out.println(LocalDateTime.now() + " Durata secondi: " + (end - start));
//		System.out.println();
		
	}

}
