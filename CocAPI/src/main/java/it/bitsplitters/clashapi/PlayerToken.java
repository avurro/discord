package it.bitsplitters.clashapi;

public interface PlayerToken {

	String getTag();
	
	String getToken();
	
	String getStatus();
}
