package it.bitsplitters.clashapi;

import java.util.concurrent.ExecutorService;

public interface CocApiAsyncConfig {

	public ExecutorService getService();
	public String getToken();
	public int timeout();
}
