package it.bitsplitters.clashapi;

public interface War {

	public String getState();
	public Integer getTeamSize();
	public String getPreparationStartTime();
	public String getStartTime();
	public String getEndTime();
	public WarClan getWarClan();
	public WarClan getOpponent();
}
