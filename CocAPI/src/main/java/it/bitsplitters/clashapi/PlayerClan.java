package it.bitsplitters.clashapi;

public interface PlayerClan {

    public String getTag();

	public String getName();
	
	public Integer getClanLevel();
}
