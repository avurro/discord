package it.bitsplitters.clashapi;

public interface Hero {

	public enum Type{
		KING, QUEEN, WARDEN, BATTLE_MACHINE;
		
		public static Type get(String type){
			switch(type){
				case "Barbarian King": return Type.KING;
				case "Archer Queen": return Type.QUEEN; 
				case "Grand Warden": return Type.WARDEN;
				case "Battle Machine": return Type.BATTLE_MACHINE;
			}
			return null;
		}
	}
	
	public enum Village{
		HOME, BUILDER_BASE;
		
		public static Village get(String type){
			switch(type){
				case "home": return Village.HOME;
				case "builderBase": return Village.BUILDER_BASE; 
			}
			return null;
		}
	}
	Type getType();
	
	Village getVillage();
	
	Integer getLevel();
	
	Integer getMaxLevel();
	
}
