package it.bitsplitters.clashapi.exception;

/**
 * Thrown when there is maintenance in progress..
 */
@SuppressWarnings("serial")
public class MaintenanceException extends ClashException {

	public MaintenanceException(String message) {
		super(message);
	}

}
