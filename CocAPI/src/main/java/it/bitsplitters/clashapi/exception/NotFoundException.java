package it.bitsplitters.clashapi.exception;

/**
 * Thrown when the URL is invalid
 */
@SuppressWarnings("serial")
public class NotFoundException extends ClashException {

    public NotFoundException(String message) {
        super(message);
    }
}
