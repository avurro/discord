package it.bitsplitters.clashapi.exception;

@SuppressWarnings("serial")
public class BadRequestException extends ClashException {

    public BadRequestException(String message) {
        super(message);
    }
}
