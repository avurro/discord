package it.bitsplitters.clashapi.exception;

public class HttpRequestTimeoutException extends ClashException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5459595557843266700L;

	public HttpRequestTimeoutException(String message) {
		super(message);
	}


}
