package it.bitsplitters.clashapi.exception;

/**
 * Thrown if an unknown error occurs
 */
@SuppressWarnings("serial")
public class UnknownErrorException extends ClashException {

    public UnknownErrorException(String message) {
        super(message);
    }
}
