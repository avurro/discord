package it.bitsplitters.clashapi.exception;

/**
 * Thrown when token is invalid or ip address isn't registered
 */
@SuppressWarnings("serial")
public class AuthenticationException extends ClashException {

    public AuthenticationException(String message) {
        super(message);
    }
}
