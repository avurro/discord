package it.bitsplitters.clashapi.exception;

/**
 * Parent class for all exceptions related to the clash API
 */
@SuppressWarnings("serial")
public class ClashException extends Exception {

    public ClashException(String message) {
        super(message);
    }
}
