package it.bitsplitters.clashapi.exception;

@SuppressWarnings("serial")
public class GatewayTimeoutException extends ClashException {

	public GatewayTimeoutException(String message) {
		super(message);
	}
}
