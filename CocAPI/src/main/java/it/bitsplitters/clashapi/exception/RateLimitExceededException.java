package it.bitsplitters.clashapi.exception;

/**
 * Thrown when rate limit has been exceeded.
 */
@SuppressWarnings("serial")
public class RateLimitExceededException extends ClashException {

    public RateLimitExceededException(String message) {
        super(message);
    }
}
