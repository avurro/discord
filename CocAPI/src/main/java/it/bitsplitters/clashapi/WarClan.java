package it.bitsplitters.clashapi;

import java.util.List;

public interface WarClan {

	public String tag();

	public String name();

	public Integer clanLevel();

	public Integer attacks();

	public Integer stars();

	public Double destructionPercentage();

	public List<WarMember> members();
}
