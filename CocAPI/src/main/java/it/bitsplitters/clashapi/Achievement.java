package it.bitsplitters.clashapi;

public interface Achievement {

	String getName();
	
	Object getCompletionInfo();
	
	Integer getStars();
	
	String getVillage();
	
	Integer getValue();
	
	Integer getTarget();
	
	Object getInfo();
	
}
