package it.bitsplitters.clashapi;

public interface WarAttack {
	
	public String attackerTag();
	
	public Double destructionPercentage();
	
	public String defenderTag();
	
	public Integer stars();
	
	/**
	 * @return torna l'ordine di attacco, ad esempio 2 se è stato il secondo attacco da inizio war
	 */
	public Integer getOrder();
}
