package it.bitsplitters.clashapi;

import it.bitsplitters.clashapi.impl.ClashAPIImpl;
import it.bitsplitters.clashapi.impl.ClashAPIImplAsync;

/**
 * API Factory class. This class should be accessed to create an instance of ClashAPI with your API token.
 */
public class ClashWrapper {

    /**
     * Creates a ClashAPI instance with the provided API token
     *
     * @param apiToken the api token
     * @return an API instance
     */
    public static ClashAPI getAPIInstance(String apiToken) {
        return new ClashAPIImpl(apiToken);
    }
    
    /**
     * Creates a ClashAPI instance with the provided API token
     *
     * @param apiToken the api token
     * @return an API instance
     */
    public static ClashAPIImplAsync getAPIInstanceAsync(CocApiAsyncConfig config) {
        return new ClashAPIImplAsync(config);
    }
}
