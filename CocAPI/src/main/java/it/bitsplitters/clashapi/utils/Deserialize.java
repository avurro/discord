package it.bitsplitters.clashapi.utils;

import org.json.JSONArray;
import org.json.JSONObject;

public class Deserialize {

	public static Object getObject(JSONObject root, String key) {
		return root.has(key) ? root.get(key): null;
	}
	
	public static JSONObject getJSONObject(JSONObject root, String key) {
		return root.has(key) ? root.getJSONObject(key): null;
	}
	
	public static JSONArray getJSONArray(JSONObject root, String key) {
		return root.has(key) ? root.getJSONArray(key): null;
	}
	
	public static String getString(JSONObject root, String key) {
		return root.has(key) ? root.getString(key): null;
	}
	
	public static Integer getInteger(JSONObject root, String key) {
		return root.has(key) ? root.getInt(key): null;
	}
}
