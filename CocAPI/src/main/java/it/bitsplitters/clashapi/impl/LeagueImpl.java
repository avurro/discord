package it.bitsplitters.clashapi.impl;

import org.json.JSONObject;

import it.bitsplitters.clashapi.IconSet;
import it.bitsplitters.clashapi.League;

public class LeagueImpl implements League {

    private int id;
    private String name;
    private IconSet icons;

    protected LeagueImpl(JSONObject root) {
        id = root.getInt("id");
        name = root.getString("name");
        icons = new IconSetImpl(root.getJSONObject("iconUrls"));
    }

    public IconSet getIcons() {
        return icons;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "LeagueImpl{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", icons=" + icons +
                '}';
    }
}
