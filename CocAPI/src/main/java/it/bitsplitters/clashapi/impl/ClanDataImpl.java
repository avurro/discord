package it.bitsplitters.clashapi.impl;

import org.json.JSONObject;

import it.bitsplitters.clashapi.ClanData;
import it.bitsplitters.clashapi.IconSet;

public class ClanDataImpl implements ClanData {

    protected String tag;
    protected String name;
    protected IconSet badgeIcons;

    protected ClanDataImpl(JSONObject root) {
        tag = root.getString("tag");
        name = root.getString("name");
        badgeIcons = new IconSetImpl(root.getJSONObject("badgeUrls"));
    }

    public IconSet getBadgeIcons() {
        return badgeIcons;
    }

    public String getName() {
        return name;
    }

    public String getTag() {
        return tag;
    }

    @Override
    public String toString() {
        return "ClanDataImpl{" +
                "tag='" + tag + '\'' +
                ", name='" + name + '\'' +
                ", badgeIcons=" + badgeIcons +
                '}';
    }
}
