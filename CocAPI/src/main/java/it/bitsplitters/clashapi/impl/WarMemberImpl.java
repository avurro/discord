package it.bitsplitters.clashapi.impl;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import it.bitsplitters.clashapi.WarAttack;
import it.bitsplitters.clashapi.WarMember;

public class WarMemberImpl implements WarMember {
	
	private String name;
	private String tag;
	private Integer townhallLevel;
	private Integer mapPosition;
	private Integer opponentAttacks;
	private List<WarAttack> attacks;
	private WarAttack bestOpponentAttack;
	
	public WarMemberImpl(JSONObject root) {
		this.name = root.getString("name");
		this.tag = root.getString("tag");
		this.townhallLevel = root.getInt("townhallLevel");
		this.mapPosition = root.getInt("mapPosition");
		this.opponentAttacks = root.getInt("opponentAttacks");
		
		this.attacks = new ArrayList<WarAttack>();
		if(root.has("attacks")){
			JSONArray memberList = root.getJSONArray("attacks");
			for (int i = 0; i < memberList.length(); i++) 
				this.attacks.add(new WarAttackImpl(memberList.getJSONObject(i)));
		}
		
		if(root.has("bestOpponentAttack"))
		this.bestOpponentAttack = new WarAttackImpl(root.getJSONObject("bestOpponentAttack"));
		
	}
	public String name() {
		return name;
	}

	public String tag() {
		return this.tag;
	}

	public Integer townHallLevel() {
		return this.townhallLevel;
	}

	public Integer mapPosition() {
		return this.mapPosition;
	}

	public Integer opponentAttacks() {
		return this.opponentAttacks;
	}

	public List<WarAttack> attacks() {
		return this.attacks;
	}

	public WarAttack bestOpponentAttack() {
		return this.bestOpponentAttack;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tag == null) ? 0 : tag.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WarMemberImpl other = (WarMemberImpl) obj;
		if (tag == null) {
			if (other.tag != null)
				return false;
		} else if (!tag.equals(other.tag))
			return false;
		return true;
	}

	
}
