package it.bitsplitters.clashapi.impl;

import org.json.JSONObject;

import it.bitsplitters.clashapi.Achievement;
import static it.bitsplitters.clashapi.utils.Deserialize.*;

public class AchievementImpl implements Achievement {
	private String name;
	
	private Object completionInfo;
	
	private Integer stars;
	
	private String village;
	
	private Integer value;
	
	private Integer target;
	
	private Object info;
	
	public AchievementImpl(JSONObject root) {
		
		this.name = getString(root, "name");
		this.completionInfo = getObject(root, "completionInfo");
		this.stars = getInteger(root, "stars");
		this.village = getString(root, "village");
		this.value = getInteger(root, "value");
		this.target = getInteger(root, "target");
		this.info = getObject(root, "info");
		
	}
	
	@Override	public String getName() {
		return this.name;
	}

	@Override	public Object getCompletionInfo() {
		return this.completionInfo;
	}

	@Override	public Integer getStars() {
		return this.stars;
	}

	@Override	public String getVillage() {
		return this.village;
	}

	@Override	public Integer getValue() {
		return this.value;
	}

	@Override	public Integer getTarget() {
		return this.target;
	}

	@Override	public Object getInfo() {
		return this.info;
	}

	@Override
	public String toString() {
		return "AchievementImpl [name=" + name + ", completionInfo=" + completionInfo + ", stars=" + stars
				+ ", village=" + village + ", value=" + value + ", target=" + target + ", info=" + info + "]\n";
	}

	
}
