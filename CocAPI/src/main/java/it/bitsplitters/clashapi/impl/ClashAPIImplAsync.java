package it.bitsplitters.clashapi.impl;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

import org.json.JSONObject;

import it.bitsplitters.clashapi.Clan;
import it.bitsplitters.clashapi.ClashAPIAsync;
import it.bitsplitters.clashapi.CocApiAsyncConfig;
import it.bitsplitters.clashapi.Player;
import it.bitsplitters.clashapi.PlayerToken;
import it.bitsplitters.clashapi.exception.AuthenticationException;
import it.bitsplitters.clashapi.exception.BadRequestException;
import it.bitsplitters.clashapi.exception.GatewayTimeoutException;
import it.bitsplitters.clashapi.exception.HttpRequestTimeoutException;
import it.bitsplitters.clashapi.exception.MaintenanceException;
import it.bitsplitters.clashapi.exception.NotFoundException;
import it.bitsplitters.clashapi.exception.RateLimitExceededException;
import it.bitsplitters.clashapi.exception.UnknownErrorException;

public class ClashAPIImplAsync implements ClashAPIAsync {
	
	private static final String API_BASE = "https://api.clashofclans.com/";
    private static final String API_VERSION = "v1";
    
	private CocApiAsyncConfig config;

    public ClashAPIImplAsync(CocApiAsyncConfig config) {
		this.config = config;
	}
    
	@Override
	public CompletableFuture<PlayerToken> verifyToken(String playerTag, String token)
			throws CompletionException, URISyntaxException, UnsupportedEncodingException {

		return postAPIAsync(String.join("", "players/",URLEncoder.encode(playerTag, "UTF-8"),"/verifytoken"), 
				     String.join("", "{\"token\":\"",token,"\"}"))
					 .thenApply(json -> new PlayerTokenImpl(json));
	}
	
	@Override
	public CompletableFuture<Clan> requestClan(String clanTag) throws CompletionException, URISyntaxException, UnsupportedEncodingException {
        return getAPIAsync("clans/"+URLEncoder.encode(clanTag, "UTF-8")).thenApply(json -> new ClanImpl(json));
    }
	 
	@Override
	public CompletableFuture<Player> requestPlayer(String playerTag) throws CompletionException, URISyntaxException, UnsupportedEncodingException {
		return getAPIAsync("players/"+URLEncoder.encode(playerTag, "UTF-8")).thenApply(json -> new PlayerImpl(json));
	}

	private CompletableFuture<JSONObject> getAPIAsync(String suffix) throws URISyntaxException, CompletionException, UnsupportedEncodingException{
		
    	HttpRequest httpRequest = HttpRequest.newBuilder(new URI(API_BASE + API_VERSION + "/" + suffix))
    	           .header("Accept", "application/json")
    	           .header("Authorization", "Bearer " + config.getToken())
    	           .version(Version.HTTP_1_1)
    	           .timeout(Duration.ofSeconds(config.timeout()))
    	           .GET().build();
    	
    	return sendAsync(httpRequest);
    }

	private CompletableFuture<JSONObject> postAPIAsync(String suffix, String content) throws URISyntaxException, CompletionException, UnsupportedEncodingException{
		
    	HttpRequest httpRequest = HttpRequest.newBuilder(new URI(API_BASE + API_VERSION + "/" + suffix))
    	           .header("Accept", "application/json")
    	           .header("Authorization", "Bearer " + config.getToken())
    	           .version(Version.HTTP_1_1)
    	           .timeout(Duration.ofSeconds(config.timeout()))
    	           .POST(BodyPublishers.ofString(content)).build();
    	
    	return sendAsync(httpRequest);
    }
	private CompletableFuture<JSONObject> sendAsync(HttpRequest httpRequest){
		return HttpClient.newBuilder().executor(config.getService()).build()
  			  .sendAsync(httpRequest, HttpResponse.BodyHandlers.ofString()) 
			  
  			  .thenApply(result -> {

  				  switch (result.statusCode()) {
	                  case 400: throw new CompletionException(new BadRequestException(result.body().replace("\n", "")));
	                  case 403: throw new CompletionException(new AuthenticationException(result.body().replace("\n", "")));
	                  case 404: throw new CompletionException(new NotFoundException(result.body().replace("\n", "")));
	                  case 408: throw new CompletionException(new HttpRequestTimeoutException(result.body().replace("\n", "")));
	                  case 429: throw new CompletionException(new RateLimitExceededException(result.body().replace("\n", "")));
	                  case 503: throw new CompletionException(new MaintenanceException(result.body().replace("\n", "")));
	                  case 504: throw new CompletionException(new GatewayTimeoutException(result.body().replace("\n", "")));
	                  case 200: return new JSONObject(result.body());
	                  default: throw new CompletionException(new UnknownErrorException(result.statusCode() + ": " + result.body().replace("\n", "")));
  			  }
  			  
  		 });
	}
	
}
