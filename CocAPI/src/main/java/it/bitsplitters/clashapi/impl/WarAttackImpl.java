package it.bitsplitters.clashapi.impl;

import org.json.JSONObject;

import it.bitsplitters.clashapi.WarAttack;

public class WarAttackImpl implements WarAttack {
	
	private String attackerTag;
	private Double destructionPercentage;
	private String defenderTag;
	private Integer stars;
	private Integer order;
	
	
	public WarAttackImpl(JSONObject root) {
		this.attackerTag = root.getString("attackerTag");
		this.defenderTag = root.getString("defenderTag");
		this.destructionPercentage = root.getDouble("destructionPercentage");
		this.stars = root.getInt("stars");
		this.order = root.getInt("order");
	}
	
	public String attackerTag() {
		return attackerTag;
	}
	public void setAttackerTag(String attackerTag) {
		this.attackerTag = attackerTag;
	}
	public Double destructionPercentage() {
		return destructionPercentage;
	}
	public void setDestructionPercentage(Double destructionPercentage) {
		this.destructionPercentage = destructionPercentage;
	}
	public String defenderTag() {
		return defenderTag;
	}
	public void setDefenderTag(String defenderTag) {
		this.defenderTag = defenderTag;
	}
	public Integer stars() {
		return stars;
	}
	public void setStars(Integer stars) {
		this.stars = stars;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}

}
