package it.bitsplitters.clashapi.impl;

import org.json.JSONObject;

import it.bitsplitters.clashapi.PlayerClan;

public class PlayerClanImpl implements PlayerClan {

	private String tag;
	private String name;
	private Integer clanLevel;
	
	public PlayerClanImpl() {}
	
	public PlayerClanImpl(JSONObject root) {
	    	name = root.getString("name");
	    	tag = root.getString("tag");
	    	clanLevel = root.getInt("clanLevel");
	}
	@Override
	public String getTag() {
		return tag;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Integer getClanLevel() {
		return clanLevel;
	}

}
