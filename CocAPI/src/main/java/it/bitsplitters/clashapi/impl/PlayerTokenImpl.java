package it.bitsplitters.clashapi.impl;

import org.json.JSONObject;

import it.bitsplitters.clashapi.PlayerToken;

public class PlayerTokenImpl implements PlayerToken {

	private String tag;
	private String token;
	private String status;
	
	public PlayerTokenImpl() {
	}
	
	public PlayerTokenImpl(JSONObject root) {
		tag = root.getString("tag");
		token = root.getString("token");
		status = root.getString("status");
	}

	
	@Override
	public String getTag() {
		return this.tag;
	}

	@Override
	public String getToken() {
		return this.token;
	}

	@Override
	public String getStatus() {
		return this.status;
	}

}
