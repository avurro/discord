package it.bitsplitters.clashapi.impl;

import org.json.JSONObject;

import it.bitsplitters.clashapi.ClanMember;
import it.bitsplitters.clashapi.ClanMemberRole;

public class ClanMemberImpl implements ClanMember {

    private ClanMemberRole role;
    private int clanRank;
    private int previousClanRank;
    private int troopsDonated;
    private int troopsReceived;
    private String name;
    private String tag;
    private int trophies;
    
    protected ClanMemberImpl(JSONObject root) {
        role = ClanMemberRole.fromId(root.getString("role"));
        clanRank = root.getInt("clanRank");
        previousClanRank = root.getInt("previousClanRank");
        troopsDonated = root.getInt("donations");
        troopsReceived = root.getInt("donationsReceived");
        name = root.getString("name");
        tag = root.getString("tag");
        trophies = root.getInt("trophies");
    }

    public int getClanRank() {
        return clanRank;
    }

    public int getNumberOfTroopsDonated() {
        return troopsDonated;
    }

    public int getNumberOfTroopsReceived() {
        return troopsReceived;
    }

    public int getPreviousClanRank() {
        return previousClanRank;
    }

    public ClanMemberRole getRole() {
        return role;
    }

    
    public String getName() {
		return name;
	}

	public String getTag() {
		return tag;
	}

	public int getTrophies() {
		return trophies;
	}

	@Override
	public String toString() {
		return "ClanMemberImpl [name=" + name + ", tag=" + tag + ", trophies=" + trophies + "]";
	}

	
}
