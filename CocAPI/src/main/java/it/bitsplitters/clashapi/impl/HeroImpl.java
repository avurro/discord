package it.bitsplitters.clashapi.impl;

import org.json.JSONObject;

import it.bitsplitters.clashapi.Hero;

public class HeroImpl implements Hero {

	private final Type type;
	private final Village village;
	private final Integer maxLevel;
	private final Integer level;
	
	public HeroImpl(JSONObject root) {
		this.maxLevel = root.getInt("maxLevel");
		this.level = root.getInt("level");
		this.type = Type.get(root.getString("name"));
		this.village = Village.get(root.getString("village"));
	}

	public Type getType() {
		return type;
	}

	public Integer getMaxLevel() {
		return maxLevel;
	}

	public Integer getLevel() {
		return level;
	}
	
	@Override
	public Village getVillage() {
		return this.village;
	}

	@Override
	public String toString() {
		return "HeroImpl [type=" + type + ", village=" + village + ", maxLevel=" + maxLevel + ", level=" + level + "]";
	}
	
	
}
