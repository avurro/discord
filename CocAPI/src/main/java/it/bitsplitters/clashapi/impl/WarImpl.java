package it.bitsplitters.clashapi.impl;

import org.json.JSONObject;

import it.bitsplitters.clashapi.War;
import it.bitsplitters.clashapi.WarClan;

public class WarImpl implements War {

	private String state;
	private Integer teamSize;
	private String preparationStartTime;
	private String startTime;
	private String endTime;
	private WarClan clan;
	private WarClan opponent;
	
	public WarImpl(JSONObject root) {
			this.state = root.getString("state");
			if(root.has("teamSize"))
				this.teamSize = root.getInt("teamSize");
			this.preparationStartTime = root.getString("preparationStartTime");
			this.startTime = root.getString("startTime");
			this.endTime = root.getString("endTime");
			this.clan = new WarClanImpl(root.getJSONObject("clan"));
			this.opponent = new WarClanImpl(root.getJSONObject("opponent"));
		
	}
	public String getState() {
		return state;
	}
	public Integer getTeamSize() {
		return teamSize;
	}
	public String getPreparationStartTime() {
		return preparationStartTime;
	}
	public String getStartTime() {
		return startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public WarClan getWarClan() {
		return clan;
	}
	public WarClan getOpponent() {
		return opponent;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setTeamSize(Integer teamSize) {
		this.teamSize = teamSize;
	}
	public void setPreparationStartTime(String preparationStartTime) {
		this.preparationStartTime = preparationStartTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public void setWarClan(WarClan warClan) {
		this.clan = warClan;
	}
	public void setOpponent(WarClan opponent) {
		this.opponent = opponent;
	}
	
	
}
