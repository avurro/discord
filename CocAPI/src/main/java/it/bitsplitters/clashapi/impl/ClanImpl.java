package it.bitsplitters.clashapi.impl;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import it.bitsplitters.clashapi.Clan;
import it.bitsplitters.clashapi.ClanMember;

public class ClanImpl extends AbbreviatedClanImpl implements Clan {

    private String description;
    private List<ClanMember> members = new ArrayList<ClanMember>();

    public ClanImpl(JSONObject root) {
        super(root);
        description = root.getString("description");
        JSONArray memberList = root.getJSONArray("memberList");
        for (int i = 0; i < memberList.length(); i++) {
            members.add(new ClanMemberImpl(memberList.getJSONObject(i)));
        }
    }

    public String getDescription() {
        return description;
    }

    public List<ClanMember> getMembers() {
        return members;
    }

    @Override
    public String toString() {
        return "ClanImpl{" +
                "description='" + description + '\'' +
                ", members=" + members +
                "} " + super.toString();
    }
}
