package it.bitsplitters.clashapi.impl;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import it.bitsplitters.clashapi.WarClan;
import it.bitsplitters.clashapi.WarMember;

public class WarClanImpl implements WarClan {

	private String tag;
	private String name;
	private Integer clanLevel;
	private Integer attacks;
	private Integer stars;
	private Double destructionPercentage;
	private List<WarMember> members;
	
	public WarClanImpl(JSONObject root) {
		
		this.tag = root.getString("tag");
		this.name = root.getString("name");
		this.clanLevel = root.getInt("clanLevel");
		this.attacks = root.getInt("attacks");
		this.stars = root.getInt("stars");
		this.destructionPercentage = root.getDouble("destructionPercentage");
		
		this.members = new ArrayList<WarMember>();
		JSONArray memberList = root.getJSONArray("members");
        for (int i = 0; i < memberList.length(); i++) 
            members.add(new WarMemberImpl(memberList.getJSONObject(i)));
		
	}

	public String tag() {
		return tag;
	}

	public String name() {
		return name;
	}

	public Integer clanLevel() {
		return clanLevel;
	}

	public Integer attacks() {
		return attacks;
	}

	public Integer stars() {
		return stars;
	}

	public Double destructionPercentage() {
		return destructionPercentage;
	}

	public List<WarMember> members() {
		return members;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setClanLevel(Integer clanLevel) {
		this.clanLevel = clanLevel;
	}

	public void setAttacks(Integer attacks) {
		this.attacks = attacks;
	}

	public void setStars(Integer stars) {
		this.stars = stars;
	}

	public void setDestructionPercentage(Double destructionPercentage) {
		this.destructionPercentage = destructionPercentage;
	}

	public void setMembers(List<WarMember> members) {
		this.members = members;
	}
	
	
}
