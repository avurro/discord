package it.bitsplitters.clashapi.impl;

import static it.bitsplitters.clashapi.utils.Deserialize.getJSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import it.bitsplitters.clashapi.Achievement;
import it.bitsplitters.clashapi.Hero;
import it.bitsplitters.clashapi.League;
import it.bitsplitters.clashapi.Player;
import it.bitsplitters.clashapi.PlayerClan;

public class PlayerImpl implements Player {

    protected String name;
    protected String tag;
    protected int expLevel;
    protected int trophies;
    protected int defenseWins;
    protected int attackWins;
    protected League league;
    protected int townHallLevel;
    protected Integer townHallWeaponLevel;
    protected List<Hero> heroes;
    protected List<Achievement> achievements;
    protected PlayerClan clan;
    
    public PlayerImpl() {}
    
    public PlayerImpl(Map<String, String> map) {
//    	System.out.println(map.get("trophies"));
    	this.trophies = Integer.parseInt(map.get("trophies"));
    	
    }
    
    public PlayerImpl(JSONObject root) {
    	heroes = new ArrayList<>();

    	JSONArray achievements = getJSONArray(root, "achievements");
    	if(achievements != null) { 
    		this.achievements = new ArrayList<>();
    		for (int i = 0; i < achievements.length(); i++) 
    			this.achievements.add(new AchievementImpl(achievements.getJSONObject(i)));
    	}
    	defenseWins = root.getInt("defenseWins");
    	attackWins = root.getInt("attackWins");
    	name = root.getString("name");
    	tag = root.getString("tag");
        expLevel = root.getInt("expLevel");
        townHallLevel = root.getInt("townHallLevel");
        
        if(root.has("townHallWeaponLevel"))
        	townHallWeaponLevel = root.getInt("townHallWeaponLevel");
        
        if (root.has("league")) 
            league = new LeagueImpl(root.getJSONObject("league"));
        
        if(root.has("heroes")){
        	JSONArray array = root.getJSONArray("heroes");
            for (int i = 0; i < array.length(); i++) 
                heroes.add(new HeroImpl(array.getJSONObject(i)));
        }
        	
        trophies = root.getInt("trophies");
        
        if(root.has("clan"))
        clan = new PlayerClanImpl(root.getJSONObject("clan"));
    }

    public PlayerImpl(String name, String tag) {
    	this.name = name;
    	this.tag = tag;
	}

	public int getExpLevel() {
        return expLevel;
    }

    public League getLeague() {
        return league;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
    	this.name = name;
    }
    public String getTag() {
        return tag;
    }

    public int getTrophies() {
        return trophies;
    }

	public List<Hero> getHeroes() {
		return heroes;
	}

	public Integer getTownHallWeaponLevel() {
		return townHallWeaponLevel;
	}

	public void setTownHallWeaponLevel(Integer townHallWeaponLevel) {
		this.townHallWeaponLevel = townHallWeaponLevel;
	}

	public PlayerClan getClan() {
		return clan;
	}
	
	
	public int getTownHallLevel() {
		return townHallLevel;
	}

	
	public int getDefenseWins() {
		return defenseWins;
	}

	
	public int getAttackWins() {
		return attackWins;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tag == null) ? 0 : tag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayerImpl other = (PlayerImpl) obj;
		if (tag == null) {
			if (other.tag != null)
				return false;
		} else if (!tag.equals(other.tag))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PlayerImpl [name=" + name + ", tag=" + tag + ", trophies=" + trophies + "]";
	}

    
}
