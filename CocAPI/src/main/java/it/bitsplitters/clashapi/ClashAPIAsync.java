package it.bitsplitters.clashapi;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

public interface ClashAPIAsync {
	
	CompletableFuture<Clan> requestClan(String clanTag) throws CompletionException, URISyntaxException, UnsupportedEncodingException;

    CompletableFuture<Player> requestPlayer(String playerTag) throws CompletionException, URISyntaxException, UnsupportedEncodingException;
    
    CompletableFuture<PlayerToken> verifyToken(String playerTag, String token) throws CompletionException, URISyntaxException, UnsupportedEncodingException;
    
}
