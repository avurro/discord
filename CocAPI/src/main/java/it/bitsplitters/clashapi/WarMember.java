package it.bitsplitters.clashapi;

import java.util.List;

public interface WarMember {

	public String name();
	
	public String tag();
	
	public Integer townHallLevel();
	
	public Integer mapPosition();
	
	public Integer opponentAttacks();
	
	public List<WarAttack> attacks();
	
	public WarAttack bestOpponentAttack();
	
}
