package it.bitsplitters.clashapi;

import java.util.List;

/**
 * The simplest representation of a Player. The data in the class will always be present when any sort of player or member
 * data is requested using the API.
 */
public interface Player {

    /**
     * Returns the experience level of a player
     *
     * @return the experience level of a player
     */
    int getExpLevel();

    /**
     * Returns the league a player is in
     *
     * @return the league a player is in, or {@code null} if the player is not in a league
     */
    League getLeague();

    /**
     * Returns the name of the player
     *
     * @return the name of the player
     */
    String getName();

    void setName(String name);
    
    String getTag();
    
    int getTownHallLevel();
    
    Integer getTownHallWeaponLevel();
    
    List<Hero> getHeroes();
    
    PlayerClan getClan();
    
    /**
     * Returns the number of trophies the player has
     *
     * @return the number of trophies the player has
     */
    int getTrophies();
    
    int getDefenseWins();
    
    int getAttackWins();
}
