package itcl.hl.commands;

import itcl.commands.HeroesLeagueCommand;
import itcl.hl.HeroesLeague;

public class CmdHLTest extends HeroesLeagueCommand {

	@Override
	public String name() {
		return "hl";
	}

	@Override
	public String helpKey() {
		return HeroesLeague.HL_TEST;
	}

}
