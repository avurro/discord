package itcl;

import static itcl.cl.jooq.models.tables.Guilds.GUILDS;

import java.time.LocalDate;
import java.util.Optional;

import org.jooq.types.ULong;

import it.bitsplitters.GuildsManager;
import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.setting.GuildSettings;
import itcl.cl.commands.service.GuildService;
import itcl.cl.jooq.models.tables.records.GuildsRecord;
import itcl.models.GuildsAdapter;
public class BotGuildsManager implements GuildsManager {

	@Override
	public Optional<GuildSettings> find(Long guildID) {
		Optional<GuildsRecord> optGuildsRecord = findRecord(guildID);
		
		if(!optGuildsRecord.isPresent())
			return Optional.empty();
		
		return Optional.of(GuildsAdapter.toGuild(optGuildsRecord.get()));
	}

	@Override
	public void guildJoin(Long guildID) {
		GuildService.guildJoin(guildID);
	}

	@Override
	public void guildLeave(Long guildID) {
		GuildService.guildLeave(guildID);
	}

	public boolean isValidPayer(Long guildID) {
		return DB.jooq().selectFrom(GUILDS)
				.where(GUILDS.ID_GUILD.eq(ULong.valueOf(guildID)))
				.and(GUILDS.PAY_END.ge(LocalDate.now())).fetchOptional().isPresent();
	}
	
	public Optional<GuildsRecord> findRecord(Long guildID) {
		return DB.jooq().selectFrom(GUILDS)
        .where(GUILDS.ID_GUILD.eq(ULong.valueOf(guildID))).fetchOptional();
	}

	@Override
	public GuildSettings register(Long guildID) {
		return null;
	}
	
}
