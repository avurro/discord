package itcl;

import static it.bitsplitters.util.ResourceBundleUtil.customControl;
import static java.util.ResourceBundle.getBundle;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class ITCLResourceBundle {
	
	public static final String RESOURCE_FULLNAME = "resource";
	
	private static Map<Locale, ResourceBundle> guildsRB = new HashMap<>();
	
	public static ResourceBundle getResourceBundle(Locale locale) {
		
		if(guildsRB.containsKey(locale))
			return guildsRB.get(locale);
		
		ResourceBundle bundle = getBundle(RESOURCE_FULLNAME, locale, customControl);
		
		guildsRB.put(locale, bundle);
		
		return bundle;
	}
	
	public static enum Languages { ENG(Locale.ENGLISH), ITA(Locale.ITALY); 
		
		private Locale locale;
		
		private Languages(Locale locale) {
			this.locale = locale;
		}
		
		public String get () {
			return this.locale.getLanguage();
		}
		
		public Locale locale() {
			return this.locale;
		}
		
		public static List<String> labels = Arrays.asList("it","en");
	};
}
