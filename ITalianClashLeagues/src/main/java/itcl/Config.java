package itcl;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.command.GeneralCommand;
import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.Init;
import it.bitsplitters.legendsdata.models.PlayerInfo;
import it.bitsplitters.legendstracker.models.PlayerData;
import it.bitsplitters.setting.BotConfig;
import itcl.cl.CupsLeague;
import itcl.cl.commands.CmdRankDetails;
import itcl.cl.commands.CmdRankPlayers;
import itcl.cl.commands.CmdRankTeams;
import itcl.cl.commands.CmdSubs;
import itcl.cl.commands.botadmin.CmdAdminAdd;
import itcl.cl.commands.botadmin.CmdAdminDel;
import itcl.cl.commands.botadmin.CmdAdminDetails;
import itcl.cl.commands.botadmin.CmdCLStart;
import itcl.cl.commands.botadmin.CmdCLStop;
import itcl.cl.commands.botadmin.CmdCLSubsAdd;
import itcl.cl.commands.botadmin.CmdCLSubsDel;
import itcl.hl.HeroesLeague;
import itcl.hl.commands.CmdHLTest;
import itcl.wl.WarLeague;
import itcl.wl.commands.CmdWLTest;

/**
 * Where all configurations reside.
 * @author AVurro
 *
 */
public class Config {
	
	private static final Logger logger = LogManager.getLogger();
	
	/** where the configurations reside */
	private static final Properties configProperties = new Properties();
	
	/** Discord token, used to communicate with Discord */
	private static final String botToken = "bot.token";
	
	/** CoC Token, used to interact with Clash of Clans API */
	private static final String cocAPI = "coc.api";
	
	/** List of bot admins ID */
	private static List<Long> botAdmins = new ArrayList<>();
    
	// URL FAKE UTILIZZATO PER EVIDENZIARE E RENDERE COPIABILE SOLO UNA PORZIONE DEL TESTO
	public static final String fakeUrl = "https://co.py/";
		
	/** BOT Admins */
	private static final long ALEX =     198341984346308609L;
	private static final long MARUZZA =  538137967948988429L;
	private static final long CRISTINA = 321665869044383755L;
	private static final long BRIGNO =   453995553005633569L;
	private static final long STEFY =    431178152912486401L;
	private static final long DANI =     637606708579139586L;
	
	/** Server Discord configuration, how the bot interacts with the server */
	public static final BotConfig botConfig;
	
	/** Guilds Manager */
	public static final BotGuildsManager guildsManager;
	
	/** Manager that verifies if a specific user can use the command invoked */
	public static final ITCLCommandsManager commandsManager;
	
	/** Bot Commands, the core of the bot */
	public static final GeneralCommand[] commands;
	
	public static final Map<String, List<Class<?>>> commandsPagination;
	
	public static final Map<String, List<Class<?>>> adminCmdPagination;
	
	static {
    	
    	commands = new GeneralCommand[] {
    			
    			//CUPS LEAGUE
    			new CmdRankPlayers(),
    			new CmdRankTeams(),
    			new CmdRankDetails(),
    			new CmdSubs(), 
    			
//    			//WAR LEAGUE
//    			new CmdWLTest(),
//    			
//    			//HEROES LEAGUE
//    			new CmdHLTest(),
    			
    			//BOT ADMIN CMD
    			new CmdCLSubsAdd(), 
    			new CmdCLSubsDel(), 
    			new CmdCLStart(), 
    			new CmdCLStop(),  
    			new CmdAdminAdd(), 
    			new CmdAdminDel(), 
    			new CmdAdminDetails()
    			};
    	
    	commandsPagination = new LinkedHashMap<>();
    	
    	commandsPagination.put("Cups League|"+CupsLeague.symbol+"|"+CupsLeague.color, Arrays.asList(
    			CmdRankPlayers.class,
    			CmdRankTeams.class, 
    			CmdRankDetails.class,
    			CmdSubs.class));
    	
//    	commandsPagination.put("War League|"+WarLeague.symbol+"|"+WarLeague.color, Arrays.asList(CmdWLTest.class));
//    	
//    	commandsPagination.put("Heroes League|"+HeroesLeague.symbol+"|"+HeroesLeague.color, Arrays.asList(CmdHLTest.class));
    	
    	adminCmdPagination = new LinkedHashMap<>();
    	
    	adminCmdPagination.put("Cups League|"+CupsLeague.symbol+"|"+CupsLeague.color, 
    			Arrays.asList(CmdCLSubsAdd.class,
    						  CmdCLSubsDel.class, 
    			              CmdCLStart.class,
    			              CmdCLStop.class));
    	
    	adminCmdPagination.put("War League|"+WarLeague.symbol+"|"+WarLeague.color, Arrays.asList(CmdWLTest.class));
    	
    	adminCmdPagination.put("Heroes League|"+HeroesLeague.symbol+"|"+HeroesLeague.color, Arrays.asList(CmdHLTest.class));
    	
    	guildsManager = new BotGuildsManager();
    	commandsManager = new ITCLCommandsManager();
    	
		botAdmins.add(ALEX);
		botAdmins.add(MARUZZA); 
		botAdmins.add(CRISTINA); 
		botAdmins.add(BRIGNO); 
		botAdmins.add(STEFY); 
		botAdmins.add(DANI); 
		
    	botConfig = new BotConfig() {
    		@Override public boolean isCaseSensitive() { return false; }
    		@Override public List<Long> adminIDs() { return botAdmins; }
			@Override public String resourceBundle() { return ITCLResourceBundle.RESOURCE_FULLNAME; }
    	};
    	
		try(FileInputStream file = new FileInputStream(Init.propertiesPath)) {
		    configProperties.load(file);
		} catch (IOException e) { logger.atError().log(e.getMessage()); }
    }
    
	public static String getBottoken() {
		return configProperties.getProperty(botToken);
	}

	public static String getCocapi() {
		return configProperties.getProperty(cocAPI);
	}

	public static PlayerData getPlayerData(String tag) {
		
		it.bitsplitters.legendsdata.models.PlayerData playerData = Cache.playersData().get(tag);
		PlayerInfo playerInfo = Cache.playersInfo().get(tag);
		
		PlayerData result = new PlayerData(playerData.getNickname(), tag, playerData.getTrophies(), playerData.getDefenseWins(), playerData.getAttackWins());
		result.setAttacks(playerData.attacks());
		result.setDefenses(playerData.defenses());
		result.setDailyAtkTrophies(playerData.getDailyAtkTrophies());
		result.setDailyDefTrophies(playerData.getDailyDefTrophies());
		result.setDailyMixTrophies(playerData.getDailyMixTrophies());
		result.setStartTrophies(playerData.getStartTrophies());
		
		result.setDailyAttacks(playerInfo.getDailyAttacks());
		result.setDailyDefenses(playerInfo.getDailyDefenses());
		result.setDailyMix(playerInfo.getDailyMix());
		return result;
	}
	
	private Config() {}
	
	public static class Attachments {
		
		public static final String IMAGES = "images/";
		public static final String EMBED_GH = "gh.png";
		public static final String EMBED_CL = "cl.png";
		public static final String EMBED_WL = "wl-dinamico.gif";
		public static final String EMBED_HL = "hl-dinamico.gif";
		public static final String ATTACHMENT = "attachment://";
		
	}

	public static class Emoji {
		
		public static final String BLANK = "<:blank:762656436752482314>";
		
	}
}