package itcl.utils;

import java.io.IOException;
import java.io.InputStream;

import it.bitsplitters.util.BotUtil;
import itcl.Config;

public class Utils {

	public static final InputStream imageToStream(String url) throws IOException {
		return BotUtil.toStream(Config.Attachments.IMAGES+ url);
	}
}
