package itcl.commands;

import static itcl.Config.Attachments.ATTACHMENT;
import static itcl.Config.Attachments.EMBED_CL;
import static itcl.Config.Attachments.EMBED_GH;
import static itcl.utils.Utils.imageToStream;

import java.awt.Color;
import java.io.IOException;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public abstract class CupsLeagueCommand extends ITCLCommand {


	@Override
	public EmbedBuilder embedHelp(MessageReceivedEvent event) {
		return super.embedHelp(event).setColor(new Color(159,41,247)).setThumbnail(ATTACHMENT+EMBED_CL);
	}
	
	@Override
	public void helpMethod(MessageReceivedEvent event) {
		
		try {
			
			event.getChannel().sendFile(imageToStream(EMBED_GH),EMBED_GH)
			                  .addFile(imageToStream(EMBED_CL),EMBED_CL)
			                  .embed(embedHelp(event).build()).queue();
		
		}catch (IOException e) {
			e.printStackTrace();
			event.getChannel().sendMessage(embedHelp(event).build()).queue();
		}
	}
}
