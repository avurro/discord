package itcl.commands;

import static it.bitsplitters.util.ResourceBundleUtil.replaceValues;
import static itcl.Config.Attachments.ATTACHMENT;
import static itcl.Config.Attachments.EMBED_CL;
import static itcl.Config.Attachments.EMBED_GH;
import static itcl.utils.Utils.imageToStream;

import java.awt.Color;
import java.io.IOException;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.command.GeneralCommand;
import it.bitsplitters.setting.GuildSettings;
import itcl.Config;
import itcl.ITCLResourceBundle;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public abstract class ITCLCommand extends GeneralCommand {

	private static final Logger logger = LogManager.getLogger();
	
	@Override
	public EmbedBuilder embedHelp(MessageReceivedEvent event) {
		return super.embedHelp(event).setColor(new Color(153, 211, 231)).setFooter("powered by Gamers' House",ATTACHMENT+EMBED_GH);
	}
	
	public abstract String helpKey();
	
	@Override
	public String helpContent(MessageReceivedEvent event) {
		Optional<GuildSettings> optGuild = Config.guildsManager.find(event.getGuild().getIdLong());
		
		if(!optGuild.isPresent()) return "";
		
		GuildSettings guild = optGuild.get();
		
		String msg = ITCLResourceBundle.getResourceBundle(guild.locale()).getString(helpKey());
			
		return replaceValues(msg, guild.prefix(), name());
	}
	
	@Override
	public void helpMethod(MessageReceivedEvent event) {
		
		try {
			
			event.getChannel().sendFile(imageToStream(EMBED_GH),EMBED_GH).embed(embedHelp(event).build()).queue();
		
		}catch (IOException e) {
			e.printStackTrace();
			logger.atError().log("Impossible to load gh.png, more details: "+e.getMessage());
			event.getChannel().sendMessage(embedHelp(event).build()).queue();
		}
	}
	
	public void sendCLEmbedMessage(MessageReceivedEvent event, String messages) throws IOException {
		EmbedBuilder message = new EmbedBuilder()
				.setColor(Color.MAGENTA)
				.setTitle("**CUPS LEAGUE**")
				.setDescription(messages)
			    .setThumbnail(ATTACHMENT+EMBED_CL)
		        .setFooter("powered by Top Italian Clans",ATTACHMENT+EMBED_GH);
		
		event.getChannel()
		.sendFile(imageToStream(EMBED_GH),EMBED_GH)
		  .addFile(imageToStream(EMBED_CL),EMBED_CL)
		  .embed(message.build()).queue();
	}

}