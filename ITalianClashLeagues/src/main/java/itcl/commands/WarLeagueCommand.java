package itcl.commands;

import static itcl.Config.Attachments.ATTACHMENT;
import static itcl.Config.Attachments.EMBED_WL;
import static itcl.Config.Attachments.EMBED_GH;
import static itcl.utils.Utils.imageToStream;

import java.awt.Color;
import java.io.IOException;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public abstract class WarLeagueCommand extends ITCLCommand {

	@Override
	public EmbedBuilder embedHelp(MessageReceivedEvent event) {
		return super.embedHelp(event).setColor(new Color(254,64,35)).setThumbnail(ATTACHMENT+EMBED_WL);
	}
	
	@Override
	public void helpMethod(MessageReceivedEvent event) {
		
		try {
			
			event.getChannel().sendFile(imageToStream(EMBED_GH),EMBED_GH)
			                  .addFile(imageToStream(EMBED_WL),EMBED_WL)
			                  .embed(embedHelp(event).build()).queue();
		
		}catch (IOException e) {
			e.printStackTrace();
			event.getChannel().sendMessage(embedHelp(event).build()).queue();
		}
	}
}
