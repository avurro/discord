package itcl;

import static itcl.Config.botConfig;
import static itcl.Config.commands;
import static itcl.Config.commandsManager;
import static itcl.Config.getBottoken;
import static itcl.Config.guildsManager;

import java.util.Optional;

import javax.security.auth.login.LoginException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.ygimenez.exception.InvalidHandlerException;
import com.github.ygimenez.method.Pages;
import com.github.ygimenez.model.Paginator;
import com.github.ygimenez.model.PaginatorBuilder;

import it.bitsplitters.BotManager;
import it.bitsplitters.command.impl.BotAdminCategorizedHelpCommand;
import it.bitsplitters.command.impl.CategorizedHelpCommand;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
public class ITCLListener extends ListenerAdapter
{
	
	public static JDA jda;

	private static final Logger logger = LogManager.getLogger();
	
	private BotManager botManager;
	
	
	public ITCLListener() {
	    botManager = new BotManager(botConfig, commandsManager, null, guildsManager, new BotAdminCategorizedHelpCommand("__***ITCL Admin Commands***__|25,249,254",Config.adminCmdPagination), new CategorizedHelpCommand("__***ITalian Clash Leagues***__|25,249,254",Config.commandsPagination), commands);
	}
	
	/**
     * This is the method where the program starts.
     */
    public static void main(String[] args)
    {
        try
        {
            jda = JDABuilder.createDefault(getBottoken()) // The token of the account that is logging in.
                    .addEventListeners(new ITCLListener())   // An instance of a class that will handle events.
                    .build();
            jda.awaitReady(); // Blocking guarantees that JDA will be completely loaded.
            
            Paginator paginator = PaginatorBuilder.createPaginator().setHandler(jda)
            		.shouldRemoveOnReact(true).build();
            
            Pages.activate(paginator);
            logger.atInfo().log("Finished Building JDA!");
            
        }
        catch (LoginException e)
        {
            //If anything goes wrong in terms of authentication, this is the exception that will represent it
        	logger.atError().log(e.getMessage());
        }
        catch (InterruptedException e)
        {
            //Due to the fact that awaitReady is a blocking method, one which waits until JDA is fully loaded,
            // the waiting can be interrupted. This is the exception that would fire in that situation.
            //As a note: in this extremely simplified example this will never occur. In fact, this will never occur unless
            // you use awaitReady in a thread that has the possibility of being interrupted (async thread usage and interrupts)
        	logger.atError().log(e.getMessage());
        } catch (InvalidHandlerException e) {
        	logger.atError().log(e.getMessage());
			e.printStackTrace();
		}
    }
    
    @Override
    public void onGuildLeave(GuildLeaveEvent event) {
    	logger.atInfo().log("Server uscito! {}", event.getGuild().getName());
    	guildsManager.guildLeave(event.getGuild().getIdLong());
    	
    }
    
    @Override
    public void onGuildJoin(GuildJoinEvent event) {
    	logger.atInfo().log("Nuovo server! {}", event.getGuild().getName());
    	guildsManager.guildJoin(event.getGuild().getIdLong());
    }
    
    /**
     * NOTE THE @Override!
     * This method is actually overriding a method in the ListenerAdapter class! We place an @Override annotation
     *  right before any method that is overriding another to guarantee to ourselves that it is actually overriding
     *  a method from a super class properly. You should do this every time you override a method!
     *
     * As stated above, this method is overriding a hook method in the
     * {@link net.dv8tion.jda.api.hooks.ListenerAdapter ListenerAdapter} class. It has convenience methods for all JDA events!
     * Consider looking through the events it offers if you plan to use the ListenerAdapter.
     *
     * In this example, when a message is received it is printed to the console.
     *
     * @param event
     *          An event containing information about a {@link net.dv8tion.jda.api.entities.Message Message} that was
     *          sent in a channel.
     */
    @Override
    public void onMessageReceived(MessageReceivedEvent event)
    {

        //Event specific information
        User author = event.getAuthor();                //The user that sent the message

        if(author.isBot()) //This boolean is useful to determine if the User that
        	return;  	   //sent the Message is a BOT or not!
		
        Message message = event.getMessage();           //The message that was received.

        //If this message was sent to a Guild TextChannel and isn't a webhookMessage
    	if (!message.isWebhookMessage() && event.isFromType(ChannelType.TEXT)){         
        
    		if(!message.getContentRaw().startsWith("itcl")) 
    			return;
    		
            Optional<Exception> optError = botManager.findAndRunGuildCommand(event);
            
            if(optError.isPresent()) {
            	optError.get().printStackTrace();
            	String msg = optError.get().getMessage();
            	if(msg != null && !msg.isEmpty())
            		event.getChannel().sendMessage(msg).queue();
            }
    	}
    }
}