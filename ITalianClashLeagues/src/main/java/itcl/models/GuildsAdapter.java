package itcl.models;

import java.util.Locale;

import itcl.cl.jooq.models.tables.records.GuildsRecord;

public class GuildsAdapter {

	public static ITCLGuild toGuild(GuildsRecord record) {
		return new ITCLGuild(record.getIdGuild().longValue(), record.getPrefix(), Locale.forLanguageTag(record.getLocale()));
	}
}
