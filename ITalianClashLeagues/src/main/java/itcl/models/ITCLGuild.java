package itcl.models;

import java.util.Locale;

import it.bitsplitters.setting.GuildSettings;

public class ITCLGuild implements GuildSettings {

	private Long id;
	private String prefix;
	private Locale locale;

	
	public ITCLGuild(Long id, String prefix, Locale locale) {
		super();
		this.id = id;
		this.prefix = prefix;
		this.locale = locale;
	}


	@Override
	public Long id() {
		return id;
	}


	@Override
	public String prefix() {
		return prefix;
	}

	@Override
	public Locale locale() {
		return locale;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	
}
