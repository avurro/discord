package itcl.wl.commands;

import it.bitsplitters.command.signature.Signature;
import itcl.commands.WarLeagueCommand;
import itcl.wl.WarLeague;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdWLTest extends WarLeagueCommand {

	@Override
	public String name() {
		return "wl";
	}

	@Override
	public String helpKey() {
		return WarLeague.WL_TEST;
	}

	@Signature
	public void test(MessageReceivedEvent event) {
		
		event.getChannel().sendMessage("war league.").queue();
		
	}
}
