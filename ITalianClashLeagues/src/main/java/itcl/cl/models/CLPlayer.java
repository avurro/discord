package itcl.cl.models;

public class CLPlayer {
	
	private String tag;
	
	private String nickname;
	
	private Integer trophies;

	private String team;

	private Integer doneAttacks = 0;
	
	private Integer doneDefenses = 0;

	public CLPlayer() { }
	
	public CLPlayer(String tag, String nickname, Integer trophies, String team) {
		super();
		this.tag = tag;
		this.nickname = nickname;
		this.trophies = trophies;
		this.team = team;
	}

	public Integer getDoneAttacks() {
		return doneAttacks;
	}

	public void setAttacks(Integer doneAttacks) {
		this.doneAttacks = doneAttacks;
	}

	public Integer getDoneDefenses() {
		return doneDefenses;
	}

	public void setDefenses(Integer doneDefenses) {
		this.doneDefenses = doneDefenses;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Integer getTrophies() {
		return trophies;
	}

	public void setTrophies(Integer trophies) {
		this.trophies = trophies;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof CLPlayer)) return false;
		CLPlayer clplayer = (CLPlayer) obj;
		
		return this.getTag().equals(clplayer.getTag());
	}
}
