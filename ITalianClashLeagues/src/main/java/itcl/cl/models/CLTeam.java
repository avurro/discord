package itcl.cl.models;

import java.util.ArrayList;
import java.util.List;

import it.bitsplitters.legendstracker.models.PlayerData;

/**
 * @author AVurro
 *
 */
public class CLTeam {

	private String teamname = "";
	
	private Integer trophies = 0;

	private Integer doneAttacks = 0;
	
	private Integer doneDefenses = 0;
	
	private Integer size = 0;
	
	private List<PlayerData> updatedPlayers = new ArrayList<>();
	
	public List<PlayerData> getUpdatedPlayers() {
		return updatedPlayers;
	}

	public void addUpdatedPlayer(PlayerData updatedPlayer) {
		this.updatedPlayers.add(updatedPlayer);
	}

	public CLTeam() { }
	
	public CLTeam(String teamname) {
		super();
		this.teamname = teamname;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Integer getDoneAttacks() {
		return doneAttacks;
	}

	public void addAttacks(Integer doneAttacks) {
		this.doneAttacks+= doneAttacks;
	}

	public Integer getDoneDefenses() {
		return doneDefenses;
	}

	public void addDefenses(Integer doneDefenses) {
		this.doneDefenses+= doneDefenses;
	}

	public String getTeamname() {
		return teamname;
	}

	public void setTeamname(String teamname) {
		this.teamname = teamname;
	}

	public Integer getTrophies() {
		return trophies;
	}

	public void sumTrophies(Short trophies) {
		this.trophies += trophies;
	}
	
	public void sumTrophies(int trophies) {
		this.trophies += trophies;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof CLTeam)) return false;
		CLTeam CLTeam = (CLTeam) obj;
		
		return this.getTeamname().equals(CLTeam.getTeamname());
	}
	
	
}
