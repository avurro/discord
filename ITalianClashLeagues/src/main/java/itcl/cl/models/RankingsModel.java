package itcl.cl.models;

import java.util.ArrayList;
import java.util.List;

public class RankingsModel {

	private List<CLPlayer> playersPrevRanking = new  ArrayList<>();
	private List<CLPlayer> playersActualRanking = new  ArrayList<>();
	
	private List<CLTeam> teamsPrevRanking = new  ArrayList<>();
	private List<CLTeam> teamsActualRanking = new  ArrayList<>();
	
	public List<CLPlayer> getPlayersPrevRanking() {
		return playersPrevRanking;
	}
	public List<CLPlayer> getPlayersActualRanking() {
		return playersActualRanking;
	}
	public List<CLTeam> getTeamsPrevRanking() {
		return teamsPrevRanking;
	}
	public List<CLTeam> getTeamsActualRanking() {
		return teamsActualRanking;
	}
}
