package itcl.cl.commands;

import static itcl.cl.commands.service.RankingsDBService.savePlayerChannel;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import it.bitsplitters.util.BotUtil;
import itcl.cl.CupsLeague;
import itcl.commands.CupsLeagueCommand;
import net.dv8tion.jda.api.entities.GuildChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdRankPlayers extends CupsLeagueCommand {

	private static final Logger logger = LogManager.getLogger();
	private final String  ok_message = "Canale salvato correttamente :+1: La classifica si aggiornera' ogni 6 ore circa dallo start della competizione.";
	private final String ko_message = "Il canale non è corretto o non visibile.";
	
	@Override public PermissionLevel permissionLevel() {
		return PermissionLevel.ADMIN;
	}

	@Override public String helpKey() {
		return CupsLeague.CMD_CL_RANKPLAYERS;
	}
	
	@Override public String key() {
		return CupsLeague.KEY_CL_RANKING;
	}
	
	@Override public String name() {
		return "cl players ranking";
	}
	
	@Signature
	public void startInCurrentChannel(MessageReceivedEvent event){
		savePlayerChannel(event, event.getChannel().getIdLong());
		event.getChannel().sendMessage(ok_message).queue();
	}
	
	@Signature(order = 1, parameters = {StringParameter.class})
	public void startFromGivenChannel(MessageReceivedEvent event, String channelName){

		logger.atInfo().log("channel name: {}", channelName);
		
		Optional<GuildChannel> optChannel = BotUtil.findChannel(event.getGuild().getChannels(), channelName);
		
		if(optChannel.isPresent()) {
			savePlayerChannel(event,optChannel.get().getIdLong());
			event.getChannel().sendMessage(ok_message).queue();
		
		}else
			event.getChannel().sendMessage(ko_message).queue();
	
	}
}
