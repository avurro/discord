package itcl.cl.commands;

import static it.bitsplitters.util.BotUtil.escapeContent;
import static itcl.Config.Emoji.BLANK;
import static itcl.cl.CupsLeague.ATK_DEF;
import static itcl.cl.CupsLeague.ATK_MIDDLE;
import static itcl.cl.CupsLeague.DEF_MIDDLE;
import static itcl.cl.CupsLeague.DOWN_MIDDLE;
import static itcl.cl.CupsLeague.END;
import static itcl.cl.CupsLeague.EQ_MIDDLE;
import static itcl.cl.CupsLeague.START;
import static itcl.cl.CupsLeague.UP_MIDDLE;
import static itcl.cl.commands.service.RankingsPrinterService.centinaiaToEmoji;
import static itcl.cl.commands.service.RankingsPrinterService.spAttacks;
import static itcl.cl.commands.service.RankingsPrinterService.spDefenses;
import static itcl.cl.commands.service.RankingsPrinterService.trophiesToEmoji;
import static itcl.cl.jooq.models.tables.CupsLeague.CUPS_LEAGUE;
import static java.util.Collections.sort;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.StringJoiner;

import org.jooq.Result;
import org.jooq.exception.DataAccessException;

import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendstracker.models.PlayerData;
import itcl.Config;
import itcl.cl.CupsLeague;
import itcl.cl.commands.service.RankingsMonitor;
import itcl.cl.jooq.models.tables.records.CupsLeagueRecord;
import itcl.commands.CupsLeagueCommand;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdRankDetails extends CupsLeagueCommand {

	
	@Override public String name() {
		return "cl ranking";
	}

	@Override public String helpKey() {
		return CupsLeague.CMD_CL_RANKDETAILS;
	}
	
	@Signature(parameters = {StringParameter.class})
	public void details(MessageReceivedEvent event, String status) {
		
		if(!RankingsMonitor.started) {
			event.getChannel().sendMessage("Attualmente non è in corso una edizione Cups League.").queue();
			return;
		}
		
		try {
			
			if(status.startsWith("#"))	playerDetails(event, status);
			else						teamDetails(event, status);
			
		} catch (DataAccessException e) {
			event.getChannel().sendMessage("Problemi con il recupero delle iscrizioni, contattare l'amministratore.").queue();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void teamDetails(MessageReceivedEvent event, String team) {
		Result<CupsLeagueRecord> result = DB.jooq().selectFrom(CUPS_LEAGUE).where(CUPS_LEAGUE.TEAM.eq(team)).fetch();
			
		if(result.isEmpty()) {
			event.getChannel().sendMessage("Non risultano players per questo team, assicurati di averlo scritto in modo corretto.").queue();
			return;
		}
		
		List<PlayerData> realTimeTeam = new ArrayList<>();
		
		for (CupsLeagueRecord clRecord : result) {
			
			String tag = clRecord.getTag();
			
			PlayerData realTimePlayer = Config.getPlayerData(tag);
			
			if(realTimePlayer == null) {
				int trophies = clRecord.getTrophies().intValue();
				String nickname = clRecord.getNickname();
				realTimePlayer = new PlayerData(nickname, tag, trophies, 0, 0);
			}
			
			realTimeTeam.add(realTimePlayer);
		}
		
		sort(realTimeTeam, playerComparator);
		
		List<String> list = new ArrayList<>();
		StringJoiner msg = new StringJoiner("\n").add(String.join("", "__***CUPS LEAGUE***__\n*",team,"*"));
		
		int lineCounter = 1;
		for (PlayerData player : realTimeTeam) {
			
			if(lineCounter <= 5) {
				msg.add(getMsg(player));
				
				lineCounter++;
			}else {
				list.add(msg.toString());
				msg = new StringJoiner("\n");
				msg.add(getMsg(player));
				lineCounter = 1;
			}
		}
		
		list.add(msg.toString());
		
		for (String str : list) 
			event.getChannel().sendMessage(str).queue();
	}
	
 	private String getMsg(PlayerData player) {
		return String.join("",trophiesToEmoji(player.getTrophies())
				,spAttacks(player.attacks())
				,spDefenses(player.defenses())
				,escapeContent(player.getNickname()));
	}

	private void playerDetails(MessageReceivedEvent event, String player) throws IOException {
		
		CupsLeagueRecord clRecord = DB.jooq().selectFrom(CUPS_LEAGUE).where(CUPS_LEAGUE.TAG.eq(player)).fetchOne();
		
		if(clRecord == null) {
			event.getChannel().sendMessage("Non corrisponde alcun player a questo tag, assicurati di averlo scritto in modo corretto.").queue();
			return;
		}
		
		String tag = clRecord.getTag();
			
		PlayerData realTimePlayer = Config.getPlayerData(tag);
			
		if(realTimePlayer == null) {
			int trophies = clRecord.getTrophies().intValue();
			String nickname = clRecord.getNickname();
			realTimePlayer = new PlayerData(nickname, tag, trophies, 0, 0);
		}
		
		String msg = String.join("\n",
						"__***CUPS LEAGUE***__",
						"*"+realTimePlayer.getNickname()+"*",
						"\n**__Overview__**",
						getMsgPlayer(realTimePlayer));

		
		event.getChannel().sendMessage(msg).queue();
		
		List<String> playerDetails = getPlayerDetails(realTimePlayer);
		if(!playerDetails.isEmpty()) {
			StringJoiner sj = new StringJoiner("\n").add(BLANK).add("**__Details__**");
			
			for (String str : playerDetails)
				sj.add(str);
			
			event.getChannel().sendMessage(sj.toString()).queue();
		}
		
	}
	
	private List<String> getPlayerDetails(PlayerData player)	{
		List<String> result = new ArrayList<>();
		
		List<String> dailyAttacks = player.getDailyAttacks();
		List<String> dailyDefenses = player.getDailyDefenses();
		List<String> dailyMix = player.getDailyMix();
		
		for (int i = 0; i < 8; i++) {
			
			if((dailyAttacks.size() <= i && dailyDefenses.size() <= i && dailyMix.size() <= i))
				break;
			
			result.add(String.join("",
					dailyAttacks.size()  > i ? dailyAttacks.get(i)  : fillSpace,
					dailyDefenses.size() > i ? dailyDefenses.get(i) : fillSpace,
					dailyMix.size()      > i ? dailyMix.get(i)      : ""));
		}
		
		return result;
	}
	
	private String getMsgPlayer(PlayerData player) {
		
		// calcolo la freccia da visualizzare
		int differenzaTrofei = player.getTrophies() - player.getStartTrophies();
		String trend = differenzaTrofei > 0 ? UP_MIDDLE : differenzaTrofei < 0 ? DOWN_MIDDLE : EQ_MIDDLE;
		
		String startTrophies = trophiesToEmoji(player.getStartTrophies());
		String currentTrophies = trophiesToEmoji(player.getTrophies());
		String cupDifference = centinaiaToEmoji(Math.abs(differenzaTrofei));
		String sumAtks = centinaiaToEmoji(player.sumAtks());
		String sumDefs = centinaiaToEmoji(player.sumDefs());
		String sumMixs = centinaiaToEmoji(player.sumMix());
		String totAtks = spAttacks(player.attacks());
		String totDefs = spDefenses(player.defenses());
		
		String mix = "";
		if(!player.getDailyMixTrophies().isEmpty())
			mix = String.join("",ATK_DEF, sumMixs);

		String result = String.join("",
			  START,startTrophies              ,  ATK_MIDDLE, sumAtks, mix
		,"\n",END  ,currentTrophies            ,  DEF_MIDDLE, sumDefs
		,"\n",BLANK, totAtks, totDefs," ",BLANK,trend, cupDifference);
		
		return result;
	}
	
 	private final String fillSpace = BLANK + BLANK + BLANK + " ";
 	
	private Comparator<PlayerData> playerComparator = (prev, actual) -> prev.getTrophies() < actual.getTrophies() ? 1 :
		prev.getTrophies() == actual.getTrophies() ? 0 : 1;

}
