package itcl.cl.commands.service;

import static itcl.cl.jooq.models.tables.Clranking.CLRANKING;
import static itcl.cl.jooq.models.tables.Guilds.GUILDS;

import java.time.LocalDate;
import java.util.Optional;

import org.jooq.types.ULong;

import it.bitsplitters.PayLevel;
import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.legendsdata.DB;
import itcl.ITCLResourceBundle;
import itcl.cl.jooq.models.tables.records.ClrankingRecord;
import itcl.cl.jooq.models.tables.records.GuildsRecord;
public class GuildService {

	public static Optional<GuildsRecord> getGuild(Long guildID){
		return DB.jooq().selectFrom(GUILDS)
		        .where(GUILDS.ID_GUILD.eq(ULong.valueOf(guildID)))
		        .fetchOptional();
	}
	
	public static void guildJoin(Long guildId) {
		Optional<GuildsRecord> optGuild = getGuild(guildId);
		ULong guildID = ULong.valueOf(guildId);
		
		if(optGuild.isPresent()) return;
			
		String prefix = "itcl";
		String locale = ITCLResourceBundle.Languages.ITA.get();
		byte payLevel = 0;
		GuildsRecord guild = new GuildsRecord(guildID, null, payLevel, prefix, locale);
		DB.jooq().executeInsert(guild);

		ULong role = ULong.valueOf(PermissionLevel.ADMIN.get());
		ClrankingRecord ranking = new ClrankingRecord(guildID, role, null, null, null, null);
		DB.jooq().executeInsert(ranking);
			
	}
	
	public static void guildLeave(Long guildId) {
		
		Optional<GuildsRecord> optGuild = getGuild(guildId);
		
		if(!optGuild.isPresent()) return;
		
		GuildsRecord guild = optGuild.get();

		//in caso di patreon attivo si lascia la guild a DB in caso di rinvito del bot
		if(PayLevel.isFree(optGuild.get().getPayLevel()))
			DB.jooq().executeDelete(optGuild.get());
		
		DB.jooq().delete(CLRANKING).where(CLRANKING.ID_GUILD.eq(guild.getIdGuild()));
		
	}
	
	public static boolean isPayGuild(Long idGuild, Integer payLevel) {
		return DB.jooq().selectFrom(GUILDS)
			.where(GUILDS.ID_GUILD.eq(ULong.valueOf(idGuild)))
			.and(GUILDS.PAY_END.ge(LocalDate.now())).fetchOptional().isPresent();
	}
}
