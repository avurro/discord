package itcl.cl.commands.service;

import static itcl.cl.commands.service.RankingsService.loadCup;
import static itcl.cl.commands.service.RankingsService.updateDB;
import static itcl.cl.commands.service.RankingsService.updateRankings;
import static java.lang.Thread.sleep;
import static java.time.temporal.ChronoUnit.MILLIS;

import java.io.IOException;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.clashapi.exception.ClashException;
import itcl.cl.models.RankingsModel;
public class RankingsMonitor implements Runnable{
	
	private static final Logger logger = LogManager.getLogger();

	/** durata in millisencodi di 1 minuto */
	private final long MINUTE = 1000 * 60;
	
	/** durata in millisencodi di 10 minuti */
	private final long MINUTE10 = 1000 * 60 * 10;
	
	/** durata in millisencodi di 6 ore */
	private final long HOURS6 = MINUTE*60*6;
	
	private final LocalDateTime start, end;
	
	public static boolean started = false;
	
	public static boolean toStop = false;
	
	public RankingsMonitor(LocalDateTime start, LocalDateTime end) {
		this.start = start;
		this.end = end;
	}

	@Override
	public void run(){
		try {
			
			Clock clockEurope = Clock.system(ZoneId.of("Europe/Berlin"));
			long startLoading = 0;

			LocalDateTime now = LocalDateTime.now();
			
			// in case of a future start
			while(now.isBefore(start)) {

				logger.atInfo().log("CUPS LEAGUE - WAITING FOR START");
				// sleep del tempo che rimane allo start
				sleep((MILLIS.between(now, start)));
				now = LocalDateTime.now();
			}
			
			startLoading = Instant.now(clockEurope).toEpochMilli();
			
			logger.atInfo().log("CUPS LEAGUE - START CUP");
			
			RankingsModel cupData = loadCup();
			updateRankings(cupData);
			updateDB(cupData);
			
			now = LocalDateTime.now();
			while(now.isBefore(end) && !toStop) {
				
				long millisToEnd = MILLIS.between(now, end);
				
				if(millisToEnd > HOURS6) {
					
					long loadingDuration = Instant.now(clockEurope).toEpochMilli() - startLoading;
					
					logger.atInfo().log("CUPS LEAGUE - durata elaborazione in secondi: {}", (loadingDuration/1000));
					
					sleep(HOURS6 - loadingDuration);
					
					startLoading = Instant.now(clockEurope).toEpochMilli();
					
				}else 
					if(millisToEnd > MINUTE10)
						sleep(millisToEnd);
					else 
						break;
								
				cupData = loadCup();
				updateRankings(cupData);
				updateDB(cupData);
				
				now = LocalDateTime.now();
			}
			
			logger.atInfo().log("END CUP");
			
		} catch (InterruptedException | IOException | ClashException e) {
			logger.atError().log(e.getMessage());
		}
	}
}