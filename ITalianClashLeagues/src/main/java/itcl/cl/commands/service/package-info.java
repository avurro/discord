/**
 * Services are used for complex commands.
 * 
 * @author AVurro
 *
 */
package itcl.cl.commands.service;