package itcl.cl.commands.service;

import static itcl.cl.jooq.models.tables.Clranking.CLRANKING;

import java.util.Optional;

import org.jooq.types.ULong;

import it.bitsplitters.legendsdata.DB;
import itcl.cl.jooq.models.tables.records.ClrankingRecord;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class RankingsDBService {

	public static void savePlayerChannel(MessageReceivedEvent event, Long channelID) {
		insertOrUpdatePlayerRankingChannel(event.getGuild().getIdLong(), channelID);
	}
	
	public static void saveTeamChannel(MessageReceivedEvent event, Long channelID) {
		insertOrUpdateTeamRankingChannel(event.getGuild().getIdLong(), channelID);
	}
	
	public static void saveLogChannel(MessageReceivedEvent event, Long channelID) {
		insertOrUpdateLogChannel(event.getGuild().getIdLong(), channelID);
	}
	
	private static void insertOrUpdateLogChannel(Long guildID, Long channelID) {
		ULong guildKey = ULong.valueOf(guildID);
		ULong channelKey = ULong.valueOf(channelID);
		
		Optional<ClrankingRecord> optRanking = DB.jooq().selectFrom(CLRANKING)
        .where(CLRANKING.ID_GUILD.eq(guildKey)).fetchOptional();
		
		if(optRanking.isPresent()) {
			ClrankingRecord record = optRanking.get();
			record.setIdChannelLog(channelKey);
			DB.jooq().executeUpdate(record);
		}else {
			ClrankingRecord record = new ClrankingRecord(guildKey, ULong.valueOf(20L), null, null, null, channelKey);
			DB.jooq().executeInsert(record);
		}
	}
	
	private static void insertOrUpdatePlayerRankingChannel(Long guildID, Long channelID) {
		ULong guildKey = ULong.valueOf(guildID);
		ULong channelKey = ULong.valueOf(channelID);
		
		Optional<ClrankingRecord> optRanking = DB.jooq().selectFrom(CLRANKING)
        .where(CLRANKING.ID_GUILD.eq(guildKey)).fetchOptional();
		
		if(optRanking.isPresent()) {
			ClrankingRecord record = optRanking.get();
			record.setIdChannelPlayers(channelKey);
			DB.jooq().executeUpdate(record);
		}else {
			ClrankingRecord record = new ClrankingRecord(guildKey, ULong.valueOf(20L), null, channelKey, null, null);
			DB.jooq().executeInsert(record);
		}
	}
	
	private static void insertOrUpdateTeamRankingChannel(Long guildID, Long channelID) {
		ULong guildKey = ULong.valueOf(guildID);
		ULong channelKey = ULong.valueOf(channelID);
		
		Optional<ClrankingRecord> optRanking = DB.jooq().selectFrom(CLRANKING)
        .where(CLRANKING.ID_GUILD.eq(guildKey)).fetchOptional();
		
		if(optRanking.isPresent()) {
			ClrankingRecord record = optRanking.get();
			record.setIdChannelTeams(channelKey);
			DB.jooq().executeUpdate(record);
		}else {
			ClrankingRecord record = new ClrankingRecord(guildKey, ULong.valueOf(20L), null, null, channelKey, null);
			DB.jooq().executeInsert(record);
		}
	}
}
