package itcl.cl.commands.service;

import static itcl.ITCLListener.jda;
import static itcl.cl.CupsLeague.SINGLE_PLAYERS;
import static itcl.cl.commands.service.RankingsPrinterService.playersRanking;
import static itcl.cl.commands.service.RankingsPrinterService.teamsRanking;
import static itcl.cl.commands.service.RankingsPrinterService.writeChannels;
import static itcl.cl.jooq.models.tables.Clranking.CLRANKING;
import static itcl.cl.jooq.models.tables.CupsLeague.CUPS_LEAGUE;
import static java.util.Collections.sort;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.Result;

import it.bitsplitters.clashapi.exception.ClashException;
import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendstracker.models.PlayerData;
import itcl.Config;
import itcl.cl.jooq.models.tables.records.ClrankingRecord;
import itcl.cl.jooq.models.tables.records.CupsLeagueRecord;
import itcl.cl.models.CLPlayer;
import itcl.cl.models.CLTeam;
import itcl.cl.models.RankingsModel;
import net.dv8tion.jda.api.entities.TextChannel;

public class RankingsService {
	
	private static final Logger logger = LogManager.getLogger();
	
	private static final Comparator<CLTeam> teamComparator = (prev, actual) -> prev.getTrophies() < actual.getTrophies() ? 1 :
			prev.getTrophies() == actual.getTrophies() ? 0 : -1;

	private static final Comparator<CLPlayer> playerComparator = (prev, actual) -> prev.getTrophies() < actual.getTrophies() ? 1 :
		prev.getTrophies() == actual.getTrophies() ? 0 : -1;

	private static final Comparator<PlayerData> playerUpdComparator = (prev, actual) -> prev.getTrophies() < actual.getTrophies() ? 1 :
		prev.getTrophies() == actual.getTrophies() ? 0 : -1;

	public static RankingsModel loadCup() throws IOException, ClashException {
		logger.atInfo().log("LOAD CUP");
		
		RankingsModel model = new RankingsModel();
		
		String team = "";
		int playersCounter = 1;
		CLTeam prevStateTeam = new CLTeam(), actualStateTeam = new CLTeam();
		Result<CupsLeagueRecord> dbPlayers = DB.jooq().selectFrom(CUPS_LEAGUE).orderBy(CUPS_LEAGUE.TEAM.asc(), CUPS_LEAGUE.TROPHIES.desc()).fetch();
		
		for (CupsLeagueRecord dbPlayer : dbPlayers) {
			
			PlayerData cocPlayer = Config.getPlayerData(dbPlayer.getTag());

			boolean isSinglePlayer = dbPlayer.getTeam().equals(SINGLE_PLAYERS);
			
			if(!isSinglePlayer) {
				
				// first time
				if(team.isEmpty()) {
					
					team = dbPlayer.getTeam();
					prevStateTeam.setTeamname(team);
					actualStateTeam.setTeamname(team);
					
				}else if (!team.equals(dbPlayer.getTeam())) {

					// se sto uscendo da un team allora setto
					// i single players sono trattati come team per facilitarne la gestione
					// ma non devono essere visualizzati come tali
					if(!team.equals(SINGLE_PLAYERS)) {
						actualStateTeam.setSize(playersCounter-1);
						
						//ordino e sommo le coppe del team allo stato attuale
						trophiesSortAndSum(actualStateTeam);
						
						model.getTeamsPrevRanking().add(prevStateTeam);
						model.getTeamsActualRanking().add(actualStateTeam);
					}

					playersCounter = 1;
					team = dbPlayer.getTeam();
					prevStateTeam = new CLTeam(team); 
					actualStateTeam = new CLTeam(team);
				}
				
				// ottengo la somma delle coppe nella precedente classifica
				if(playersCounter <= 5) 
					prevStateTeam.sumTrophies(dbPlayer.getTrophies());
				
				// invece per lo stato attuale raggruppo, per ordinare e sommare solo alla fine
				actualStateTeam.addUpdatedPlayer(cocPlayer);
				
				actualStateTeam.addAttacks(cocPlayer.attacks());
				actualStateTeam.addDefenses(cocPlayer.defenses());

			}else if (!team.isEmpty() && !team.equals(SINGLE_PLAYERS)) {
				actualStateTeam.setSize(playersCounter-1);
				
				//ordino e sommo le coppe del team allo stato attuale
				trophiesSortAndSum(actualStateTeam);
				
				model.getTeamsPrevRanking().add(prevStateTeam);
				model.getTeamsActualRanking().add(actualStateTeam);
				
				playersCounter = 1;
				team = dbPlayer.getTeam();
			}
			
			CLPlayer prevStatePlayer = loadFrom(dbPlayer), actualStatePlayer = loadFrom(dbPlayer);
			
			actualStatePlayer.setTrophies(cocPlayer.getTrophies());
			actualStatePlayer.setAttacks(cocPlayer.attacks());
			actualStatePlayer.setDefenses(cocPlayer.defenses());
			
			model.getPlayersPrevRanking().add(prevStatePlayer);
			model.getPlayersActualRanking().add(actualStatePlayer);
			
			playersCounter++;
		}
		
		actualStateTeam.setSize(playersCounter-1);
		
		//ordino e sommo le coppe del team allo stato attuale
		trophiesSortAndSum(actualStateTeam);
		
		model.getTeamsPrevRanking().add(prevStateTeam);
		model.getTeamsActualRanking().add(actualStateTeam);
		
		sort(model.getTeamsPrevRanking(), teamComparator);
		sort(model.getTeamsActualRanking(), teamComparator);
		sort(model.getPlayersPrevRanking(), playerComparator);
		sort(model.getPlayersActualRanking(), playerComparator);
		
		return model;
	}
	
	public static void updateDB(RankingsModel model) {
		logger.atInfo().log("UPDATE DB CUP");
		
		for (CLPlayer player : model.getPlayersActualRanking()) 
			DB.jooq().executeUpdate(loadFrom(player));
	}
	
	public static void updateRankings(RankingsModel model) {
		logger.atInfo().log("UPDATE RANKINGS CUP");
		
		List<TextChannel> rankPlayersChannels = new ArrayList<>();
		List<TextChannel> rankTeamsChannels = new ArrayList<>();
		
		for (ClrankingRecord guild : DB.jooq().selectFrom(CLRANKING).fetch()) {
			
			long guildID = guild.getIdGuild().longValue();
			
			if(guild.getIdChannelPlayers() != null)
				rankPlayersChannels.add(jda.getGuildById(guildID).getTextChannelById(guild.getIdChannelPlayers().longValue()));
			
			if(guild.getIdChannelTeams() != null)
				rankTeamsChannels.add(jda.getGuildById(guildID).getTextChannelById(guild.getIdChannelTeams().longValue()));
		}
		
		updateRanking(rankPlayersChannels, playersRanking(model));
		updateRanking(rankTeamsChannels, teamsRanking(model));
	}

	private static void updateRanking(List<TextChannel> channels, List<String> messagesToWrite) {
		
		for (TextChannel channel : channels) {
			try {
				channel.purgeMessages(channel.getHistoryBefore(channel.getLatestMessageIdLong(), 50).complete().getRetrievedHistory());
				channel.purgeMessagesById(channel.getLatestMessageIdLong());
			}catch (Exception e) {
				logger.atError().log(e.getMessage());
			}
		}
		
		writeChannels(channels, messagesToWrite);
	}
	
	private static void trophiesSortAndSum(CLTeam team) {
		sort(team.getUpdatedPlayers(), playerUpdComparator);
		
		int playersCounter = 0;
		for (PlayerData player : team.getUpdatedPlayers()) {

			if(playersCounter++ > 4) break;
			
			team.sumTrophies(player.getTrophies());
		}
	}
	
	private static CLPlayer loadFrom(CupsLeagueRecord dbPlayer) {
		return new CLPlayer(dbPlayer.getTag(), dbPlayer.getNickname(), dbPlayer.getTrophies().intValue(), dbPlayer.getTeam());
	}
	
	private static CupsLeagueRecord loadFrom(CLPlayer clPlayer) {
		return new CupsLeagueRecord(clPlayer.getTag(), clPlayer.getNickname(), clPlayer.getTrophies().shortValue(), clPlayer.getTeam());
	}
}
