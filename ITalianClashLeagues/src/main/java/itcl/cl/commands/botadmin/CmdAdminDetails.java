package itcl.cl.commands.botadmin;

import java.util.StringJoiner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.command.signature.Signature;
import itcl.Config;
import itcl.cl.CupsLeague;
import itcl.commands.ITCLCommand;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdAdminDetails extends ITCLCommand{

	private static final Logger logger = LogManager.getLogger();
	
	@Override
	public String name() {
		return "admin subs";
	}

	@Override public String helpKey() {
		return CupsLeague.CMD_CL_ADMIN_DETAILS;
	}

	@Override
	public PermissionLevel permissionLevel() {
		return PermissionLevel.BOT_ADMIN;
	}
	
	@Signature
	public void command(MessageReceivedEvent event) {
		
		logger.atInfo().log("({})[{}]<{}>: {}", event.getGuild().getName(), event.getTextChannel().getName(), event.getAuthor(), event.getMessage().getContentDisplay());
		
		StringJoiner result = new StringJoiner("").add("Bot Admins ID").add("\n");
		
		for (Long botAdminID : Config.botConfig.adminIDs())
			result = result.add("<@"+botAdminID.toString()+"> ("+botAdminID.toString()+")").add("\n");
		
		event.getChannel().sendMessage(result.toString()).queue();
	}

}