package itcl.cl.commands.botadmin;

import static itcl.cl.jooq.models.tables.CupsLeague.CUPS_LEAGUE;

import java.util.Optional;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendstracker.models.PlayerData;
import itcl.Config;
import itcl.cl.CupsLeague;
import itcl.cl.jooq.models.tables.records.CupsLeagueRecord;
import itcl.commands.CupsLeagueCommand;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
/**
 * Register the player to the cups league.
 *
 * @author AVurro
 *
 */
public class CmdCLSubsAdd extends CupsLeagueCommand{
	
	@Override
	public String name() {
		return "cl+";
	}

	@Override public String helpKey() {
		return CupsLeague.CMD_CL_ADMIN_ADD_SUBS;
	}

	@Override
	public PermissionLevel permissionLevel() {
		return PermissionLevel.BOT_ADMIN;
	}
	
	@Signature(parameters = {StringParameter.class})
	public void command(MessageReceivedEvent event, String tagPlayer) {
		addPlayer(event, tagPlayer, CupsLeague.SINGLE_PLAYERS);
	}
	
	@Signature(order = 1, parameters = {StringParameter.class, StringParameter.class})
	public void command(MessageReceivedEvent event, String tagPlayers, String team) {
		
		for (String tagPlayer : tagPlayers.split(","))
			addPlayer(event, tagPlayer, team);
	}
	
	private void addPlayer(MessageReceivedEvent event, String tagPlayer, String team) {
			
			if(team.length()>CUPS_LEAGUE.TEAM.getDataType().length()) {
				event.getChannel().sendMessage("Il nome del team non puo essere maggiore di 15 caratteri.").queue();
				return;
			}
				
			//verifico che non sia già presente in DB
			Optional<CupsLeagueRecord> optDBPlayer = DB.jooq().selectFrom(CUPS_LEAGUE)
					.where(CUPS_LEAGUE.TAG.equal(tagPlayer)).fetchOptional();
			
			if(optDBPlayer.isPresent()) {
				CupsLeagueRecord dbPlayer = optDBPlayer.get();
				event.getChannel().sendMessage("Player: "+dbPlayer.getNickname()+" ("+tagPlayer+") già registrato! Eliminarlo e reinserirlo per eventuali modifiche.").queue();
				return;
			} 

			// verifico che il tag esista
			PlayerData cocPlayer = Config.getPlayerData(tagPlayer);
			
			
			CupsLeagueRecord cl = new CupsLeagueRecord(tagPlayer, cocPlayer.getNickname(), cocPlayer.getTrophies().shortValue(), team);
			DB.jooq().insertInto(CUPS_LEAGUE).set(cl).execute();
				
			event.getChannel().sendMessage("Player: "+cocPlayer.getNickname()+" ("+tagPlayer+") registrato!").queue();
	}
}
