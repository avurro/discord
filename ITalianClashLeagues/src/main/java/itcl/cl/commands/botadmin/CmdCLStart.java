package itcl.cl.commands.botadmin;

import static java.time.format.DateTimeFormatter.ofPattern;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import itcl.cl.CupsLeague;
import itcl.cl.commands.service.RankingsMonitor;
import itcl.commands.CupsLeagueCommand;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
public class CmdCLStart extends CupsLeagueCommand{
	
	private static final Logger logger = LogManager.getLogger();
	private final String datePattern = "dd/MM/yyyy-HH:mm";
	private DateTimeFormatter formatter = ofPattern(datePattern);

	@Override
	public String name() {
		return "cl start";
	}

	@Override public String helpKey() {
		return CupsLeague.CMD_CL_ADMIN_START;
	}

	@Override
	public PermissionLevel permissionLevel() {
		return PermissionLevel.BOT_ADMIN;
	}

	@Signature(parameters = {StringParameter.class, StringParameter.class})
	public void command(MessageReceivedEvent event, String strStart, String strEnd) {
		
		
		if(RankingsMonitor.started)
			event.getChannel().sendMessage("Start già effettuato in precedenza.").queue();
		
		LocalDateTime start, end;
		
		try {
			start = LocalDateTime.parse(strStart, formatter);
		}catch (DateTimeParseException e) {
			event.getChannel().sendMessage("la data di inizio non rispetta il pattern: "+datePattern).queue();
			return;
		}
       
		try {
			end = LocalDateTime.parse(strEnd, formatter);
		}catch (DateTimeParseException e) {
			event.getChannel().sendMessage("la data di fine non rispetta il pattern: "+datePattern).queue();
			return;
		}

		logger.atInfo().log("start: {} end: {}", start, end);
		
		ExecutorService executorService = Executors.newSingleThreadExecutor();
			
		executorService.execute(new RankingsMonitor(start, end));
		RankingsMonitor.started = true;
		
		event.getChannel().sendMessage("Cups League avviata con successo!").queue();
		
	}

}
