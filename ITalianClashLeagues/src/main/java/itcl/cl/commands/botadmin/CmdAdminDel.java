package itcl.cl.commands.botadmin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import itcl.Config;
import itcl.cl.CupsLeague;
import itcl.commands.ITCLCommand;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdAdminDel extends ITCLCommand{

	private static final Logger logger = LogManager.getLogger();
	
	@Override
	public String name() {
		return "admin-";
	}

	@Override public String helpKey() {
		return CupsLeague.CMD_CL_ADMIN_DEL_ADMIN;
	}

	@Override
	public PermissionLevel permissionLevel() {
		return PermissionLevel.BOT_ADMIN;
	}
	
	@Signature(parameters = {StringParameter.class})
	public void command(MessageReceivedEvent event, String adminsID) {
		
		logger.atInfo().log("({})[{}]<{}>: {}", event.getGuild().getName(), event.getTextChannel().getName(), event.getAuthor(), event.getMessage().getContentDisplay());
		
		for (String adminID : adminsID.split(",")) {
			try {
				Long id = Long.valueOf(adminID);
				
				if(Config.botConfig.adminIDs().remove(id))
					event.getChannel().sendMessage("L'utente <@"+adminID+"> ("+adminID+"), è stato rimosso.").queue();
				else
					event.getChannel().sendMessage("L'utente <@"+adminID+"> ("+adminID+"), non è presente in lista, pertanto non è stato rimosso.").queue();
				
			}catch (NumberFormatException e) {
				event.getChannel().sendMessage("L'ID dichiarato: "+adminID+", non è un ID valido.").queue();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}