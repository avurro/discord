package itcl.cl.commands.botadmin;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.command.signature.Signature;
import itcl.cl.CupsLeague;
import itcl.cl.commands.service.RankingsMonitor;
import itcl.commands.CupsLeagueCommand;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdCLStop extends CupsLeagueCommand{
	
	@Override
	public String name() {
		return "cl end";
	}

	@Override public String helpKey() {
		return CupsLeague.CMD_CL_ADMIN_STOP;
	}

	@Override
	public PermissionLevel permissionLevel() {
		return PermissionLevel.BOT_ADMIN;
	}

	@Signature
	public void stop(MessageReceivedEvent event) {
		RankingsMonitor.toStop = true;
		event.getChannel().sendMessage("Stop programmato :+1:").queue();
	}
}
