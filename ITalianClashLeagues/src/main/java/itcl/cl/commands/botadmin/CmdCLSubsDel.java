package itcl.cl.commands.botadmin;

import static itcl.cl.jooq.models.tables.CupsLeague.CUPS_LEAGUE;

import org.jooq.Condition;
import org.jooq.exception.DataAccessException;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import it.bitsplitters.legendsdata.DB;
import itcl.cl.CupsLeague;
import itcl.commands.CupsLeagueCommand;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
public class CmdCLSubsDel extends CupsLeagueCommand {

	@Override
	public String name() {
		return "cl-";
	}

	@Override public String helpKey() {
		return CupsLeague.CMD_CL_ADMIN_DEL_SUBS;
	}

	@Override
	public PermissionLevel permissionLevel() {
		return PermissionLevel.BOT_ADMIN;
	}
	
	@Signature(parameters = {StringParameter.class})
	public void command(MessageReceivedEvent event, String toDelete) {
		
			boolean isPlayer = toDelete.startsWith("#");
			Condition condition = isPlayer ? CUPS_LEAGUE.TAG.equal(toDelete) : CUPS_LEAGUE.TEAM.equal(toDelete);
			String message = String.join(" ","Questo",isPlayer?"tag":"team","non risulta registrato, controlla che sia corretto.");
		
			delete(event, condition, message);
	}
	
	private void delete(MessageReceivedEvent event,Condition condition, String message) {
		try {
			
			if(DB.jooq().selectFrom(CUPS_LEAGUE).where(condition).execute() == 0) {
				event.getChannel().sendMessage(message).queue();
				return;
			}
			
			DB.jooq().delete(CUPS_LEAGUE).where(condition).execute();
			
			event.getChannel().sendMessage("Registrazione eliminata con successo!").queue();
		
		} catch (DataAccessException e) {
			event.getChannel().sendMessage("Problemi con la cancellazione, contattare l'amministratore.").queue();
		}
	}
}

