package itcl.cl.commands;

import static it.bitsplitters.util.BotUtil.escapeContent;
import static itcl.cl.jooq.models.tables.CupsLeague.CUPS_LEAGUE;

import java.util.StringJoiner;

import org.jooq.Result;
import org.jooq.exception.DataAccessException;

import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import it.bitsplitters.legendsdata.DB;
import itcl.cl.CupsLeague;
import itcl.cl.jooq.models.tables.records.CupsLeagueRecord;
import itcl.commands.ITCLCommand;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdSubs extends ITCLCommand {

	private String helpContent;
	
	@Override
	public String name() {
		return "cl subs";
	}
	@Override
	public String helpKey() {
		return null;
	}
	@Override
	public String helpContent(MessageReceivedEvent event) {
		if(helpContent == null || helpContent.isEmpty())
			helpContent = String.join("", "`itcl ",name(),"`\nMostra tutti i players iscritti.\n\n",
					"`itcl ",name()," <team>`\nMostra i players iscritti appartenenti ad un specifico team.");
			
		return helpContent;
	}
	
	@Signature
	public void showAll(MessageReceivedEvent event) {
		Result<CupsLeagueRecord> result = DB.jooq().selectFrom(CUPS_LEAGUE).orderBy(CUPS_LEAGUE.TEAM).fetch();
		show(event, result, "Non risultano players iscritti.", false);
	}
	
	@Signature(order = 1, parameters = {StringParameter.class})
	public void showTeam(MessageReceivedEvent event, String team) {
		Result<CupsLeagueRecord> result = DB.jooq().selectFrom(CUPS_LEAGUE).where(CUPS_LEAGUE.TEAM.eq(team)).fetch();
		show(event, result, "Non risultano players iscritti per questo team, assicurati di averlo scritto in modo corretto.", true);
	}
	
	private void show(MessageReceivedEvent event, Result<CupsLeagueRecord> result, String errorMessage, boolean oneTeam) {
		
		try {
			if(result.isEmpty()) {
				event.getChannel().sendMessage(errorMessage).queue();
				return;
			}
			
			StringJoiner teams = new StringJoiner("").add("**TEAMS ISCRITTI**\n\n");
			StringJoiner singlePlayers = new StringJoiner("").add("**SINGLE PLAYERS ISCRITTI**\n\n");
			int spTitle = singlePlayers.toString().length();
			
			String team = "";
			for (CupsLeagueRecord player : result) {
				
				if(team.isEmpty() || !team.equals(player.getTeam())) {
					team = player.getTeam();
					
					if(team.equals(CupsLeague.SINGLE_PLAYERS))
						singlePlayers.add("\n__***").add(team).add("***__\n");
					else
						teams.add("\n__***").add(team).add("***__\n");
				}
				
				if(team.equals(CupsLeague.SINGLE_PLAYERS))
					singlePlayers.add(escapeContent(player.getNickname())).add("\n");
				else
					teams.add(escapeContent(player.getNickname())).add("\n");
			}

			sendCLEmbedMessage(event, teams.toString());
			
			if(!oneTeam && singlePlayers.toString().length() > spTitle)
				sendCLEmbedMessage(event, singlePlayers.toString());
			
			
		} catch (DataAccessException e) {
			event.getChannel().sendMessage("Problemi con il recupero delle iscrizioni, contattare l'amministratore.").queue();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
