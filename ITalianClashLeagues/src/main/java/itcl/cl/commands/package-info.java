/**
 * <b>Cups League Commands</b></br>
 * Commands related to the management of the Cups League.
 * @author AVurro
 *
 */
package itcl.cl.commands;