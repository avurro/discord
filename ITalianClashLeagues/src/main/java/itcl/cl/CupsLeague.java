package itcl.cl;

public class CupsLeague {
	
	public static final String color = "159,41,247";
	public static final String symbol = "\uD83C\uDFC6";
	
	public static final short START_TROPHIES = 0;
	
	// TREND EMOJI
	public static final String UP2 = "<:clup2:926418977289289758>";
	public static final String UP = "<:clup:926418976886628384>";
	public static final String EQUAL = "<:cleq2:926418977092153394>";
	public static final String DOWN = "<:clcdn:926418976479776789>";
	public static final String DOWN2 = "<:clcdn2:926418976903430144>";
	
	public static final String START = "<:cupv:926414997704278036>";
	public static final String END = "<:cupr:926414997670723584>";
	public static final String DOWN_MIDDLE = "<:down2:926414997721079818>";
	public static final String UP_MIDDLE = "<:up1:926414997704282193>";
	public static final String EQ_MIDDLE = "<:eq:926414997393920020>";
	public static final String ATK_MIDDLE = "<:atkm:926418976957947944>"; 
	public static final String DEF_MIDDLE = "<:defm:926418977121517600>";
	
	// TROPHIES LOG EMOJI
	public static final String ATK = "<:atk:926414997301624902>";
	public static final String ATK_AND = "<:atk_and:926415579030634496>";
	public static final String ATK_DEF = "<:atkdef:926414997242916914>";
	public static final String ATK_DEF_WHITE = "<:white_def:926415579093532732>";
	public static final String ATK_MORE_DEF = "<:atkdefs:926414997310042112>";
	public static final String DEF = "<:def:926414997649776680>";
	public static final String MORE_ATK = "<:atks:926414997268099133>";
	public static final String MORE_DEF = "<:defs:926414997393924096>";
	public static final String WIN_DEF = "<:wdef:926414997767213106>"; 

	// CONFIGURATION CONSTANTS
	public static final String SINGLE_PLAYERS = "SinglePlayers";
	
	// PATREON COMMANDS
	public static final String KEY_CL_RANKING = "clranking";	
	
	// ORA DI INIZIO GIORNO DI LEGA
	public static final int LEGEND_LEAGUE_HOUR = 6;
	
	//HELP
	public static final String CMD_CL_RANKDETAILS = "cmd.cl.rankdetails.help";
	public static final String CMD_CL_RANKPLAYERS = "cmd.cl.rankplayers.help";
	public static final String CMD_CL_RANKTEAMS = "cmd.cl.rankteams.help";	
	public static final String CMD_CL_SUBSDETAILS = "cmd.cl.subsdetails.help"; 	
	public static final String CMD_CL_CHANNELLOG = "cmd.cl.channellog.help";
	public static final String CMD_CL_ADMIN_ADD_ADMIN = "cmd.cl.admin.add.admin.help";
	public static final String CMD_CL_ADMIN_DEL_ADMIN = "cmd.cl.admin.del.admin.help";
	public static final String CMD_CL_ADMIN_DETAILS = "cmd.cl.admin.details.help"; 
	public static final String CMD_CL_ADMIN_START = "cmd.cl.admin.start"; 		
	public static final String CMD_CL_ADMIN_STOP = "cmd.cl.admin.stop"; 		
	public static final String CMD_CL_ADMIN_GUILDPAYLEVEL = "cmd.cl.admin.guildpaylevel"; 
	public static final String CMD_CL_ADMIN_ADD_SUBS = "cmd.cl.admin.add.sub.help"; 
	public static final String CMD_CL_ADMIN_DEL_SUBS = "cmd.cl.admin.del.sub.help"; 
	
	//WARNING
	public static final String CMD_CL_SUBSDETAILS_WARNING_NOTEAM = "cmd.cl.subsdetails.warning.noteam";
	public static final String CMD_CL_SUBSDETAILS_WARNING_NOPLAYERS = "cmd.cl.subsdetails.warning.noplayers";
	
}

