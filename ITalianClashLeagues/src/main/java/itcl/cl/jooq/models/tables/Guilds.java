/*
 * This file is generated by jOOQ.
 */
package itcl.cl.jooq.models.tables;


import itcl.cl.jooq.models.Itcl;
import itcl.cl.jooq.models.Keys;
import itcl.cl.jooq.models.tables.records.GuildsRecord;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row5;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;
import org.jooq.types.ULong;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Guilds extends TableImpl<GuildsRecord> {

    private static final long serialVersionUID = -945340368;

    /**
     * The reference instance of <code>itcl.guilds</code>
     */
    public static final Guilds GUILDS = new Guilds();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<GuildsRecord> getRecordType() {
        return GuildsRecord.class;
    }

    /**
     * The column <code>itcl.guilds.ID_GUILD</code>.
     */
    public final TableField<GuildsRecord, ULong> ID_GUILD = createField(DSL.name("ID_GUILD"), org.jooq.impl.SQLDataType.BIGINTUNSIGNED.nullable(false), this, "");

    /**
     * The column <code>itcl.guilds.PAY_END</code>.
     */
    public final TableField<GuildsRecord, LocalDate> PAY_END = createField(DSL.name("PAY_END"), org.jooq.impl.SQLDataType.LOCALDATE.defaultValue(org.jooq.impl.DSL.field("NULL", org.jooq.impl.SQLDataType.LOCALDATE)), this, "");

    /**
     * The column <code>itcl.guilds.PAY_LEVEL</code>.
     */
    public final TableField<GuildsRecord, Byte> PAY_LEVEL = createField(DSL.name("PAY_LEVEL"), org.jooq.impl.SQLDataType.TINYINT.defaultValue(org.jooq.impl.DSL.field("0", org.jooq.impl.SQLDataType.TINYINT)), this, "");

    /**
     * The column <code>itcl.guilds.PREFIX</code>.
     */
    public final TableField<GuildsRecord, String> PREFIX = createField(DSL.name("PREFIX"), org.jooq.impl.SQLDataType.VARCHAR(4).defaultValue(org.jooq.impl.DSL.field("NULL", org.jooq.impl.SQLDataType.VARCHAR)), this, "");

    /**
     * The column <code>itcl.guilds.LOCALE</code>.
     */
    public final TableField<GuildsRecord, String> LOCALE = createField(DSL.name("LOCALE"), org.jooq.impl.SQLDataType.VARCHAR(2).defaultValue(org.jooq.impl.DSL.field("NULL", org.jooq.impl.SQLDataType.VARCHAR)), this, "");

    /**
     * Create a <code>itcl.guilds</code> table reference
     */
    public Guilds() {
        this(DSL.name("guilds"), null);
    }

    /**
     * Create an aliased <code>itcl.guilds</code> table reference
     */
    public Guilds(String alias) {
        this(DSL.name(alias), GUILDS);
    }

    /**
     * Create an aliased <code>itcl.guilds</code> table reference
     */
    public Guilds(Name alias) {
        this(alias, GUILDS);
    }

    private Guilds(Name alias, Table<GuildsRecord> aliased) {
        this(alias, aliased, null);
    }

    private Guilds(Name alias, Table<GuildsRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    public <O extends Record> Guilds(Table<O> child, ForeignKey<O, GuildsRecord> key) {
        super(child, key, GUILDS);
    }

    @Override
    public Schema getSchema() {
        return Itcl.ITCL;
    }

    @Override
    public UniqueKey<GuildsRecord> getPrimaryKey() {
        return Keys.KEY_GUILDS_PRIMARY;
    }

    @Override
    public List<UniqueKey<GuildsRecord>> getKeys() {
        return Arrays.<UniqueKey<GuildsRecord>>asList(Keys.KEY_GUILDS_PRIMARY);
    }

    @Override
    public Guilds as(String alias) {
        return new Guilds(DSL.name(alias), this);
    }

    @Override
    public Guilds as(Name alias) {
        return new Guilds(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Guilds rename(String name) {
        return new Guilds(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Guilds rename(Name name) {
        return new Guilds(name, null);
    }

    // -------------------------------------------------------------------------
    // Row5 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row5<ULong, LocalDate, Byte, String, String> fieldsRow() {
        return (Row5) super.fieldsRow();
    }
}
