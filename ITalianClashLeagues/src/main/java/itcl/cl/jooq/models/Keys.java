/*
 * This file is generated by jOOQ.
 */
package itcl.cl.jooq.models;


import itcl.cl.jooq.models.tables.Clranking;
import itcl.cl.jooq.models.tables.CupsLeague;
import itcl.cl.jooq.models.tables.Guilds;
import itcl.cl.jooq.models.tables.records.ClrankingRecord;
import itcl.cl.jooq.models.tables.records.CupsLeagueRecord;
import itcl.cl.jooq.models.tables.records.GuildsRecord;

import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.Internal;


/**
 * A class modelling foreign key relationships and constraints of tables of 
 * the <code>itcl</code> schema.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Keys {

    // -------------------------------------------------------------------------
    // IDENTITY definitions
    // -------------------------------------------------------------------------


    // -------------------------------------------------------------------------
    // UNIQUE and PRIMARY KEY definitions
    // -------------------------------------------------------------------------

    public static final UniqueKey<ClrankingRecord> KEY_CLRANKING_PRIMARY = UniqueKeys0.KEY_CLRANKING_PRIMARY;
    public static final UniqueKey<CupsLeagueRecord> KEY_CUPS_LEAGUE_PRIMARY = UniqueKeys0.KEY_CUPS_LEAGUE_PRIMARY;
    public static final UniqueKey<GuildsRecord> KEY_GUILDS_PRIMARY = UniqueKeys0.KEY_GUILDS_PRIMARY;

    // -------------------------------------------------------------------------
    // FOREIGN KEY definitions
    // -------------------------------------------------------------------------


    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private static class UniqueKeys0 {
        public static final UniqueKey<ClrankingRecord> KEY_CLRANKING_PRIMARY = Internal.createUniqueKey(Clranking.CLRANKING, "KEY_clranking_PRIMARY", new TableField[] { Clranking.CLRANKING.ID_GUILD }, true);
        public static final UniqueKey<CupsLeagueRecord> KEY_CUPS_LEAGUE_PRIMARY = Internal.createUniqueKey(CupsLeague.CUPS_LEAGUE, "KEY_cups_league_PRIMARY", new TableField[] { CupsLeague.CUPS_LEAGUE.TAG }, true);
        public static final UniqueKey<GuildsRecord> KEY_GUILDS_PRIMARY = Internal.createUniqueKey(Guilds.GUILDS, "KEY_guilds_PRIMARY", new TableField[] { Guilds.GUILDS.ID_GUILD }, true);
    }
}
