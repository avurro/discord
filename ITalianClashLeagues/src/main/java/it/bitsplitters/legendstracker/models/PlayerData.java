package it.bitsplitters.legendstracker.models;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import it.bitsplitters.clashapi.Player;
public class PlayerData{

	


	private String nickname;
	private String tag;
	private Integer trophies;
	private Integer startTrophies;
	private Integer attacks;
	private Integer defenses;
	private Integer defenseWins;
	private Integer attackWins;
	private List<String> dailyAttacks;
	private List<String> dailyDefenses;
	private List<String> dailyMix;
	private List<Integer> dailyAtkTrophies;
	private List<Integer> dailyDefTrophies;
	private List<Integer> dailyMixTrophies;
	private long sumWithBonus;
	
	public PlayerData(String nickname, String tag, Integer trophies, Integer defenseWins, Integer attackWins) {
		this.nickname = nickname;
		this.tag = tag;
		this.trophies = trophies;
		this.defenseWins = defenseWins;
		this.attackWins = attackWins;
		reset();
	}
	
	public PlayerData(Player player) {
		this(player.getName(), player.getTag(), player.getTrophies(), player.getDefenseWins(), player.getAttackWins());
	}
	
	public void reset() {
		this.attacks = 0;
		this.defenses = 0;
		this.dailyAttacks = new ArrayList<>(8);
		this.dailyDefenses = new ArrayList<>(8);
		this.dailyMix = new ArrayList<>(8);
		dailyAtkTrophies = new ArrayList<>(8);
		dailyDefTrophies = new ArrayList<>(8);
		dailyMixTrophies = new ArrayList<>(8);
		this.startTrophies = this.trophies;
	}
	
	public Integer getAttacks() {
		return attacks;
	}

	public void setAttacks(Integer attacks) {
		this.attacks = attacks;
	}

	public Integer getDefenses() {
		return defenses;
	}

	public void setDefenses(Integer defenses) {
		this.defenses = defenses;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public void setStartTrophies(Integer startTrophies) {
		this.startTrophies = startTrophies;
	}

	public void setDailyAttacks(List<String> dailyAttacks) {
		this.dailyAttacks = dailyAttacks;
	}

	public void setDailyDefenses(List<String> dailyDefenses) {
		this.dailyDefenses = dailyDefenses;
	}

	public void setDailyMix(List<String> dailyMix) {
		this.dailyMix = dailyMix;
	}

	public void setDailyAtkTrophies(List<Integer> dailyAtkTrophies) {
		this.dailyAtkTrophies = dailyAtkTrophies;
	}

	public void setDailyDefTrophies(List<Integer> dailyDefTrophies) {
		this.dailyDefTrophies = dailyDefTrophies;
	}

	public void setDailyMixTrophies(List<Integer> dailyMixTrophies) {
		this.dailyMixTrophies = dailyMixTrophies;
	}
	public long getSumWithBonus() {
		return sumWithBonus;
	}

	public void setSumWithBonus(long sumWithBonus) {
		this.sumWithBonus = sumWithBonus;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getTag() {
		return tag;
	}
	
	public Integer getTrophies() {
		return trophies;
	}
	
	public void setTrophies(Integer trophies) {
		this.trophies = trophies;
	}
	
	public void addAttack() {
		this.attacks++;
	}
	
	public Integer attacks() {
		return this.attacks;
	}
	
	public void addDefense() {
		this.defenses++;
	}
	
	public Integer defenses() {
		return this.defenses;
	}
	
	public Integer getDefenseWins() {
		return defenseWins;
	}

	public void setDefenseWins(Integer defenseWins) {
		this.defenseWins = defenseWins;
	}

	
	public Integer getAttackWins() {
		return attackWins;
	}

	public void setAttackWins(Integer attackWins) {
		this.attackWins = attackWins;
	}

	
	public List<String> getDailyAttacks() {
		return dailyAttacks;
	}

	public void addDailyAttacks(String dailyAttacks) {
		this.dailyAttacks.add(dailyAttacks);
	}

	public List<String> getDailyDefenses() {
		return dailyDefenses;
	}

	public void addDailyDefenses(String dailyDefenses) {
		this.dailyDefenses.add(dailyDefenses);
	}

	public List<String> getDailyMix() {
		return dailyMix;
	}

	public void addDailyMix(String mix) {
		this.dailyMix.add(mix);
	}
	
	public Integer getStartTrophies() {
		return startTrophies;
	}
	
	public PlayerData clone(){
		PlayerData clone = new PlayerData(nickname, tag, trophies, defenseWins, attackWins);
		clone.startTrophies = this.startTrophies;
		clone.attacks = this.attacks;
		clone.defenses = this.defenses;
		
		clone.dailyAttacks  = new ArrayList<>(this.dailyAttacks);
		clone.dailyDefenses = new ArrayList<>(this.dailyDefenses);
		clone.dailyMix      = new ArrayList<>(this.dailyMix);
		
		clone.dailyAtkTrophies = new ArrayList<>(this.dailyAtkTrophies);
		clone.dailyDefTrophies = new ArrayList<>(this.dailyDefTrophies);
		clone.dailyMixTrophies = new ArrayList<>(this.dailyMixTrophies);
		
		return clone;
	}

	public List<Integer> getDailyAtkTrophies() {
		return dailyAtkTrophies;
	}

	public void addDailyAtkTrophies(Integer trophies) {
		this.dailyAtkTrophies.add(trophies);
	}

	public List<Integer> getDailyDefTrophies() {
		return dailyDefTrophies;
	}

	public void addDailyDefTrophies(Integer trophies) {
		this.dailyDefTrophies.add(trophies);
	}

	public List<Integer> getDailyMixTrophies() {
		return dailyMixTrophies;
	}

	public void addDailyMixTrophies(Integer trophies) {
		this.dailyMixTrophies.add(trophies);
	}
	
	public Integer sumAtks() {
		Integer sum = 0;
		
		for (Integer trophies : dailyAtkTrophies) 
			sum+= trophies;
		
		return sum;
	}
	
	public Integer sumDefs() {
		Integer sum = 0;
		
		for (Integer trophies : dailyDefTrophies) 
			sum+= trophies;
		
		return sum;
	}
	
	public Integer sumMix() {
		Integer sum = 0;
		
		for (Integer trophies : dailyMixTrophies) 
			sum+= trophies;
		
		return sum;
	}

	@Override
	public String toString() {
		return "CLPlayerLegendTrophies [nickname=" + nickname + ", tag=" + tag + ", trophies=" + trophies
				+ ", startTrophies=" + startTrophies + ", attacks=" + attacks + ", defenses=" + defenses
				+ ", defenseWins=" + defenseWins + ", attackWins=" + attackWins + ", dailyAtkTrophies="
				+ toString(dailyAtkTrophies) + ", dailyDefTrophies=" + toString(dailyDefTrophies) + ", dailyMixTrophies=" + toString(dailyMixTrophies)+ "]";
	}


//	@Override
//	public String toString() {
//		StringJoiner channels = new StringJoiner(", ");
//		for (GuildChannels textChannel : guildChannels)
//			channels.add(textChannel.getLogChannel() == null ? "null" : textChannel.getLogChannel().toString());
//		
//		return "nickname=" + nickname + ", tag=" + tag + ", trophies=" + trophies + ", logChannels ="+ channels.toString();
//	}

	private String toString(List<?> list) {
		StringJoiner sj = new StringJoiner(", ");
		for (Object obj : list) 
			sj.add(obj.toString());
		
		return sj.toString();
	}
}
