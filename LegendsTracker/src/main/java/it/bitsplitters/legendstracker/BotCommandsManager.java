package it.bitsplitters.legendstracker;

import static net.dv8tion.jda.api.Permission.ADMINISTRATOR;

import java.util.Optional;

import it.bitsplitters.CommandsManager;
import it.bitsplitters.command.Command;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class BotCommandsManager implements CommandsManager {

	@Override
	public boolean isCommandRunnable(Command command, MessageReceivedEvent event) {
		
		if(command.permissionLevel().isPublic()) 
			return true;

		if(event.getMember().getPermissions().contains(ADMINISTRATOR))
			return command.permissionLevel().isAdmin() || command.permissionLevel().isReserved();
		
		
		if(command.permissionLevel().isReserved()) {
			
			Optional<GuildsRecord> optGuild = Config.guildsManager.findRecord(event.getGuild().getIdLong());
			if(!optGuild.isPresent())
				return false;
			
			GuildsRecord guild = optGuild.get();
			
			long roleId = guild.getIdRoleReserved().longValue();
			 
			if(roleId == 0) 
				return false;
			
			return event.getMember().getRoles().contains(event.getGuild().getRoleById(roleId));
		}

		return false;
	}

}
