package it.bitsplitters.legendstracker;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.util.concurrent.RateLimiter;

import it.bitsplitters.clashapi.Clan;
import it.bitsplitters.clashapi.ClashAPIAsync;
import it.bitsplitters.clashapi.ClashWrapper;
import it.bitsplitters.clashapi.CocApiAsyncConfig;
import it.bitsplitters.clashapi.Player;

public enum COC {

	API;

	private final Logger logger = LogManager.getLogger();

	private ClashAPIAsync requestAPIAsync;
	private CocApiAsyncConfig requestConfig;
	private RateLimiter requestRateLimiter;
	private final Properties configProperties = new Properties();
	private final String requestApiTokenKey = "api.token.batch";
	private String requestApiToken; 
	
	private String getRequestApi() {
		return requestApiToken;
	}
	
	
	private COC() {
		
		try(FileInputStream file = new FileInputStream("./legendstracker.properties")) {
			configProperties.load(file);
			requestApiToken = configProperties.getProperty(requestApiTokenKey);

		}catch (IOException e) { logger.atError().log(e.getMessage()); }
		
		requestConfig = new CocApiAsyncConfig() {
			ExecutorService service = Executors.newSingleThreadExecutor();
			@Override public String getToken() { return getRequestApi(); }
			@Override public ExecutorService getService() { return service; }
			@Override public int timeout() { return 10; }
		};

		requestRateLimiter = RateLimiter.create(15);
		requestAPIAsync = ClashWrapper.getAPIInstanceAsync(requestConfig);
	}
	
	public ClashAPIAsync getRequestAsync() {
		requestRateLimiter.acquire();
		return this.requestAPIAsync;
	}
	
	public CompletableFuture<Player> requestAsyncPlayer(String playerTag){
		
		try {
		
			return getRequestAsync().requestPlayer(playerTag);
		
		} catch (CompletionException | UnsupportedEncodingException | URISyntaxException e) {
			logger.atError().log(e.toString());
			return null;
		}
	}
	
	public CompletableFuture<Clan> requestAsyncClan(String clanTag){
		
		try {
		
			return getRequestAsync().requestClan(clanTag);
		
		} catch (CompletionException | UnsupportedEncodingException | URISyntaxException e) {
			logger.atError().log(e.toString());
			return null;
		}
	}
}