package it.bitsplitters.legendstracker;

import static it.bitsplitters.legendstracker.Config.getBottoken;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.ygimenez.method.Pages;
import com.github.ygimenez.model.Paginator;
import com.github.ygimenez.model.PaginatorBuilder;
import com.github.ygimenez.type.Emote;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.requests.restaction.CommandListUpdateAction;

public class Start {

public static void main(String[] args){
		
		Logger logger = LogManager.getLogger();

		try {
			
			Bot bot = new Bot();
			
			JDA jda =  //JDABuilder.createLight(getBottoken(), EnumSet.noneOf(GatewayIntent.class))
	          JDABuilder.createLight(getBottoken(), GatewayIntent.GUILD_MESSAGES, GatewayIntent.GUILD_MESSAGE_REACTIONS, GatewayIntent.GUILD_EMOJIS)
	                    .addEventListeners(bot)   // An instance of a class that will handle events.
//	                    .setActivity(Activity.playing(".help"))
	                    .build();
	            
				jda.awaitReady(); // Blocking guarantees that JDA will be completely loaded.
	            
            Paginator paginator = PaginatorBuilder.createPaginator().setHandler(jda)
            		.shouldRemoveOnReact(true)
            		.setEmote(Emote.GOTO_FIRST,"<:ss2:809233195354685480>")
            		.setEmote(Emote.PREVIOUS,"<:s2:809233195303436298>")
            		.setEmote(Emote.GOTO_LAST,"<:dd2:809233195181670410>") 
            		.setEmote(Emote.NEXT,"<:d2:809233195136188467>")
            		.setEmote(Emote.CANCEL, "<:cancel3:809239881002909737>")
            		.build();

            Pages.activate(paginator);
            
            logger.atInfo().log("Finished Building JDA!");
        
            bot.startBattlefield();
	            
	        	
            if(args == null || args.length == 0) {
            
            } else if(args[0].contentEquals("0")) {
	        	
	        	//Pubblicazione dei soli comandi accessibili al pubblico
	        	CommandListUpdateAction commands = bot.getStructure().getPublicAction(jda);
	        	bot.getStructure().publicCommands(commands).queue();
	        
	        }else if(args[0].contentEquals("1")) {
	        	
	        	//Pubblicazione dei soli comandi accessibili al pubblico
	        	CommandListUpdateAction commands = bot.getStructure().getPublicAction(jda);
	        	bot.getStructure().publicCommands(commands).complete();

	        	long headquarterID = 747102960651075685L;
	        	
	        	//Pubblicazione nel server Headquarter di tutti i comandi, inclusi quelli di gestione bot
	        	commands = bot.getStructure().getGuildAction(jda, headquarterID);
	        	commands.complete();
	        	commands = bot.getStructure().getGuildAction(jda, headquarterID);
	        	bot.getStructure().updateReservedCommands(bot.getStructure().publicCommands(commands)).queue();
	        
	        }else if(args[0].contentEquals("2")){

	        	long headquarterID = 747102960651075685L;
	        	
	        	//Pubblicazione nel server Headquarter di tutti i comandi, inclusi quelli di gestione bot
	        	CommandListUpdateAction commands = bot.getStructure().getGuildAction(jda, headquarterID);
	        	commands.complete();
	        	commands = bot.getStructure().getGuildAction(jda, headquarterID);
	        	bot.getStructure().updateReservedCommands(bot.getStructure().publicCommands(commands)).queue();
	        
	        }else if(args[0].contentEquals("3")){
	        	//Cancellazione comandi
	        	jda.updateCommands().queue();
	        }
	        

	        Bot.jda = jda;
		}catch (Exception e) {
			e.printStackTrace();
			logger.atError().log(e.getMessage());
		}
    }
}
