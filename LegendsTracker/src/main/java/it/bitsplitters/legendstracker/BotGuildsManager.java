package it.bitsplitters.legendstracker;

import static it.bitsplitters.legends.batch.service.PlayersService.unlinkCachedPlayerFromGuild;
import static it.bitsplitters.legendsdata.service.SrvGuild.getOptGuildRecord;
import static it.bitsplitters.legendsdata.service.SrvGuild.insertGuild;
import static it.bitsplitters.legendsdata.service.SrvGuild.isSubscriber;
import static it.bitsplitters.legendsdata.service.SrvPlayer.deleteGuildsPlayers;
import static it.bitsplitters.legendsdata.service.SrvPlayer.deletePlayer;
import static it.bitsplitters.legendsdata.service.SrvPlayer.guildsPlayers;
import static it.bitsplitters.legendsdata.service.SrvPlayer.playerById;
import static it.bitsplitters.legendstracker.adapters.GuildsAdapter.toGuild;
import static it.bitsplitters.legendstracker.resourcebundle.ResourceBundleLegendsTracker.Languages.ENG;

import java.time.LocalDate;
import java.util.Optional;

import org.jooq.types.ULong;

import it.bitsplitters.GuildsManager;
import it.bitsplitters.PayLevel;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsPlayersRecord;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendsdata.jooq.models.tables.records.PlayersRecord;
import it.bitsplitters.setting.GuildSettings;
public class BotGuildsManager implements GuildsManager {

	@Override
	public Optional<GuildSettings> find(Long guildID) {
		Optional<GuildsRecord> optGuildsRecord = findRecord(guildID);
		
		if(!optGuildsRecord.isPresent())
			return Optional.empty();
		
		return Optional.of(toGuild(optGuildsRecord.get()));
	}

	@Override
	public GuildSettings register(Long guildID) {
		return toGuild(registerRecord(guildID));
	}
	
	private GuildsRecord registerRecord(Long guildID) {
		ULong id = ULong.valueOf(guildID);
		byte payLevel = 0;
		LocalDate payEnd = null;
		String prefix = ".";
		String locale = ENG.get();
		Short profilesMax = 0;
		ULong idChannelLog = null;
		ULong idChannelComs = null;
		GuildsRecord guild = new GuildsRecord(id, payLevel, payEnd, prefix, locale, profilesMax, idChannelLog, idChannelComs, ULong.valueOf(0));
		
		insertGuild(guild);
		
		return guild;
	}
	
	@Override
	public void guildJoin(Long guildID) {
		Optional<GuildSettings> optGuild = find(guildID);
	
		if(optGuild.isPresent()) return;
		
		register(guildID);
	}

	@Override
	public void guildLeave(Long guildID) {
		
		Optional<GuildsRecord> optGuild = findRecord(guildID);
		
		if(!optGuild.isPresent()) return;
		
		GuildsRecord guild = optGuild.get();
		
		unlinkGuild(guild.getIdGuild());

		if(PayLevel.isFree(guild.getPayLevel())) 
			guild.delete();
		
	}

	public boolean isValidPayer(Long guildID) {
		return isSubscriber(guildID);
	}
	
	public Optional<GuildsRecord> findRecord(Long guildID) {
		Optional<GuildsRecord> optGuildsRecord = getOptGuildRecord(guildID);
		return optGuildsRecord.isPresent() ?  optGuildsRecord : Optional.of(registerRecord(guildID));
	}
	
	private void unlinkGuild(ULong guildID) {
		
		for (GuildsPlayersRecord guildsPlayers : guildsPlayers(guildID)) {
			
			PlayersRecord player = playerById(guildsPlayers.getIdPlayer());
			
			// elimino il player per questa guild dalla cache
			unlinkCachedPlayerFromGuild(player.getTag(), guildID.longValue());
			
			// elimino il player per questa guild dal DB
			deleteGuildsPlayers(guildsPlayers);
			
			// elimino il player se non e' più agganciato da nessuna guild
			deletePlayer(player);
		}
	}
}