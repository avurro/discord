package it.bitsplitters.legendstracker;

import static it.bitsplitters.legendstracker.service.DiscordLogWriterService.writeChannels;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.cache.Cache.Entry;

import org.apache.ignite.IgniteCache;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.models.GuildChannels;
import it.bitsplitters.legendsdata.models.PlayerData;
import it.bitsplitters.legendsdata.models.PlayerInfo;
import it.bitsplitters.legendsdata.models.PlayerLog;

public class BattlefieldLogging implements Runnable {

	private static final Logger logger = LogManager.getLogger();
	
	private static IgniteCache<String, PlayerLog> playersLog = Cache.playersLog();
	private static final long MINUTES_5 = 1000*60*5;
	private static Map<GuildChannels, Set<PlayerInfo>> battlefieldLog = new HashMap<>();
	
	public BattlefieldLogging() {
		playersLog.clear();
	}
	
	private void loadBattlefieldLog(List<GuildChannels> guildChannels, PlayerInfo info) {
	
		for (GuildChannels guildChannel : guildChannels) {
			
			Set<PlayerInfo> players = battlefieldLog.containsKey(guildChannel) 
									 ? battlefieldLog.get(guildChannel) 
									 : new HashSet<>();
			players.add(info);
			battlefieldLog.put(guildChannel, players);
		}
	}
	
	@Override
	public void run() {
	
		Set<String> tagsToDelete = new HashSet<>();
		
		while(Config.battlefieldIsActive) {
			
			try {
				
				Iterator<Entry<String, PlayerLog>> iterator = playersLog.iterator();
				
				while(iterator.hasNext()) {
					
					Entry<String, PlayerLog> playerLog = iterator.next();
					String tag = playerLog.getKey();
					
					PlayerData data = Cache.playersData().get(tag);
					tagsToDelete.add(tag);
					
					if(data == null) continue;
					
					PlayerInfo info = Cache.playersInfo().get(tag);
					info.setLastData(playerLog.getValue());
					loadBattlefieldLog(data.getGuildChannels(), info);
				}

				writeChannels(battlefieldLog);
				playersLog.clearAll(tagsToDelete);
				battlefieldLog.clear();
				tagsToDelete.clear();
				
				Thread.sleep(MINUTES_5);
				
			} catch (Exception e) {
				logger.atError().log(e.getMessage());
			}
			
		}		
	}
}