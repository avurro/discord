package it.bitsplitters.legendstracker;


import static it.bitsplitters.legendstracker.resourcebundle.ResourceBundleLegendsTracker.getResourceBundle;
import static it.bitsplitters.legendstracker.service.MessageUtil.stopMessage;
import static it.bitsplitters.legendstracker.service.MessageUtil.unauthorizedMessage;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GUILD_CONFIG_CHANNEL_COMS;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GUILD_CONFIG_CHANNEL_LOG;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GUILD_SUBSCRIPTION_INACTIVE;
import static it.bitsplitters.util.ResourceBundleSlashUtil.getSlashResourceBundle;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;

import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
/**
 * Where all configurations reside.
 * @author AVurro
 *
 */
public class Config {
	
	public static boolean battlefieldIsActive = true;
	
	public static final Integer NUMBER_TRY = 3;
	
	// CON ACCODATO IL TAG QUESTO LINK APRE DIRETTAMENTE IL GAME NEL PROFILO DI INTERESSE
	public static final String inGamePlayerLink = "https://link.clashofclans.com/?action=OpenPlayerProfile&tag=";
	
	// URL FAKE UTILIZZATO PER EVIDENZIARE E RENDERE COPIABILE SOLO UNA PORZIONE DEL TESTO
	public static final String fakeUrl = "https://co.py/";
	
	// URL UTILIZZATO PER L'INVIO DEL COMANDO STATE
	public static final String stateUrl = "https://cmd.state/";
		
	// COLORE UTILIZZATO PER GLI EMBED
	public static final Color legendsColor = new Color(159,41,247);
	
	public static final Integer durataEmbed = 120;
	
	// TROPHIES LOG EMOJI
	public static final String PERC = "<:percento:926415579303252008>";
	public static final String ATK = "<:atk:926414997301624902>";
	public static final String ATK_AND = "<:atk_and:926415579030634496>";
	public static final String ATK_DEF = "<:atkdef:926414997242916914>";
	public static final String ATK_DEF_WHITE = "<:white_def:926415579093532732>";
	public static final String ATK_MORE_DEF = "<:atkdefs:926414997310042112>";
	public static final String BLANK = "<:blank:926415579001270282>";
	public static final String DEF = "<:def:926414997649776680>";
	public static final String DEF_WINS = "<:defwins:926415579009671169>";
	public static final String MORE_ATK = "<:atks:926414997268099133>";
	public static final String MORE_DEF = "<:defs:926414997393924096>";
	public static final String WIN_DEF = "<:wdef:926414997767213106>"; 
	public static final String CUP = "<:cup:926414997641379862>";
	
	public static final String START = "<:cupv:926414997704278036>";
	public static final String END = "<:cupr:926414997670723584>";
	public static final String DOWN_MIDDLE = "<:down2:926414997721079818>";
	public static final String UP_MIDDLE = "<:up1:926414997704282193>";
	public static final String EQ_MIDDLE = "<:eq:926414997393920020>";
	public static final String ATK_MIDDLE = "<:atkm:926418976957947944>"; 
	public static final String DEF_MIDDLE = "<:defm:926418977121517600>";

	public static final String LOADING = "<a:loading:926415579512983562>";
	public static final String CMD = "<:cmd:926415579089346631>";
	public static final String GEMME = "<:prizepool:832059009212743690>";
	public static final String BANNED = "<:redcard:926415578825097247>";
	
	/** where the configurations reside */
	private static final Properties configProperties = new Properties();
	
	/** Discord token, used to communicate with Discord */
	private static final String botToken = "bot.token";
	
	/** resource bundle name */
	public static final String RESOURCE_NAME = "resource";
	
	/** Guilds Manager */
	public static final BotGuildsManager guildsManager;
	
	/** BOT Admins */
	public static final long ALEX = 198341984346308609L;
	public static Locale defaultLocale = null;
	
	static {
		
		guildsManager = new BotGuildsManager();
		try(FileInputStream file = new FileInputStream("./legendstracker.properties")) {
				    
					configProperties.load(file);
					
		} catch (IOException e) { e.printStackTrace(); }
    	
    }
	
	
	public static Message sendLoading(SlashCommandEvent event) {
		EmbedBuilder eb = new EmbedBuilder();
		eb.setDescription("***Loading*** "+LOADING);
		eb.setColor(legendsColor);
		return  event.getHook().sendMessageEmbeds(eb.build()).complete();
	}
	
	public static Message sendLoading(SlashCommandEvent event, String tag) {
		EmbedBuilder eb = new EmbedBuilder();
		eb.setDescription("***"+tag+" Loading*** "+LOADING);
		eb.setColor(legendsColor);
		return  event.getHook().sendMessageEmbeds(eb.build()).complete();
	}
	
	public static Message sendLoading(MessageReceivedEvent event) {
		EmbedBuilder eb = new EmbedBuilder();
		eb.setDescription("***Loading*** "+LOADING);
		eb.setColor(legendsColor);
		return  event.getChannel().sendMessageEmbeds(eb.build()).complete();
	}
	
	public static Message sendLoading(MessageReceivedEvent event, String tag) {
		EmbedBuilder eb = new EmbedBuilder();
		eb.setDescription("***"+tag+" Loading*** "+LOADING);
		eb.setColor(legendsColor);
		return  event.getChannel().sendMessageEmbeds(eb.build()).complete();
	}
	
	public static boolean checkBasicCondition(SlashCommandEvent event) {
		
		Optional<GuildsRecord> optGuild = Config.guildsManager.findRecord(event.getGuild().getIdLong());
		
		if(!optGuild.isPresent()) return false;
		
		return isntActiveOrConfiguredServer(event, optGuild.get());
	}
	
	public static boolean isValidPayer(SlashCommandEvent event, GuildsRecord guild) {
		
		if(!Config.guildsManager.isValidPayer(guild.getIdGuild().longValue())) {
			unauthorizedMessage(event, GUILD_SUBSCRIPTION_INACTIVE);
			return false;
		}
		
		return true;
	}
	
	public static boolean isntActiveOrConfiguredServer(SlashCommandEvent event, GuildsRecord guild) {
		
		if(!isValidPayer(event, guild)) {
			return true;
		}
		
		if(guild.getIdChannelLog() == null) {
			stopMessage(event, GUILD_CONFIG_CHANNEL_LOG);
			return true;
		}
		
		if(guild.getIdChannelComs() == null) {
			stopMessage(event, GUILD_CONFIG_CHANNEL_COMS);
			return true;
		}
		
		return false;
	}
	
	public static String getMsg(GuildsRecord guild,String labelKey) {
		return getResourceBundle(Locale.forLanguageTag(guild.getLocale())).getString(labelKey);
	}
	
	public static String getSlashMsg(GuildsRecord guild,String labelKey) {
		return getSlashResourceBundle(Locale.forLanguageTag(guild.getLocale())).getString(labelKey);
	}
	
	public static String getBottoken() {
		return configProperties.getProperty(botToken);
	}

	public static String getPlayerLink(String name, String tag) {
		return String.join("","[",name,"](",inGamePlayerLink,tag,")");
	}
	
	public static String getFakeLink(String name, String content) {
		return String.join("","[",name,"](",fakeUrl,content,")");
	}
	
	public static String getFakeLink(String name, String content, String other) {
		return String.join("","[",name,"](",fakeUrl,content,")", other);
	}
	
	public static String getStateLink(String name, String content) {
		return String.join("","[",name,"](",stateUrl,content,")");
	}
	private Config() {}
}
