package it.bitsplitters.legendstracker.slashcommands.team;

import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;
import static it.bitsplitters.legendstracker.Config.getFakeLink;
import static it.bitsplitters.legendstracker.Config.isntActiveOrConfiguredServer;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_LIST_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_LIST_NAME;
import static it.bitsplitters.util.BotUtil.infoEmbed;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendstracker.Config;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdTeamList extends TeamSlashComand {

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of();
	}

	@Override
	public boolean isEphemeral() {
		return false;
	}

	@Override
	public void run(SlashCommandEvent event) {
		Config.guildsManager.findRecord(event.getGuild().getIdLong()).ifPresent(guild -> {
			
			if(isntActiveOrConfiguredServer(event, guild)) 
				return;

			List<String> teams = new ArrayList<>();
			for (String teamName : DB.jooq().selectDistinct(GUILDS_PLAYERS.TEAM).from(GUILDS_PLAYERS)
									     .where(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild()))
									     .fetch(GUILDS_PLAYERS.TEAM)) {

				int teamSize = DB.jooq().fetchCount(
						DB.jooq().select(GUILDS_PLAYERS.fields()).from(GUILDS_PLAYERS)
						.where(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild())
						.and(GUILDS_PLAYERS.TEAM.eq(teamName))));
				
				teams.add(getFakeLink(teamName, teamName)+"*("+teamSize+")*");
			}
			
			infoEmbed(event, String.join("\n", teams));
			
		});
	}

	@Override
	public String helpKey() {
		return TEAM_LIST_DESCRIPTION;
	}

	@Override
	public String nameKey() {
		return TEAM_LIST_NAME;
	}

}
