package it.bitsplitters.legendstracker.slashcommands;

import static it.bitsplitters.util.BotUtil.stopEmbed;

import java.util.Locale;

import it.bitsplitters.command.GeneralSlashCommand;
import it.bitsplitters.legendstracker.Config;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public abstract class LegendsTrackerSlashCommand extends GeneralSlashCommand {

	public static boolean isMaintenance = false;
	public static String maintenanceMessage = "";
	
	@Override
	public final void execute(SlashCommandEvent event) {
		if(isMaintenance)
			stopEmbed(event, maintenanceMessage);
		else 
			run(event);
			
	}
	
	public abstract void run(SlashCommandEvent event);
	
	public OptionData opt(OptionType optType, String name, String description, Locale locale) {
		return new OptionData(optType, getMsg(name, locale), getMsg(description, locale), false);
	}
	
	public OptionData optRequired(OptionType optType, String name, String description, Locale locale) {
		return new OptionData(optType, getMsg(name, locale), getMsg(description, locale), true);
	}
	
	public OptionMapping getOption(SlashCommandEvent event, String optName) {
		return event.getOption(getMsg(optName, Config.defaultLocale));
	}
}
