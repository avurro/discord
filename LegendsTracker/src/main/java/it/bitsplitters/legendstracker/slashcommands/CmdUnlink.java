package it.bitsplitters.legendstracker.slashcommands;

import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;
import static it.bitsplitters.legendsdata.jooq.models.tables.Players.PLAYERS;
import static it.bitsplitters.legendstracker.Config.getPlayerLink;
import static it.bitsplitters.legendstracker.service.MessageUtil.warningMessage;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_UNLINK_TAG_NOTEXIST;

import java.util.Optional;

import org.jooq.exception.DataAccessException;

import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsPlayersRecord;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendsdata.jooq.models.tables.records.PlayersRecord;
import it.bitsplitters.legendstracker.service.PlayersLog;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;

public abstract class CmdUnlink extends LegendsTrackerSlashCommand{

protected String removePlayer(SlashCommandEvent event, GuildsRecord guild, String tag) {
		
		String linkedPlayer = null;

		try {
			
			tag = tag.toUpperCase();
			
			PlayersRecord player = DB.jooq().selectFrom(PLAYERS).where(PLAYERS.TAG.eq(tag)).fetchOne();
			
			if(player == null ) {
				warningMessage(event, PLAYER_UNLINK_TAG_NOTEXIST, tag);
				return null;
			}
			
			linkedPlayer = getPlayerLink(player.getNickname(), tag);

			// Verifico che il tracciamento richiesto da cancellare sia effettivamente presente
			Optional<GuildsPlayersRecord> optGuildsPlayersRecord = DB.jooq().
					selectFrom(GUILDS_PLAYERS)
					    .where(GUILDS_PLAYERS.ID_PLAYER.eq(player.getIdPlayer())
					      .and(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild()))).fetchOptional();
				
			if(!optGuildsPlayersRecord.isPresent()) {
				warningMessage(event, PLAYER_UNLINK_TAG_NOTEXIST, linkedPlayer);
				return null;
			}
		
			// elimino il tracking dalla mappa trackingLog
			PlayersLog.removePlayer(player.getTag(), guild.getIdGuild().longValue());

			// elimino il tracking dalla tabella GUILDS PLAYERS
			optGuildsPlayersRecord.get().delete();
			
			// Controllo che al player siano agganciate altre guild, altrimenti lo cancello dalla tabella PLAYERS
			if(!DB.jooq().fetchExists(GUILDS_PLAYERS, GUILDS_PLAYERS.ID_PLAYER.eq(player.getIdPlayer())))
				player.delete();
			
			return linkedPlayer;
			
		}catch (DataAccessException e) {
			event.getChannel().sendMessage("Problems with data access, contact the administrator.").queue();
		}
		
		return null;
	}
}
