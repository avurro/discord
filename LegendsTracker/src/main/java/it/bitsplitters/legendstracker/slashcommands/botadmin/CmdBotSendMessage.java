package it.bitsplitters.legendstracker.slashcommands.botadmin;

import static it.bitsplitters.legendsdata.jooq.models.tables.Guilds.GUILDS;
import static it.bitsplitters.legendstracker.Bot.jda;
import static it.bitsplitters.util.BotUtil.okEmbed;

import java.util.List;
import java.util.Locale;

import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdBotSendMessage extends BotAdminSlashCommand {

	@Override
	public String name(Locale locale) {
		return "bot-send";
	}

	@Override
	public String description(Locale locale) {
		return "Invia messaggi in tutti i server";
	}

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(new OptionData(OptionType.STRING, "message", "Il messaggio da visualizzare a fronte di un comando eseguito", true));
	}
	
	@Override
	public void run(SlashCommandEvent event) {

		String message = event.getOption("message").getAsString();
		
		for(GuildsRecord guild: DB.jooq().selectFrom(GUILDS).fetch()) {
			
			if(guild.getIdChannelComs() != null) {
				
				TextChannel textChannel = jda.getTextChannelById(guild.getIdChannelComs().longValue());
				
				if(textChannel != null)
					textChannel.sendMessage(message).queue();
				
			}
		}

		okEmbed(event, "messaggio inviato");
			
	}

	@Override
	public boolean isEphemeral() {
		return true;
	}
}
