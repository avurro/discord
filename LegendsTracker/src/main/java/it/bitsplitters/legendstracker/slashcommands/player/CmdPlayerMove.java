package it.bitsplitters.legendstracker.slashcommands.player;

import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;
import static it.bitsplitters.legendsdata.jooq.models.tables.Players.PLAYERS;
import static it.bitsplitters.legendstracker.Config.getPlayerLink;
import static it.bitsplitters.legendstracker.Config.isntActiveOrConfiguredServer;
import static it.bitsplitters.legendstracker.service.MessageUtil.okMessage;
import static it.bitsplitters.legendstracker.service.MessageUtil.warningMessage;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsPlayersRecord;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendsdata.jooq.models.tables.records.PlayersRecord;
import it.bitsplitters.legendstracker.Config;
import it.bitsplitters.legendstracker.models.Result;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdPlayerMove extends PlayerSlashComand{

	@Override
	public PermissionLevel permissionLevel() {
		return PermissionLevel.RESERVED;
	}
	
	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(
				optRequired(OptionType.STRING, PLAYER_MOVE_TAG_NAME, PLAYER_MOVE_TAG_DESCRIPTION, locale),
				optRequired(OptionType.STRING, PLAYER_MOVE_TEAM_NAME, PLAYER_MOVE_TEAM_DESCRIPTION, locale)
				);
	}

	@Override
	public boolean isEphemeral() {
		return false;
	}

	@Override
	public void run(SlashCommandEvent event) {
		
		Config.guildsManager.findRecord(event.getGuild().getIdLong()).ifPresent(guild -> {
			
			if(isntActiveOrConfiguredServer(event, guild)) 
				return;

			List<String> accountsMoved = new ArrayList<>();
			
			String playersTag = getOption(event, PLAYER_MOVE_TAG_NAME).getAsString().toUpperCase();
			String newTeam = getOption(event, PLAYER_MOVE_TEAM_NAME).getAsString().toUpperCase();
			
			int playersCount = 0;
			for (String playerTag : playersTag.toUpperCase().split(",")) {
				
				Result<LegendsPlayer> result = movePlayer(event, guild, playerTag, newTeam);
				
				if(result.isOk() && playersCount++ <= 40) 
					accountsMoved.add(getPlayerLink(result.getObj().getNickname(), result.getObj().getTag()));
			}
			
			if(playersCount >= 40)
				accountsMoved.add("...");
			
			if(!accountsMoved.isEmpty()) 
				okMessage(event, PLAYER_MOVE_OK, String.join(", ", accountsMoved), newTeam);
		});
	}

	private Result<LegendsPlayer> movePlayer(SlashCommandEvent event, GuildsRecord guild, String playerTag, String newTeam) {

		playerTag = playerTag.toUpperCase();

		PlayersRecord player = DB.jooq().selectFrom(PLAYERS).where(PLAYERS.TAG.eq(playerTag)).fetchOne();
		
		// Si sta tentando di muovere un player non esistente a DB
		if(player == null) {
			warningMessage(event, PLAYER_MOVE_TAG_NOTEXIST, playerTag);
			return new Result<LegendsPlayer>(false, null);
		}
		
		String playerLink = getPlayerLink(player.getNickname(), player.getTag());
		
		GuildsPlayersRecord guildsPlayersRecord = DB.jooq().selectFrom(GUILDS_PLAYERS)
		      .where(GUILDS_PLAYERS.ID_PLAYER.eq(player.getIdPlayer())
			  .and(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild()))).fetchOne();
		
		// Il player che si sta muovendo non è presente tra quelli tracciati nel server
		if(guildsPlayersRecord == null) {
			warningMessage(event, PLAYER_MOVE_TAG_NOTEXIST, playerLink);
			return new Result<LegendsPlayer>(false, null);
		
		// Il player è già presente nel team in cui lo si vorrebbe spostare
		}else if(newTeam.equals(guildsPlayersRecord.getTeam())) {
			warningMessage(event, PLAYER_MOVE_TAG_EXIST, playerLink, newTeam);
			return new Result<LegendsPlayer>(false, null);
		}
		
		guildsPlayersRecord.setTeam(newTeam);
		guildsPlayersRecord.store();
	
		return new Result<LegendsPlayer>(true, new LegendsPlayer(player.getNickname(), playerTag));
	}

	private class LegendsPlayer {
		
		private String nickname;
		private String tag;
		
		public LegendsPlayer(String nickname, String tag) {
			super();
			this.nickname = nickname;
			this.tag = tag;
		}
		
		public String getNickname() { return nickname; }
		public String getTag() { return tag; }
	}

	@Override
	public String helpKey() {
		return PLAYER_MOVE_DESCRIPTION;
	}

	@Override
	public String nameKey() {
		return PLAYER_MOVE_NAME;
	}

}
