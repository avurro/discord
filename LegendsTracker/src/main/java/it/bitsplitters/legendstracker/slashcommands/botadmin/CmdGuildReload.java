package it.bitsplitters.legendstracker.slashcommands.botadmin;

import static it.bitsplitters.legendstracker.Config.guildsManager;
import static it.bitsplitters.util.BotUtil.okEmbed;
import static it.bitsplitters.util.BotUtil.warningEmbed;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendstracker.service.GuildService;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdGuildReload extends BotAdminSlashCommand {

	private static final Logger logger = LogManager.getLogger();
	
	@Override
	public String name(Locale locale) {
		return "guild-reload";
	}

	@Override
	public String description(Locale locale) {
		return "Ricarica gli accounts tracciati da una guild";
	}

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(new OptionData(OptionType.STRING, "guild-id", "ID del server da ricaricare", true));
	}

	@Override
	public void run(SlashCommandEvent event) {
		
		long guildID = Long.valueOf(event.getOption("guild-id").getAsString());
		
		Optional<GuildsRecord> optNewCustomer = guildsManager.findRecord(guildID);
		
		if(!optNewCustomer.isPresent()) {
			warningEmbed(event, "Non risulta alcun server discord con questo ID: "+guildID+", agganciato a Legends.");
			return;
		}
		
		GuildService.loadGuild(guildID);
		
		okEmbed(event, "Server: "+guildID+" ricaricato con successo.");
		
		logger.atInfo().log("Server: {} ricaricato con successo.", guildID);
	
	}
	
	@Override
	public boolean isEphemeral() {
		return true;
	}
}
