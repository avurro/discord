package it.bitsplitters.legendstracker.slashcommands.botadmin;

import static it.bitsplitters.util.BotUtil.okEmbed;

import java.util.List;
import java.util.Locale;

import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdBotMaintenance extends BotAdminSlashCommand {

	@Override
	public String name(Locale locale) {
		return "bot-mnt";
	}

	@Override
	public String description(Locale locale) {
		return "Abilita/disabilita la manutenzione";
	}

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(new OptionData(OptionType.BOOLEAN, "maintenance", "Vero per avviare la manutenzione, Falso negli altri casi", true),
				       new OptionData(OptionType.STRING, "message", "Il messaggio da visualizzare a fronte di un comando eseguito", true));
	}

	@Override
	public void run(SlashCommandEvent event) {

		isMaintenance = event.getOption("maintenance").getAsBoolean();
		maintenanceMessage = event.getOption("message").getAsString();
		
		okEmbed(event, "Manutenzione: "+ (isMaintenance ? "attiva" : "disattiva" ) + "\nMessaggio: "+maintenanceMessage);
	}

	@Override
	public boolean isEphemeral() {
		return true;
	}
}