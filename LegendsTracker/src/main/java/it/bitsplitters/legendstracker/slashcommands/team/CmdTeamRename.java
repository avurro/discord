package it.bitsplitters.legendstracker.slashcommands.team;

import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;
import static it.bitsplitters.legendstracker.Config.isntActiveOrConfiguredServer;
import static it.bitsplitters.legendstracker.service.MessageUtil.okMessage;
import static it.bitsplitters.legendstracker.service.MessageUtil.warningMessage;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_RENAME_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_RENAME_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_RENAME_NEWNAME_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_RENAME_NEWNAME_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_RENAME_NOTEXIST;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_RENAME_OK;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_RENAME_OLDNAME_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_RENAME_OLDNAME_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_RENAME_SIZE;
import static it.bitsplitters.util.BotUtil.warningEmbed;

import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.Result;
import org.jooq.exception.DataAccessException;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsPlayersRecord;
import it.bitsplitters.legendstracker.Config;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdTeamRename extends TeamSlashComand {

	private static final Logger logger = LogManager.getLogger();
	
	@Override public PermissionLevel permissionLevel() {
		return PermissionLevel.RESERVED;
	}
	
	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(
				optRequired(OptionType.STRING, TEAM_RENAME_OLDNAME_NAME, TEAM_RENAME_OLDNAME_DESCRIPTION, locale),
				optRequired(OptionType.STRING, TEAM_RENAME_NEWNAME_NAME, TEAM_RENAME_NEWNAME_DESCRIPTION, locale)
				);
	}

	@Override
	public boolean isEphemeral() {
		return false;
	}

	@Override
	public void run(SlashCommandEvent event) {

		Config.guildsManager.findRecord(event.getGuild().getIdLong()).ifPresent(guild -> {
			
			if(isntActiveOrConfiguredServer(event, guild)) 
				return;

			String currentTeam = getOption(event, TEAM_RENAME_OLDNAME_NAME).getAsString().toUpperCase();
			String newTeam = getOption(event, TEAM_RENAME_NEWNAME_NAME).getAsString().toUpperCase();
			
			if(newTeam.length() > 20) {
				warningMessage(event, TEAM_RENAME_SIZE);
				return;
			}
			
			try {
				
				Result<GuildsPlayersRecord> currentTeamRecord = DB.jooq().selectFrom(GUILDS_PLAYERS)
				      .where(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild())
					  .and(GUILDS_PLAYERS.TEAM.eq(currentTeam))).fetch();
				
				if(currentTeamRecord.isEmpty()) {
					warningMessage(event, TEAM_RENAME_NOTEXIST, currentTeam);
					return;
				}
				
				for (GuildsPlayersRecord currentTeamPlayer : currentTeamRecord) {
					
					currentTeamPlayer.setTeam(newTeam);
					
					try {
						
						DB.jooq().executeUpdate(currentTeamPlayer);
						
					} catch (DataAccessException e) {
						e.printStackTrace();
						warningEmbed(event, "Problems with data access, contact the administrator");
						logger.atError().log("Errore nella esecuzione di qualche query: {}", e.getMessage());
					}
				}

				okMessage(event, TEAM_RENAME_OK, currentTeam, newTeam);
			
			} catch (DataAccessException e) {
				event.getChannel().sendMessage("Problems with data access, contact the administrator.").queue();
				logger.atError().log("Problemi di connessione al server Supercell, contattare l'admin del bot.");
			}
		});
	}

	@Override
	public String helpKey() {
		return TEAM_RENAME_DESCRIPTION;
	}

	@Override
	public String nameKey() {
		return TEAM_RENAME_NAME;
	}

}
