package it.bitsplitters.legendstracker.slashcommands.team;

import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;
import static it.bitsplitters.legendsdata.jooq.models.tables.Players.PLAYERS;
import static it.bitsplitters.legendstracker.Config.ATK_MIDDLE;
import static it.bitsplitters.legendstracker.Config.DEF_MIDDLE;
import static it.bitsplitters.legendstracker.Config.END;
import static it.bitsplitters.legendstracker.Config.PERC;
import static it.bitsplitters.legendstracker.Config.START;
import static it.bitsplitters.legendstracker.Config.isntActiveOrConfiguredServer;
import static it.bitsplitters.legendstracker.service.MessageUtil.warningMessage;
import static it.bitsplitters.legendstracker.service.PlayersLog.getTrophiesLog;
import static it.bitsplitters.legendstracker.service.PrinterService.spAttacks;
import static it.bitsplitters.legendstracker.service.PrinterService.spDefenses;
import static it.bitsplitters.legendstracker.service.PrinterService.trophiesToEmoji;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_DETAILS_TYPE_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_DETAILS_TYPE_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_STATE_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_STATE_INFO_LEGEND;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_STATE_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_STATE_NAME_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_STATE_NAME_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_STATE_NOTEXIST;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_STATE_NUM_TO_LOAD_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_STATE_NUM_TO_LOAD_NAME;
import static java.util.Collections.sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;

import com.github.ygimenez.method.Pages;
import com.github.ygimenez.model.Page;

import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendsdata.models.PlayerData;
import it.bitsplitters.legendstracker.Config;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdTeamState extends TeamSlashComand {

	private static String option = "ALL";
	
	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(
				optRequired(OptionType.STRING, TEAM_STATE_NAME_NAME, TEAM_STATE_NAME_DESCRIPTION, locale),
				optRequired(OptionType.STRING, GENERAL_DETAILS_TYPE_NAME, GENERAL_DETAILS_TYPE_DESCRIPTION, locale)
				.addChoice("compact", "compact")
				.addChoice("wide", "wide"),
				opt(OptionType.INTEGER, TEAM_STATE_NUM_TO_LOAD_NAME, TEAM_STATE_NUM_TO_LOAD_DESCRIPTION, locale)
				);
	}

	@Override
	public boolean isEphemeral() {
		return false;
	}

	@Override
	public void run(SlashCommandEvent event) {
		Config.guildsManager.findRecord(event.getGuild().getIdLong()).ifPresent(guild -> {
			
			if(isntActiveOrConfiguredServer(event, guild)) 
				return;

			String teamsState = getOption(event, TEAM_STATE_NAME_NAME).getAsString().toUpperCase();
			boolean isAll = getOption(event, GENERAL_DETAILS_TYPE_NAME).getAsString().equals("wide") ? true : false;
			OptionMapping optPlayersToLoad = getOption(event, TEAM_STATE_NUM_TO_LOAD_NAME);
			Optional<Integer> number = optPlayersToLoad != null ? Optional.of(Long.valueOf(optPlayersToLoad.getAsLong()).intValue()) : Optional.empty();
			
			String[] teams = null;
			
			if(option.equalsIgnoreCase(teamsState)) {
				List<String> dbTeams = DB.jooq().selectDistinct(GUILDS_PLAYERS.TEAM).from(GUILDS_PLAYERS).where(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild())).fetch(GUILDS_PLAYERS.TEAM);
				
				if(dbTeams.isEmpty()) {
					warningMessage(event, TEAM_STATE_NOTEXIST, teamsState);
					return;
				}
				
				teams = dbTeams.toArray(new String[dbTeams.size()]);
				
			}else {
				
				teams = teamsState.split(",");
				
				for (String team : teams) {
					
					if(!DB.jooq().fetchExists(GUILDS_PLAYERS, GUILDS_PLAYERS.TEAM.eq(team.trim())
							.and(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild())))) {
						
						warningMessage(event, TEAM_STATE_NOTEXIST, team.trim());
						return;
					}
				}
			}
			
			teamDetails(event, isAll, guild, teams, number);
		});
	}

	private void teamDetails(SlashCommandEvent event, boolean isAll, GuildsRecord guild, String[] teams, Optional<Integer> number) {
		
		List<PlayerData> realTimeTeam = new ArrayList<>();
		
		List<String> playersTag = DB.jooq().select(GUILDS_PLAYERS.players().TAG)
										.from(GUILDS_PLAYERS)
										.where(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild())
									    .and(GUILDS_PLAYERS.TEAM.in(teams))).fetch(PLAYERS.TAG);
		
		
		realTimeTeam.addAll(getTrophiesLog().getAll(new HashSet<String>(playersTag)).values());
		
		List<PlayerData> startTimeTeam = new ArrayList<>(realTimeTeam);
		
		sort(startTimeTeam, startTrophiesComparator);
		sort(realTimeTeam, actualTrophiesComparator);
		
		
		boolean isBestOf = false;
		if(number.isPresent() && realTimeTeam.size() > number.get()) {
			realTimeTeam = realTimeTeam.subList(0, number.get());
			startTimeTeam = startTimeTeam.subList(0, number.get());
			isBestOf = true;
		}
		
		int lineCounter = 1;
		List<String> list = new ArrayList<>();
		
		StringJoiner msg = new StringJoiner("\n");
		
		for (PlayerData player : realTimeTeam) {
			
			if(lineCounter <= 20) {
				
				msg.add(getMsg(player));
				lineCounter++;

			}else {
				list.add(msg.toString());
				msg = new StringJoiner("\n");
				msg.add(getMsg(player));
				lineCounter = 2;
			}
		}
		
		list.add(msg.toString());
		
		
		String intro = isBestOf ? "BEST OF\n" : "STATE OF\n";
		String title = String.join("", intro, String.join(", ", Arrays.asList(teams)));
		
		if(title.length()>256) 
			title = title.substring(250) + "...";
		
		String footer = Config.getSlashMsg(guild, TEAM_STATE_INFO_LEGEND);
		
		EmbedBuilder overview = getOverview(title, realTimeTeam, startTimeTeam);
		
		if(isAll) {
			
			List<MessageEmbed> embeds = new ArrayList<>();
			overview.setFooter(footer);
			embeds.add(overview.build());
			
			for (String str : list) {
				EmbedBuilder eb = new EmbedBuilder();
				eb.setDescription(str);
				eb.setColor(Config.legendsColor);
				embeds.add(eb.build());
			}
			
			for (MessageEmbed page : embeds) 
				event.getHook().sendMessageEmbeds(page).queue();
		
		}else {
			
			List<Page> pages = new ArrayList<>();
			overview.setFooter(footer + "\n\n1/"+(list.size()+1)+" Pages");
			pages.add(new Page(overview.build()));
			
			int pageNumber = 2;
			for (String str : list) {
				
				EmbedBuilder eb = new EmbedBuilder();
				eb.setDescription(str);
				eb.setFooter(pageNumber+++"/"+(list.size()+1)+" Pages");
				eb.setColor(Config.legendsColor);
				pages.add(new Page(eb.build()));
			}

			event.getHook().sendMessageEmbeds((MessageEmbed)pages.get(0).getContent()).queue(s -> Pages.paginate(s, pages, false, Config.durataEmbed, TimeUnit.SECONDS, true));
			
		}
	}
	
	private EmbedBuilder getOverview(String title, List<PlayerData> realTimeTeam, List<PlayerData> startTimeTeam) {
		
		
		int // conteggio degli attacchi fatti e difese subite
			atkDone = 0, defDone = 0, 
			// totale players in team e calcolo del massimo di attacchi/difese effettuabili
			totalPlayers = realTimeTeam.size(),totalActions = totalPlayers*8,
			// calcolo delle percentuali di attacchi effettuati e difese subite
			atkPerc = 0, defPerc = 0;
		
		List<Integer> allCurrentTrophies = new ArrayList<>();
		
		for (PlayerData playerData : realTimeTeam) {
			
			atkDone += playerData.attacks();
			defDone += playerData.defenses();
			
			allCurrentTrophies.add(playerData.getTrophies());
			
		}
		
		List<Integer> allStartTrophies = new ArrayList<>();
		
		for (PlayerData playerData : startTimeTeam) 
			allStartTrophies.add(playerData.getStartTrophies());
		
		int startTrophies = trophiesCalculator(allStartTrophies);
		int currentTrophies = trophiesCalculator(allCurrentTrophies);
		
		atkPerc = (int)(((double)atkDone/totalActions) * 100);
		defPerc = (int)(((double)defDone/totalActions) * 100);
		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle(title);
		eb.setColor(Config.legendsColor);
		String description = 
				// totalStartTrophies
		String.join("",START, trophiesToEmoji(startTrophies), ATK_MIDDLE, trophiesToEmoji(atkPerc)," ",PERC,"\n",
		END, trophiesToEmoji(currentTrophies), DEF_MIDDLE, trophiesToEmoji(defPerc)," ",PERC,"\n",
		":busts_in_silhouette:",trophiesToEmoji(totalPlayers),
		"\n\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_");
		
		eb.setDescription(description);
		return eb;
	}
	
	private int trophiesCalculator(List<Integer> trophies) {
		int pos = 1, totalTrophies = 0;
		for (Integer points : trophies) {
			
			if(pos > 50) break;
			
			totalTrophies += 
					pos <= 10 ? (int) (points*0.5): 
					pos <= 20 ? (int) (points*0.25):
					pos <= 30 ? (int) (points*0.12):
					pos <= 40 ? (int) (points*0.10):
					pos <= 50 ? (int) (points*0.03):
					0;
			
			pos++;
		}
		
		return totalTrophies;
	}
	
	private String getMsg(PlayerData player) {
		return String.join("",trophiesToEmoji(player.getTrophies())
				,spAttacks(player.attacks())
				,spDefenses(player.defenses())
				,Config.getStateLink(player.getNickname(), player.getTag()));
	}
	
	private Comparator<PlayerData> actualTrophiesComparator = (prev, actual) -> prev.getTrophies() < actual.getTrophies() ? 1 :
		prev.getTrophies() == actual.getTrophies() ? 0 : -1;

	private Comparator<PlayerData> startTrophiesComparator = (prev, actual) -> prev.getStartTrophies() < actual.getStartTrophies() ? 1 :
		prev.getStartTrophies() == actual.getStartTrophies() ? 0 : -1;

	@Override
	public String helpKey() {
		return TEAM_STATE_DESCRIPTION;
	}

	@Override
	public String nameKey() {
		return TEAM_STATE_NAME;
	}

}
