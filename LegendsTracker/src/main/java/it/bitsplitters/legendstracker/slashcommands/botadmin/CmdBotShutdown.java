package it.bitsplitters.legendstracker.slashcommands.botadmin;

import static it.bitsplitters.util.BotUtil.okEmbed;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendstracker.Config;
import it.bitsplitters.legendstracker.service.DiscordLogWriterService;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdBotShutdown extends BotAdminSlashCommand {

	private static final Logger logger = LogManager.getLogger();
	
	@Override
	public String name(Locale locale) {
		return "bot-shutdown";
	}

	@Override
	public String description(Locale locale) {
		return "Avvia lo shutdown del bot";
	}
	
	@Override
	public List<OptionData> options(Locale locale) {
		return Collections.<OptionData>emptyList();
	}

	@Override
	public void run(SlashCommandEvent event) {

		logger.atInfo().log("Shutdown effettuato");
		
		okEmbed(event, "Shutdown effettuato");
		
		Config.battlefieldIsActive = false;
		
		DiscordLogWriterService.cleanAllMessages();
		
		Cache.ignite().close();

		DB.jooq().close();

		event.getJDA().shutdown();

	}

	@Override
	public boolean isEphemeral() {
		return true;
	}
}