package it.bitsplitters.legendstracker.slashcommands.botadmin;

import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;

import java.util.List;
import java.util.Locale;

import org.jooq.types.ULong;

import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendstracker.Config;
import it.bitsplitters.legendstracker.slashcommands.SlashBundle;
import static it.bitsplitters.util.BotUtil.*;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdGuildKick extends BotAdminSlashCommand {

	@Override
	public String name(Locale locale) {
		return "guild-kick";
	}

	@Override
	public String description(Locale locale) {
		return "Elimina il server, compreso tutti i giocatori tracciati";
	}

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(new OptionData(OptionType.STRING, "guild-id", "ID of the discord server to clean up", true));
	}

	@Override
	public void run(SlashCommandEvent event) {

		long guildID = Long.valueOf(event.getOption("guild-id").getAsString());
		
		Config.guildsManager.guildLeave(guildID);
		
		boolean result = DB.jooq().fetchExists(
			DB.jooq().selectFrom(GUILDS_PLAYERS).where(GUILDS_PLAYERS.ID_GUILD.eq(ULong.valueOf(guildID)))
		);
		
		okEmbed(event, "Players tracciati, tutti eliminati: "+result);
	}

	@Override
	public boolean isEphemeral() {
		return true;
	}
	
}
