package it.bitsplitters.legendstracker.slashcommands.global;

import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;
import static it.bitsplitters.legendsdata.jooq.models.tables.Players.PLAYERS;
import static it.bitsplitters.legendstracker.Config.getPlayerLink;
import static it.bitsplitters.legendstracker.Config.isntActiveOrConfiguredServer;
import static it.bitsplitters.legendstracker.service.MessageUtil.errorMessage;
import static it.bitsplitters.legendstracker.service.MessageUtil.okMessage;
import static it.bitsplitters.legendstracker.service.MessageUtil.warningMessage;
import static it.bitsplitters.legendstracker.service.PlayersLog.getTrophiesLog;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.BOTCROSS_MSG_DATAACCESS;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_CLEAN_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_CLEAN_LIMIT_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_CLEAN_LIMIT_KO_NOPLAYERS;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_CLEAN_LIMIT_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_CLEAN_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_CLEAN_OK;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_CLEAN_TAG_NOTEXIST;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_CLEAN_TEAM_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_CLEAN_TEAM_NAME;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.Record1;
import org.jooq.SelectConditionStep;
import org.jooq.exception.DataAccessException;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsPlayersRecord;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendsdata.jooq.models.tables.records.PlayersRecord;
import it.bitsplitters.legendsdata.models.PlayerData;
import it.bitsplitters.legendstracker.Config;
import it.bitsplitters.legendstracker.service.PlayersLog;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdGeneralClean extends GeneralSlashCommand {

	private static final Logger logger = LogManager.getLogger();
	
	@Override
	public PermissionLevel permissionLevel() {
		return PermissionLevel.RESERVED;
	}
	
	@Override
	public String helpKey() {
		return GENERAL_CLEAN_DESCRIPTION;
	}

	@Override
	public String nameKey() {
		return GENERAL_CLEAN_NAME;
	}

	@Override
	public boolean isEphemeral() {
		return true;
	}
	
	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(
				optRequired(OptionType.INTEGER, GENERAL_CLEAN_LIMIT_NAME, GENERAL_CLEAN_LIMIT_DESCRIPTION, locale),
				opt(OptionType.STRING, GENERAL_CLEAN_TEAM_NAME, GENERAL_CLEAN_TEAM_DESCRIPTION, locale)
				);
	}


	@Override
	public void run(SlashCommandEvent event) {
		
		
		Config.guildsManager.findRecord(event.getGuild().getIdLong()).ifPresent(guild -> {
			
			if(isntActiveOrConfiguredServer(event, guild)) 
				return;

			SelectConditionStep<Record1<String>> queryTags = DB.jooq().select(PLAYERS.TAG).
					from(PLAYERS).naturalJoin(GUILDS_PLAYERS).
					where(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild()));

			long limit = getOption(event, GENERAL_CLEAN_LIMIT_NAME).getAsLong();
			OptionMapping optTeam = getOption(event, GENERAL_CLEAN_TEAM_NAME);
			
			if(optTeam != null && !optTeam.getAsString().isEmpty()) 
				queryTags = queryTags.and(GUILDS_PLAYERS.TEAM.in(optTeam.getAsString().split(",")));
			
			List<String> tags = queryTags.fetch(PLAYERS.TAG);
			
			if(tags.isEmpty()) {
				warningMessage(event, GENERAL_CLEAN_LIMIT_KO_NOPLAYERS, limit+"");
				return;
			}
		
			Set<String> tagsToCheck = new HashSet<>();
			for (String playerTag : tags) 
				if(getTrophiesLog().containsKey(playerTag))
					tagsToCheck.add(playerTag);
			
			List<String> tagsToClean = new ArrayList<>();
			for(Map.Entry<String,PlayerData> player:getTrophiesLog().getAll(tagsToCheck).entrySet()) 
				if(player.getValue().getTrophies() <= limit) 
						tagsToClean.add(player.getKey());
			
			
			List<String> playersLink = new ArrayList<>();
			
			int length = 1;
			for (String tag : tagsToClean) {
				
				String link = removePlayer(event, guild, tag);
			
				if(link != null && length ++ <= 20)
					playersLink.add(link);
			
			}
			
			if(length > 20) playersLink.add("...");
			
			okMessage(event, GENERAL_CLEAN_OK, String.join(", ", playersLink));
			
		});
		
	}
	
	private String removePlayer(SlashCommandEvent event, GuildsRecord guild, String tag) {
		
		String linkedPlayer = null;
		
		try {
			
			tag = tag.toUpperCase();
			
			PlayersRecord player = DB.jooq().selectFrom(PLAYERS).where(PLAYERS.TAG.eq(tag)).fetchOne();

			if(player == null) {
				
				warningMessage(event, GENERAL_CLEAN_TAG_NOTEXIST, tag);
				
				return null;
		
			}
			
			linkedPlayer = getPlayerLink(player.getNickname(), tag);
			
			// Verifico che il tracciamento richiesto da cancellare sia effettivamente presente
			Optional<GuildsPlayersRecord> optGuildsPlayersRecord = DB.jooq().
					selectFrom(GUILDS_PLAYERS)
					    .where(GUILDS_PLAYERS.ID_PLAYER.eq(player.getIdPlayer())
					      .and(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild()))).fetchOptional();
				
			if(!optGuildsPlayersRecord.isPresent()) {
				warningMessage(event, GENERAL_CLEAN_TAG_NOTEXIST, linkedPlayer);
				return null;
			}
		
			// elimino il tracking dalla mappa trackingLog
			PlayersLog.removePlayer(player.getTag(), guild.getIdGuild().longValue());

			// elimino il tracking dalla tabella GUILDS PLAYERS
			optGuildsPlayersRecord.get().delete();
			
			// Controllo che al player siano agganciate altre guild, altrimenti lo cancello dalla tabella PLAYERS
			if(DB.jooq().fetchCount(GUILDS_PLAYERS, GUILDS_PLAYERS.ID_PLAYER.eq(player.getIdPlayer())) <= 0)
				player.delete();
			
			return linkedPlayer;
			
		} catch (DataAccessException e) {
			errorMessage(event, BOTCROSS_MSG_DATAACCESS);
			logger.atError().log("Problemi di connessione al server Supercell, contattare l'admin del bot.");
		}
		
		return null;
	}
}
