package it.bitsplitters.legendstracker.slashcommands.team;

import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;
import static it.bitsplitters.legendsdata.jooq.models.tables.Players.PLAYERS;
import static it.bitsplitters.legendstracker.Config.getPlayerLink;
import static it.bitsplitters.legendstracker.Config.isntActiveOrConfiguredServer;
import static it.bitsplitters.legendstracker.service.MessageUtil.listToString;
import static it.bitsplitters.legendstracker.service.MessageUtil.okMessage;
import static it.bitsplitters.legendstracker.service.MessageUtil.warningMessage;
import static it.bitsplitters.legendstracker.service.PlayersLog.addPlayer;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERIC_ACCOUNTS_FULL;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERIC_ACCOUNTS_SPACE_INSUFFICIENT;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_LINK_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_LINK_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_LINK_NEW_NAME_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_LINK_NEW_NAME_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_LINK_NUM_TO_LOAD_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_LINK_NUM_TO_LOAD_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_LINK_OK;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_LINK_TAG_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_LINK_TAG_EXIST;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_LINK_TAG_EXIST_INTERNAL;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_LINK_TAG_NAME;
import static it.bitsplitters.legendstracker.slashcommands.service.ClanService.findAPIClan;
import static it.bitsplitters.util.BotUtil.codeEmbed;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.ignite.IgniteCache;

import it.bitsplitters.clashapi.ClanMember;
import it.bitsplitters.clashapi.Player;
import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.legends.batch.service.PlayersService;
import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsPlayersRecord;
import it.bitsplitters.legendsdata.jooq.models.tables.records.PlayersRecord;
import it.bitsplitters.legendsdata.models.GuildChannels;
import it.bitsplitters.legendsdata.models.PlayerData;
import it.bitsplitters.legendsdata.service.LoadBalancer;
import it.bitsplitters.legendstracker.Config;
import it.bitsplitters.legendstracker.service.LegendsAdapter;
import it.bitsplitters.legendstracker.service.MessageUtil;
import it.bitsplitters.legendstracker.service.PlayersInfo;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdTeamLink extends TeamSlashComand {

	@Override public PermissionLevel permissionLevel() {
		return PermissionLevel.RESERVED;
	}
	
	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(
				optRequired(OptionType.STRING, TEAM_LINK_TAG_NAME, TEAM_LINK_TAG_DESCRIPTION, locale),
				opt(OptionType.STRING, TEAM_LINK_NEW_NAME_NAME, TEAM_LINK_NEW_NAME_DESCRIPTION, locale),
				opt(OptionType.INTEGER, TEAM_LINK_NUM_TO_LOAD_NAME, TEAM_LINK_NUM_TO_LOAD_DESCRIPTION, locale)
				);
	}

	@Override
	public boolean isEphemeral() {
		return false;
	}

	@Override
	public void run(SlashCommandEvent event) {
		Config.guildsManager.findRecord(event.getGuild().getIdLong()).ifPresent(guild -> {
			
			if(isntActiveOrConfiguredServer(event, guild)) 
				return;

			int followedPlayers = DB.jooq().fetchCount(GUILDS_PLAYERS,GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild()));
			
			// Se eccedo il limite di players tracciabili pagato
			if(followedPlayers >= guild.getProfilesMax()) {
				warningMessage(event, GENERIC_ACCOUNTS_FULL);
				return;
			}

			String clanTag = getOption(event, TEAM_LINK_TAG_NAME).getAsString().toUpperCase();
			OptionMapping optNewName = getOption(event, TEAM_LINK_NEW_NAME_NAME);
			String newName = optNewName != null ? optNewName.getAsString() : null;
			
			OptionMapping optPlayersToLoad = getOption(event, TEAM_LINK_NUM_TO_LOAD_NAME);
			Integer playersToLoad = optPlayersToLoad != null ? Long.valueOf(optPlayersToLoad.getAsLong()).intValue() : 50;
			
			findAPIClan(event, clanTag, (clan) -> {
				
				List<ClanMember> members = clan.getMembers();

				List<String> playersToAdd = new ArrayList<>();
				List<String> tagsAdded = new ArrayList<>();
				List<String> tagListLinkedExternal = new ArrayList<>();
				List<String> playersListLinkedExternal = new ArrayList<>();
				List<String> playersAlreadyLinkedTargetTeam = new ArrayList<>();
				
				int playersCount = 1;
				String clanName = newName == null ? clan.getName().replace(" ", "_") : newName;
				for (ClanMember clanMember : members) {
					
					if(playersCount > playersToLoad) break;
					
					
					PlayersRecord player = DB.jooq().select(PLAYERS.fields())
					   .from(PLAYERS)
					   .naturalJoin(GUILDS_PLAYERS)
	                   .where(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild())
	                   .and(PLAYERS.TAG.eq(clanMember.getTag()))).fetchOneInto(PLAYERS);
					
					// Se non presente lo conto
					if(player == null) 
						playersToAdd.add(clanMember.getTag());

					else {
						
						String playerLink = getPlayerLink(player.getNickname(), player.getTag());
						
						String playerTeam = DB.jooq().selectFrom(GUILDS_PLAYERS)
						      .where(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild())
				              .and(GUILDS_PLAYERS.ID_PLAYER.eq(player.get(PLAYERS.ID_PLAYER)))).fetchOne().getTeam();
						
						if(clanName.equals(playerTeam)) {
							playersAlreadyLinkedTargetTeam.add(playerLink);
						}else {
							tagListLinkedExternal.add(clanMember.getTag());
							playersListLinkedExternal.add(playerLink);
						}
					}
					
					playersCount ++;
				}
					
				int remainingSpace = guild.getProfilesMax() - followedPlayers;
				
				if(remainingSpace < playersToAdd.size()) {
					warningMessage(event, GENERIC_ACCOUNTS_SPACE_INSUFFICIENT, remainingSpace+"", playersToAdd.size()+"");
					return;
				}
				
				if(!tagListLinkedExternal.isEmpty()) {
					
					String playersList = listToString(playersListLinkedExternal, ", ", 20);
					
					warningMessage(event, TEAM_LINK_TAG_EXIST, playersList, clanName);

					String tagList = listToString(tagListLinkedExternal, ",", 100);
					String command = String.join(" ","/player move", tagList, clanName);

					codeEmbed(event, command);
				}
				
				if(!playersAlreadyLinkedTargetTeam.isEmpty()) {
					String list = listToString(playersAlreadyLinkedTargetTeam, ", ", 20);
					warningMessage(event, TEAM_LINK_TAG_EXIST_INTERNAL, list, clanName);
				}
				
				List<String> newPlayersToAdd = new ArrayList<>();
				IgniteCache<String, PlayerData> cache = Cache.playersData();
				
				for (String playerTag : playersToAdd) {
					
					//INSERT IN PLAYERS
					PlayersRecord player = DB.jooq().selectFrom(PLAYERS).where(PLAYERS.TAG.eq(playerTag)).fetchOne();
					
					if(player == null) {
						newPlayersToAdd.add(playerTag);
						continue;
					}

					tagsAdded.add(getPlayerLink(player.getNickname(), player.getTag()));
					
					//INSERT IN GUILDS_PLAYERS
					DB.jooq().executeInsert(new GuildsPlayersRecord(guild.getIdGuild(), player.getIdPlayer(), clanName));
					
					PlayerData playerData = cache.get(playerTag);
					if(playerData == null)
						playerData = PlayersService.getPlayer(playerTag);
					
					//LOADING DATA IN CACHE
					addPlayer(playerData, new GuildChannels(guild));
					
				}
				
				@SuppressWarnings("unchecked")
				CompletableFuture<Player>[] cfPlayers = new CompletableFuture[newPlayersToAdd.size()];
				int index = 0;
				
				for (String playerTag : newPlayersToAdd) {
					
					cfPlayers[index++] = PlayersInfo.findAPIAccount(event, playerTag, (e, cocPlayer) -> {
						
						tagsAdded.add(getPlayerLink(cocPlayer.getName(),cocPlayer.getTag()));
						
						//INSERT IN PLAYERS
						PlayersRecord player = DB.jooq().selectFrom(PLAYERS).where(PLAYERS.TAG.eq(playerTag)).fetchOne();
						
						if(player == null) {
							player = DB.jooq().newRecord(PLAYERS);
							player.setNickname(cocPlayer.getName());
							player.setTag(cocPlayer.getTag());
							player.store();
						}
						
						//INSERT IN GUILDS_PLAYERS
						DB.jooq().executeInsert(new GuildsPlayersRecord(guild.getIdGuild(), player.getIdPlayer(), clanName));
						
						//LOADING DATA IN CACHE
						addPlayer(LegendsAdapter.getPlayer(cocPlayer), new GuildChannels(guild));
						
					});
				}
				
				try {
					CompletableFuture.allOf(cfPlayers).get();
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
					
				if(!tagsAdded.isEmpty()) {
					String list = MessageUtil.listToString(tagsAdded,", ", 20);
					okMessage(event, TEAM_LINK_OK, clanName, list);
				}
				
			});
			LoadBalancer.loadRebalance();
		});
	}

	@Override
	public String helpKey() {
		return TEAM_LINK_DESCRIPTION;
	}

	@Override
	public String nameKey() {
		return TEAM_LINK_NAME;
	}

}
