package it.bitsplitters.legendstracker.slashcommands.botadmin;

import static it.bitsplitters.legendstracker.Config.guildsManager;
import static it.bitsplitters.util.BotUtil.okEmbed;
import static it.bitsplitters.util.BotUtil.warningEmbed;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendstracker.service.GuildService;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdGuildExtend extends BotAdminSlashCommand {

	private static final Logger logger = LogManager.getLogger();
	
	@Override
	public String name(Locale locale) {
		return "guild-extend";
	}

	@Override
	public String description(Locale locale) {
		return "Estende l'abbonamento della guild";
	}

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(new OptionData(OptionType.STRING, "guild-id", "ID del server", true),
				       new OptionData(OptionType.STRING, "end-date", "Data col pattern dd-MM-yy, oppure un numero che specifica i mesi", true));
	}

	@Override
	public void run(SlashCommandEvent event) {

		long guildID = Long.valueOf(event.getOption("guild-id").getAsString());
		
		Optional<GuildsRecord> optRecord = guildsManager.findRecord(guildID);
		if(!optRecord.isPresent()) return;
		
		GuildsRecord guild = optRecord.get();
		
		String endSubs = event.getOption("end-date").getAsString();
		
		try {
		
			int months = Integer.valueOf(endSubs);
			
			LocalDate payEnd = guild.getPayEnd();
			LocalDate now = DB.localDateNow();
			boolean toLoad = false;
			
			if(payEnd == null || payEnd.isBefore(now)) {
				payEnd = now;
				toLoad = true;
			}
			
			guild.setPayEnd(payEnd.plusMonths(months));
			
			DB.jooq().executeUpdate(guild);
			
			if(toLoad)
				GuildService.loadGuild(guild.getIdGuild().longValue());
			
			okEmbed(event, "Esteso di "+months+" mesi l'abbonamento del server discord "+guildID);
		
			logger.atInfo().log("Esteso di {} mesi l'abbonamento del server discord {}", months, guildID);

		}catch (Exception e) {

			try {
				
				LocalDate payEnd = LocalDate.parse(endSubs, DateTimeFormatter.ofPattern("dd-MM-yy"));
				
				guild.setPayEnd(payEnd);
				
				DB.jooq().executeUpdate(guild);
				
				okEmbed(event, "Esteso fino al giorno "+endSubs+" l'abbonamento del server discord "+guildID);
				
				logger.atInfo().log("Esteso fino al giorno {} l'abbonamento del server discord {}", endSubs, guildID);

			}catch (DateTimeParseException e2) {
				
				warningEmbed(event, "formato data non rispettato: "+endSubs+", pattern da usare: dd-MM-yy");
				
				logger.atInfo().log("formato data non rispettato: {}, pattern da usare: dd-MM-yy", endSubs);
				
			}
			
		}
	}
	
	@Override
	public boolean isEphemeral() {
		return true;
	}
}