package it.bitsplitters.legendstracker.slashcommands.guild;

import static it.bitsplitters.legendsdata.jooq.models.tables.Guilds.GUILDS;
import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;
import static it.bitsplitters.legendsdata.jooq.models.tables.Players.PLAYERS;
import static it.bitsplitters.legendstracker.Bot.jda;
import static it.bitsplitters.legendstracker.Config.isValidPayer;
import static it.bitsplitters.legendstracker.service.MessageUtil.okMessage;
import static it.bitsplitters.legendstracker.service.MessageUtil.warningMessage;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GUILD_CHANNELLOG_CHANNEL_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GUILD_CHANNELLOG_CHANNEL_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GUILD_CHANNELLOG_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GUILD_CHANNELLOG_KO_CANTTALK;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GUILD_CHANNELLOG_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GUILD_CHANNELLOG_OK;
import static net.dv8tion.jda.api.interactions.commands.OptionType.CHANNEL;

import java.util.List;
import java.util.Locale;

import org.jooq.types.ULong;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.models.GuildChannels;
import it.bitsplitters.legendsdata.models.PlayerData;
import it.bitsplitters.legendstracker.Config;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdGuildChannelLog extends GuildSlashCommand {

	@Override
	public PermissionLevel permissionLevel() {
		return PermissionLevel.ADMIN;
	}
	
	@Override
	public String nameKey() {
		return GUILD_CHANNELLOG_NAME;
	}
	
	@Override
	public String helpKey() {
		return GUILD_CHANNELLOG_DESCRIPTION;
	}

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(opt(CHANNEL, GUILD_CHANNELLOG_CHANNEL_NAME, GUILD_CHANNELLOG_CHANNEL_DESCRIPTION, locale));
	}

	@Override
	public void run(SlashCommandEvent event) {
	
		Config.guildsManager.findRecord(event.getGuild().getIdLong()).ifPresent(guild -> {
		
			if(!isValidPayer(event, guild)) 
				return;
			
			OptionMapping optChannel = getOption(event, GUILD_CHANNELLOG_CHANNEL_NAME);
			
			long channelLogID = optChannel == null ? event.getGuildChannel().getIdLong() : optChannel.getAsGuildChannel().getIdLong();
			
			TextChannel channelLog = jda.getTextChannelById(channelLogID);
			
			if(!channelLog.canTalk()) {
				warningMessage(event, GUILD_CHANNELLOG_KO_CANTTALK, channelLog.getName());
				return;
			}
			
			guild.setIdChannelLog(ULong.valueOf(channelLogID));
			guild.store();
			
			for (String trackedPlayer : DB.jooq().select(PLAYERS.TAG)
					.from(PLAYERS)
					.naturalJoin(GUILDS_PLAYERS)
					.naturalJoin(GUILDS)
					.where(GUILDS.ID_GUILD.eq(guild.getIdGuild())).fetch(PLAYERS.TAG)) {
				
				PlayerData player = Cache.playersData().get(trackedPlayer);
				
				if(player == null) return;
				
				List<GuildChannels> cachedChannelsID = player.getGuildChannels();
				
				int indexOfGuild = cachedChannelsID.indexOf(new GuildChannels(guild.getIdGuild().longValue()));
				
				if(indexOfGuild == -1) continue;
				
				cachedChannelsID.get(indexOfGuild).setLogChannel(channelLogID);
				
				Cache.playersData().put(trackedPlayer, player);
				
			}
			
			okMessage(event, GUILD_CHANNELLOG_OK);
			
		});
		
	}
	
	@Override
	public boolean isEphemeral() {
		return true;
	}
}