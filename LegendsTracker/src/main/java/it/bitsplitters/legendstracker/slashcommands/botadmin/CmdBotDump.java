package it.bitsplitters.legendstracker.slashcommands.botadmin;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdBotDump extends BotAdminSlashCommand {

	@Override
	public String name(Locale locale) {
		return "bot-dump";
	}

	@Override
	public String description(Locale locale) {
		return "Effettua il dump del database";
	}

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of();
	}

	@Override
	public void run(SlashCommandEvent event) {

		try {
			String dump = "mysqldump -u bot --databases legendstracker > ./dump.sql";
			String[] cmdarray = {"/bin/sh","-c", dump};
			Process p = Runtime.getRuntime().exec(cmdarray);
			p.waitFor();
			
			event.getHook().sendFile(new File("./dump.sql")).queue();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		
	}

	@Override
	public boolean isEphemeral() {
		return false;
	}

}
