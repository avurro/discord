package it.bitsplitters.legendstracker.slashcommands.global;

import java.util.List;

import it.bitsplitters.legendstracker.slashcommands.LegendsTrackerSlashCommand;
import it.bitsplitters.legendstracker.slashcommands.SlashBundle;

public abstract class GeneralSlashCommand extends LegendsTrackerSlashCommand {

	@Override
	public List<String> pathKeys() {
		return List.of(SlashBundle.GENERAL_NAME);
	}

}