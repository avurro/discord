package it.bitsplitters.legendstracker.slashcommands.team;

import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;
import static it.bitsplitters.legendsdata.jooq.models.tables.Players.PLAYERS;
import static it.bitsplitters.legendstracker.Config.isntActiveOrConfiguredServer;
import static it.bitsplitters.legendstracker.service.MessageUtil.okMessage;
import static it.bitsplitters.legendstracker.service.MessageUtil.warningMessage;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERIC_TEAM_NOTEXIST;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_UNLINK_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_UNLINK_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_UNLINK_NAME_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_UNLINK_NAME_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TEAM_UNLINK_OK;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.jooq.Record1;
import org.jooq.Result;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.service.LoadBalancer;
import it.bitsplitters.legendstracker.Config;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdTeamUnlink extends TeamSlashComand {

	@Override public PermissionLevel permissionLevel() {
		return PermissionLevel.RESERVED;
	}
	
	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(
				optRequired(OptionType.STRING, TEAM_UNLINK_NAME_NAME, TEAM_UNLINK_NAME_DESCRIPTION, locale)
				);
	}

	@Override
	public boolean isEphemeral() {
		return false;
	}

	@Override
	public void run(SlashCommandEvent event) {
		
		Config.guildsManager.findRecord(event.getGuild().getIdLong()).ifPresent(guild -> {
				
			if(isntActiveOrConfiguredServer(event, guild)) 
				return;

			String team = getOption(event, TEAM_UNLINK_NAME_NAME).getAsString().toUpperCase();
			
			Result<Record1<String>> playersTag = DB.jooq().select(GUILDS_PLAYERS.players().TAG)
		              .from(GUILDS_PLAYERS)
		      		  .where(GUILDS_PLAYERS.TEAM.eq(team)
		    		  .and(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild()))).fetch();
			
			if(playersTag.size() <= 0) {
				warningMessage(event, GENERIC_TEAM_NOTEXIST);
				return;
			}
			
			List<String> playersRemoved = new ArrayList<>();
			for (Record1<String> player : playersTag) {
				String playerName = removePlayer(event, guild, player.get(PLAYERS.TAG));
				
				if(playerName != null)
					playersRemoved.add(playerName);
			}
			
			if(!playersRemoved.isEmpty()) 
				okMessage(event, TEAM_UNLINK_OK, team);
			
			LoadBalancer.loadRebalance();
		});
	}

	@Override
	public String helpKey() {
		return TEAM_UNLINK_DESCRIPTION;
	}

	@Override
	public String nameKey() {
		return TEAM_UNLINK_NAME;
	}

}
