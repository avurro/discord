package it.bitsplitters.legendstracker.slashcommands.botadmin;

import static it.bitsplitters.legendstracker.Config.guildsManager;
import static it.bitsplitters.util.BotUtil.okEmbed;

import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdGuildAccounts extends BotAdminSlashCommand {

	private static final Logger logger = LogManager.getLogger();
	
	@Override
	public String name(Locale locale) {
		return "guild-accounts";
	}

	@Override
	public String description(Locale locale) {
		return "modifica il numero massimo di accounts tracciabili";
	}

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(new OptionData(OptionType.STRING, "guild-id", "ID del server", true),
				new OptionData(OptionType.INTEGER, "accounts", "numero di accounts limite per questa guild", true));
	}

	@Override
	public void run(SlashCommandEvent event) {

		long guildID = Long.valueOf(event.getOption("guild-id").getAsString());
		short accounts = Long.valueOf(event.getOption("accounts").getAsLong()).shortValue();
		
		guildsManager.findRecord(guildID).ifPresent(record -> {
			
			record.setProfilesMax(accounts);
			record.store();
			
			logger.atInfo().log("Modificato il numero massimo di profili tracciabili a {} per il server: {}", accounts, guildID);
			okEmbed(event, "Modificato il numero massimo di profili tracciabili a "+accounts+" per il server: "+guildID);
		});
	}
	
	@Override
	public boolean isEphemeral() {
		return true;
	}

}
