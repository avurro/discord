package it.bitsplitters.legendstracker.slashcommands.trophieschallenge;

import static it.bitsplitters.legendstracker.Config.isntActiveOrConfiguredServer;
import static it.bitsplitters.legendstracker.service.MessageUtil.okMessage;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_CA_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_CA_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_CA_TEAM_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_CA_TEAM_NAME;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendsdata.models.PlayerData;
import it.bitsplitters.legendstracker.Config;
import it.bitsplitters.legendstracker.service.TeamService;
import it.bitsplitters.legendstracker.slashcommands.SlashBundle;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdTCCheckAtks extends TCSlashComand {

	@Override
	public PermissionLevel permissionLevel() {
		return PermissionLevel.BOT_ADMIN;
	}
	
	@Override
	public String nameKey() {
		return TC_CA_NAME;
	}
	
	
	@Override
	public String helpKey() {
		return TC_CA_DESCRIPTION;
	}

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(optRequired(OptionType.STRING, TC_CA_TEAM_NAME, TC_CA_TEAM_DESCRIPTION, locale));
	}

	@Override
	public void run(SlashCommandEvent event) {

		Config.guildsManager.findRecord(event.getGuild().getIdLong()).ifPresent(guild -> {
			
			if(isntActiveOrConfiguredServer(event, guild)) 
				return;

			String team = event.getOption(TC_CA_TEAM_NAME).getAsString();
			
			Optional<GuildsRecord> optGuild = Config.guildsManager.findRecord(event.getGuild().getIdLong());
			if(!optGuild.isPresent()) return;
			
			for(PlayerData player: TeamService.getTeam(optGuild.get(), team)) {
				
				Integer correctionNumAtks = getNumAtksCorrection().get(player.getTag());
				correctionNumAtks = correctionNumAtks == null ? 0 : correctionNumAtks;
				
				Integer numAtks =  player.attacks() + correctionNumAtks;
				
				if(numAtks > 7)
					getBanCorrection().add(player.getTag());
			}
		
			okMessage(event, SlashBundle.TROPHIES_CHALLENGE_OK_GLOBAL_CHECK);
		});

	}

	@Override
	public boolean isEphemeral() {
		return true;
	}

}