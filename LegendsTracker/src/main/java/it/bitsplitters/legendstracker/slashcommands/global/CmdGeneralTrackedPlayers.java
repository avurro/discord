package it.bitsplitters.legendstracker.slashcommands.global;

import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;
import static it.bitsplitters.legendsdata.jooq.models.tables.Players.PLAYERS;
import static it.bitsplitters.legendstracker.Config.BLANK;
import static it.bitsplitters.legendstracker.service.MessageUtil.warningMessage;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_DETAILS_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_DETAILS_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_DETAILS_TAGDETAILS;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_DETAILS_TAGDETAILS_TEAM;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_DETAILS_TEAM_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_DETAILS_TEAM_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_DETAILS_TYPE_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERAL_DETAILS_TYPE_NAME;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;

import org.jooq.Record3;
import org.jooq.Result;
import org.jooq.SelectConditionStep;

import com.github.ygimenez.method.Pages;
import com.github.ygimenez.model.Page;
import com.google.common.collect.Lists;

import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendstracker.Config;
import it.bitsplitters.legendstracker.slashcommands.SlashBundle;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
public class CmdGeneralTrackedPlayers extends GeneralSlashCommand {

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(
				optRequired(OptionType.STRING, GENERAL_DETAILS_TYPE_NAME, GENERAL_DETAILS_TYPE_DESCRIPTION, locale)
				.addChoice("compact", "compact")
				.addChoice("wide", "wide"),
				opt(OptionType.STRING, GENERAL_DETAILS_TEAM_NAME, GENERAL_DETAILS_TEAM_DESCRIPTION, locale)
				);
	}

	@Override
	public boolean isEphemeral() {
		return false;
	}

	@Override
	public void run(SlashCommandEvent event) {
		boolean isAll = getOption(event, GENERAL_DETAILS_TYPE_NAME).getAsString().equals("wide") ? true : false;
		OptionMapping optTeam = getOption(event, GENERAL_DETAILS_TEAM_NAME);
		
		Optional<GuildsRecord> optGuild = Config.guildsManager.findRecord(event.getGuild().getIdLong());
		
		if(!optGuild.isPresent()) return;
		
		GuildsRecord guild = optGuild.get();

		SelectConditionStep<Record3<String, String, String>> query = 
			DB.jooq().select(GUILDS_PLAYERS.players().TAG,
			                 GUILDS_PLAYERS.players().NICKNAME, 
			                 GUILDS_PLAYERS.TEAM)
				       .from(GUILDS_PLAYERS)
				      .where(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild()));
		
		boolean isTeam = false;
		if(optTeam != null && !optTeam.getAsString().isEmpty()) {
			isTeam = true;
			String[] teams = optTeam.getAsString().split(",");
			for(String team: teams) {
				if(!team.startsWith("#")) {
					warningMessage(event, SlashBundle.GENERAL_DETAILS_TEAM_KO_BADTAG, team);
					return;
				}
			}
			
			query = query.and(GUILDS_PLAYERS.TEAM.in(teams));
		}
		
		
		Result<Record3<String, String, String>> records = query.orderBy(GUILDS_PLAYERS.TEAM).fetch();
		
		if(records.isEmpty()) {
			warningMessage(event, isTeam ? GENERAL_DETAILS_TAGDETAILS_TEAM : GENERAL_DETAILS_TAGDETAILS);
			return;
		}
		
		Map<String, List<String>> values = new HashMap<>(records.size());
		
		for (Record3<String, String, String> record : records) {
			
			String player = Config.getFakeLink(record.get(PLAYERS.TAG), record.get(PLAYERS.TAG), record.get(PLAYERS.NICKNAME));
			String team = record.get(GUILDS_PLAYERS.TEAM);

			if(values.containsKey(team))
				values.get(team).add(player);
			else {
				List<String> players = new ArrayList<>();
				players.add(player);
				values.put(team, players);
			}
		}
		
		List<String> pagesContent = new ArrayList<>();
		StringJoiner sj = new StringJoiner("\n");
		int  counter = 0;
		for (String team : values.keySet()) {
			sj.add(BLANK).add("***"+team+"*** *("+values.get(team).size()+")*");
			counter+=2;
			
			for (String player : values.get(team)) {
				sj.add(player);
				
				if(counter <= 40) {
					counter++;
				}else {
					pagesContent.add(sj.toString());
					sj = new StringJoiner("\n").add(BLANK);
					counter = 0;
				}
			}
		}
		
		if(sj.length() > 0 && !emptySJ.equals(sj.toString()))
			pagesContent.add(sj.toString());
		
		List<MessageEmbed> msgs = new ArrayList<>();
		
		int pageCount = 1;
		for (String pageContent : pagesContent) 
			msgs.add(makeEmbed(isAll, "Details", pageContent, pageCount++, pagesContent.size()).build());
		
		if(!isAll) {
			
			List<Page> pages = new ArrayList<>();
			for(MessageEmbed message: msgs)
				pages.add(new Page(message));
			
			event.getHook().sendMessageEmbeds((MessageEmbed) pages.get(0).getContent()).queue(s -> {
				Pages.paginate(s, pages, false, Config.durataEmbed, TimeUnit.SECONDS, true);
			});	
		
		}else 
			for (List<MessageEmbed> messageEmbed : Lists.partition(msgs, 10)) 
				event.getHook().sendMessageEmbeds(messageEmbed).queue();
			
		
			
	}
	
	private EmbedBuilder makeEmbed(boolean isAll, String category, String content, int numberPage, int totalPage) {
		
		EmbedBuilder eb = new EmbedBuilder();
		eb.setDescription(content);

		if(numberPage == 1 || !isAll)
			eb.setTitle(category);
		
		if(!isAll) 
			eb.setFooter(numberPage + "/" + totalPage + " Pages");
		
		eb.setColor(Config.legendsColor);
		
		return eb;
	}
	
	private static final String emptySJ = new StringJoiner("\n").add(BLANK).toString();


	@Override
	public String helpKey() {
		return GENERAL_DETAILS_DESCRIPTION;
	}

	@Override
	public String nameKey() {
		return GENERAL_DETAILS_NAME;
	}

}
