package it.bitsplitters.legendstracker.slashcommands.guild;

import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;
import static it.bitsplitters.legendstracker.service.MessageUtil.infoMessage;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GUILD_STATUS_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GUILD_STATUS_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GUILD_STATUS_OK;
import static java.time.format.DateTimeFormatter.ofPattern;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendstracker.Bot;
import it.bitsplitters.legendstracker.Config;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdGuildInfo extends GuildSlashCommand {

	private final String ND = "N/D";
	
	@Override
	public String nameKey() {
		return GUILD_STATUS_NAME;
	}
	
	@Override
	public String helpKey() {
		return GUILD_STATUS_DESCRIPTION;
	}

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of();
	}

	@Override
	public void run(SlashCommandEvent event) {
		
		
		Optional<GuildsRecord> optGuild = Config.guildsManager.findRecord(event.getGuild().getIdLong());
		
		if(!optGuild.isPresent()) 
			return;
		
		GuildsRecord guild = optGuild.get();
		
		String endDate = guild.getPayEnd() == null ? ND : guild.getPayEnd().format(ofPattern("dd-MMM-yyyy", Config.defaultLocale));
		String maxProfiles = guild.getProfilesMax().toString();
		Integer trackedProfiles = (Integer) DB.jooq().selectCount().from(GUILDS_PLAYERS).where(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild())).fetchOne(0);
		long roleID = guild.getIdRoleReserved().longValue();
		String role = roleID == 0 ? "N/D" : event.getGuild().getRoleById(roleID).getAsMention();
		String logChannel = guild.getIdChannelLog() == null ? ND : Bot.jda.getTextChannelById(guild.getIdChannelLog().longValue()).getName();
		String comsChannel = guild.getIdChannelLog() == null ? ND : Bot.jda.getTextChannelById(guild.getIdChannelComs().longValue()).getName();
		infoMessage(event, GUILD_STATUS_OK, endDate, maxProfiles, trackedProfiles+"", role, logChannel, comsChannel);

	}
	
	@Override
	public boolean isEphemeral() {
		return true;
	}
}
