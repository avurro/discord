package it.bitsplitters.legendstracker.slashcommands.global;

import java.awt.Color;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;

import org.jooq.types.ULong;

import com.github.ygimenez.method.Pages;
import com.github.ygimenez.model.Page;

import it.bitsplitters.legendstracker.Config;
import it.bitsplitters.legendstracker.slashcommands.LegendsTrackerSlashCommand;
import it.bitsplitters.legendstracker.slashcommands.SlashBundle;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Emoji;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.MessageEmbed.Field;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdGeneralHelp extends LegendsTrackerSlashCommand {

	private static final Color embedColor = new Color(159, 41, 247);
	
	private static final String COMMAND_ICON = "<:cmd:796549774491779154>";
	private static final String MENU_ICON = "<:index:926415579009650728>";
	private static final String USER_ICON = "<:user:926415579101945876>";
	private static final String TEAM_ICON = "<:team:926415579202592799>";
	private static final String GLOBAL_ICON = "<:global:926415579072569344>";
	private static final String SERVER_ICON = "<:server:926415578669940768>";
	private static final String INFO_ICON = "<:info:827590806293446737>";

	private static final String MENU_TITLE = MENU_ICON	  +" "+"**Menu**";
	private static final String USER_TITLE = USER_ICON	  +" "+"**Player commands**";
	private static final String TEAM_TITLE = TEAM_ICON	  +" "+"**Team commands**";
	private static final String GLOBAL_TITLE = GLOBAL_ICON+" "+"**General commands**";
	private static final String SERVER_TITLE = SERVER_ICON+" "+"**Guild settings**";
	private static final String INFO_TITLE = INFO_ICON	  +" "+"**Information**";

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of();
	}

	@Override
	public boolean isEphemeral() {
		return false;
	}

	private MessageEmbed getIndex() {
		EmbedBuilder builder = new EmbedBuilder();
		builder.setTitle("__LEGENDS                     __");
		
		StringJoiner indexContent = new StringJoiner("\n\n");
		indexContent.add(MENU_TITLE);
		indexContent.add(USER_TITLE);
		indexContent.add(TEAM_TITLE);
		indexContent.add(GLOBAL_TITLE);
		indexContent.add(SERVER_TITLE);
		indexContent.add(INFO_TITLE);
		
		builder.setDescription(indexContent.toString());
		builder.setColor(embedColor);
		return builder.build();
	}
	
	private MessageEmbed getInfo(SlashCommandEvent event) {
		EmbedBuilder builder = new EmbedBuilder();
		builder.setTitle(INFO_TITLE);
		
		
		builder.setDescription("If you want more information or want to subscribe:\n[Legends server discord](https://discord.gg/EHj24Kh97T)\n" + 
				"\n\nThe bot is **private**. In order to use it you need to contact the Legends bot admin on discord server. The bot is released via subscription.\n" + 
				"\n\nThe commands have three levels of visibility:\n:small_orange_diamond: **everyone**, accessible to all\n\n" + 
				":small_orange_diamond: **reserved**, commands reserved for the role specified by the administrators (accessible only to administrators in absence of a defined role)\n\n" + 
				":small_orange_diamond: **admin**, commands accessible only to server administrators.\n" + 
				"\n\nLegends Server: "+event.getJDA().getGuildCache().size());
		builder.setColor(embedColor);
		return builder.build();
	}
	
	private MessageEmbed getEmbed(String title, MessageEmbed.Field... fields) {
		EmbedBuilder builder = new EmbedBuilder();
		builder.setTitle(title);
		
		for (Field field : fields) 
			builder.addField(field);
		
		builder.addField("\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_", "", false);
		builder.setColor(embedColor);
		builder.setFooter(getFooter());
		return builder.build();
	}
	
	private MessageEmbed.Field getField(String title, String description){
		return new Field(title, description, false);
	}
	
	private MessageEmbed.Field getFieldAdmin(String title, String description){
		return new Field("*(for admin only)*\n"+ title, description, false);
	}
	
	private MessageEmbed.Field getFieldReserved(String role, String title, String description){
		return new Field("*("+(role == null ? "reserved": role)+")*\n"+title, description, false);
	}
	
	private String getFooter() {
		return "The following brackets <> and [] are used to explain the use of parameters:\n" + 
				"<>: mandatory parameters\n" + 
				"[]: optional parameters\n" + 
				"Parameters must not have spaces internally.";
	}
	
	@Override
	public void run(SlashCommandEvent event) {
		
		Map<Emoji, Page> categories = new LinkedHashMap<>();
		
		MessageEmbed index = getIndex();
		
		ULong guildRole = Config.guildsManager.findRecord(event.getGuild().getIdLong()).get().getIdRoleReserved();
		String role = guildRole == null || guildRole.equals(ULong.valueOf(0L))? null : event.getGuild().getRoleById(guildRole.longValue()).getName();
		
		categories.put(Emoji.fromMarkdown(MENU_ICON),new Page(index));
		categories.put(Emoji.fromMarkdown(USER_ICON),new Page(getEmbed(USER_TITLE, 
				getFieldReserved(role, "/player link <tag> <team>", 
						"The command allows to link the former tag to the tag-list of monitored players and add it to the tracked team.\n" + 
						"You can declare multiple tag by separating them with commas (tag1,tag2,tag3)."),
				
				getFieldReserved(role, "/player unlink <tag>", 
						"The command removes the former tag account from the tracking list.\n" + 
						"You can declare multiple tag by separating them with commas (tag1,tag2,tag3), do not leave spaces internally.\n" + 
						"**Alert!** If no one else besides you is tracking this player, the data collected so far will be lost."),
				
				getFieldReserved(role, "/player move <tag> <team>", 
						"Move the player from the current team to the new team.\n" + 
						"You can declare multiple tag by separating them by commas (tag1,tag2,tag3), **IMPORTANT!** do not leave spaces internally."),
				
				getField("/player stats <tag>", 
						"Command shows the real-time player status."))));
		
		
		categories.put(Emoji.fromMarkdown(TEAM_ICON),new Page(getEmbed(TEAM_TITLE, 
				getFieldReserved(role, "/team link <clan-tag> [new-name] [players-to-load]", 
						"The command allow to link the first *players to load* (if defined, otherwise all presents) from the specified *clan tag*. It is possible to define a new name."),
				
				getFieldReserved(role, "/team unlink <team-name>", 
						"The command removes the entire team.\n" + 
						"**Alert!** If no one else besides you is tracking this players, the data collected so far will be lost."),
				
				getFieldReserved(role, "/team rename <team-name> <new-name>", 
						"Rename the current team name with the new name."),
				
				getField("/team state <team-name> <view-type> [best-players]", 
						"Command shows the real-time team status.\n" + 
						"You can declare multiple teams by separating them by commas (team1,team2,team3), **IMPORTANT!** do not leave spaces internally.\n" + 
						"By specifying **all** as *team-name*, you are declaring all teams defined.\n" + 
						"The *best-players* is optional, use it if you are interested in viewing only the best.\n" + 
						"With *view-type* you declare how to print data."),
				
				getField("/team list", 
						"List of registered teams."))));
		
		categories.put(Emoji.fromMarkdown(GLOBAL_ICON),new Page(getEmbed(GLOBAL_TITLE, 
				getFieldReserved(role, "/general clean-under <trophies> [team]", 
						"Removes all players below the specified *trophies*, within the *team* if specified.\n" + 
						"You can declare multiple teams by separating them with commas (team1,team2,team3)."),
				
				getField("/general tracked-players <view-type> [team]", 
						"Command shows details of the full tracked players list, if specified belonging to a specific *team*.\n" + 
						"With *view-type* you declare how to print data."))));
		
		categories.put(Emoji.fromMarkdown(SERVER_ICON),new Page(getEmbed(SERVER_TITLE, 
				getFieldAdmin("/guild channel-log [channel]", 
						"Upload to this channel all attacks made/suffered by monitored players, if specified the tagged channel will be used."),
				
				getFieldAdmin("/guild channel-coms [channel]", 
						"Set this channel as channel for bot's communications, if specified the tagged channel will be used."),
				
				getFieldAdmin("/guild role <role>", 
						"Permits to define a specific role for add/remove of players and teams."),
				
				getFieldAdmin("/guild info", 
						"Info regarding the subscription.")
				
				)));
		
		categories.put(Emoji.fromMarkdown(INFO_ICON),new Page(getInfo(event)));
		
		event.getHook().sendMessageEmbeds(index).queue( s -> Pages.categorize(s, categories, false,2,TimeUnit.MINUTES));
	}

	@Override
	public String helpKey() {
		return SlashBundle.HELP_DESCRIPTION;
	}

	@Override
	public String nameKey() {
		return SlashBundle.HELP_NAME;
	}

	@Override
	public List<String> pathKeys() {
		return List.of();
	}

}
