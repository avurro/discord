package it.bitsplitters.legendstracker.slashcommands.service;

import static it.bitsplitters.legendstracker.service.MessageUtil.exHandling;

import java.util.function.Consumer;

import it.bitsplitters.clashapi.Clan;
import it.bitsplitters.legendstracker.COC;
import it.bitsplitters.legendstracker.Config;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;

public class ClanService {

	public static void findAPIClan(SlashCommandEvent event, String tag, Consumer<Clan> consumer) {
		findAPIClan(event, tag, 0, consumer);
	}
		
	private static void findAPIClan(SlashCommandEvent event, String tag, int numberTry, Consumer<Clan> consumer) {
		
		COC.API.requestAsyncClan(tag)
		.whenCompleteAsync((clanData, playerEx) -> {
			
			if(playerEx != null)
				if(numberTry < Config.NUMBER_TRY) {
					findAPIClan(event, tag, numberTry+1, consumer);
					return;
				}else {
					exHandling(event, playerEx, tag);
					return;
				}
			consumer.accept(clanData);
		});
	}
}
