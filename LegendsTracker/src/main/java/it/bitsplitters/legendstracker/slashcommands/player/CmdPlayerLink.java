package it.bitsplitters.legendstracker.slashcommands.player;

import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;
import static it.bitsplitters.legendsdata.jooq.models.tables.Players.PLAYERS;
import static it.bitsplitters.legendstracker.Config.getPlayerLink;
import static it.bitsplitters.legendstracker.Config.isntActiveOrConfiguredServer;
import static it.bitsplitters.legendstracker.service.MessageUtil.okMessage;
import static it.bitsplitters.legendstracker.service.MessageUtil.warningMessage;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERIC_ACCOUNTS_ALREADY_TRACKED;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERIC_ACCOUNTS_EXIST;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERIC_ACCOUNTS_FULL;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERIC_ACCOUNTS_SPACE_INSUFFICIENT;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GENERIC_TAG_NOTEXIST;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_LINK_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_LINK_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_LINK_OK;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_LINK_TAG_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_LINK_TAG_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_LINK_TEAM_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_LINK_TEAM_NAME;
import static it.bitsplitters.util.BotUtil.codeEmbed;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.ignite.IgniteCache;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.legends.batch.service.PlayersService;
import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsPlayersRecord;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendsdata.jooq.models.tables.records.PlayersRecord;
import it.bitsplitters.legendsdata.models.GuildChannels;
import it.bitsplitters.legendsdata.models.PlayerData;
import it.bitsplitters.legendsdata.service.LoadBalancer;
import it.bitsplitters.legendstracker.Config;
import it.bitsplitters.legendstracker.service.MessageUtil;
import it.bitsplitters.legendstracker.service.PlayersInfo;
import it.bitsplitters.legendstracker.service.PlayersLog;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdPlayerLink extends PlayerSlashComand{

	@Override
	public PermissionLevel permissionLevel() {
		return PermissionLevel.RESERVED;
	}
	
	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(
				optRequired(OptionType.STRING, PLAYER_LINK_TAG_NAME, PLAYER_LINK_TAG_DESCRIPTION, locale),
				optRequired(OptionType.STRING, PLAYER_LINK_TEAM_NAME, PLAYER_LINK_TEAM_DESCRIPTION, locale)
				);
	}

	@Override
	public boolean isEphemeral() {
		return false;
	}

	@Override
	public void run(SlashCommandEvent event) {
		Config.guildsManager.findRecord(event.getGuild().getIdLong()).ifPresent(guild -> {
			
			if(isntActiveOrConfiguredServer(event, guild)) 
				return;

			String playersTag = getOption(event, PLAYER_LINK_TAG_NAME).getAsString().toUpperCase();
			String team = getOption(event, PLAYER_LINK_TEAM_NAME).getAsString().toUpperCase();
		
			int followedPlayers = DB.jooq().fetchCount(GUILDS_PLAYERS,GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild()));
			
			// Se eccedo il limite di players tracciabili pagato
			if(followedPlayers >= guild.getProfilesMax()) {
				warningMessage(event, GENERIC_ACCOUNTS_FULL);
				return;
			}
			
			int emptySlots = guild.getProfilesMax() - followedPlayers;
			linkPlayers(event, team, emptySlots, guild, playersTag.toUpperCase().split(","));
			
			LoadBalancer.loadRebalance();
		});
	}

	private void linkPlayers(SlashCommandEvent event, String clanName, int remainingSpace, GuildsRecord guild, String... tags){
		List<String> playersToAdd = new ArrayList<>();
		List<String> tagListLinkedExternal = new ArrayList<>();
		List<String> playersListLinkedExternal = new ArrayList<>();
		List<String> playersAlreadyLinkedTargetTeam = new ArrayList<>();
		
		for (String playerTag : tags) {
			
			if(!playerTag.startsWith("#")) {
				warningMessage(event, GENERIC_TAG_NOTEXIST, playerTag);
				continue;
			}
			
			PlayersRecord player = DB.jooq().select(PLAYERS.fields())
									     .from(PLAYERS)
									     .naturalJoin(GUILDS_PLAYERS)
						                 .where(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild())
						                 .and(PLAYERS.TAG.eq(playerTag))).fetchOneInto(PLAYERS);
			
			// Se non presente lo conto
			if(player == null) 
				playersToAdd.add(playerTag);

			else {
				
				String playerLink = getPlayerLink(player.getNickname(), player.getTag());
				
				String playerTeam = DB.jooq().selectFrom(GUILDS_PLAYERS)
				      .where(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild())
		              .and(GUILDS_PLAYERS.ID_PLAYER.eq(player.get(PLAYERS.ID_PLAYER)))).fetchOne().getTeam();
				
				if(clanName.equals(playerTeam)) {
					playersAlreadyLinkedTargetTeam.add(playerLink);
				}else {
					tagListLinkedExternal.add(playerTag);
					playersListLinkedExternal.add(playerLink);
				}
			}
		}
			
		if(remainingSpace < playersToAdd.size()) {
			warningMessage(event, GENERIC_ACCOUNTS_SPACE_INSUFFICIENT, remainingSpace+"", playersToAdd.size()+"");
			return;
		}
		
		if(!tagListLinkedExternal.isEmpty()) {
			
			String playersList = MessageUtil.listToString(playersListLinkedExternal, ", ", 20);
			
			String tagList = MessageUtil.listToString(tagListLinkedExternal, ",", 100);
				
			warningMessage(event, GENERIC_ACCOUNTS_EXIST, playersList, clanName);

			String command = String.join(" ","/player move", tagList, clanName);
			codeEmbed(event, command);
		}
		
		if(!playersAlreadyLinkedTargetTeam.isEmpty()) {
			String list = MessageUtil.listToString(playersAlreadyLinkedTargetTeam, ", ", 20);
			warningMessage(event, GENERIC_ACCOUNTS_ALREADY_TRACKED, list, clanName);
		}
		
		List<String> newPlayersToAdd = new ArrayList<>();
		IgniteCache<String, PlayerData> cache = Cache.playersData();
		
		for (String playerTag : playersToAdd) {
			
			//INSERT IN PLAYERS
			PlayersRecord player = DB.jooq().selectFrom(PLAYERS).where(PLAYERS.TAG.eq(playerTag)).fetchOne();
			
			if(player == null) {
				newPlayersToAdd.add(playerTag);
				continue;
			}

			//INSERT IN GUILDS_PLAYERS
			DB.jooq().executeInsert(new GuildsPlayersRecord(guild.getIdGuild(), player.getIdPlayer(), clanName));
			
			
			PlayerData playerData = cache.get(playerTag);
			
			if(playerData == null)
				playerData = PlayersService.getPlayer(playerTag);
			
			//LOADING DATA IN CACHE
			GuildChannels guildChannels = new GuildChannels(guild.getIdGuild().longValue(), guild.getIdChannelLog().longValue(), guild.getIdChannelComs().longValue());

			PlayersLog.addPlayer(playerData, guildChannels);
			
			okMessage(event, PLAYER_LINK_OK, getPlayerLink(player.getNickname(), player.getTag()));
				
		}
		
		for (String playerTag : newPlayersToAdd) {
			
			PlayersInfo.findAPIAccount(event, playerTag, (e, cocPlayer) -> {
				
				//INSERT IN PLAYERS
				PlayersRecord player = DB.jooq().newRecord(PLAYERS);
				player.setNickname(cocPlayer.getName());
				player.setTag(cocPlayer.getTag());
				player.store();
				
				//INSERT IN GUILDS_PLAYERS
				DB.jooq().executeInsert(new GuildsPlayersRecord(guild.getIdGuild(), player.getIdPlayer(), clanName));
				
				//LOADING DATA IN CACHE
				PlayerData legendPlayer = new PlayerData(cocPlayer.getName(), cocPlayer.getTag(), cocPlayer.getTrophies(), cocPlayer.getDefenseWins(), cocPlayer.getAttackWins());
				GuildChannels guildChannels = new GuildChannels(guild.getIdGuild().longValue(), guild.getIdChannelLog().longValue(), guild.getIdChannelComs().longValue());
				PlayersLog.addPlayer(legendPlayer, guildChannels);
				
				okMessage(event, PLAYER_LINK_OK, getPlayerLink(cocPlayer.getName(),cocPlayer.getTag()));
			});
		}
		
	}
	
	@Override
	public String helpKey() {
		return PLAYER_LINK_DESCRIPTION;
	}

	@Override
	public String nameKey() {
		return PLAYER_LINK_NAME;
	}

}
