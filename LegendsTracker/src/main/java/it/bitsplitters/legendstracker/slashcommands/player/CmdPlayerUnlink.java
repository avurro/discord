package it.bitsplitters.legendstracker.slashcommands.player;

import static it.bitsplitters.legendstracker.Config.isntActiveOrConfiguredServer;
import static it.bitsplitters.legendstracker.service.MessageUtil.okMessage;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_UNLINK_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_UNLINK_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_UNLINK_OK;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_UNLINK_TAG_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_UNLINK_TAG_NAME;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.legendsdata.service.LoadBalancer;
import it.bitsplitters.legendstracker.Config;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdPlayerUnlink extends PlayerSlashComand{

	@Override public PermissionLevel permissionLevel() {
		return PermissionLevel.RESERVED;
	}
	
	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(
				optRequired(OptionType.STRING, PLAYER_UNLINK_TAG_NAME, PLAYER_UNLINK_TAG_DESCRIPTION, locale)
				);
	}

	@Override
	public boolean isEphemeral() {
		return false;
	}

	@Override
	public void run(SlashCommandEvent event) {
		
		Config.guildsManager.findRecord(event.getGuild().getIdLong()).ifPresent(guild -> {
			
			if(isntActiveOrConfiguredServer(event, guild)) 
				return;

			String tags = getOption(event, PLAYER_UNLINK_TAG_NAME).getAsString().toUpperCase();
			
			List<String> playersRemoved = new ArrayList<>();
			
			for (String tag : tags.toUpperCase().split(",")) {
				String playerName = removePlayer(event, guild, tag);
				
				if(playerName != null)
					playersRemoved.add(playerName);
			}
			
			if(!playersRemoved.isEmpty()) 
				okMessage(event,  PLAYER_UNLINK_OK, String.join(", ", playersRemoved));
			
			LoadBalancer.loadRebalance();
		});	
	}

	@Override
	public String helpKey() {
		return PLAYER_UNLINK_DESCRIPTION;
	}

	@Override
	public String nameKey() {
		return PLAYER_UNLINK_NAME;
	}

}
