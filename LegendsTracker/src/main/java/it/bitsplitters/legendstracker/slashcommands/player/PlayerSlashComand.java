package it.bitsplitters.legendstracker.slashcommands.player;

import java.util.List;

import it.bitsplitters.legendstracker.slashcommands.CmdUnlink;
import it.bitsplitters.legendstracker.slashcommands.SlashBundle;

public abstract class PlayerSlashComand extends CmdUnlink {

	@Override
	public List<String> pathKeys() {
		return List.of(SlashBundle.PLAYER_NAME);
	}

}