package it.bitsplitters.legendstracker.slashcommands.trophieschallenge;

import static it.bitsplitters.legendstracker.service.MessageUtil.errorMessage;
import static it.bitsplitters.legendstracker.service.MessageUtil.stopMessage;
import static it.bitsplitters.legendstracker.service.MessageUtil.warningMessage;
import static it.bitsplitters.legendstracker.service.PrinterService.centinaiaToEmoji;
import static it.bitsplitters.legendstracker.service.PrinterService.spAttacks;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_STANDINGS_RANK_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_STANDINGS_RANK_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_STANDINGS_TEAM_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_STANDINGS_TEAM_NAME;
import static java.lang.Math.min;
import static java.lang.Math.round;
import static java.util.Collections.sort;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.StringJoiner;

import com.google.common.collect.Lists;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.legendsdata.models.PlayerData;
import it.bitsplitters.legendstracker.Config;
import it.bitsplitters.legendstracker.service.TeamService;
import it.bitsplitters.legendstracker.slashcommands.SlashBundle;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdTCStandings extends TCSlashComand {

	private static final int NUMERO_MINIMO_PARTECIPANTI = 6;
	private static final int NUMERO_MASSIMO_LINEE = 15;
	private static final long MAX_BONUS_MALUS = 10;
	private static final int MAX_LINES = 40;
	
	@Override
	public PermissionLevel permissionLevel() {
		return PermissionLevel.PUBLIC;
	}
	
	@Override
	public String nameKey() {
		return null;
	}
	
	@Override
	public List<String> pathKeys() {
		return null;
	}
	
	@Override
	public String helpKey() {
		return SlashBundle.TROPHIES_CHALLENGE_HELP_STANDINGS;
	}

	@Override
	public List<OptionData> options(Locale locale) {
		//TODO rendere le opzioni multilingua
		return List.of(optRequired(OptionType.STRING, TC_STANDINGS_TEAM_NAME, TC_STANDINGS_TEAM_DESCRIPTION, locale),
					   optRequired(OptionType.BOOLEAN, TC_STANDINGS_RANK_NAME, TC_STANDINGS_RANK_DESCRIPTION, locale));
	}

	@Override
	public void run(SlashCommandEvent event) {
		
		Config.guildsManager.findRecord(event.getGuild().getIdLong()).ifPresent(guild -> {
			
			String teamName = event.getOption(TC_STANDINGS_TEAM_NAME).getAsString();
			boolean standings = event.getOption(TC_STANDINGS_RANK_NAME).getAsBoolean();
			
			if(!TeamService.exists(guild, teamName)) {
				stopMessage(event, SlashBundle.TEAM_NOTEXIST, teamName);
				return;
			}
			
			List<PlayerData> team = TeamService.getTeam(guild, teamName);
			
			if(team.size() < NUMERO_MINIMO_PARTECIPANTI) {
				stopMessage(event, SlashBundle.TROPHIES_CHALLENGE_NUMBERLOW, teamName);
				return;
			}
			
			List<PlayerData> scartati = new ArrayList<>();
			List<PlayerData> entryLevel = new ArrayList<>();
			List<PlayerData> basicLevel = new ArrayList<>();
			List<PlayerData> proLevel = new ArrayList<>();
			List<PlayerData> masterLevel = new ArrayList<>();
			
			// ------------------------------
			// RAGGRUPPA I PLAYERS PER FASCIA
			// e aggiunge al totale trofei di ognuno il bonus/malus
			// ------------------------------
			for (PlayerData player : team) {
				
				int start = player.getStartTrophies();
				Integer playerCorrection = TCSlashComand.getAtksCorrection().get(player.getTag());
				Integer sum = player.sumAtks() + (playerCorrection == null ? 0 : playerCorrection);
				player.setSumWithBonus(addBonusMalus(player, sum));
				
				//Entry Level
				if(start < 5000) 
					scartati.add(player);
					
				else if(start >= 5000 && start <5200) 
					entryLevel.add(player);
				
				//Basic Level
				else if (start >= 5200 && start <5400) 
					basicLevel.add(player);
				
				//Pro Level
				else if (start >= 5400 && start <5600) 
					proLevel.add(player);
				
				//Master Level	
				else if (start >= 5600) 
					masterLevel.add(player);
				
			}
			
			// NOTIFICO GLI SCARTATI
			if(scartati.size() > 0) {
				
				StringJoiner sj = new StringJoiner(", ");
				int playerCount = 1;
				
				for (PlayerData player : scartati)
					if(playerCount++ <= MAX_LINES)
						sj.add(Config.getPlayerLink(player.getNickname(), player.getTag()));
					else 
						break;
				
				if(playerCount > MAX_LINES)
					sj.add("...");
				
				warningMessage(event, SlashBundle.TROPHIES_CHALLENGE_DISCARDED, sj.toString());
			}
			
			// ------------------------------
			// VERIFICO LE FASCE
			// In caso di numero insufficiente sposto alla fascia sotto
			// Altrimenti li ordino per trofei totali di attacco
			// ------------------------------
			StringJoiner livelliNonAvviati = new StringJoiner(", ");
			
			if(masterLevel.size() < NUMERO_MINIMO_PARTECIPANTI) {
				proLevel.addAll(masterLevel);
				masterLevel.clear();
				livelliNonAvviati.add("Master");
			}else
				sort(masterLevel, atkTrophiesComparator);
			
			if(proLevel.size() < NUMERO_MINIMO_PARTECIPANTI) {
				basicLevel.addAll(proLevel);
				proLevel.clear();
				livelliNonAvviati.add("Pro");
			}else
				sort(proLevel, atkTrophiesComparator);
			
			if(basicLevel.size() < NUMERO_MINIMO_PARTECIPANTI) {
				entryLevel.addAll(basicLevel);
				basicLevel.clear();
				livelliNonAvviati.add("Basic");
			}else
				sort(basicLevel, atkTrophiesComparator);
			
			if(livelliNonAvviati.length() != 0) 
				warningMessage(event, SlashBundle.TROPHIES_CHALLENGE_CLOSED_GROUPS, livelliNonAvviati.toString());
			
			if(entryLevel.size() < NUMERO_MINIMO_PARTECIPANTI) {
				
				StringJoiner sj = new StringJoiner(", ");
				for (PlayerData player : entryLevel) 
					sj.add(Config.getPlayerLink(player.getNickname(), player.getTag()));
				
				warningMessage(event, SlashBundle.TROPHIES_CHALLENGE_CLOSED_ENTRYLEVEL, sj.toString());
				
				entryLevel.clear();
				
			}else
				sort(entryLevel, atkTrophiesComparator);
			
			// ------------------------------
			// CLASSIFICA VINCITORI
			// ------------------------------
			if(standings) {
				
				StringJoiner sj = new StringJoiner("");
				if(!masterLevel.isEmpty() && getFirstNotBanned(masterLevel , 1) != null) {
					int size = masterLevel.size();
					sj.add("**Master** *(").add(size+"").add(")*\n");
					sj.add(":first_place:").add(getPlayer(getFirstNotBanned(masterLevel , 1), false, 1, size)).add("\n");
					
					if(getFirstNotBanned(masterLevel , 2) != null)
						sj.add(":second_place:").add(getPlayer(getFirstNotBanned(masterLevel , 2), false, 2, size)).add("\n");
					
					if(getFirstNotBanned(masterLevel , 3) != null)
						sj.add(":third_place:").add(getPlayer(getFirstNotBanned(masterLevel , 3), false, 3, size)).add("\n\n");
				}
				
				if(!proLevel.isEmpty() && getFirstNotBanned(proLevel , 1) != null) {
					int size = proLevel.size();
					sj.add("**Pro** *(").add(proLevel.size()+"").add(")*\n");
					sj.add(":first_place:").add(getPlayer(getFirstNotBanned(proLevel , 1), false, 1, size)).add("\n");
					
					if(getFirstNotBanned(proLevel , 2) != null)
						sj.add(":second_place:").add(getPlayer(getFirstNotBanned(proLevel , 2), false, 2, size)).add("\n");
					
					if(getFirstNotBanned(proLevel , 3) != null)
						sj.add(":third_place:").add(getPlayer(getFirstNotBanned(proLevel , 3), false, 3, size)).add("\n\n");
				}
				
				if(!basicLevel.isEmpty() && getFirstNotBanned(basicLevel , 1) != null) {
					int size = basicLevel.size();
					sj.add("**Basic** *(").add(basicLevel.size()+"").add(")*\n");
					sj.add(":first_place:").add(getPlayer(getFirstNotBanned(basicLevel , 1), false, 1, size)).add("\n");
					
					if(getFirstNotBanned(basicLevel , 2) != null)
						sj.add(":second_place:").add(getPlayer(getFirstNotBanned(basicLevel , 2), false, 2, size)).add("\n");
					
					if(getFirstNotBanned(basicLevel , 3) != null)
						sj.add(":third_place:").add(getPlayer(getFirstNotBanned(basicLevel , 3), false, 3, size)).add("\n\n");
				}
				
				if(!entryLevel.isEmpty() && getFirstNotBanned(entryLevel , 1) != null) {
					int size = entryLevel.size();
					sj.add("**Entry** *(").add(size+"").add(")*\n");
					sj.add(":first_place:").add(getPlayer(getFirstNotBanned(entryLevel , 1), false, 1, size)).add("\n");
					
					if(getFirstNotBanned(entryLevel , 2) != null)
						sj.add(":second_place:").add(getPlayer(getFirstNotBanned(entryLevel , 2), false, 2, size)).add("\n");
					
					if(getFirstNotBanned(entryLevel , 3) != null)
						sj.add(":third_place:").add(getPlayer(getFirstNotBanned(entryLevel , 3), false, 3, size)).add("\n\n");
				}
				
				if(sj.length() > 0) {
					
					sj.add("\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\n");
					sj.add(Config.getSlashMsg(guild, SlashBundle.TROPHIES_CHALLENGE_FOOTER));
					
					EmbedBuilder eb = new EmbedBuilder();
					eb.setTitle("STANDINGS");
					eb.setDescription(sj.toString());
					eb.setColor(Config.legendsColor);
					
					event.getHook().sendMessageEmbeds(eb.build()).queue();
					
				}else {
					errorMessage(event, SlashBundle.TROPHIES_CHALLENGE_NOWINNERS);
					return;
				}
				
			// ------------------------------
			// CLASSIFICA COMPLETA
			// ------------------------------
			}else {
				
				List<String> ranking = printRank("Master", masterLevel);
				ranking.addAll(printRank("Pro", proLevel));
				ranking.addAll(printRank("Basic", basicLevel));
				ranking.addAll(printRank("Entry", entryLevel));
				
				List<MessageEmbed> pages = new ArrayList<>();
				EmbedBuilder eb = new EmbedBuilder();
				eb.setTitle("Trophies Challenge");
				for(List<String> page: Lists.partition(ranking, NUMERO_MASSIMO_LINEE)) {
					eb.setDescription(String.join("\n", page));
					eb.setColor(Config.legendsColor);
					pages.add(eb.build());
					eb = new EmbedBuilder();
				}
				
				for (List<MessageEmbed> list : Lists.partition(pages, 10)) 
					event.getHook().sendMessageEmbeds(list).queue();
			
			}
		
		});
		
	}

	private List<String> printRank(String level, List<PlayerData> players){
		List<String> result = new ArrayList<String>();
		
		if(!players.isEmpty()) {
			result.add("**"+level+" Level** *("+players.size()+")*");
			
			for (PlayerData player : players) 
				result.add(getPlayer(player));
			
			result.add("\n");
		}
		
		return result;
	}
	
	private PlayerData getFirstNotBanned(List<PlayerData> group, int rankPos) {
		
		int pos = 1;
		for (PlayerData playerData : group) 
			if(!isBanned(playerData.getTag()) && pos++ == rankPos)
				return playerData;
		
		return null;
	}
	
	private long applyBonusMalus(int percentuale, int sumAtks) {
		long percentage = round(((double)((sumAtks * percentuale) / 100)));
		return min(MAX_BONUS_MALUS, percentage);
	}
	
	private long addBonusMalus(PlayerData player, int sumAtks) {
		
		int startTrophies = player.getStartTrophies() > 5600 ? 5600
				           :player.getStartTrophies() > 5400 ? 5400
				           :player.getStartTrophies() > 5200 ? 5200 : 5000;
				           
		int trophies = player.getStartTrophies() - startTrophies;
		long result = sumAtks;
		
		if(trophies >= 0 && trophies < 20) 
			result = sumAtks - applyBonusMalus(5, sumAtks);
		
		else if (trophies >= 20 && trophies < 40) 
			result = sumAtks - applyBonusMalus(4, sumAtks);
		
		else if (trophies >= 40 && trophies < 60) 
			result = sumAtks - applyBonusMalus(3, sumAtks);
			
		else if (trophies >= 60 && trophies < 80) 
			result = sumAtks - applyBonusMalus(2, sumAtks);
			
		else if (trophies >= 80 && trophies < 90) 
			result = sumAtks - applyBonusMalus(1, sumAtks);
			
		else if (trophies >= 90 && trophies < 110) {} 
			
		else if (trophies >= 110 && trophies < 120) 
			result = sumAtks + applyBonusMalus(1, sumAtks);
			
		else if (trophies >= 120 && trophies < 140) 
			result = sumAtks + applyBonusMalus(2, sumAtks);
			
		else if (trophies >= 140 && trophies < 160) 
			result = sumAtks + applyBonusMalus(3, sumAtks);
			
		else if (trophies >= 160 && trophies < 180) 
			result = sumAtks + applyBonusMalus(4, sumAtks);
			
		else if (trophies >= 180 && (trophies < 200 || player.getStartTrophies() >= 5800)) 
			result = sumAtks + applyBonusMalus(5, sumAtks);
			
		return result;
	}
	
	private Comparator<PlayerData> atkTrophiesComparator = ((prev, actual) -> {
		long prevSum = prev.getSumWithBonus();
		long actualSum = actual.getSumWithBonus();
		
	    return prevSum < actualSum  ? 1 :
	    	prevSum == actualSum ? 0 : -1;
	});
	
	private String getPlayer(PlayerData player) {
		return getPlayer(player, true, null, null);
	}
	
	private String getPlayer(PlayerData player, boolean withNumAtks, Integer position, Integer size) {
		
		Integer correctionSumAtks = TCSlashComand.getAtksCorrection().get(player.getTag());
		correctionSumAtks = correctionSumAtks == null ? 0: correctionSumAtks;
		
		Integer correctionNumAtks = TCSlashComand.getNumAtksCorrection().get(player.getTag());
		correctionNumAtks = correctionNumAtks == null ? 0 : correctionNumAtks;
		
		Integer numAtks =  player.attacks() + correctionNumAtks;
		
		boolean correction = correctionSumAtks != 0 || correctionNumAtks != 0;
		
		String totaleTrofei = centinaiaToEmoji(Long.valueOf(player.getSumWithBonus()).intValue());
		String totaleAttacchi = withNumAtks ? spAttacks(numAtks) : "";
		String correzione = correction?"**\\*** ":"";
		String nickname = player.getNickname();
		String difference = "*("+String.valueOf(player.getSumWithBonus() - (player.sumAtks() + correctionSumAtks))+")* ";
		String posizionePremiante = position != null ? isPosizionePremiante(position, size) ? Config.GEMME : "" : "";
		posizionePremiante = isBanned(player.getTag()) ? Config.BANNED : posizionePremiante;
		return String.join("", totaleTrofei, totaleAttacchi, posizionePremiante, correzione, difference, nickname).toString();
	}
	
	private boolean isPosizionePremiante(Integer posizione, Integer size) {
		if(size >= NUMERO_MINIMO_PARTECIPANTI && size < 12) 
			return posizione <= 1;
		else if(size >= 12 && size < 18) 
			return posizione <= 2;
		else
			return posizione <= 3;
	}
	
	@Override
	public boolean isEphemeral() {
		return true;
	}
}