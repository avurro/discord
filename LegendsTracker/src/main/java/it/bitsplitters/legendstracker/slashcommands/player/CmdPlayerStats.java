package it.bitsplitters.legendstracker.slashcommands.player;

import static it.bitsplitters.legendstracker.Config.isntActiveOrConfiguredServer;
import static it.bitsplitters.legendstracker.service.MessageUtil.warningMessage;
import static it.bitsplitters.legendstracker.service.PlayersInfo.isPlayerTracked;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_STATE_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_STATE_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_STATE_TAG_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_STATE_TAG_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.PLAYER_STATE_TAG_NOTEXIST;

import java.util.List;
import java.util.Locale;

import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendstracker.Config;
import it.bitsplitters.legendstracker.service.DiscordLogWriterService;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdPlayerStats extends PlayerSlashComand{

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(
				optRequired(OptionType.STRING, PLAYER_STATE_TAG_NAME, PLAYER_STATE_TAG_DESCRIPTION, locale)
				);
	}

	@Override
	public boolean isEphemeral() {
		return false;
	}

	@Override
	public void run(SlashCommandEvent event) {
		
		Config.guildsManager.findRecord(event.getGuild().getIdLong()).ifPresent(guild -> {
				
				if(isntActiveOrConfiguredServer(event, guild)) 
					return;
			
			String playerTag = getOption(event, PLAYER_STATE_TAG_NAME).getAsString().toUpperCase();
			
			if(!isPlayerTracked(guild, playerTag)){
				warningMessage(event, PLAYER_STATE_TAG_NOTEXIST, playerTag);
				return;
			}
			
			event.getHook().sendMessageEmbeds(DiscordLogWriterService.getEmbedPlayerDetails(guild.getIdGuild().longValue(), Cache.playersInfo().get(playerTag))).queue();
		
		});
		
	}

	@Override
	public String helpKey() {
		return PLAYER_STATE_DESCRIPTION;
	}

	@Override
	public String nameKey() {
		return PLAYER_STATE_NAME;
	}

}
