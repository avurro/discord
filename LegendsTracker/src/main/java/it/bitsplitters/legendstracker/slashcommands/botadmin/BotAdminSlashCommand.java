package it.bitsplitters.legendstracker.slashcommands.botadmin;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.legendstracker.slashcommands.LegendsTrackerSlashCommand;

public abstract class BotAdminSlashCommand extends LegendsTrackerSlashCommand{

	@Override
	public String nameKey() {
		throw new UnsupportedOperationException("Chiave del nome comando non correttamente sostituita");
	}
	
	@Override
	public String helpKey() {
		throw new UnsupportedOperationException("Chiave della descrizione non correttamente sostituita");
	}
	
	@Override
	public String path(Locale locale) {
		List<String> pathPieces = new ArrayList<>(2);
		
		pathPieces.add("bot-admin");
		pathPieces.add(name(locale));
		
		return String.join("/", pathPieces);
	}
	
	@Override
	public List<String> pathKeys() {
		throw new UnsupportedOperationException("Errore. metodo invocato per sbaglio");
	}
	
	@Override
	public PermissionLevel permissionLevel() {
		return PermissionLevel.BOT_ADMIN;
	}

}
