package it.bitsplitters.legendstracker.slashcommands;

import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.*;
import static it.bitsplitters.util.ResourceBundleSlashUtil.SLASH_COMMAND_RESOURCE_FULLNAME;
import static it.bitsplitters.util.ResourceBundleSlashUtil.getSlashResourceBundle;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import it.bitsplitters.command.SlashCommand;
import it.bitsplitters.legendstracker.slashcommands.botadmin.CmdBotDump;
import it.bitsplitters.legendstracker.slashcommands.botadmin.CmdBotMaintenance;
import it.bitsplitters.legendstracker.slashcommands.botadmin.CmdBotSendMessage;
import it.bitsplitters.legendstracker.slashcommands.botadmin.CmdBotShutdown;
import it.bitsplitters.legendstracker.slashcommands.botadmin.CmdGuildAccounts;
import it.bitsplitters.legendstracker.slashcommands.botadmin.CmdGuildDetails;
import it.bitsplitters.legendstracker.slashcommands.botadmin.CmdGuildExtend;
import it.bitsplitters.legendstracker.slashcommands.botadmin.CmdGuildKick;
import it.bitsplitters.legendstracker.slashcommands.botadmin.CmdGuildReload;
import it.bitsplitters.legendstracker.slashcommands.botadmin.CmdGuildSubscribe;
import it.bitsplitters.legendstracker.slashcommands.global.CmdGeneralClean;
import it.bitsplitters.legendstracker.slashcommands.global.CmdGeneralHelp;
import it.bitsplitters.legendstracker.slashcommands.global.CmdGeneralTrackedPlayers;
import it.bitsplitters.legendstracker.slashcommands.guild.CmdGuildChannelComs;
import it.bitsplitters.legendstracker.slashcommands.guild.CmdGuildChannelLog;
import it.bitsplitters.legendstracker.slashcommands.guild.CmdGuildInfo;
import it.bitsplitters.legendstracker.slashcommands.guild.CmdGuildRole;
import it.bitsplitters.legendstracker.slashcommands.player.CmdPlayerLink;
import it.bitsplitters.legendstracker.slashcommands.player.CmdPlayerMove;
import it.bitsplitters.legendstracker.slashcommands.player.CmdPlayerStats;
import it.bitsplitters.legendstracker.slashcommands.player.CmdPlayerUnlink;
import it.bitsplitters.legendstracker.slashcommands.team.CmdTeamLink;
import it.bitsplitters.legendstracker.slashcommands.team.CmdTeamList;
import it.bitsplitters.legendstracker.slashcommands.team.CmdTeamRename;
import it.bitsplitters.legendstracker.slashcommands.team.CmdTeamState;
import it.bitsplitters.legendstracker.slashcommands.team.CmdTeamUnlink;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;
import net.dv8tion.jda.api.requests.restaction.CommandListUpdateAction;

public class BotStructure {
	
	// ------------------
	// BOT ADMIN COMMANDS
	private SlashCommand guildKick 		= new CmdGuildKick();
	private SlashCommand guildExtend	= new CmdGuildExtend();
	private SlashCommand guildAccounts 	= new CmdGuildAccounts();
	private SlashCommand guildSub 		= new CmdGuildSubscribe();
	private SlashCommand guildReload 	= new CmdGuildReload();
	private SlashCommand guildDetails 	= new CmdGuildDetails();
	
	private SlashCommand botMnt 		= new CmdBotMaintenance();
	private SlashCommand botShutdown 	= new CmdBotShutdown();
	private SlashCommand botDump		= new CmdBotDump();
	private SlashCommand botSendMessage = new CmdBotSendMessage();
	// ------------------

//	private CmdTCCheckAtks tcBan 		= new CmdTCCheckAtks();
//	private CmdTCFixes tcFixes 			= new CmdTCFixes();
//	private CmdTCStandings tcStandings 	= new CmdTCStandings();
	
	// ------------------
	// ADMIN COMMANDS
	private SlashCommand channelLog     = new CmdGuildChannelLog();
	private SlashCommand channelComs 	= new CmdGuildChannelComs();
	private SlashCommand role 		 	= new CmdGuildRole();
	// ------------------
	
	// ------------------
	// RESERVED COMMANDS
	private SlashCommand cleanUnder  	= new CmdGeneralClean();
	private SlashCommand playerMove  	= new CmdPlayerMove();
	private SlashCommand playerLink     = new CmdPlayerLink();
	private SlashCommand playerUnlink   = new CmdPlayerUnlink();
	private SlashCommand teamLink       = new CmdTeamLink();
	private SlashCommand teamUnlink     = new CmdTeamUnlink();
	private SlashCommand teamRename     = new CmdTeamRename();
	// ------------------
	
	// ------------------
	// PUBLIC COMMANDS
	private SlashCommand playerStats 	= new CmdPlayerStats();
	private SlashCommand status 	 	= new CmdGuildInfo();
	private SlashCommand trackedPlayers = new CmdGeneralTrackedPlayers();
	private SlashCommand teamList       = new CmdTeamList();
	private SlashCommand teamState      = new CmdTeamState();
	private SlashCommand help			= new CmdGeneralHelp();
	// ------------------
	
	private Locale defaultLocale;
	
	public BotStructure(Locale defaultLocale) {
		this.defaultLocale = defaultLocale;
	}
	
	public Map<String, SlashCommand> pathCommandsMap(){
		
		Map<String, SlashCommand> commands = new HashMap<>();
		
		List<SlashCommand> commandsList = List.of(
				
				// bot admin commands
				botMnt, 
				botShutdown, 
				botDump,
				botSendMessage,
				guildAccounts, 
				guildKick, 
				guildExtend, 
				guildSub, 
				guildReload, 
				guildDetails,
				
				// admin commands
				channelLog,
				channelComs,
				role,
				
				// reserved commands
				cleanUnder,
				playerMove,
				playerLink,
				playerUnlink,
				teamLink,
				teamUnlink,
				teamRename,
				
				// public commands
				playerStats,
				trackedPlayers,
				status,
				teamList,
				teamState,
				help
				);
		
			for (SlashCommand slashCommand : commandsList)
				commands.put(slashCommand.path(defaultLocale), slashCommand);
		
		return commands;
				
	}
	
	public CommandListUpdateAction getPublicAction(JDA jda) {
		return jda.updateCommands();
	}
	
	public CommandListUpdateAction getGuildAction(JDA jda, long headquarterID) {
		return jda.getGuildById(headquarterID).updateCommands();
	}
	
	public CommandListUpdateAction publicCommands(CommandListUpdateAction commands) {
		
		return commands.addCommands(
				cmd(GUILD_NAME, GUILD_DESCRIPTION, defaultLocale)
				.addSubcommands(
					subCmd(channelLog, defaultLocale),
					subCmd(channelComs, defaultLocale),
					subCmd(role, defaultLocale),
					subCmd(status, defaultLocale)
				),
				cmd(GENERAL_NAME, GENERAL_DESCRIPTION, defaultLocale)
				.addSubcommands(
					subCmd(cleanUnder, defaultLocale),
					subCmd(trackedPlayers, defaultLocale)
				)
				,
				cmd(PLAYER_NAME, PLAYER_DESCRIPTION, defaultLocale)
				.addSubcommands(
					subCmd(playerStats, defaultLocale),
					subCmd(playerMove, defaultLocale),
					subCmd(playerLink, defaultLocale),
					subCmd(playerUnlink, defaultLocale)
				)
				,
				cmd(TEAM_NAME, TEAM_DESCRIPTION, defaultLocale)
				.addSubcommands(
					subCmd(teamLink, defaultLocale)
					,subCmd(teamUnlink, defaultLocale)
					,subCmd(teamState, defaultLocale)
					,subCmd(teamList, defaultLocale)
					,subCmd(teamRename, defaultLocale)
				),
				cmd(HELP_NAME, HELP_DESCRIPTION, defaultLocale)
				);
		
	}
	
	public CommandListUpdateAction updateReservedCommands(CommandListUpdateAction commands) {
		
		return botAdminCommands(commands);
//		trophiesChallenge(jda);
		
	}
	
//	private void trophiesChallenge(CommandListUpdateAction commands) {
//
//		Locale locale = Locale.ITALIAN;
//		
//		commands.addCommands(
//				
//			cmd("trophies-challenge", "Trophies Challenge")
//				.addSubcommands(
//					subCmd(tcBan, locale),
//					subCmd(tcFixes, locale),
//					subCmd(tcStandings, locale)
//				)
//		);
//	}
	
	private CommandListUpdateAction botAdminCommands(CommandListUpdateAction commands) {
		
		// Setting bot admin commands..
		return commands.addCommands(
				
			cmd("bot-admin", "riservati allo sviluppatore del bot")
				.addSubcommands(
						subCmd(botMnt, defaultLocale),
						subCmd(botShutdown, defaultLocale),
						subCmd(botDump, defaultLocale),
						subCmd(botSendMessage, defaultLocale),
						subCmd(guildAccounts, defaultLocale),
						subCmd(guildKick, defaultLocale),
						subCmd(guildExtend, defaultLocale),
						subCmd(guildSub, defaultLocale),
						subCmd(guildReload, defaultLocale),
						subCmd(guildDetails, defaultLocale)
				)
		);
	}
	
	private String getMsg(String key, Locale locale) {
		return getSlashResourceBundle(locale, SLASH_COMMAND_RESOURCE_FULLNAME).getString(key);
	}

	private CommandData cmd(String name, String description) {
		return new CommandData(name, description);
	} 
	
	private CommandData cmd(String keyName, String keyDescription, Locale locale) {
		return new CommandData(getMsg(keyName, locale), getMsg(keyDescription, locale));
	} 
	
	private SubcommandData subCmd(SlashCommand cmd, Locale locale) {
		SubcommandData result = new SubcommandData(cmd.name(locale), cmd.description(locale));
		
		List<OptionData> options = cmd.options(locale);
		if(!options.isEmpty())
			result.addOptions(options);
		
		return result;
	}
	
}
