package it.bitsplitters.legendstracker.slashcommands.guild;

import static it.bitsplitters.legendstracker.Config.isntActiveOrConfiguredServer;
import static it.bitsplitters.legendstracker.service.MessageUtil.okMessage;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GUILD_ROLE_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GUILD_ROLE_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GUILD_ROLE_OK;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GUILD_ROLE_ROLE_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.GUILD_ROLE_ROLE_NAME;

import java.util.List;
import java.util.Locale;

import org.jooq.types.ULong;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.legendstracker.Config;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
public class CmdGuildRole extends GuildSlashCommand {

	@Override
	public PermissionLevel permissionLevel() {
		return PermissionLevel.ADMIN;
	}
	
	@Override
	public String nameKey() {
		return GUILD_ROLE_NAME;
	}
	
	@Override
	public String helpKey() {
		return GUILD_ROLE_DESCRIPTION;
	}

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(optRequired(OptionType.ROLE, GUILD_ROLE_ROLE_NAME, GUILD_ROLE_ROLE_DESCRIPTION, locale));
	}

	@Override
	public void run(SlashCommandEvent event) {
		
		Config.guildsManager.findRecord(event.getGuild().getIdLong()).ifPresent(guild -> {
			
			if(isntActiveOrConfiguredServer(event, guild)) 
					return;
	
			Role role = getOption(event, GUILD_ROLE_ROLE_NAME).getAsRole();
			
			guild.setIdRoleReserved(ULong.valueOf(role.getIdLong()));
			guild.store();
			
			okMessage(event, GUILD_ROLE_OK);
		});
	}
	
	@Override
	public boolean isEphemeral() {
		return true;
	}
}