package it.bitsplitters.legendstracker.slashcommands.botadmin;

import static it.bitsplitters.legendstracker.Config.guildsManager;
import static it.bitsplitters.util.BotUtil.okEmbed;
import static it.bitsplitters.util.BotUtil.warningEmbed;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdGuildSubscribe extends BotAdminSlashCommand {

	private static final Logger logger = LogManager.getLogger();
	
	@Override
	public String name(Locale locale) {
		return "guild-sub";
	}

	@Override
	public String description(Locale locale) {
		return "Avvia l'abbonamento a una guild specifica";
	}

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(new OptionData(OptionType.STRING, "guild-id", "ID del server", true),
					   new OptionData(OptionType.INTEGER, "accounts", "numero di accounts da attivare", true),
				       new OptionData(OptionType.STRING, "end-date", "Data con pattern dd-MM-yy, o numero specifico dei mesi", true));
	}

	@Override
	public void run(SlashCommandEvent event) {

		long guildID = Long.valueOf(event.getOption("guild-id").getAsString());
		long accounts = event.getOption("accounts").getAsLong();
		String endSubs = event.getOption("end-date").getAsString();
		
		Optional<GuildsRecord> optNewCustomer = guildsManager.findRecord(guildID);
		
		if(!optNewCustomer.isPresent()) {
			warningEmbed(event, "Non risulta alcun server discord con questo ID: "+guildID+", agganciato a Legends.");
			return;
		}
		
		LocalDate payEnd = null;
		
		try {
			Long months = Long.valueOf(endSubs);
			payEnd = DB.localDateNow().plusMonths(months);
		}catch (NumberFormatException e) {
			
			try {
				
				payEnd = LocalDate.parse(endSubs, DateTimeFormatter.ofPattern("dd-MM-yy"));
			
			}catch (DateTimeParseException e2) {
				
				warningEmbed(event, "formato data non rispettato: "+endSubs+", pattern da usare: dd-MM-yy");
				
				logger.atInfo().log("formato data non rispettato: {}, pattern da usare: dd-MM-yy", endSubs);
				return;
			}
		}
		
		
		GuildsRecord newCustomer = optNewCustomer.get();
		newCustomer.setPayLevel((byte)1);
		newCustomer.setPayEnd(payEnd);
		newCustomer.setProfilesMax(Long.valueOf(accounts).shortValue());
		newCustomer.store();
		
		okEmbed(event, "Primo abbonamento effettuato per il server: "+guildID+", max profili: "+accounts+", con scadenza il "+payEnd);
		
		logger.atInfo().log("Primo abbonamento effetuato per il server: {}, max profili: {}, con scadenza il {}.", guildID, accounts, payEnd);
	}
	
	@Override
	public boolean isEphemeral() {
		return true;
	}
}