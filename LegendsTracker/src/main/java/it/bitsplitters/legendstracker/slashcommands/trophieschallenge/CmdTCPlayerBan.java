package it.bitsplitters.legendstracker.slashcommands.trophieschallenge;

import static it.bitsplitters.legendstracker.Config.isntActiveOrConfiguredServer;
import static it.bitsplitters.legendstracker.service.MessageUtil.okMessage;
import static it.bitsplitters.legendstracker.service.MessageUtil.warningMessage;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_BAN_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_BAN_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_BAN_TAG_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_BAN_TAG_NAME;

import java.util.List;
import java.util.Locale;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.legendstracker.Config;
import it.bitsplitters.legendstracker.slashcommands.SlashBundle;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
public class CmdTCPlayerBan extends TCSlashComand {

	@Override
	public PermissionLevel permissionLevel() {
		return PermissionLevel.BOT_ADMIN;
	}
	
	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(optRequired(OptionType.STRING, TC_BAN_TAG_NAME, TC_BAN_TAG_DESCRIPTION, locale));
	}

	@Override
	public boolean isEphemeral() {
		return true;
	}

	@Override
	public void run(SlashCommandEvent event) {
		
		Config.guildsManager.findRecord(event.getGuild().getIdLong()).ifPresent(guild -> {
			
			if(isntActiveOrConfiguredServer(event, guild)) 
				return;

			String tagBanned = event.getOption(TC_BAN_TAG_NAME).getAsString();
			
			if(getBanCorrection().add(tagBanned))
				okMessage(event, SlashBundle.TROPHIES_CHALLENGE_OK_BAN, tagBanned);
			else
				warningMessage(event, SlashBundle.TROPHIES_CHALLENGE_KO_BAN, tagBanned);
		});
		
	}

	@Override
	public String helpKey() {
		return TC_BAN_DESCRIPTION;
	}

	@Override
	public String nameKey() {
		return TC_BAN_NAME;
	}

}
