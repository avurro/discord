package it.bitsplitters.legendstracker.slashcommands.guild;

import java.util.List;

import it.bitsplitters.legendstracker.slashcommands.LegendsTrackerSlashCommand;
import it.bitsplitters.legendstracker.slashcommands.SlashBundle;

public abstract class GuildSlashCommand extends LegendsTrackerSlashCommand {

	@Override
	public List<String> pathKeys() {
		return List.of(SlashBundle.GUILD_NAME);
	}

}
