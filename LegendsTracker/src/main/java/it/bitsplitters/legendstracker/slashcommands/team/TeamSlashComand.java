package it.bitsplitters.legendstracker.slashcommands.team;

import java.util.List;

import it.bitsplitters.legendstracker.slashcommands.CmdUnlink;
import it.bitsplitters.legendstracker.slashcommands.SlashBundle;

public abstract class TeamSlashComand extends CmdUnlink{

	@Override
	public List<String> pathKeys() {
		return List.of(SlashBundle.TEAM_NAME);
	}
}
