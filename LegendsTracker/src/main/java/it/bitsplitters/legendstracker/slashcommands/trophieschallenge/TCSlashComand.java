package it.bitsplitters.legendstracker.slashcommands.trophieschallenge;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import it.bitsplitters.legendstracker.slashcommands.LegendsTrackerSlashCommand;
import it.bitsplitters.legendstracker.slashcommands.SlashBundle;

public abstract class TCSlashComand extends LegendsTrackerSlashCommand{

	private static Set<String> banCorrection = new HashSet<>();
	private static Map<String, Integer> atksCorrection = new HashMap<>();
	private static Map<String, Integer> numAtksCorrection = new HashMap<>();
	
	@Override
	public List<String> pathKeys() {
		return List.of(SlashBundle.TC_NAME);
	}

	public Set<String> getBanCorrection() {
		return banCorrection;
	}
	
	public boolean isBanned(String tag) {
		return banCorrection.contains(tag);
	}
	
	public static Map<String, Integer> getAtksCorrection() {
		return atksCorrection;
	}

	public static Map<String, Integer> getNumAtksCorrection() {
		return numAtksCorrection;
	}
}
