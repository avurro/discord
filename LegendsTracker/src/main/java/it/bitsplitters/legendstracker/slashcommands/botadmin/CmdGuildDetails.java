package it.bitsplitters.legendstracker.slashcommands.botadmin;

import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;
import static java.time.format.DateTimeFormatter.ofPattern;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.jooq.SelectWhereStep;
import org.jooq.types.ULong;

import com.google.common.collect.Lists;

import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.jooq.models.tables.Guilds;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.util.BotUtil;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdGuildDetails extends BotAdminSlashCommand {

	@Override
	public String name(Locale locale) {
		return "guild-details";
	}

	@Override
	public String description(Locale locale) {
		return "Mostra i dettagli degli abbonati";
	}

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(new OptionData(OptionType.STRING, "guild-id", "ID del server, in sua assenza sara stampato tutto", false));
	}

	@Override
	public void run(SlashCommandEvent event) {
		
		OptionMapping opt = event.getOption("guild-id");
		
		Long guildID = opt == null ? null : Long.valueOf(opt.getAsString());
		
		SelectWhereStep<GuildsRecord> selectGuilds = DB.jooq().selectFrom(Guilds.GUILDS);
		
		if(guildID != null)
			selectGuilds.where(Guilds.GUILDS.ID_GUILD.eq(ULong.valueOf(guildID)));
		
		selectGuilds.orderBy(Guilds.GUILDS.PAY_END.desc());
		
		List<MessageEmbed> embeds = new ArrayList<>();
		
		for(GuildsRecord guild: selectGuilds.fetch()) {
			
			String endDate = guild.getPayEnd() != null ? guild.getPayEnd().format(ofPattern("dd-MMM-yyyy")) : "N/A";
			String maxProfiles = guild.getProfilesMax().toString();
			Integer trackedProfiles = (Integer) DB.jooq().selectCount().from(GUILDS_PLAYERS).where(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild())).fetchOne(0);
			
			Guild guildById = event.getJDA().getGuildById(guild.getIdGuild().longValue());
			
			String guildName = guildById != null ? guildById.getName() : "bot espulso";
			
			long roleID = guild.getIdRoleReserved().longValue();
			String role = roleID == 0 ? "N/A" : guildById.getRoleById(roleID) == null ? roleID+"" : guildById.getRoleById(roleID).getName();
			
			String infoMsg = String.join(" ", 
					          "guild name:",guildName,
					          "\nguild id:",guild.getIdGuild().longValue()+"",
							  "\nrole defined:", role,
						      "\nprofiles:",trackedProfiles+"/"+maxProfiles,
						      "\nend date:",endDate);
			
			embeds.add(BotUtil.infoEmbedBuilder(infoMsg).build());
		}
		
		for (List<MessageEmbed> sublist : Lists.partition(embeds, 10)) 
			event.getHook().sendMessageEmbeds(sublist).queue();
		
	}
	
	@Override
	public boolean isEphemeral() {
		return true;
	}
}