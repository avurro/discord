package it.bitsplitters.legendstracker.slashcommands.trophieschallenge;

import static it.bitsplitters.legendstracker.Config.isntActiveOrConfiguredServer;
import static it.bitsplitters.legendstracker.service.MessageUtil.okMessage;
import static it.bitsplitters.legendstracker.service.MessageUtil.warningMessage;
import static it.bitsplitters.legendstracker.service.PlayersLog.getTrophiesLog;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_FIX_ATK_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_FIX_ATK_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_FIX_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_FIX_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_FIX_TAG_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_FIX_TAG_NAME;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_FIX_TROPHIES_DESCRIPTION;
import static it.bitsplitters.legendstracker.slashcommands.SlashBundle.TC_FIX_TROPHIES_NAME;

import java.util.List;
import java.util.Locale;

import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.legendsdata.models.PlayerData;
import it.bitsplitters.legendstracker.Config;
import it.bitsplitters.legendstracker.slashcommands.SlashBundle;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class CmdTCFixes extends TCSlashComand {

	@Override
	public PermissionLevel permissionLevel() {
		return PermissionLevel.BOT_ADMIN;
	}

	@Override
	public List<OptionData> options(Locale locale) {
		return List.of(
				optRequired(OptionType.STRING, TC_FIX_TAG_NAME, TC_FIX_TAG_DESCRIPTION, locale),
				optRequired(OptionType.INTEGER, TC_FIX_TROPHIES_NAME, TC_FIX_TROPHIES_DESCRIPTION, locale),
				opt(OptionType.INTEGER, TC_FIX_ATK_NAME, TC_FIX_ATK_DESCRIPTION, locale)
		);
	}

	@Override
	public void run(SlashCommandEvent event) {

		Config.guildsManager.findRecord(event.getGuild().getIdLong()).ifPresent(guild -> {
			
			if(isntActiveOrConfiguredServer(event, guild)) 
				return;

			String tag = event.getOption(TC_FIX_TAG_NAME).getAsString();
			Long trophies = event.getOption(TC_FIX_TROPHIES_NAME).getAsLong();
			
			OptionMapping attacks = event.getOption(TC_FIX_ATK_NAME);
			
			PlayerData playerData = getTrophiesLog().get(tag);
			
			if(playerData == null) {
				warningMessage(event, SlashBundle.TROPHIES_CHALLENGE_KO_PLAYER, tag);
				return;
			}
			
			getAtksCorrection().put(tag, trophies.intValue());
			
			if(attacks != null)
				getNumAtksCorrection().put(tag, Long.valueOf(attacks.getAsLong()).intValue());
			
			okMessage(event, SlashBundle.TROPHIES_CHALLENGE_OK_FIX);
		});
	}

	@Override
	public boolean isEphemeral() {
		return true;
	}

	@Override
	public String helpKey() {
		return TC_FIX_DESCRIPTION;
	}

	@Override
	public String nameKey() {
		return TC_FIX_NAME;
	}
}