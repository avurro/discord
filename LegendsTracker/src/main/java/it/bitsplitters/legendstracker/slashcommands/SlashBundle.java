package it.bitsplitters.legendstracker.slashcommands;

public class SlashBundle {

	//-------------------------
	// GENERIC MESSAGES
	//-------------------------
	public static String GENERIC_TAG_NOTEXIST = "generic.tag.notexist";
	public static String GENERIC_TEAM_NOTEXIST = "generic.team.notexist";
	public static String GENERIC_ACCOUNTS_FULL = "generic.accounts.full";
	public static String GENERIC_ACCOUNTS_SPACE_INSUFFICIENT = "generic.accounts.space.insufficient";
	public static String GENERIC_ACCOUNTS_EXIST = "generic.accounts.exist";
	public static String GENERIC_ACCOUNTS_ALREADY_TRACKED = "generic.accounts.alreadytracked";
	
	
	//-------------------------
	// TROPHIES CHALLENGE
	//-------------------------
	public static String TC_NAME = "tc.name";
	public static String TC_DESCRIPTION = "tc.description";

	// |[CMD -> CHECK ATTACKS]|
	public static String TC_CA_NAME="tc.ca.name";
	public static String TC_CA_DESCRIPTION="tc.ca.description";
	public static String TC_CA_TEAM_NAME="tc.ca.opt.team.name";
	public static String TC_CA_TEAM_DESCRIPTION="tc.ca.opt.team.description";
	
	
	// |[CMD -> BAN]|
	public static String TC_BAN_NAME="tc.ban.name";
	public static String TC_BAN_DESCRIPTION="tc.ban.description";
	public static String TC_BAN_TAG_NAME="tc.ban.opt.tag.name";
	public static String TC_BAN_TAG_DESCRIPTION="tc.ban.opt.tag.description";
	
	// |[CMD -> UNBAN]|
	public static String TC_UNBAN_NAME="tc.unban.name";
	public static String TC_UNBAN_DESCRIPTION="tc.unban.description";
	public static String TC_UNBAN_TAG_NAME="tc.unban.opt.tag.name";
	public static String TC_UNBAN_TAG_DESCRIPTION="tc.unban.opt.tag.description";
	
	// |[CMD -> FIX]|
	public static String TC_FIX_NAME="tc.fix.name";
	public static String TC_FIX_DESCRIPTION="tc.fix.description";
	public static String TC_FIX_TAG_NAME="tc.fix.opt.tag.name";
	public static String TC_FIX_TAG_DESCRIPTION="tc.fix.opt.tag.description";
	public static String TC_FIX_TROPHIES_NAME="tc.fix.opt.trophies.name";
	public static String TC_FIX_TROPHIES_DESCRIPTION="tc.fix.opt.trophies.description";
	public static String TC_FIX_ATK_NAME="tc.fix.opt.atk.name";
	public static String TC_FIX_ATK_DESCRIPTION="tc.fix.opt.atk.description";
		
	// |[CMD -> STANDINGS]|
	public static String TC_STANDINGS_NAME="tc.standings.name";
	public static String TC_STANDINGS_DESCRIPTION="tc.standings.description";
	public static String TC_STANDINGS_TEAM_NAME="tc.standings.opt.team.name";
	public static String TC_STANDINGS_TEAM_DESCRIPTION="tc.standings.opt.team.description";
	public static String TC_STANDINGS_RANK_NAME="tc.standings.opt.rank.name";
	public static String TC_STANDINGS_RANK_DESCRIPTION="tc.standings.opt.rank.description";
		
		
	public static String TROPHIES_CHALLENGE_HELP_FIXES = "tc.help.fixes";
	public static String TROPHIES_CHALLENGE_HELP_BAN = "tc.help.ban";
	public static String TROPHIES_CHALLENGE_KO_PLAYER = "tc.ko.player";
	public static String TROPHIES_CHALLENGE_OK_FIX ="tc.ok";
	public static String TROPHIES_CHALLENGE_OK_GLOBAL_CHECK = "tc.ok.globalcheck";
	public static String TROPHIES_CHALLENGE_OK_UNBAN ="tc.ok.unban";
	public static String TROPHIES_CHALLENGE_KO_UNBAN ="tc.ko.unban";
	public static String TROPHIES_CHALLENGE_OK_BAN ="tc.ok.ban";
	public static String TROPHIES_CHALLENGE_KO_BAN ="tc.ko.ban";
	public static String TROPHIES_CHALLENGE_HELP_STANDINGS = "tc.help.standings";
	public static String TROPHIES_CHALLENGE_NUMBERLOW = "tc.numberlow";
	public static String TROPHIES_CHALLENGE_DISCARDED = "tc.discarded";
	public static String TROPHIES_CHALLENGE_CLOSED_GROUPS = "tc.closedgroups";
	public static String TROPHIES_CHALLENGE_CLOSED_ENTRYLEVEL = "tc.closedentrylevel";
	public static String TROPHIES_CHALLENGE_FOOTER = "tc.footer";		
	public static String TROPHIES_CHALLENGE_NOWINNERS = "tc.nowinners";
	
	//-------------------------
	// HELP COMMAND
	//-------------------------
	public static String HELP_NAME = "help.name";
	public static String HELP_DESCRIPTION = "help.description";
	
	//-------------------------
	// PLAYER COMMANDS
	//-------------------------
	
	// |[CMD -> PLAYER]|
	public static String PLAYER_NAME = "player.name";
	public static String PLAYER_DESCRIPTION = "player.description";

	// |[SUBCMD -> LINK]|
	public static String PLAYER_LINK_NAME = "player.link.name";
	public static String PLAYER_LINK_DESCRIPTION = "player.link.description";
	public static String PLAYER_LINK_TAG_NAME = "player.link.opt.tag.name";
	public static String PLAYER_LINK_TAG_DESCRIPTION = "player.link.opt.tag.description";
	public static String PLAYER_LINK_TEAM_NAME = "player.link.opt.team.name";
	public static String PLAYER_LINK_TEAM_DESCRIPTION = "player.link.opt.team.description";
	public static String PLAYER_LINK_OK = "player.link.ok";
	
	// |[SUBCMD -> UNLINK]|
	public static String PLAYER_UNLINK_NAME = "player.unlink.name";
	public static String PLAYER_UNLINK_DESCRIPTION = "player.unlink.description";
	public static String PLAYER_UNLINK_TAG_NAME = "player.unlink.opt.tag.name";
	public static String PLAYER_UNLINK_TAG_DESCRIPTION = "player.unlink.opt.tag.description";
	public static String PLAYER_UNLINK_TAG_NOTEXIST = "player.state.err.tag.notexist";
	public static String PLAYER_UNLINK_OK = "player.unlink.ok";
	
	// |[SUBCMD -> STATE]|
	public static String PLAYER_STATE_NAME = "player.state.name";
	public static String PLAYER_STATE_DESCRIPTION = "player.state.description";
	public static String PLAYER_STATE_TAG_NAME = "player.state.opt.tag.name";
	public static String PLAYER_STATE_TAG_DESCRIPTION = "player.state.opt.tag.description";
	public static String PLAYER_STATE_TAG_NOTEXIST = "player.state.err.tag.notexist";
	
	// |[SUBCMD -> MOVE]|
	public static String PLAYER_MOVE_NAME = "player.move.name";
	public static String PLAYER_MOVE_DESCRIPTION = "player.move.description";
	public static String PLAYER_MOVE_TAG_NAME = "player.move.opt.tag.name";
	public static String PLAYER_MOVE_TAG_DESCRIPTION = "player.move.opt.tag.description";
	public static String PLAYER_MOVE_TEAM_NAME = "player.move.opt.team.name";
	public static String PLAYER_MOVE_TEAM_DESCRIPTION = "player.move.opt.team.description";
	public static String PLAYER_MOVE_TAG_NOTEXIST = "player.move.err.tag.notexist";
	public static String PLAYER_MOVE_TAG_EXIST = "player.move.err.tag.exist";
	public static String PLAYER_MOVE_OK = "player.move.ok";
	
	
	//-------------------------
	// TEAM COMMANDS
	//-------------------------
	
	// |[CMD -> TEAM]|
	public static String TEAM_NAME = "team.name";
	public static String TEAM_DESCRIPTION = "team.description";

	// |[SUBCMD -> LINK]|
	public static String TEAM_LINK_NAME = "team.link.name";
	public static String TEAM_LINK_DESCRIPTION = "team.link.description";
	public static String TEAM_LINK_TAG_NAME = "team.link.opt.tag.name";
	public static String TEAM_LINK_TAG_DESCRIPTION = "team.link.opt.tag.description";
	public static String TEAM_LINK_NEW_NAME_NAME = "team.link.opt.newteam.name";
	public static String TEAM_LINK_NEW_NAME_DESCRIPTION = "team.link.opt.newteam.description";
	public static String TEAM_LINK_NUM_TO_LOAD_NAME = "team.link.opt.numtoload.name";
	public static String TEAM_LINK_NUM_TO_LOAD_DESCRIPTION = "team.link.opt.numtoload.description";
	public static String TEAM_LINK_TAG_EXIST = "team.link.err.tag.exist";
	public static String TEAM_LINK_TAG_EXIST_INTERNAL = "team.link.err.tag.exist.internal";
	public static String TEAM_LINK_OK = "team.link.ok";
	
	// |[SUBCMD -> UNLINK]|
	public static String TEAM_UNLINK_NAME = "team.unlink.name";
	public static String TEAM_UNLINK_DESCRIPTION = "team.unlink.description";
	public static String TEAM_UNLINK_NAME_NAME = "team.unlink.opt.name.name";
	public static String TEAM_UNLINK_NAME_DESCRIPTION = "team.unlink.opt.name.description";
	public static String TEAM_UNLINK_OK = "team.unlink.ok";
	
	// |[SUBCMD -> STATE]|
	public static String TEAM_STATE_NAME = "team.state.name";
	public static String TEAM_STATE_DESCRIPTION = "team.state.description";
	public static String TEAM_STATE_NAME_NAME = "team.state.opt.name.name";
	public static String TEAM_STATE_NAME_DESCRIPTION = "team.state.opt.name.description";
	public static String TEAM_STATE_NUM_TO_LOAD_NAME = "team.state.opt.numtoload.name";
	public static String TEAM_STATE_NUM_TO_LOAD_DESCRIPTION = "team.state.opt.numtoload.description";
	public static String TEAM_STATE_NOTEXIST = "team.state.err.notexist";
	public static String TEAM_STATE_INFO_LEGEND = "team.state.info.legend";
	
	// |[SUBCMD -> RENAME]|
	public static String TEAM_RENAME_NAME = "team.rename.name";
	public static String TEAM_RENAME_DESCRIPTION = "team.rename.description";
	public static String TEAM_RENAME_OLDNAME_NAME = "team.rename.opt.oldname.name";
	public static String TEAM_RENAME_OLDNAME_DESCRIPTION = "team.rename.opt.oldname.description";
	public static String TEAM_RENAME_NEWNAME_NAME = "team.rename.opt.newname.name";
	public static String TEAM_RENAME_NEWNAME_DESCRIPTION = "team.rename.opt.newname.description";
	public static String TEAM_RENAME_OK = "team.rename.ok";
	public static String TEAM_RENAME_SIZE = "team.rename.err.size";
	public static String TEAM_RENAME_NOTEXIST = "team.rename.err.notexist";
	
	// |[SUBCMD -> LIST]|	
	public static String TEAM_LIST_NAME = "team.list.name";
	public static String TEAM_LIST_DESCRIPTION = "team.list.description";
	
	//-------------------------
	// GENERAL COMMANDS
	//-------------------------
	
	// |[CMD -> GENERAL]|
	public static String GENERAL_NAME = "general.name";
	public static String GENERAL_DESCRIPTION = "general.description";
	
	// |[SUBCMD -> CLEAN]|
	public static String GENERAL_CLEAN_NAME = "general.clean.name";
	public static String GENERAL_CLEAN_DESCRIPTION = "general.clean.description";
	public static String GENERAL_CLEAN_OK = "general.clean.ok";
	public static String GENERAL_CLEAN_LIMIT_NAME = "general.clean.opt.limit.name";
	public static String GENERAL_CLEAN_LIMIT_DESCRIPTION = "general.clean.opt.limit.description";
	public static String GENERAL_CLEAN_TEAM_NAME = "general.clean.opt.team.name";
	public static String GENERAL_CLEAN_TEAM_DESCRIPTION = "general.clean.opt.team.description";
	public static String GENERAL_CLEAN_TEAM_KO_BADTAG = "general.err.team.badtag";
	public static String GENERAL_CLEAN_LIMIT_KO_NOPLAYERS = "general.err.limit.noplayers";
	public static String GENERAL_CLEAN_TAG_NOTEXIST = "general.err.tag.notexist";
	
	// |[SUBCMD -> DETAILS]|
	public static String GENERAL_DETAILS_NAME = "general.details.name";
	public static String GENERAL_DETAILS_DESCRIPTION = "general.details.description";
	public static String GENERAL_DETAILS_TYPE_NAME = "general.details.opt.type.name";
	public static String GENERAL_DETAILS_TYPE_DESCRIPTION = "general.details.opt.type.description";
	public static String GENERAL_DETAILS_TEAM_NAME = "general.details.opt.team.name";
	public static String GENERAL_DETAILS_TEAM_DESCRIPTION = "general.details.opt.team.description";
	public static String GENERAL_DETAILS_TEAM_KO_BADTAG = "general.details.err.team.badtag";
	public static String GENERAL_DETAILS_TAGDETAILS = "general.details.err.tagdetails";
	public static String GENERAL_DETAILS_TAGDETAILS_TEAM = "general.details.err.tagdetails.team";
	
	//-------------------------
	// LEGENDS COMMANDS
	//-------------------------
	
	// |[CMD -> GUILD]|
	public static String GUILD_NAME = "guild.name";
	public static String GUILD_DESCRIPTION = "guild.description";
	
	// |[SUBCMD -> CHANNEL LOG]|
	public static String GUILD_CHANNELLOG_NAME = "guild.channellog.name";
	public static String GUILD_CHANNELLOG_DESCRIPTION = "guild.channellog.description";
	public static String GUILD_CHANNELLOG_CHANNEL_NAME = "guild.channellog.opt.channel.name";
	public static String GUILD_CHANNELLOG_CHANNEL_DESCRIPTION = "guild.channellog.opt.channel.description";
	public static String GUILD_CHANNELLOG_OK = "guild.channellog.ok";
	public static String GUILD_CHANNELLOG_KO_CANTTALK = "guild.channellog.ko.canttalk";

	// |[SUBCMD -> CHANNEL COMS]|
	public static String GUILD_CHANNELCOMS_NAME = "guild.channelcoms.name";
	public static String GUILD_CHANNELCOMS_DESCRIPTION = "guild.channelcoms.description";
	public static String GUILD_CHANNELCOMS_CHANNEL_NAME = "guild.channelcoms.opt.channel.name";
	public static String GUILD_CHANNELCOMS_CHANNEL_DESCRIPTION = "guild.channelcoms.opt.channel.description";
	public static String GUILD_CHANNELCOMS_OK = "guild.channelcoms.ok";
	public static String GUILD_CHANNELCOMS_KO_CANTTALK = "guild.channelcoms.ko.canttalk";
	
	// |[SUBCMD -> ROLE]|
	public static String GUILD_ROLE_NAME = "guild.role.name";
	public static String GUILD_ROLE_DESCRIPTION = "guild.role.description";
	public static String GUILD_ROLE_ROLE_NAME = "guild.role.opt.role.name";
	public static String GUILD_ROLE_ROLE_DESCRIPTION = "guild.role.opt.role.description";
	public static String GUILD_ROLE_OK = "guild.role.ok";

	// |[SUBCMD -> STATUS]|
	public static String GUILD_STATUS_NAME = "guild.status.name";
	public static String GUILD_STATUS_DESCRIPTION = "guild.status.description";
	public static String GUILD_STATUS_OK = "guild.status.ok";
		
	
	
	
	
	
	public static String GUILD_HELP_ROLES = "guild.help.roles";
	public static String GUILD_SUBSCRIPTION_INACTIVE = "guild.subscription.inactive";
	public static String GUILD_CONFIG_CHANNEL_LOG = "guild.config.channellog";
	public static String GUILD_CONFIG_CHANNEL_COMS = "guild.config.channelcoms";
	
	public static String BOTCROSS_MSG_DATAACCESS = "bot.cross.dataaccess";
	public static String BOTCROSS_MSG_MAINTENANCE = "bot.cross.maintenance";
	public static String BOTCROSS_MSG_SERVER_NOT_FOUND = "server.notfound";
	
	public static String TEAM_NOTEXIST = "team.notexist";
	
}
