package it.bitsplitters.legendstracker.resourcebundle;

import static it.bitsplitters.util.ResourceBundleUtil.customControl;
import static java.util.ResourceBundle.getBundle;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class ResourceBundleLegendsTracker {
	
	public static final String GENERIC_TAG_NOTFOUND = "control.tag.notfound";
	public static final String GENERIC_APP_BADREQUEST ="control.app.badrequest";  
	public static final String GENERIC_APP_AUTHERROR ="control.app.autherror";   
	public static final String GENERIC_APP_RATELIMIT ="control.app.ratelimit";  	
	public static final String GENERIC_APP_UNKNOWNERROR ="control.app.unknown";
	
	public static final String LOG = "generic.logenabled";
	public static final String TIMING = "generic.timingenabled";
	public static final String RATELIMIT = "generic.ratelimit";
	public static final String LOAD = "generic.guildload";
	
	public static final String RESOURCE_FULLNAME = "resource";

	public static final String TRACKED_PLAYERS = "debug.trackedplayers";
	
	public static final String GENERIC_APP_TIMEOUT ="control.app.timeout";
	public static final String GENERIC_TAG_NOTEXIST = "generic.tag.notexist";
	public static final String GENERIC_TEAM_NOTEXIST = "generic.team.notexist";
	public static final String GENERIC_SERVER_UNDERLOAD = "generic.server.underload";
	public static final String GENERIC_SUB_INVALID = "generic.guild.notvalidpayer";
	public static final String GENERIC_CHANNEL_NOTFOUND = "generic.channel.notfound";
	public static final String GENERIC_CHANNEL_DOCKED = "generic.channel.docked";
	public static final String SEND_MESSAGES = "command.help.send";
	public static final String SET_TEMPERATURA = "command.help.temperature";
	
	public static final String CLEAN = "command.help.clean";
	public static final String CLEAN_NOTFOUND = "command.msg.clean.notfound";
	public static final String CLEAN_NOTFOUND_TEAM = "command.msg.clean.notfoundteam";
	public static final String CLEAN_OK = "command.ok.clean";
	
	public static final String GUILDS_NEW = "command.help.guildsnew";
	public static final String GUILDS_NEW_NOTEXIST = "command.msg.guildsnew.wrongid";
	public static final String GUILDS_NEW_OK = "command.ok.guildsnew";
	
	public static final String GUILDS_ACCOUNTS = "command.help.guildsaccounts";
	public static final String GUILDS_ACCOUNTS_OK = "command.ok.guildsaccounts";
	
	public static final String GUILDS_ADDDAYS = "command.help.guildadddays";
	public static final String GUILDS_MONTHS = "command.help.guildsmonths";
	public static final String GUILDS_MONTHS_OK = "command.ok.guildsmonths";
	
	public static final String GUILDS_ROLE = "command.help.guildsrole";
	public static final String GUILDS_ROLE_MULTY = "command.msg.guildrole.many";
	public static final String GUILDS_ROLE_NOTEXIST = "command.msg.guildrole.notexist";
	public static final String GUILDS_ROLE_OK = "command.ok.guildrole";
	
	public static final String GUILDS_SUBS = "command.help.guildsubs";
	
	public static final String GUILDS_LOG = "command.help.guildslog";
	public static final String GUILDS_MSG_CANTTALK = "command.msg.guildslog.canttalk";
	public static final String GUILDS_COMS = "command.help.guildscoms";
	public static final String GUILDS_MSG_NOLOGCHANNEL = "command.msg.guildscoms.nologchannel";
	public static final String GUILDS_MSG_NOCLEAREMOJI ="command.msg.guildscoms.nocleanemoji";
	
	public static final String ACCOUNTS_ADD = "command.help.addtag";
	public static final String ACCOUNTS_ADD_MSG_LOG_OB = "command.msg.addtag.channellog";
	public static final String ACCOUNTS_ADD_MSG_COMS_OB = "command.msg.addtag.channelcoms";
	public static final String ACCOUNTS_ADD_MSG_MAX = "command.msg.addtag.max";
	public static final String ACCOUNTS_ADD_MSG_EXIST = "command.msg.addtag.exist";
	public static final String ACCOUNTS_ADD_MSG_EXIST_INTERNAL = "command.msg.addtag.exist.internal";
	public static final String ACCOUNTS_ADD_MSG_EXIST_PLAYER = "command.msg.addtag.exist.player";
	public static final String ACCOUNTS_ADD_MSG_EXIST_PLAYER_INTERNAL = "command.msg.addtag.exist.player.internal";
	public static final String ACCOUNTS_ADD_MSG_SPACE = "command.msg.addteam.space";
	public static final String ACCOUNTS_ADD_OK = "command.ok.addtag";
	
	public static final String TEAMS_ADD = "command.help.addteam";
	public static final String TEAMS_NAME = "command.msg.newname";
	
	public static final String ACCOUNTS_REMOVE = "command.help.removetag";
	public static final String ACCOUNTS_REMOVE_MSG_NOTEXIST = "command.msg.removetag.notexist";
	public static final String ACCOUNTS_REMOVE_OK = "command.ok.removetag";
	
	public static final String TEAMS_REMOVE = "command.help.removeteam";
	public static final String TEAMS_REMOVE_OK = "command.ok.removeteam";
	
	public static final String ACCOUNTS_MOVE = "command.help.edittag";
	public static final String ACCOUNTS_MOVE_OK = "command.ok.edittag";
	public static final String ACCOUNTS_MOVE_NOTEXIST = "command.msg.edittag.notexist";
	public static final String ACCOUNTS_MOVE_TEAM_NOTEXIST = "command.msg.editteam.notexist";
	public static final String ACCOUNTS_MOVE_TEAM_OK = "command.ok.editteam";
	public static final String ACCOUNTS_MOVE_TEAM_MSG_PLAYEREXIST = "command.msg.editteam.playerexist";
	public static final String ACCOUNTS_RENAME_NOTEXIST = "command.msg.renametag.notexist";
	public static final String ACCOUNTS_RENAME = "command.help.rename";
	public static final String ACCOUNTS_RENAME_OK = "command.ok.rename";
	
	public static final String TEAMS_RENAME = "command.help.editteam";
	public static final String TEAMS_STATE = "command.help.state.team";
	public static final String TEAMS_STATE_NOTEXIST = "command.msg.stateteam.notexist";
	public static final String TEAMS_LINK_OK = "command.ok.addteam";
	public static final String TEAMS_DESCRIPTION = "command.msg.stateteam.legend";
			
	public static final String ACCOUNTS_DETAILS = "command.help.tagdetails";
	public static final String ACCOUNTS_DETAILS_KO = "command.ko.tagdetails";
	public static final String ACCOUNTS_DETAILS_KO_TEAM = "command.ko.tagdetails.team";

	public static final String STATE = "command.help.state";
	public static final String STATE_KO_PLAYER = "command.ko.state.player";
	
	public static final String GUILDS_STATUS = "command.help.guildsstatus";
	public static final String GUILDS_STATUS_OK = "command.ok.guildsstatus";
	public static final String GUILDS_STATUS_NOTEXIST = "command.msg.guildsstatus.notexist";
	public static final String GUILDS_STATUS_NOTACTIVE = "command.msg.guildsstatus.notactive";
	
	public static final String GUILDS_PREFIX = "command.help.guildsprefix";
	public static final String GUILDS_PREFIX_LENGTH ="command.msg.guildsprefix.length";
	public static final String GUILDS_PREFIX_OK = "command.ok.guildsprefix";
	
	public static final String GUILDS_LANG = "command.help.guildslanguage";
	public static final String GUILDS_LANG_NOTEXIST = "command.msg.guildslanguage.error";
	public static final String GUILDS_LANG_OK = "command.ok.guildslanguage";
	
	public static final String GUILDS_TEAMS = "command.help.guildslist";
	
	public static final String INFO = "command.help.info";
	
	public static final String START = "command.help.start";
	
	public static final String STOP = "command.help.stop";
	
	public static final String HOUR = "command.help.hour";
	
	public static final String DUMP = "command.help.dump";
	public static final String SHUTDOWN = "command.help.shutdown";
	public static final String RESET = "command.help.reset";
	public static final String BOT_MNT = "command.help.maintenance";
	public static final String BOT_MNT_OK = "command.ok.bot.maintenance";
	public static final String GENERIC_APP_MAINTENANCE ="control.app.maintenance";

	public static final String GUILD_CLEAN = "command.help.guild.clean";
	
	public static final String TROPHIES_CHALLENGE = "command.help.trophieschallenge";
	public static final String TROPHIES_CHALLENGE_NUM_INSUFFICIENT = "command.ko.trophieschallenge.insufficient";
	public static final String TROPHIES_CHALLENGE_CLOSED_GROUPS = "command.msg.trophieschallenge.closedgroups";
	public static final String TROPHIES_CHALLENGE_CLOSED_ENTRYLEVEL = "command.msg.trophieschallenge.entrylevel";
	public static final String TROPHIES_CHALLENGE_DISCARDED_PLAYERS = "command.msg.trophieschallenge.discarded";
	public static final String TROPHIES_CHALLENGE_EDIT_OK = "command.ok.trophieschallenge.editok";
	public static final String TROPHIES_CHALLENGE_FOOTER = "command.msg.trophieschallenge.footer";
	public static final String TROPHIES_CHALLENGE_FIX = "command.help.trophieschallenge.fix";
	
	public static enum Languages { ENG(Locale.ENGLISH), ITA(Locale.ITALY); 
		
		private Locale locale;
		
		private Languages(Locale locale) {
			this.locale = locale;
		}
		
		public String get () {
			return this.locale.getLanguage();
		}
		
		public Locale locale() {
			return this.locale;
		}
		
		public static List<String> labels = Arrays.asList("it","en");
	};
	
	
	private static Map<Locale, ResourceBundle> guildsRB = new HashMap<>();
	
	public static ResourceBundle getResourceBundle(Locale locale) {
		
		if(guildsRB.containsKey(locale))
			return guildsRB.get(locale);
		
		ResourceBundle bundle = getBundle(RESOURCE_FULLNAME, locale, customControl);
		
		guildsRB.put(locale, bundle);
		
		return bundle;
	}
	
	
}
