package it.bitsplitters.legendstracker;

import static it.bitsplitters.legendstracker.Config.guildsManager;

import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.SlashCommandsManager;
import it.bitsplitters.SlashEasyBotManager;
import it.bitsplitters.command.SlashCommand;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendstracker.slashcommands.BotStructure;
import it.bitsplitters.setting.SlashBotConfig;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
public class Bot extends ListenerAdapter
{
	
	public static JDA jda;

	private static final Logger logger = LogManager.getLogger();

	private static BotStructure structure;
	
	private static SlashBotConfig slashBotConfig;
	
	private static SlashCommandsManager slashCommandsManager; 
	
	private final SlashEasyBotManager slashBotManager;
	
	public Bot() {

	    slashCommandsManager = (SlashCommand command, SlashCommandEvent event) -> 
		{
			if(command.permissionLevel().isPublic()) 
				return true;

			if(event.getMember().hasPermission(Permission.ADMINISTRATOR))
				return command.permissionLevel().isAdmin() || command.permissionLevel().isReserved();
			
			
			if(command.permissionLevel().isReserved()) {
				
				Optional<GuildsRecord> optGuild = guildsManager.findRecord(event.getGuild().getIdLong());
				
				if(optGuild.isEmpty()) {
					guildsManager.register(event.getGuild().getIdLong());
					return false;
				}
				
				long roleId = optGuild.get().getIdRoleReserved().longValue();
				 
				if(roleId == 0) 
					return false;
				
				return event.getMember().getRoles().contains(event.getGuild().getRoleById(roleId));
			}

			return false;
		};
		
		slashBotConfig = () -> Arrays.asList(Config.ALEX);
		
		Config.defaultLocale = slashBotConfig.defaultLocale();
		
		structure = new BotStructure(slashBotConfig.defaultLocale());
		
	    slashBotManager = new SlashEasyBotManager(structure.pathCommandsMap(), slashCommandsManager, slashBotConfig);
	}
	
	public void startBattlefield() {
		// Start tracking players
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        executorService.execute(new BattlefieldLogging());
     	
        logger.atInfo().log("Started battlefield!");
	}
    
	@Override
    public void onGuildLeave(GuildLeaveEvent event) {
    	logger.atInfo().log("\n-------------\nServer uscito! {}\n-------------\n", event.getGuild().getName());
    	guildsManager.guildLeave(event.getGuild().getIdLong());
    	
    }
    
    @Override
    public void onGuildJoin(GuildJoinEvent event) {
    	logger.atInfo().log("\n-------------\nNuovo server! {}\n-------------\n", event.getGuild().getName());
    	guildsManager.guildJoin(event.getGuild().getIdLong());
    }
    
    @Override
    public void onSlashCommand(SlashCommandEvent event) {
		this.slashBotManager.findAndRunGuildCommand(event);
    }
    
    public BotStructure getStructure() {
    	return structure;
    }
}
