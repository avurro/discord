package it.bitsplitters.legendstracker.service;

import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;
import static it.bitsplitters.legendsdata.jooq.models.tables.Players.PLAYERS;
import static it.bitsplitters.legendstracker.service.PlayersLog.getTrophiesLog;

import java.util.ArrayList;
import java.util.List;

import org.jooq.Record1;

import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendsdata.models.PlayerData;

public class TeamService {

	
	public static boolean exists(GuildsRecord guild, String team) {
		return DB.jooq().fetchExists(GUILDS_PLAYERS, GUILDS_PLAYERS.TEAM.eq(team)
                .and(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild())));
	}
	
	public static List<PlayerData> getTeam(GuildsRecord guild, String teamName){
		List<PlayerData> team = new ArrayList<>();
		
		for (Record1<String> player : DB.jooq().select(GUILDS_PLAYERS.players().TAG)
									.from(GUILDS_PLAYERS)
									.where(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild())
								    .and(GUILDS_PLAYERS.TEAM.eq(teamName))).fetch()) {

			PlayerData playerData = getTrophiesLog().get(player.get(PLAYERS.TAG));
			
			if(playerData != null)
				team.add(playerData);
			
		}
		
		return team;
	}
}
