package it.bitsplitters.legendstracker.service;

import static it.bitsplitters.legendstracker.Config.guildsManager;
import static it.bitsplitters.legendstracker.Bot.jda;
import static it.bitsplitters.legendstracker.resourcebundle.ResourceBundleLegendsTracker.GUILDS_MSG_NOCLEAREMOJI;
import static it.bitsplitters.legendstracker.resourcebundle.ResourceBundleLegendsTracker.getResourceBundle;
import static net.dv8tion.jda.internal.utils.PermissionUtil.checkPermission;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.ygimenez.method.Pages;
import com.github.ygimenez.model.Page;
import com.google.common.collect.Lists;

import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendsdata.models.GuildChannels;
import it.bitsplitters.legendsdata.models.PlayerInfo;
import it.bitsplitters.legendsdata.models.PlayerLog;
import it.bitsplitters.legendstracker.Config;
import it.bitsplitters.util.BotUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Emoji;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.exceptions.InsufficientPermissionException;
public class DiscordLogWriterService {

	private static final String index = "<:index:926415579009650728>";
	
	private static final String bf1 = "<:1bf:950919117769162772>";
	private static final String bf2 = "<:2bf:950919117848850433>";
	private static final String bf3 = "<:3bf:950919117764972584>";
	private static final String bf4 = "<:4bf:950919118477983827>";
	private static final String bf5 = "<:5bf:950919117974683668>";
	private static final String bf6 = "<:6bf:950919117836263424>";
	private static final String bf7 = "<:7bf:950919117924356177>";
	private static final String bf8 = "<:8bf:950919117676892202>";
	private static final String bf9 = "<:9bf:950919117777563690>";
	private static final String bf10 = "<:10bf:950919117869838337>";
	
	private static final Map<Integer, String> logNumbers = Map.of(1, bf1, 2, bf2, 3, bf3, 4, bf4, 5, bf5, 6, bf6, 7, bf7, 8, bf8, 9, bf9, 10, bf10);
	
	private static final String bfe1 = "<:1bfe:950916554801950781>";
	private static final String bfe2 = "<:2bfe:950916554709688400>";
	private static final String bfe3 = "<:3bfe:950916554734850059>";
	private static final String bfe4 = "<:4bfe:950916554671931413>";
	private static final String bfe5 = "<:5bfe:950916554516758539>";
	private static final String bfe6 = "<:6bfe:950916554244108341>";
	private static final String bfe7 = "<:7bfe:950916554604814346>";
	private static final String bfe8 = "<:8bfe:950916554739052554>";
	private static final String bfe9 = "<:9bfe:950916554223157339>";
	private static final String bfe10 = "<:10bfe:950916554940370944>";
	
	private static final Map<Integer, String> emojiNumbers = Map.of(1, bfe1, 2, bfe2, 3, bfe3, 4, bfe4, 5, bfe5, 6, bfe6, 7, bfe7, 8, bfe8, 9, bfe9, 10, bfe10);
	
	private static Map<GuildChannels, ArrayDeque<Long>> playerMsg = new HashMap<>();
	private static final int MAX_MSGS = 12;
	private static final int MAX_LOG_FORPOST = 10;
	private static final Logger logger = LogManager.getLogger();
	
	public static void writeChannels(Map<GuildChannels, Set<PlayerInfo>> lastData) {

		for (GuildChannels guildChannels : lastData.keySet()) {
			
			if(guildChannels.getLogChannel() == null) continue;
					
			TextChannel logChannel = jda.getTextChannelById(guildChannels.getLogChannel());
			
			if(logChannel == null || !logChannel.canTalk()) continue;
			
			Member member = logChannel.getGuild().getMember(jda.getSelfUser());
			Set<PlayerInfo> players = lastData.get(guildChannels);

			if(!checkPermission(logChannel, member, Permission.MESSAGE_ADD_REACTION)) 
				logChannel.sendMessage((Message) getLog(players)).queue();
			
			else {
				
				List<List<PlayerInfo>> playersPartitioned = Lists.partition(new ArrayList<PlayerInfo>(players), MAX_LOG_FORPOST);
				for (List<PlayerInfo> list : playersPartitioned) {
				
					MessageEmbed firstPage = getLogEmbed(list);
					
					Map<Emoji, Page> categories = new LinkedHashMap<>();
					categories.put(Emoji.fromMarkdown(index),new Page(firstPage));
					
					for (int i = 0; i < list.size(); i++) 
						categories.put(Emoji.fromMarkdown(emojiNumbers.get(i+1)), new Page(getEmbedPlayerDetails(guildChannels.getGuildID(), list.get(i))));
					
					logChannel.sendMessageEmbeds(firstPage).queue(
							
							s -> {
								Pages.categorize(s, categories, false);
								cleanMessages(logChannel, guildChannels, s);
							},
							
							e -> {
								
								logChannel.sendMessageEmbeds(firstPage).queueAfter(1, TimeUnit.SECONDS, 
								s -> {
									Pages.categorize(s, categories, false);
									cleanMessages(logChannel, guildChannels, s);
								});
							}
					);	
				}
			}
		}
	}
	
	public static void cleanAllMessages() {
		for(GuildChannels guild: playerMsg.keySet()) {
			ArrayDeque<Long> messages = playerMsg.get(guild);
			if(messages == null) continue;
			
			for (Long message : messages) {
				jda.getTextChannelById(guild.getLogChannel()).retrieveMessageById(message)
					.queue(oldMsg -> {
						Pages.getHandler().removeEvent(oldMsg);
						oldMsg.clearReactions().submit();
					});
			}
		}
	}
	private static void cleanMessages(TextChannel logChannel, GuildChannels guildChannels, Message msg) {
		
		ArrayDeque<Long> messages = playerMsg.get(guildChannels);
		
		if(messages == null) 
			messages = new ArrayDeque<Long>(MAX_MSGS);
		
		Long oldMessageID = messages.size() >= MAX_MSGS ? messages.removeLast() : null;
		
		messages.addFirst(msg.getIdLong());
		
		playerMsg.put(guildChannels, messages);
		
		if(oldMessageID == null) return;
		
		logChannel.retrieveMessageById(oldMessageID).queue(oldMsg -> {
			Pages.getHandler().removeEvent(oldMsg);
			try {
				oldMsg.clearReactions().submit();
			}catch (InsufficientPermissionException  e) {
				
				TextChannel comsChannel = jda.getTextChannelById(guildChannels.getComsChannel());
				if(comsChannel != null) {
					
					Optional<GuildsRecord> optRecord = guildsManager.findRecord(guildChannels.getGuildID());
					if(!optRecord.isPresent()) return;

					GuildsRecord guild = optRecord.get();
					
					MessageEmbed embed = BotUtil.warningEmbedBuilder(getResourceBundle(Locale.forLanguageTag(guild.getLocale())).getString(GUILDS_MSG_NOCLEAREMOJI)).build();
					comsChannel.sendMessageEmbeds(embed).queue();
				}
				
				logger.atError().log("RIMOZIONE EMOJI FALLITA - PERMESSI INSUFFICIENTI - {}", msg.getGuild().getId());
			}
		});
		
	}
	
	public static MessageEmbed getEmbedPlayerDetails(Long guildID, PlayerInfo player)	{
 		
 		EmbedBuilder eb = new EmbedBuilder();
		eb.setColor(Config.legendsColor);
		eb.setTitle(BotUtil.escapeContent(player.getName()));
		eb.setDescription(player.getState());
		eb.setFooter(PlayersInfo.getPlayerTeam(guildID, player.getTag()));
 		return eb.build();
 	}
	
	private static MessageEmbed getLogEmbed(Collection<PlayerInfo> players) {
		
		EmbedBuilder log = new EmbedBuilder();
		StringJoiner sj = new StringJoiner("\n");
		int pos = 1;
		for (PlayerInfo playerInfo : players) {
			PlayerLog plt = playerInfo.getLastData();
			sj.add(String.join(" ", logNumbers.get(pos), plt.getLastLog(), Config.getStateLink(plt.getName(), plt.getTag())));
			pos++;
		}
		log.setDescription(sj.toString());
		log.setColor(Config.legendsColor);
		return log.build();
	}
	
	private static Message getLog(Set<PlayerInfo> players) {
		MessageBuilder log = new MessageBuilder();
		StringJoiner sj = new StringJoiner("\n");
		for (PlayerInfo playerInfo : players) {
			PlayerLog info = playerInfo.getLastData();
			sj.add(String.join("", info.getLastLog(), "**|** ", info.getTag(), " **"+BotUtil.escapeContent((info.getName()))+"**"));
		}
		log.setContent(sj.toString());
		return log.build();
	}
}