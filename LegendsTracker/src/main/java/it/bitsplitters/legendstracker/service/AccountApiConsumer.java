package it.bitsplitters.legendstracker.service;

/**
 * @author AVurro
 *
 * @param <Event> Reference to the user request
 * @param <API> Account data retrieved from API call
 */
@FunctionalInterface
public interface AccountApiConsumer<Event, API> {

    /**
     * Performs this operation on the given arguments.
     * @param event
     * @param apiData
     */
    void accept(Event event, API apiData);
}
