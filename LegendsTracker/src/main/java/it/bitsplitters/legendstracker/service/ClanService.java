package it.bitsplitters.legendstracker.service;

import static it.bitsplitters.legendstracker.service.MessageUtil.exHandling;

import java.util.function.Consumer;

import it.bitsplitters.clashapi.Clan;
import it.bitsplitters.legendstracker.COC;
import it.bitsplitters.legendstracker.Config;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class ClanService {

	public static void findAPIClan(MessageReceivedEvent event, String tag, Consumer<Clan> consumer) {
		findAPIClan(event, tag, 0, consumer, it.bitsplitters.legendstracker.Config.sendLoading(event));
	}
		
	private static void findAPIClan(MessageReceivedEvent event, String tag, int numberTry, Consumer<Clan> consumer, Message loading) {
		
		COC.API.requestAsyncClan(tag)
		.whenCompleteAsync((clanData, playerEx) -> {
			
			if(playerEx != null)
				if(numberTry < Config.NUMBER_TRY) {
					findAPIClan(event, tag, numberTry+1, consumer, loading);
					return;
				}else {
					loading.delete().queue();
					exHandling(event, playerEx, tag);
					return;
				}
					
			loading.delete().queue();
			consumer.accept(clanData);
		});
	}
}
