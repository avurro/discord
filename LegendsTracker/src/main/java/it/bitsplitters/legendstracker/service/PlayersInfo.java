package it.bitsplitters.legendstracker.service;

import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;
import static it.bitsplitters.legendsdata.jooq.models.tables.Players.PLAYERS;

import java.util.concurrent.CompletableFuture;

import org.jooq.types.ULong;

import it.bitsplitters.clashapi.Player;
import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendstracker.COC;
import it.bitsplitters.legendstracker.Config;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class PlayersInfo {

	public static String getPlayerTeam(Long guildID, String playerTag) {
		return DB.jooq().select(GUILDS_PLAYERS.TEAM)
				.from(GUILDS_PLAYERS).naturalJoin(PLAYERS)
				.where(GUILDS_PLAYERS.ID_GUILD.eq(ULong.valueOf(guildID))
				.and(PLAYERS.TAG.eq(playerTag))).fetchOne(GUILDS_PLAYERS.TEAM);
	}
	
	public static boolean isPlayerTracked(GuildsRecord guild, String playerTag) {
		return DB.jooq().fetchExists(
				DB.jooq().select(GUILDS_PLAYERS.fields())
					.from(GUILDS_PLAYERS).naturalJoin(PLAYERS)
					.where(GUILDS_PLAYERS.ID_GUILD.eq(guild.getIdGuild())
					.and(PLAYERS.TAG.eq(playerTag)))
			   );
	}
	
	public static CompletableFuture<Player> findAPIAccount(MessageReceivedEvent event, String tag, AccountApiConsumer<MessageReceivedEvent, Player> consumer) {
		return findAPIAccount(event, tag, 0, consumer, Config.sendLoading(event, tag));
	}
		
	private static CompletableFuture<Player> findAPIAccount(MessageReceivedEvent event, String tag, int numberTry, AccountApiConsumer<MessageReceivedEvent, Player> consumer, Message loading) {
		
		return COC.API.requestAsyncPlayer(tag)
		.whenComplete((apiAccount, playerEx) -> {
			
			if(playerEx != null)
				if(numberTry < Config.NUMBER_TRY) {
					findAPIAccount(event, tag, numberTry+1, consumer, loading);
					return;
				}else {
					loading.delete().queue();
					MessageUtil.exHandling(event, playerEx, tag);
					return;
				}
					
			loading.delete().queue();
			consumer.accept(event, apiAccount);
			
		});
	}
	
	public static CompletableFuture<Player> findAPIAccount(SlashCommandEvent event, String tag, AccountApiConsumer<SlashCommandEvent, Player> consumer) {
		return findAPIAccount(event, tag, 0, consumer);
	}
		
	private static CompletableFuture<Player> findAPIAccount(SlashCommandEvent event, String tag, int numberTry, AccountApiConsumer<SlashCommandEvent, Player> consumer) {
		
		return COC.API.requestAsyncPlayer(tag)
		.whenComplete((apiAccount, playerEx) -> {
			
			if(playerEx != null)
				if(numberTry < Config.NUMBER_TRY) {
					findAPIAccount(event, tag, numberTry+1, consumer);
					return;
				}else {
					MessageUtil.exHandling(event, playerEx, tag);
					return;
				}
					
			consumer.accept(event, apiAccount);
			
		});
	}
}