package it.bitsplitters.legendstracker.service;

import static it.bitsplitters.legendsdata.jooq.models.tables.Guilds.GUILDS;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.ignite.IgniteCache;

import it.bitsplitters.legends.batch.service.PlayersService;
import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.models.GuildChannels;
import it.bitsplitters.legendsdata.models.PlayerData;
import it.bitsplitters.legendsdata.service.SrvGuild;
import it.bitsplitters.legendsdata.service.SrvPlayer;

public class GuildService {

	
	public static Set<Long> activeGuildsID() {
		return DB.jooq().select(GUILDS.ID_GUILD)
		 .from(GUILDS)
		 .where(GUILDS.PAY_END.ge(DB.localDateNow()))
		 .fetch(GUILDS.ID_GUILD)
		 .stream().map(id -> id.longValue()).collect(Collectors.toSet());
	}
	
	public static void loadGuild(Long guildID) {
		//TODO inglobare in un computable future così da renderlo asincrono
		IgniteCache<String, PlayerData> trophiesLog = Cache.playersData();
		Optional<GuildChannels> guildChannels = SrvGuild.getOptGuild(guildID);
		
		for(String tag: SrvPlayer.playersTrackedBySubActiveGuild(guildID)) {

			PlayerData playerData = trophiesLog.get(tag);
			
			if(playerData == null)
				playerData = PlayersService.getPlayer(tag);
			
			PlayersLog.addPlayer(playerData, guildChannels.get());
		}
	}
}