package it.bitsplitters.legendstracker.service;

import static it.bitsplitters.legendstracker.Config.BLANK;
import static java.lang.Integer.parseInt;

/**
 * This service takes care of loading, updating and constructs the rankings.
 * 
 * @author AVurro
 *
 */
public class PrinterService {

	
	public static String trophiesToEmoji(int trophies) {
		return trophies<100 ? emojiDecinaCentinaia(trophies, false)
							: trophies<1000 ? centinaiaToEmoji(trophies)
											: migliaiaToEmoji(trophies);
	}

	private static String migliaiaToEmoji(int number) {
		String strNumber = number + "";
		
		String migliaia = strNumber.substring(0,strNumber.length()-3);
		String centinaia = strNumber.substring(strNumber.length()-3);
		
		return emojiMigliaia(parseInt(migliaia)) + centinaiaToEmoji(centinaia, true);
	}
	
	private static String centinaiaToEmoji(String centinaia, boolean isMigliaia) {
		boolean isNegative = false;
		
		if(centinaia.startsWith("-")) {
			centinaia = centinaia.substring(1);
			isNegative = true;
		}
		
		String decineCentinaia = centinaia.substring(0,centinaia.length()-1);
		String unita = centinaia.substring(centinaia.length()-1);
		int n100 = decineCentinaia.isEmpty() ? -1 : parseInt(decineCentinaia);
		String str = isMigliaia ? emojiDecinaCentinaiaMigliaia(n100) : emojiDecinaCentinaia(n100,isNegative);
		return str + emojiUnita(parseInt(unita)) ;
	}
	
	public static String centinaiaToEmoji(int centinaia) {
		return centinaiaToEmoji(centinaia + "", false);
	}
	
	public static String unitaToEmoji(int unita) {
		return emojiUnita(unita);
	}
	
	public static String spAttacks(int number) {
		switch (number) {
			case 0: return"<:cl0A:926415328890724393>";
			case 1: return"<:cl1A:926415328878149704>";
			case 2: return"<:cl2A:926415328806834196>";
			case 3: return"<:cl3A:926415328999796736>";
			case 4: return"<:cl4A:926415328811053056>";
			case 5: return"<:cl5A:926415328903331840>";
			case 6: return"<:cl6A:926415328819441684>";
			case 7: return"<:cl7A:926415328865570838>";
			case 8:
			default: return"<:cl8A:926415329104633866>";
		}
	}
	
	public static String spDefenses(int number) {
		switch (number) {
			case 0: return"<:cl0D:926415328756531210>";
			case 1: return"<:cl1D:926415328827813913>";
			case 2: return"<:cl2D:926415329029136404>";
			case 3: return"<:cl3D:926415328811040768>";
			case 4: return"<:cl4D:926415328928489512>";
			case 5: return"<:cl5D:926415328802635776>";
			case 6: return"<:cl6D:926415328920104990>";
			case 7: return"<:cl7D:926415328899108864>";
			case 8: 
			default:return"<:cl8D:926415328597123123>";
		}
	}
	
	private static String emojiMigliaia(int number) {
		switch(number) {
			case 4: return"<:clcds4:926418977066975272>";
			case 5: return"<:clcds5:926418976668540929>";
			case 6: return"<:clcds6:926418977066979348>";
			case 7: return"<:clcds7:926418976911814656>";
			case 20: return"<:cldt20:926418977100550164>";
			case 21: return"<:cldt21:926418976668524565>";
			case 22: return"<:cldt22:926418976945373184>";
			case 23: return"<:cldt23:926418976936980480>";
			case 24: return"<:cldt24:926418976660131851>";
			case 25: return"<:cldt25:926418976970539098>";
			case 26: return"<:cldt26:926418977264136202>";
			case 27: return"<:cldt27:926418976781787157>";
			case 28: return"<:cldt28:926418976639184987>";
			case 29: return"<:cldt29:926418977159282688>";
			case 30: return"<:cldt30:926418977033449484>";
			case 31: return"<:cldt31:926418976634974259>";
			case 32: return"<:cldt32:926418977117315102>";
			case 33: return"<:cldt33:926418977012453426>";
			case 34: return"<:cldt34:926418976949538907>";
			case 35: return"<:cldt35:926418977066975292>";
			case 50: return"<:50:926415578971926568>";
			case 51: return"<:51:926415578955145236>";
			case 52: return"<:52:926415578980315147>";
			case 53: return"<:53:926415578753814540>";
			case 54: return"<:54:926415579198423070>";
			case 55: return"<:55:926415578988687431>";
			case 56: return"<:56:926415579051614318>";
			case 57: return"<:57:926415578594418719>";
			case 58: return"<:58:926415578799931435>";
			case 59: return"<:59:926415579068370984>";
			case 60: return"<:60:926415578854457395>";
			case 61: return"<:61:926415579185831956>";
			case 62: return"<:62:926415579668176896>";
			default: return emojiDecinaCentinaia(number, false);
		}
	}
	
	private static String emojiUnita(int pos) {
		switch (pos) {
			case 0: return "<:clps0:926418976853090305> ";
			case 1: return " <:clps1:926418977066975274>";
			case 2: return "<:clps2:926418976874041366> ";
			case 3: return "<:clps3:926418977180237884> ";
			case 4: return "<:clps4:926418976806928437> ";
			case 5: return "<:clps5:926418977180233738> ";
			case 6: return "<:clps6:926418977264123904> ";
			case 7: return "<:clps7:926418977243156510> ";
			case 8: return "<:clps8:926418977255723018> ";
			case 9: return "<:clps9:926418977167654913> ";
			default: return BLANK;
		}
	}
	
	private static String emojiDecinaCentinaiaMigliaia(int pos) {
		switch (pos) {
			case 0: return"<:clpd00:926418977037643816>";
			case 1: return"<:clpd01:926418977176059914>";
			case 2: return"<:clpd02:926418976752427050>";
			case 3: return"<:clpd03:926418977385754635>";
			case 4: return"<:clpd04:926418977146687558>";
			case 5: return"<:clpd05:926418977100550144>";
			case 6: return"<:clpd06:926418977205399552>";
			case 7: return"<:clpd07:926418977163464724>";
			case 8: return"<:clpd08:926418977066975273>";
			case 9: return"<:clpd09:926418977268314123>";
			default: return emojiDecinaCentinaiaStandard(pos);
		}
	}
	
	private static String emojiDecinaCentinaia(int pos, boolean isNegative) {
		
		if(isNegative)
			switch(pos) {
				// se -1 allora se è negativo metterci meno.
				case -1:return"<:nsign:926415579051593758>";
				case 1: return"<:n1:926415578695077940>";
				case 2: return"<:n2:926415579072565248>";
				case 3: return"<:n3:926415579135488011>";
				case 4: return"<:n4:926415579106148402>";
				case 5: return"<:n5:926415579131314186>";
				case 6: return"<:n6:926415578799931425>";
				case 7: return"<:n7:926415579043209246>";
				case 8: return"<:n8:926433264288268329>";
				case 9: return"<:n9:926415578636386415>";
				case 10:return"<:n10:926415579160649749>";
				case 11:return"<:n11:926415579148058664>";
				case 12:return"<:n12:926415579122913310>";
				default: return emojiDecinaCentinaiaStandard(pos);
			}
		else
			switch (pos) {
				case -1:return BLANK; 
				case 1: return "<:clpd1:926416167722168332>";
				case 2: return "<:clpd2:926416168024178738>";
				case 3: return "<:clpd3:926416168506519582>";
				case 4: return "<:clpd4:926416168087064576>";
				case 5: return "<:clpd5:926416168103870534>";
				case 6: return "<:clpd6:926416167839604757>";
				case 7: return "<:clpd7:926416168019959819>";
				case 8: return "<:clpd8:926416168158384218>";
				case 9: return "<:clpd9:926416168032559164>";
				default: return emojiDecinaCentinaiaStandard(pos);
			}
	}
	
	private static String emojiDecinaCentinaiaStandard(int pos) {
		switch (pos) {
			case 10: return "<:clpd10:926416167994785802>";
			case 11: return "<:clpd11:926416168112259122>";
			case 12: return "<:clpd12:926416168196112475>";
			case 13: return "<:clpd13:926416168321970256>";
			case 14: return "<:clpd14:926416168204501042>";
			case 15: return "<:clpd15:926416168095469568>";
			case 16: return "<:clpd16:926416168246444032>";
			case 17: return "<:clpd17:926416168082882590>";
			case 18: return "<:clpd18:926416168141590558>";
			case 19: return "<:clpd19:926416168057733151>";
			case 20: return "<:clpd20:926416168112246895>";
			case 21: return "<:clpd21:926416168099672125>";
			case 22: return "<:clpd22:926416167747326024>";
			case 23: return "<:clpd23:926416167944454145>";
			case 24: return "<:clpd24:926416168280002570>";
			case 25: return "<:clpd25:926416168221298708>";
			case 26: return "<:clpd26:926416168129007616>";
			case 27: return "<:clpd27:926416168179363841>";
			case 28: return "<:clpd28:926416168200306728>";
			case 29: return "<:clpd29:926416168108044308>";
			case 30: return "<:clpd30:926416168007401474>";
			case 31: return "<:clpd31:926416168003199068>";
			case 32: return "<:clpd32:926416168384864256>";
			case 33: return "<:clpd33:926416167910916108>";
			case 34: return "<:clpd34:926416168116437032>";
			case 35: return "<:clpd35:926416168196141056>";
			case 36: return "<:clpd36:926416168091271168>";
			case 37: return "<:clpd37:926416168158384198>";
			case 38: return "<:clpd38:926416167927681035>";
			case 39: return "<:clpd39:926416168217108490>";
			case 40: return "<:clpd40:926416168103862333>";
			case 41: return "<:clpd41:926416168145801256>";
			case 42: return "<:clpd42:926416168187723776>";
			case 43: return "<:clpd43:926416168158367756>";
			case 44: return "<:clpd44:926416168242253855>";
			case 45: return "<:clpd45:926416168137424916>";
			case 46: return "<:clpd46:926416168166776852>";
			case 47: return "<:clpd47:926416168330362930>";
			case 48: return "<:clpd48:926416167931895839>";
			case 49: return "<:clpd49:926439285601759232>";
			case 50: return "<:clpd50:926416168032538665>";
			case 51: return "<:clpd51:926418122041339935>";
			case 52: return "<:clpd52:926418122141999104>";
			case 53: return "<:clpd53:926418121701593129>";
			case 54: return "<:clpd54:926418122049716274>";
			case 55: return "<:clpd55:926418122045546586>";
			case 56: return "<:clpd56:926418122049724476>";
			case 57: return "<:clpd57:926418122095861781>";
			case 58: return "<:clpd58:926418122142011442>";
			case 59: return "<:clpd59:926418122146205776>";
			case 60: return "<:clpd60:926418122116829224>";
			case 61: return "<:clpd61:926418122058104862>";
			case 62: return "<:clpd62:926418122448203796>";
			case 63: return "<:clpd63:926418122095857705>";
			case 64: return "<:clpd64:926418122100060200>";
			case 65: return "<:clpd65:926418122074898442>";
			case 66: return "<:clpd66:926418122112655390>";
			case 67: return "<:clpd67:926418122142007326>";
			case 68: return "<:clpd68:926418122318172160>";
			case 69: return "<:clpd69:926418121751937055>";
			case 70: return "<:clpd70:926418122121035826>";
			case 71: return "<:clpd71:926418122125238322>";
			case 72: return "<:clpd72:926418122171367464>";
			case 73: return "<:clpd73:926418122339123200>";
			case 74: return "<:clpd74:926418122154602506>";
			case 75: return "<:clpd75:926418122418835546>";
			case 76: return "<:clpd76:926418122305593354>";
			case 77: return "<:clpd77:926418122121035807>";
			case 78: return "<:clpd78:926418121907122188>";
			case 79: return "<:clpd79:926418121915531295>";
			case 80: return "<:clpd80:926418122162970674>";
			case 81: return "<:clpd81:926418122301403136>";
			case 82: return "<:clpd82:926418122238468146>";
			case 83: return "<:clpd83:926418122376896542>";
			case 84: return "<:clpd84:926418122154594406>";
			case 85: return "<:clpd85:926418122343346177>";
			case 86: return "<:clpd86:926418122221695017>";
			case 87: return "<:clpd87:926418122267852810>";
			case 88: return "<:clpd88:926418121915510835>";
			case 89: return "<:clpd89:926418122402045973>";
			case 90: return "<:clpd90:926418122183954432>";
			case 91: return "<:clpd91:926418122469158932>";
			case 92: return "<:clpd92:926418122041339966>";
			case 93: return "<:clpd93:926418122183946261>";
			case 94: return "<:clpd94:926418122343350342>";
			case 95: return "<:clpd95:926418122246868992>";
			case 96: return "<:clpd96:926418122519494657>";
			case 97: return "<:clpd97:926418122469150730>";
			case 98: return "<:clpd98:926418122225909810>";
			case 99: return "<:clpd99:926418122263642163>";
			default: return BLANK;
		}
	}
}