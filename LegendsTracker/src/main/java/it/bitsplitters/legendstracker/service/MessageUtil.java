package it.bitsplitters.legendstracker.service;

import static it.bitsplitters.legendstracker.resourcebundle.ResourceBundleLegendsTracker.GENERIC_APP_AUTHERROR;
import static it.bitsplitters.legendstracker.resourcebundle.ResourceBundleLegendsTracker.GENERIC_APP_BADREQUEST;
import static it.bitsplitters.legendstracker.resourcebundle.ResourceBundleLegendsTracker.GENERIC_APP_MAINTENANCE;
import static it.bitsplitters.legendstracker.resourcebundle.ResourceBundleLegendsTracker.GENERIC_APP_RATELIMIT;
import static it.bitsplitters.legendstracker.resourcebundle.ResourceBundleLegendsTracker.GENERIC_APP_TIMEOUT;
import static it.bitsplitters.legendstracker.resourcebundle.ResourceBundleLegendsTracker.GENERIC_TAG_NOTFOUND;
import static it.bitsplitters.util.BotUtil.errorEmbed;
import static it.bitsplitters.util.BotUtil.infoEmbed;
import static it.bitsplitters.util.BotUtil.okEmbed;
import static it.bitsplitters.util.BotUtil.stopEmbed;
import static it.bitsplitters.util.BotUtil.unauthorizedEmbed;
import static it.bitsplitters.util.BotUtil.warningEmbed;
import static it.bitsplitters.util.BotUtil.codeEmbed;
import static it.bitsplitters.util.ResourceBundleUtil.replaceValues;

import java.net.http.HttpConnectTimeoutException;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.clashapi.exception.AuthenticationException;
import it.bitsplitters.clashapi.exception.BadRequestException;
import it.bitsplitters.clashapi.exception.MaintenanceException;
import it.bitsplitters.clashapi.exception.NotFoundException;
import it.bitsplitters.clashapi.exception.RateLimitExceededException;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendstracker.Config;
import net.dv8tion.jda.api.events.interaction.SelectionMenuEvent;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class MessageUtil {
	
	private static final Logger logger = LogManager.getLogger();
	
	public static Optional<String> getMsg(GuildsRecord guild, String labelKey) {
		return Optional.of(Config.getMsg(guild, labelKey));
	}
	
	public static Optional<String> getMsg(MessageReceivedEvent event, String labelKey) {
		Optional<GuildsRecord> guild = Config.guildsManager.findRecord(event.getGuild().getIdLong());
		if(!guild.isPresent()) return Optional.empty();
		return Optional.of(Config.getMsg(guild.get(), labelKey));
	}
	
	public static Optional<String> getMsg(SlashCommandEvent event, String labelKey) {
		Optional<GuildsRecord> guild = Config.guildsManager.findRecord(event.getGuild().getIdLong());
		if(!guild.isPresent()) return Optional.empty();
		return Optional.of(Config.getSlashMsg(guild.get(), labelKey));
	}
	
	public static void stopMessage(SlashCommandEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> stopEmbed(event, replaceValues(msg, values)));
	}
	
	public static void stopMessage(MessageReceivedEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> stopEmbed(event, replaceValues(msg, values)));
	}
	
	public static void unauthorizedMessage(SlashCommandEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> unauthorizedEmbed(event, replaceValues(msg, values)));
	}
	
	public static void unauthorizedMessage(MessageReceivedEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> unauthorizedEmbed(event, replaceValues(msg, values)));
	}
	
	public static void infoMessage(SlashCommandEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> infoEmbed(event, replaceValues(msg, values)));
	}
	
	public static void infoMessage(MessageReceivedEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> infoEmbed(event, replaceValues(msg, values)));
	}
	
	public static void warningMessage(SlashCommandEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> warningEmbed(event, replaceValues(msg, values)));
	}
	
	public static void warningMessage(MessageReceivedEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> warningEmbed(event, replaceValues(msg, values)));
	}
	
	public static void okMessage(SlashCommandEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> okEmbed(event, replaceValues(msg, values)));
	}
	
	public static void okMessage(GuildsRecord guild, SelectionMenuEvent event, String labelKey, String... values) {
		getMsg(guild, labelKey)
		.ifPresent(msg -> okEmbed(event, replaceValues(msg, values)));
	}
	
	public static void okMessage(MessageReceivedEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> okEmbed(event, replaceValues(msg, values)));
	}
	
	public static void errorMessage(SlashCommandEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> errorEmbed(event, replaceValues(msg, values)));
	}
	
	public static void errorMessage(MessageReceivedEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> errorEmbed(event, replaceValues(msg, values)));
	}
	
	public static void codeMessage(SlashCommandEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> codeEmbed(event, replaceValues(msg, values)));
	}
	
	public static boolean exHandling(SlashCommandEvent event, Throwable ex, String tag) {
		
		if(ex.getCause() == null) {
			timeout(event, tag);
			return false;
		}
		
		// 400
		if(ex.getCause() instanceof BadRequestException) {
			badRequest(event);
			logger.atError().log(ex.toString());
		// 403
		}else if(ex.getCause() instanceof AuthenticationException) {
			authError(event);
			logger.atError().log(ex.toString());
		// 404
		}else if(ex.getCause() instanceof NotFoundException) {
			tagNotFound(event, tag);
			logger.atError().log("Tag player errato: {}", tag);
		// 429
		}else if(ex.getCause() instanceof RateLimitExceededException) {
			rateLimit(event);
			logger.atError().log(ex.toString());
		// 503
		}else if(ex.getCause() instanceof MaintenanceException) {
			maintenance(event);
			logger.atError().log(ex.toString());
		}else if(ex.getCause() instanceof HttpConnectTimeoutException) {
			timeout(event, tag);
			logger.atError().log(ex.toString());
		}else{
			unknownError(event);
			logger.atError().log(ex.toString());
		}
		
		return true;
	}
	
	public static boolean exHandling(MessageReceivedEvent event, Throwable ex, String tag) {
		
		if(ex.getCause() == null) {
			timeout(event, tag);
			return false;
		}
		
		// 400
		if(ex.getCause() instanceof BadRequestException) {
			badRequest(event);
			logger.atError().log(ex.toString());
		// 403
		}else if(ex.getCause() instanceof AuthenticationException) {
			authError(event);
			logger.atError().log(ex.toString());
		// 404
		}else if(ex.getCause() instanceof NotFoundException) {
			tagNotFound(event, tag);
			logger.atError().log("Tag player errato: {}", tag);
		// 429
		}else if(ex.getCause() instanceof RateLimitExceededException) {
			rateLimit(event);
			logger.atError().log(ex.toString());
		// 503
		}else if(ex.getCause() instanceof MaintenanceException) {
			maintenance(event);
			logger.atError().log(ex.toString());
		}else if(ex.getCause() instanceof HttpConnectTimeoutException) {
			timeout(event, tag);
			logger.atError().log(ex.toString());
		}else{
			unknownError(event);
			logger.atError().log(ex.toString());
		}
		
		return true;
	}

	private static void tagNotFound(SlashCommandEvent event, String tag) {
		getMsg(event, GENERIC_TAG_NOTFOUND)
		.ifPresent(msg -> errorEmbed(event, replaceValues(msg, tag)));
	}
	
	private static void badRequest(SlashCommandEvent event) {
		getMsg(event, GENERIC_APP_BADREQUEST)
		.ifPresent(msg -> errorEmbed(event, msg));
	}
	
	private static void authError(SlashCommandEvent event) {
		getMsg(event, GENERIC_APP_AUTHERROR)
		.ifPresent(msg -> errorEmbed(event, msg));
	}
	
	private static void rateLimit(SlashCommandEvent event) {
		getMsg(event, GENERIC_APP_RATELIMIT)
		.ifPresent(msg -> warningEmbed(event, msg));
	}
	
	private static void maintenance(SlashCommandEvent event) {
		getMsg(event, GENERIC_APP_MAINTENANCE)
		.ifPresent(msg -> warningEmbed(event, msg));
	}
	
	private static void unknownError(SlashCommandEvent event) {
		getMsg(event, GENERIC_APP_TIMEOUT)
		.ifPresent(msg -> errorEmbed(event, msg));
	}
	
	private static void timeout(SlashCommandEvent event, String tag) {
		errorMessage(event, GENERIC_APP_TIMEOUT, tag);
	}
	
	private static void tagNotFound(MessageReceivedEvent event, String tag) {
		getMsg(event, GENERIC_TAG_NOTFOUND)
		.ifPresent(msg -> errorEmbed(event, replaceValues(msg, tag)));
	}
	
	private static void badRequest(MessageReceivedEvent event) {
		getMsg(event, GENERIC_APP_BADREQUEST)
		.ifPresent(msg -> errorEmbed(event, msg));
	}
	
	private static void authError(MessageReceivedEvent event) {
		getMsg(event, GENERIC_APP_AUTHERROR)
		.ifPresent(msg -> errorEmbed(event, msg));
	}
	
	private static void rateLimit(MessageReceivedEvent event) {
		getMsg(event, GENERIC_APP_RATELIMIT)
		.ifPresent(msg -> warningEmbed(event, msg));
	}
	
	private static void maintenance(MessageReceivedEvent event) {
		getMsg(event, GENERIC_APP_MAINTENANCE)
		.ifPresent(msg -> warningEmbed(event, msg));
	}
	
	private static void unknownError(MessageReceivedEvent event) {
		getMsg(event, GENERIC_APP_TIMEOUT)
		.ifPresent(msg -> errorEmbed(event, msg));
	}
	
	private static void timeout(MessageReceivedEvent event, String tag) {
		errorMessage(event, GENERIC_APP_TIMEOUT, tag);
	}
	public static String listToString(List<String> list, String separator, int size) {
		String result = "";
		if(list.size() > size) {
			List<String> subList = list.subList(0, size);
			subList.add("...");
			result = String.join(separator, subList);
		}else {
			result = String.join(separator, list);
		}
		
		return result;
	}
}
