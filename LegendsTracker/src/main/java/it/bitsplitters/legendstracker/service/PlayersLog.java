package it.bitsplitters.legendstracker.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.ignite.IgniteCache;

import it.bitsplitters.legends.batch.logic.PlayersInfoManager;
import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.models.GuildChannels;
import it.bitsplitters.legendsdata.models.PlayerData;
import it.bitsplitters.legendsdata.models.PlayerInfo;
public class PlayersLog {
	
	private static IgniteCache<String, PlayerData> trophiesLog = Cache.playersData();
	private static IgniteCache<String, PlayerInfo> playersInfo = Cache.playersInfo();
	
	public static void addPlayer(PlayerData player, GuildChannels guildChannelsToAdd) {
		
		if(trophiesLog.containsKey(player.getTag())) {
			PlayerData playerData = trophiesLog.get(player.getTag());
			List<GuildChannels> channels = playerData.getGuildChannels();
			if(!channels.contains(guildChannelsToAdd)) {
				channels.add(guildChannelsToAdd);
				trophiesLog.put(playerData.getTag(), playerData);
			}
		
		} else {
			
			List<GuildChannels> channels = new ArrayList<>();
			channels.add(guildChannelsToAdd);
			player.getGuildChannels().addAll(channels);
			
			trophiesLog.put(player.getTag(), player);
			
			PlayerInfo playerInfo = new PlayerInfo(player.getTag(), player.getNickname());
			playerInfo.setState(PlayersInfoManager.getOverview(player));
			
			playersInfo.put(player.getTag(), playerInfo);
			
		}
	}
	
	public static void removePlayer(String tag, Long guildID) {
		
		if(!trophiesLog.containsKey(tag)) return;
		
		PlayerData player = trophiesLog.get(tag);
		player.getGuildChannels().removeIf(guildChannels -> guildChannels.getGuildID().equals(guildID));
		
		if(player.getGuildChannels().isEmpty()) {
			trophiesLog.remove(tag);
			Cache.playersInfo().remove(tag);
			Cache.requestState().remove(tag);
		}
		else
			trophiesLog.put(tag, player);
	}
	
	public static IgniteCache<String, PlayerData> getTrophiesLog(){
		return trophiesLog;
	}

}