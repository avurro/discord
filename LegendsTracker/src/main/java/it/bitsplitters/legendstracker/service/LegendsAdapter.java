package it.bitsplitters.legendstracker.service;

import it.bitsplitters.clashapi.Player;
import it.bitsplitters.legendsdata.models.PlayerData;

public class LegendsAdapter {

	public static PlayerData getPlayer(Player player) {
		return new PlayerData(player.getName(), player.getTag(), player.getTrophies(), player.getDefenseWins(), player.getAttackWins());
	}
}
