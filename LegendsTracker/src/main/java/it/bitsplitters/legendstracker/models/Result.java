package it.bitsplitters.legendstracker.models;

public class Result<T> {

	private Boolean result;
	private T obj;
	
	public Result(Boolean result, T obj) {
		super();
		this.result = result;
		this.obj = obj;
	}
	
	public Boolean isOk() {
		return result;
	}
	public void setResult(Boolean result) {
		this.result = result;
	}
	public T getObj() {
		return obj;
	}
	public void setObj(T obj) {
		this.obj = obj;
	}
	
	
}
