package it.bitsplitters.legendstracker.adapters;

import java.util.Locale;

import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendstracker.models.LegendsGuildSettings;
public class GuildsAdapter {

	public static LegendsGuildSettings toGuild(GuildsRecord record) {
		return new LegendsGuildSettings(record.getIdGuild().longValue(), record.getPrefix(), Locale.forLanguageTag(record.getLocale()));
	}
}
