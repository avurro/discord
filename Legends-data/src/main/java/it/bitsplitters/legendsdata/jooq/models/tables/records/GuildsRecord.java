/*
 * This file is generated by jOOQ.
 */
package it.bitsplitters.legendsdata.jooq.models.tables.records;


import java.time.LocalDate;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record9;
import org.jooq.Row9;
import org.jooq.impl.UpdatableRecordImpl;
import org.jooq.types.ULong;

import it.bitsplitters.legendsdata.jooq.models.tables.Guilds;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class GuildsRecord extends UpdatableRecordImpl<GuildsRecord> implements Record9<ULong, Byte, LocalDate, String, String, Short, ULong, ULong, ULong> {

    private static final long serialVersionUID = 829029071;

    /**
     * Setter for <code>legendstracker.guilds.ID_GUILD</code>.
     */
    public void setIdGuild(ULong value) {
        set(0, value);
    }

    /**
     * Getter for <code>legendstracker.guilds.ID_GUILD</code>.
     */
    public ULong getIdGuild() {
        return (ULong) get(0);
    }

    /**
     * Setter for <code>legendstracker.guilds.PAY_LEVEL</code>.
     */
    public void setPayLevel(Byte value) {
        set(1, value);
    }

    /**
     * Getter for <code>legendstracker.guilds.PAY_LEVEL</code>.
     */
    public Byte getPayLevel() {
        return (Byte) get(1);
    }

    /**
     * Setter for <code>legendstracker.guilds.PAY_END</code>.
     */
    public void setPayEnd(LocalDate value) {
        set(2, value);
    }

    /**
     * Getter for <code>legendstracker.guilds.PAY_END</code>.
     */
    public LocalDate getPayEnd() {
        return (LocalDate) get(2);
    }

    /**
     * Setter for <code>legendstracker.guilds.PREFIX</code>.
     */
    public void setPrefix(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>legendstracker.guilds.PREFIX</code>.
     */
    public String getPrefix() {
        return (String) get(3);
    }

    /**
     * Setter for <code>legendstracker.guilds.LOCALE</code>.
     */
    public void setLocale(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>legendstracker.guilds.LOCALE</code>.
     */
    public String getLocale() {
        return (String) get(4);
    }

    /**
     * Setter for <code>legendstracker.guilds.PROFILES_MAX</code>.
     */
    public void setProfilesMax(Short value) {
        set(5, value);
    }

    /**
     * Getter for <code>legendstracker.guilds.PROFILES_MAX</code>.
     */
    public Short getProfilesMax() {
        return (Short) get(5);
    }

    /**
     * Setter for <code>legendstracker.guilds.ID_CHANNEL_LOG</code>.
     */
    public void setIdChannelLog(ULong value) {
        set(6, value);
    }

    /**
     * Getter for <code>legendstracker.guilds.ID_CHANNEL_LOG</code>.
     */
    public ULong getIdChannelLog() {
        return (ULong) get(6);
    }

    /**
     * Setter for <code>legendstracker.guilds.ID_CHANNEL_COMS</code>.
     */
    public void setIdChannelComs(ULong value) {
        set(7, value);
    }

    /**
     * Getter for <code>legendstracker.guilds.ID_CHANNEL_COMS</code>.
     */
    public ULong getIdChannelComs() {
        return (ULong) get(7);
    }

    /**
     * Setter for <code>legendstracker.guilds.ID_ROLE_RESERVED</code>.
     */
    public void setIdRoleReserved(ULong value) {
        set(8, value);
    }

    /**
     * Getter for <code>legendstracker.guilds.ID_ROLE_RESERVED</code>.
     */
    public ULong getIdRoleReserved() {
        return (ULong) get(8);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<ULong> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record9 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row9<ULong, Byte, LocalDate, String, String, Short, ULong, ULong, ULong> fieldsRow() {
        return (Row9) super.fieldsRow();
    }

    @Override
    public Row9<ULong, Byte, LocalDate, String, String, Short, ULong, ULong, ULong> valuesRow() {
        return (Row9) super.valuesRow();
    }

    @Override
    public Field<ULong> field1() {
        return Guilds.GUILDS.ID_GUILD;
    }

    @Override
    public Field<Byte> field2() {
        return Guilds.GUILDS.PAY_LEVEL;
    }

    @Override
    public Field<LocalDate> field3() {
        return Guilds.GUILDS.PAY_END;
    }

    @Override
    public Field<String> field4() {
        return Guilds.GUILDS.PREFIX;
    }

    @Override
    public Field<String> field5() {
        return Guilds.GUILDS.LOCALE;
    }

    @Override
    public Field<Short> field6() {
        return Guilds.GUILDS.PROFILES_MAX;
    }

    @Override
    public Field<ULong> field7() {
        return Guilds.GUILDS.ID_CHANNEL_LOG;
    }

    @Override
    public Field<ULong> field8() {
        return Guilds.GUILDS.ID_CHANNEL_COMS;
    }

    @Override
    public Field<ULong> field9() {
        return Guilds.GUILDS.ID_ROLE_RESERVED;
    }

    @Override
    public ULong component1() {
        return getIdGuild();
    }

    @Override
    public Byte component2() {
        return getPayLevel();
    }

    @Override
    public LocalDate component3() {
        return getPayEnd();
    }

    @Override
    public String component4() {
        return getPrefix();
    }

    @Override
    public String component5() {
        return getLocale();
    }

    @Override
    public Short component6() {
        return getProfilesMax();
    }

    @Override
    public ULong component7() {
        return getIdChannelLog();
    }

    @Override
    public ULong component8() {
        return getIdChannelComs();
    }

    @Override
    public ULong component9() {
        return getIdRoleReserved();
    }

    @Override
    public ULong value1() {
        return getIdGuild();
    }

    @Override
    public Byte value2() {
        return getPayLevel();
    }

    @Override
    public LocalDate value3() {
        return getPayEnd();
    }

    @Override
    public String value4() {
        return getPrefix();
    }

    @Override
    public String value5() {
        return getLocale();
    }

    @Override
    public Short value6() {
        return getProfilesMax();
    }

    @Override
    public ULong value7() {
        return getIdChannelLog();
    }

    @Override
    public ULong value8() {
        return getIdChannelComs();
    }

    @Override
    public ULong value9() {
        return getIdRoleReserved();
    }

    @Override
    public GuildsRecord value1(ULong value) {
        setIdGuild(value);
        return this;
    }

    @Override
    public GuildsRecord value2(Byte value) {
        setPayLevel(value);
        return this;
    }

    @Override
    public GuildsRecord value3(LocalDate value) {
        setPayEnd(value);
        return this;
    }

    @Override
    public GuildsRecord value4(String value) {
        setPrefix(value);
        return this;
    }

    @Override
    public GuildsRecord value5(String value) {
        setLocale(value);
        return this;
    }

    @Override
    public GuildsRecord value6(Short value) {
        setProfilesMax(value);
        return this;
    }

    @Override
    public GuildsRecord value7(ULong value) {
        setIdChannelLog(value);
        return this;
    }

    @Override
    public GuildsRecord value8(ULong value) {
        setIdChannelComs(value);
        return this;
    }

    @Override
    public GuildsRecord value9(ULong value) {
        setIdRoleReserved(value);
        return this;
    }

    @Override
    public GuildsRecord values(ULong value1, Byte value2, LocalDate value3, String value4, String value5, Short value6, ULong value7, ULong value8, ULong value9) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached GuildsRecord
     */
    public GuildsRecord() {
        super(Guilds.GUILDS);
    }

    /**
     * Create a detached, initialised GuildsRecord
     */
    public GuildsRecord(ULong idGuild, Byte payLevel, LocalDate payEnd, String prefix, String locale, Short profilesMax, ULong idChannelLog, ULong idChannelComs, ULong idRoleReserved) {
        super(Guilds.GUILDS);

        set(0, idGuild);
        set(1, payLevel);
        set(2, payEnd);
        set(3, prefix);
        set(4, locale);
        set(5, profilesMax);
        set(6, idChannelLog);
        set(7, idChannelComs);
        set(8, idRoleReserved);
    }
}
