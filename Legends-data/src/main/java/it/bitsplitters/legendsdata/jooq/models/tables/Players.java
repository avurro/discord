/*
 * This file is generated by jOOQ.
 */
package it.bitsplitters.legendsdata.jooq.models.tables;


import java.util.Arrays;
import java.util.List;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row3;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;

import it.bitsplitters.legendsdata.jooq.models.Keys;
import it.bitsplitters.legendsdata.jooq.models.Legendstracker;
import it.bitsplitters.legendsdata.jooq.models.tables.records.PlayersRecord;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Players extends TableImpl<PlayersRecord> {

    private static final long serialVersionUID = 642169034;

    /**
     * The reference instance of <code>legendstracker.players</code>
     */
    public static final Players PLAYERS = new Players();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<PlayersRecord> getRecordType() {
        return PlayersRecord.class;
    }

    /**
     * The column <code>legendstracker.players.ID_PLAYER</code>.
     */
    public final TableField<PlayersRecord, Short> ID_PLAYER = createField(DSL.name("ID_PLAYER"), org.jooq.impl.SQLDataType.SMALLINT.nullable(false).identity(true), this, "");

    /**
     * The column <code>legendstracker.players.TAG</code>.
     */
    public final TableField<PlayersRecord, String> TAG = createField(DSL.name("TAG"), org.jooq.impl.SQLDataType.VARCHAR(10).nullable(false), this, "");

    /**
     * The column <code>legendstracker.players.NICKNAME</code>.
     */
    public final TableField<PlayersRecord, String> NICKNAME = createField(DSL.name("NICKNAME"), org.jooq.impl.SQLDataType.VARCHAR(20).nullable(false), this, "");

    /**
     * Create a <code>legendstracker.players</code> table reference
     */
    public Players() {
        this(DSL.name("players"), null);
    }

    /**
     * Create an aliased <code>legendstracker.players</code> table reference
     */
    public Players(String alias) {
        this(DSL.name(alias), PLAYERS);
    }

    /**
     * Create an aliased <code>legendstracker.players</code> table reference
     */
    public Players(Name alias) {
        this(alias, PLAYERS);
    }

    private Players(Name alias, Table<PlayersRecord> aliased) {
        this(alias, aliased, null);
    }

    private Players(Name alias, Table<PlayersRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    public <O extends Record> Players(Table<O> child, ForeignKey<O, PlayersRecord> key) {
        super(child, key, PLAYERS);
    }

    @Override
    public Schema getSchema() {
        return Legendstracker.LEGENDSTRACKER;
    }

    @Override
    public Identity<PlayersRecord, Short> getIdentity() {
        return Keys.IDENTITY_PLAYERS;
    }

    @Override
    public UniqueKey<PlayersRecord> getPrimaryKey() {
        return Keys.KEY_PLAYERS_PRIMARY;
    }

    @Override
    public List<UniqueKey<PlayersRecord>> getKeys() {
        return Arrays.<UniqueKey<PlayersRecord>>asList(Keys.KEY_PLAYERS_PRIMARY, Keys.KEY_PLAYERS_TAG);
    }

    @Override
    public Players as(String alias) {
        return new Players(DSL.name(alias), this);
    }

    @Override
    public Players as(Name alias) {
        return new Players(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Players rename(String name) {
        return new Players(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Players rename(Name name) {
        return new Players(name, null);
    }

    // -------------------------------------------------------------------------
    // Row3 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row3<Short, String, String> fieldsRow() {
        return (Row3) super.fieldsRow();
    }
}
