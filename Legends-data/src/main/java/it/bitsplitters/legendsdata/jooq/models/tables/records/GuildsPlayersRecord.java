/*
 * This file is generated by jOOQ.
 */
package it.bitsplitters.legendsdata.jooq.models.tables.records;


import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;
import org.jooq.types.ULong;

import it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class GuildsPlayersRecord extends UpdatableRecordImpl<GuildsPlayersRecord> implements Record3<ULong, Short, String> {

    private static final long serialVersionUID = 1962599679;

    /**
     * Setter for <code>legendstracker.guilds_players.ID_GUILD</code>.
     */
    public void setIdGuild(ULong value) {
        set(0, value);
    }

    /**
     * Getter for <code>legendstracker.guilds_players.ID_GUILD</code>.
     */
    public ULong getIdGuild() {
        return (ULong) get(0);
    }

    /**
     * Setter for <code>legendstracker.guilds_players.ID_PLAYER</code>.
     */
    public void setIdPlayer(Short value) {
        set(1, value);
    }

    /**
     * Getter for <code>legendstracker.guilds_players.ID_PLAYER</code>.
     */
    public Short getIdPlayer() {
        return (Short) get(1);
    }

    /**
     * Setter for <code>legendstracker.guilds_players.TEAM</code>.
     */
    public void setTeam(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>legendstracker.guilds_players.TEAM</code>.
     */
    public String getTeam() {
        return (String) get(2);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record2<ULong, Short> key() {
        return (Record2) super.key();
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row3<ULong, Short, String> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    @Override
    public Row3<ULong, Short, String> valuesRow() {
        return (Row3) super.valuesRow();
    }

    @Override
    public Field<ULong> field1() {
        return GuildsPlayers.GUILDS_PLAYERS.ID_GUILD;
    }

    @Override
    public Field<Short> field2() {
        return GuildsPlayers.GUILDS_PLAYERS.ID_PLAYER;
    }

    @Override
    public Field<String> field3() {
        return GuildsPlayers.GUILDS_PLAYERS.TEAM;
    }

    @Override
    public ULong component1() {
        return getIdGuild();
    }

    @Override
    public Short component2() {
        return getIdPlayer();
    }

    @Override
    public String component3() {
        return getTeam();
    }

    @Override
    public ULong value1() {
        return getIdGuild();
    }

    @Override
    public Short value2() {
        return getIdPlayer();
    }

    @Override
    public String value3() {
        return getTeam();
    }

    @Override
    public GuildsPlayersRecord value1(ULong value) {
        setIdGuild(value);
        return this;
    }

    @Override
    public GuildsPlayersRecord value2(Short value) {
        setIdPlayer(value);
        return this;
    }

    @Override
    public GuildsPlayersRecord value3(String value) {
        setTeam(value);
        return this;
    }

    @Override
    public GuildsPlayersRecord values(ULong value1, Short value2, String value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached GuildsPlayersRecord
     */
    public GuildsPlayersRecord() {
        super(GuildsPlayers.GUILDS_PLAYERS);
    }

    /**
     * Create a detached, initialised GuildsPlayersRecord
     */
    public GuildsPlayersRecord(ULong idGuild, Short idPlayer, String team) {
        super(GuildsPlayers.GUILDS_PLAYERS);

        set(0, idGuild);
        set(1, idPlayer);
        set(2, team);
    }
}
