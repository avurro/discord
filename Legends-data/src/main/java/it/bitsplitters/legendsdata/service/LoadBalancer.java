package it.bitsplitters.legendsdata.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.models.BatchParameters;

public class LoadBalancer {
	
	/** logger object */
	private static final Logger logger = LogManager.getLogger();
	
	/**
	 * Ritorna il range di players da tracciare al batch dedicato.
	 * @param parameters
	 * @return
	 */
	public static List<String> playersToTrack() {
		BatchParameters params = Cache.batchList().get(Cache.ignite().name());
		return SrvPlayer.playersTrackedWithSubActiveWithRange(params.getStart(), params.getEnd());
	}
	
	public static void newBatch() {
		Map<String, BatchParameters> batchList = Cache.batchList();
		
		String batchName = Cache.ignite().name();
		
		logger.atInfo().log("nuovo batch: {}", batchName);
		
		BatchParameters newBatch = new BatchParameters(batchName);

		if(batchList == null || batchList.isEmpty()) {
			batchList = new HashMap<>();
			newBatch.setHaveToReset(true);
		}
		
		batchList.put(batchName, newBatch);
		
		loadRebalance(batchList);
	}
		
	public static void loadRebalance() {
		loadRebalance(Cache.batchList());
	}
	
	public static void loadRebalance(Map<String, BatchParameters> batchList) {
		
		if(batchList == null) return;
		
		int size = batchList.size();
		
		logger.atInfo().log("numero batch attivi: {}", size);
		
		Set<String> mapKeys = batchList.keySet();
		int totalPlayers = SrvPlayer.totalPlayersTrackedWithSubActive();
		
		if(size == 1) {
			
			String key = mapKeys.iterator().next();
			BatchParameters value = batchList.get(key);
			value.setStart(Optional.empty());
			value.setEnd(Optional.empty());
			batchList.replace(key, value);
			
		}else if (size == 2) {
			
			int share = totalPlayers/2;
			Iterator<String> iterator = mapKeys.iterator();
			
			String key = iterator.next();
			BatchParameters value = batchList.get(key);
			value.setStart(Optional.empty());
			value.setEnd(Optional.of(share)); 
			batchList.replace(key, value);
			
			logger.atInfo().log("Recupero il primo batch ({}), impostandogli start: null, end: {}",key,value.getEnd().get());
			
			key = iterator.next();
			value = batchList.get(key);
			value.setStart(Optional.of(share)); 
			value.setEnd(Optional.empty());
			batchList.replace(key, value);
		
			logger.atInfo().log("Recupero il secondo batch ({}), impostandogli start: {}, end: null",key,value.getStart().get());
			
		}else if (size == 3) {
			
			int share = totalPlayers/3;
			Iterator<String> iterator = mapKeys.iterator();
			
			String key = iterator.next();
			BatchParameters value = batchList.get(key);
			value.setStart(Optional.empty());
			value.setEnd(Optional.of(share)); 
			batchList.replace(key, value);
			
			logger.atInfo().log("Recupero il primo batch ({}), impostandogli start: null, end: {}",key,value.getEnd().get());
			
			key = iterator.next();
			value = batchList.get(key);
			value.setStart(Optional.of(share)); 
			value.setEnd(Optional.of(share));
			batchList.replace(key, value);
			
			logger.atInfo().log("Recupero il secondo batch ({}), impostandogli start: {}, end: {}",key,value.getStart().get(),value.getEnd().get());
			
			key = iterator.next();
			value = batchList.get(key);
			value.setStart(Optional.of(share*2)); 
			value.setEnd(Optional.empty());
			batchList.replace(key, value);
			
			logger.atInfo().log("Recupero il terzo batch ({}), impostandogli start: {}, end: null",key,value.getStart().get());
			
		}
		
		Cache.putBatchList(batchList);
	}
}