package it.bitsplitters.legendsdata.service;

import static it.bitsplitters.legendsdata.jooq.models.tables.Guilds.GUILDS;
import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;
import static it.bitsplitters.legendsdata.jooq.models.tables.Players.PLAYERS;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.jooq.Record3;
import org.jooq.Result;
import org.jooq.types.ULong;

import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.legendsdata.models.GuildChannels;

public class SrvGuild {
	
	public static List<GuildsRecord> guilds(){
		return DB.jooq().selectFrom(GUILDS).fetch();
	}
	
	public static Optional<GuildChannels> getOptGuild(Long guildID){
		
		Optional<GuildsRecord> optGuildRecord = getOptGuildRecord(guildID);
		
		if(optGuildRecord.isEmpty()) return Optional.empty();
		
		return Optional.of(new GuildChannels(optGuildRecord.get()));
	}
	
	public static Optional<GuildsRecord> getOptGuildRecord(Long guildID){
		return DB.jooq().selectFrom(GUILDS)
				.where(GUILDS.ID_GUILD.eq(ULong.valueOf(guildID))).fetchOptional();
	}
	
	public static boolean isSubscriber(Long guildID) {
		return DB.jooq().selectFrom(GUILDS)
		        .where(GUILDS.ID_GUILD.eq(ULong.valueOf(guildID))
		        .and(GUILDS.PAY_END.greaterOrEqual(DB.localDateNow()))).fetchOptional().isPresent();
	}
	
	public static int insertGuild(GuildsRecord guild) {
		return DB.jooq().executeInsert(guild);
	}
	
	/**
	 * Returns all the guilds data that have an active subscription and 
	 * that have the required player tag among the players tracked. 
	 * @param tag player tag
	 * @return
	 */
	public static List<GuildChannels> getGuilds(String tag) {
		List<GuildChannels> result = new ArrayList<>();
		for (Record3<ULong, ULong, ULong> record : getDBGuilds(tag)) 
			result.add(guildAdapter(record));
		
		return result;
	}
	
	/**
	 * @return the IDs of the guilds currently subscribed
	 */
	public static Set<Long> activeGuildsID() {
		return DB.jooq().select(GUILDS.ID_GUILD)
		 .from(GUILDS)
		 .where(GUILDS.PAY_END.ge(DB.localDateNow()))
		 .fetch(GUILDS.ID_GUILD)
		 .stream().map(id -> id.longValue()).collect(Collectors.toSet());
	}
	
	/**
	 * Returns all the guilds data, as db format, that have an active subscription and 
	 * that have the required player tag among the players tracked. 
	 * @param tag player tag
	 * @return ID, channel log and channel coms for each guild
	 */
	private static Result<Record3<ULong, ULong, ULong>> getDBGuilds(String tag) {
		return DB.jooq()
				.select(GUILDS.ID_GUILD, GUILDS.ID_CHANNEL_LOG, GUILDS.ID_CHANNEL_COMS)
				.from(GUILDS)
				.naturalJoin(GUILDS_PLAYERS)
				.naturalJoin(PLAYERS)
				.where(PLAYERS.TAG.eq(tag)
				.and(GUILDS.PAY_END.ge(DB.localDateNow()))).fetch();
	}
	
	/**
	 * @param record  ID, channel log ID and channel coms id of a guild
	 * @return instance of GuildChannels
	 */
	private static GuildChannels guildAdapter(Record3<ULong, ULong, ULong> record) {
		Long guildID = record.get(GUILDS.ID_GUILD).longValue();
		
		ULong dbLogChannel = record.get(GUILDS.ID_CHANNEL_LOG);
		Long idLogChannel = dbLogChannel != null ? dbLogChannel.longValue() : null;
		
		ULong dbComsChannel = record.get(GUILDS.ID_CHANNEL_COMS);
		Long idComsChannel = dbComsChannel != null ? dbComsChannel.longValue() : null;
		
		return new GuildChannels(guildID, idLogChannel, idComsChannel);
	}
}
