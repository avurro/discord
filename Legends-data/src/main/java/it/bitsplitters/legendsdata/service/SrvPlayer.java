package it.bitsplitters.legendsdata.service;

import static it.bitsplitters.legendsdata.DB.jooq;
import static it.bitsplitters.legendsdata.jooq.models.tables.Guilds.GUILDS;
import static it.bitsplitters.legendsdata.jooq.models.tables.GuildsPlayers.GUILDS_PLAYERS;
import static it.bitsplitters.legendsdata.jooq.models.tables.Players.PLAYERS;

import java.util.List;
import java.util.Optional;

import org.jooq.Record1;
import org.jooq.SelectConditionStep;
import org.jooq.types.ULong;

import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsPlayersRecord;
import it.bitsplitters.legendsdata.jooq.models.tables.records.PlayersRecord;


public class SrvPlayer {

	/**
	 * First check that other guilds are linked to the player, otherwise delete it from the Players table.
	 * @param player player to delete
	 * @return -1 if player is linked from others guild, 0 if delete fail, 1 player deleted
	 */
	public static int deletePlayer(PlayersRecord player) {
		
		// Controllo che al player siano agganciate altre guild, altrimenti lo cancello dalla tabella PLAYERS
		return jooq().fetchCount(GUILDS_PLAYERS, GUILDS_PLAYERS.ID_PLAYER.eq(player.getIdPlayer())) <= 0
			   ? jooq().executeDelete(player) : -1;
	}
	/**
	 * Get a Players record by id.
	 * @param idPlayer id player
	 * @return
	 */
	public static PlayersRecord playerById(short idPlayer) {
		return jooq().selectFrom(PLAYERS).where(PLAYERS.ID_PLAYER.eq(idPlayer)).fetchOne();
	}
	
	public static int deleteGuildsPlayers(GuildsPlayersRecord record) {
		return jooq().executeDelete(record);
	}
	
	/**
	 * Return the list of GuildsPlayers record tracked by a Guild.
	 * @return players tag
	 */
	public static List<GuildsPlayersRecord> guildsPlayers(ULong guildID){
		
		return jooq().selectFrom(GUILDS_PLAYERS)
					.where(GUILDS_PLAYERS.ID_GUILD.eq(guildID)).fetch();
	}
	
	/**
	 * Return the list of players tracked by active subscriptions.
	 * @return players tag
	 */
	public static List<String> playersTrackedWithSubActive() {

		return jooq().selectDistinct(PLAYERS.TAG)
				 .from(GUILDS)
				 .naturalJoin(GUILDS_PLAYERS)
				 .naturalJoin(PLAYERS)
				 .where(GUILDS.PAY_END.ge(DB.localDateNow())).fetch(PLAYERS.TAG);
	}
	
	/**
	 * Return the list of players tracked by active subscriptions.
	 * @return players tag
	 */
	public static List<String> playersTrackedWithSubActiveWithRange(Optional<Integer> start, Optional<Integer> end) {

		SelectConditionStep<Record1<String>> query = jooq().selectDistinct(PLAYERS.TAG)
				 .from(GUILDS)
				 .naturalJoin(GUILDS_PLAYERS)
				 .naturalJoin(PLAYERS)
				 .where(GUILDS.PAY_END.ge(DB.localDateNow()));
		
		//OFFSET è lo start, LIMIT il numero dallo start
		
		if(start.isPresent()) 
			query.offset(start.get());
		
		
		if(end.isPresent()) 
			query.limit(end.get());
		
		
		return query.fetch(PLAYERS.TAG);
	}
	
	/**
	 * Return the total of players tracked by active subscriptions.
	 * @return total players
	 */
	public static int totalPlayersTrackedWithSubActive() {

		return jooq().fetchCount(jooq().selectDistinct(PLAYERS.TAG)
				 .from(GUILDS)
				 .naturalJoin(GUILDS_PLAYERS)
				 .naturalJoin(PLAYERS)
				 .where(GUILDS.PAY_END.ge(DB.localDateNow())));
	}
	
	/**
	 * Return the list of players tracked by active subscriptions.
	 * @param guildID
	 * @return guild players tag
	 */
	public static List<String> playersTrackedBySubActiveGuild(Long guildID){
		return playersTrackedBySubActiveGuild(ULong.valueOf(guildID));
	}
	
	/**
	 * Return the list of players tracked by active subscriptions.
	 * @param guildID
	 * @return guild players tag
	 */
	public static List<String> playersTrackedBySubActiveGuild(ULong guildID){
		return jooq().selectDistinct(PLAYERS.TAG)
				 .from(GUILDS)
				 .naturalJoin(GUILDS_PLAYERS)
				 .naturalJoin(PLAYERS)
				 .where(GUILDS.PAY_END.ge(DB.localDateNow())
				 .and(GUILDS.ID_GUILD.eq(guildID))).fetch(PLAYERS.TAG);
	}
	
	public static PlayersRecord getDBPlayer(String tag) {
		return jooq().selectFrom(PLAYERS).where(PLAYERS.TAG.eq(tag)).fetchOne();
	}
}