package it.bitsplitters.legendsdata.models;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
public class PlayerData{

	private String nickname;
	private String tag;
	private Integer trophies;
	private Integer startTrophies;
	private Integer attacks;
	private Integer defenses;
	private Integer defenseWins;
	private Integer attackWins;
	private List<Integer> dailyAtkTrophies;
	private List<Integer> dailyDefTrophies;
	private List<Integer> dailyMixTrophies;
	private List<GuildChannels> guildChannels;
	private long sumWithBonus;
	
	
	public List<GuildChannels> getGuildChannels() {
		return guildChannels;
	}

	public PlayerData(String nickname, String tag, Integer trophies, Integer defenseWins, Integer attackWins) {
		this.nickname = nickname;
		this.tag = tag;
		this.trophies = trophies;
		this.defenseWins = defenseWins;
		this.attackWins = attackWins;
		this.guildChannels = new ArrayList<>();
		reset();
	}
	
	public PlayerData reset() {
		this.attacks = 0;
		this.defenses = 0;
		dailyAtkTrophies = new ArrayList<>(8);
		dailyDefTrophies = new ArrayList<>(8);
		dailyMixTrophies = new ArrayList<>(8);
		this.startTrophies = this.trophies;
		return this;
	}
	
	public boolean isFullLoaded() {
		return this.attacks >= 8 && this.defenses >= 8;
	}
	
	public long getSumWithBonus() {
		return sumWithBonus;
	}

	public void setSumWithBonus(long sumWithBonus) {
		this.sumWithBonus = sumWithBonus;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getTag() {
		return tag;
	}
	
	public Integer getTrophies() {
		return trophies;
	}
	
	public void setTrophies(Integer trophies) {
		this.trophies = trophies;
	}
	
	public void addAttack() {
		this.attacks++;
	}
	
	public void addAttacks(int atks) {
		this.attacks += atks;
	}
	
	public Integer attacks() {
		return this.attacks;
	}
	
	public void addDefenses(int defs) {
		this.defenses += defs;
	}
	
	public void addDefense() {
		this.defenses++;
	}
	
	public Integer defenses() {
		return this.defenses;
	}
	
	public Integer getDefenseWins() {
		return defenseWins;
	}

	public void setDefenseWins(Integer defenseWins) {
		this.defenseWins = defenseWins;
	}

	
	public Integer getAttackWins() {
		return attackWins;
	}

	public void setAttackWins(Integer attackWins) {
		this.attackWins = attackWins;
	}

	
	public Integer getStartTrophies() {
		return startTrophies;
	}
	
	public List<Integer> getDailyAtkTrophies() {
		return dailyAtkTrophies;
	}

	public void addDailyAtkTrophies(Integer trophies) {
		this.dailyAtkTrophies.add(trophies);
	}

	public List<Integer> getDailyDefTrophies() {
		return dailyDefTrophies;
	}

	public void addDailyDefTrophies(Integer trophies) {
		this.dailyDefTrophies.add(trophies);
	}

	public List<Integer> getDailyMixTrophies() {
		return dailyMixTrophies;
	}

	public void addDailyMixTrophies(Integer trophies) {
		this.dailyMixTrophies.add(trophies);
	}
	
	public Integer sumAtks() {
		Integer sum = 0;
		
		for (Integer trophies : dailyAtkTrophies) 
			sum+= trophies;
		
		return sum;
	}
	
	public Integer sumDefs() {
		Integer sum = 0;
		
		for (Integer trophies : dailyDefTrophies) 
			sum+= trophies;
		
		return sum;
	}
	
	public Integer sumMix() {
		Integer sum = 0;
		
		for (Integer trophies : dailyMixTrophies) 
			sum+= trophies;
		
		return sum;
	}

	@Override
	public String toString() {
		return "Player: "+ nickname +" (" + tag + ")\n"+
			   " start " + startTrophies +  ", now " + trophies +"\n ATK " + attacks + ", DEF " + defenses
				+ ", WIN DEF " + defenseWins + ", ATK WINS " + attackWins + "\n DAILY ATK TROPHIES "
				+ toString(dailyAtkTrophies) + ", DAILY DEF TROPHIES " + toString(dailyDefTrophies) + ", DAILY MIX TROPHIES " + toString(dailyMixTrophies);
	}


	private String toString(List<?> list) {
		StringJoiner sj = new StringJoiner(", ");
		for (Object obj : list) 
			sj.add(obj.toString());
		
		return sj.toString();
	}
}
