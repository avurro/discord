package it.bitsplitters.legendsdata.models;

import java.util.Optional;

public class BatchParameters {

	private String name;
	private Optional<Integer> start;
	private Optional<Integer> end;
	private Boolean haveToReset = false; 
	
	
	public BatchParameters(String name) {
		super();
		this.name = name;
	}
	
	public void setRange(Integer start, Integer end) {
		this.start = getOpt(start);
		this.end = getOpt(end);
	}

	private Optional<Integer> getOpt(Integer value){
		return value == null ? Optional.empty() : Optional.of(value);
	}
	
	public Optional<Integer> getStart() {
		return start;
	}
	public void setStart(Optional<Integer> start) {
		this.start = start;
	}
	public Optional<Integer> getEnd() {
		return end;
	}
	public void setEnd(Optional<Integer> end) {
		this.end = end;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getHaveToReset() {
		return haveToReset;
	}

	public void setHaveToReset(Boolean haveToReset) {
		this.haveToReset = haveToReset;
	}

	@Override
	public String toString() {
		return "BatchParameters [name=" + name + ", start=" + start.orElseGet(()->-1) + ", end=" + end.orElseGet(()->-1) + ", haveToReset=" + haveToReset
				+ "]";
	}
	
	
}
