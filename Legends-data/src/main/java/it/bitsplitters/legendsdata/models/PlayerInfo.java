package it.bitsplitters.legendsdata.models;

import java.util.ArrayList;
import java.util.List;

public class PlayerInfo {

	private final String tag;
	
	private String name;
	private String state;
	private List<String> dailyAttacks;
	private List<String> dailyDefenses;
	private List<String> dailyMix;
	private PlayerLog lastData;
	
	public PlayerInfo(String tag, String name) {
		this.tag = tag;
		this.name = name;
		reset();
	}
	
	public PlayerInfo reset() {
		this.dailyAttacks = new ArrayList<>(8);
		this.dailyDefenses = new ArrayList<>(8);
		this.dailyMix = new ArrayList<>(8);
		return this;
	}

	public List<String> getDailyAttacks() {
		return dailyAttacks;
	}

	public void addDailyAttacks(String dailyAttacks) {
		this.dailyAttacks.add(dailyAttacks);
	}

	public List<String> getDailyDefenses() {
		return dailyDefenses;
	}

	public void addDailyDefenses(String dailyDefenses) {
		this.dailyDefenses.add(dailyDefenses);
	}

	public List<String> getDailyMix() {
		return dailyMix;
	}

	public void addDailyMix(String dailyMix) {
		this.dailyMix.add(dailyMix);
	}

	public String getTag() {
		return tag;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "\nPlayerInfo [tag=" + tag + ", name=" + name + ", state=" + state + 
				"\n\n dailyAttacks=" + dailyAttacks.size() + ", dailyDefenses=" + dailyDefenses.size()
				+ ", dailyMix=" + dailyMix.size() + "]";
	}

	public PlayerLog getLastData() {
		return lastData;
	}

	public void setLastData(PlayerLog lastData) {
		this.lastData = lastData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tag == null) ? 0 : tag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayerInfo other = (PlayerInfo) obj;
		if (tag == null) {
			if (other.tag != null)
				return false;
		} else if (!tag.equals(other.tag))
			return false;
		return true;
	}

	
}