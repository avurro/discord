package it.bitsplitters.legendsdata.models;

import it.bitsplitters.legendsdata.jooq.models.tables.records.GuildsRecord;

public class GuildChannels {

	private Long guildID;
	private Long logChannel;
	private Long comsChannel;
	
	public GuildChannels(Long guildID) {
		super();
		this.guildID = guildID;
	}

	public GuildChannels(Long guildID, Long logChannel, Long comsChannel) {
		super();
		this.guildID = guildID;
		this.logChannel = logChannel;
		this.comsChannel = comsChannel;
	}

	public GuildChannels(GuildsRecord guild) {
		this(guild.getIdGuild().longValue(), guild.getIdChannelLog().longValue(), guild.getIdChannelComs().longValue());
	}
	
	public Long getGuildID() {
		return guildID;
	}

	public void setGuildID(Long guildID) {
		this.guildID = guildID;
	}

	public Long getLogChannel() {
		return logChannel;
	}
	public void setLogChannel(Long logChannel) {
		this.logChannel = logChannel;
	}
	public Long getComsChannel() {
		return comsChannel;
	}
	public void setComsChannel(Long comsChannel) {
		this.comsChannel = comsChannel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guildID == null) ? 0 : guildID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GuildChannels other = (GuildChannels) obj;
		if (guildID == null) {
			if (other.guildID != null)
				return false;
		} else if (!guildID.equals(other.guildID))
			return false;
		return true;
	}
	
	
}
