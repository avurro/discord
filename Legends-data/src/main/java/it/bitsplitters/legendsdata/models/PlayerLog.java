package it.bitsplitters.legendsdata.models;

import java.awt.Color;

public class PlayerLog {

	private final String tag;
	private final String name;
	
	private String lastLog;
	private Color lastLogColor;
	
	public PlayerLog(String name, String tag) {
		this.tag = tag;
		this.name = name;
	}
	
	public PlayerLog(String name, String tag, String lastLog, Color lastLogColor) {
		this(name, tag);
		this.lastLog = lastLog;
		this.lastLogColor = lastLogColor;
	}
	
	public String getLastLog() {
		return lastLog;
	}
	public void setLastLog(String lastLog) {
		this.lastLog = lastLog;
	}
	public Color getLastLogColor() {
		return lastLogColor;
	}
	public void setLastLogColor(Color lastLogColor) {
		this.lastLogColor = lastLogColor;
	}
	public String getTag() {
		return tag;
	}

	public String getName() {
		return name;
	}
}
