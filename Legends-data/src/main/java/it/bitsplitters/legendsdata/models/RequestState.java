package it.bitsplitters.legendsdata.models;

/**
 * Enumeration used to keep track of recovered player data.
 * 
 * @author Alessandro Vurro
 *
 */
public enum RequestState {
	STARTED, TIMEOUT, LOADED;
}
