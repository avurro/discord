package it.bitsplitters.legendsdata;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

public class DB {

	private static final Logger logger = LogManager.getLogger();
	
	/** clock set with European time zone Rome */
	private static Clock clockEurope = Clock.system(ZoneId.of("Europe/Rome"));
	
	/** JOOQ context, used to interact with DB */
	private static DSLContext context;
	
	/** where the configurations reside */
	private static final Properties configProperties = new Properties();

	/** DB username */
	private static final String dbUsername = "db.username";
	
	/** DB password */
	private static final String dbPassword = "db.password";
	
	/** DB url */
	private static final String dbUrl = "db.url";

	static {
		
		try(FileInputStream file = new FileInputStream(Init.propertiesPath)) {
			
		    configProperties.load(file);
	
		    String dbURL = configProperties.getProperty(dbUrl);
		    
		    if(dbURL != null && !dbURL.isEmpty()) {
				BasicDataSource ds = new BasicDataSource();
				ds.setUrl(dbURL);
				ds.setDriverClassName("org.mariadb.jdbc.Driver");
				ds.setUsername(configProperties.getProperty(dbUsername));
				ds.setPassword(configProperties.getProperty(dbPassword));
				ds.setMinIdle(5);
				ds.setMaxIdle(10);
				ds.setMaxOpenPreparedStatements(10);
				context = DSL.using(ds, SQLDialect.MYSQL);
		    }
			
		} catch (IOException e) { logger.atError().log(e.getMessage()); }

	}
	
	public static DSLContext jooq() {
    	return context;
    }
	
	public static LocalDate localDateNow() {
		return LocalDate.now(clockEurope);
	}
	
	public static LocalDateTime localDateTimeNow() {
		return LocalDateTime.now(clockEurope);
	}
	
	public static LocalTime localTimeNow() {
		return LocalTime.now(clockEurope);
	}
	
	public static Instant instantNow() {
		return Instant.now(clockEurope);
	}
}
