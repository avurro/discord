package it.bitsplitters.legendsdata;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteEvents;
import org.apache.ignite.Ignition;
import org.apache.ignite.cluster.ClusterState;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.multicast.TcpDiscoveryMulticastIpFinder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.legendsdata.models.BatchParameters;
import it.bitsplitters.legendsdata.models.PlayerData;
import it.bitsplitters.legendsdata.models.PlayerInfo;
import it.bitsplitters.legendsdata.models.PlayerLog;
import it.bitsplitters.legendsdata.models.RequestState;

public class Cache {
	
	/** logger object */
	private static final Logger logger = LogManager.getLogger();
	
	/** Where the configurations reside */
	private static final Properties configProperties = new Properties();

	/** Ignite IP */
	private static final String igniteIP = "ignite.ip";

	private static final String igniteNodeName = "ignite.name";
	
	/** Ignite configuration */
	private static Ignite ignite;
	
	private static String cache_legends = "legends";
	private static String cache_legends_info = "legends-info";
	private static String cache_legends_log = "legends-log";
	private static String cache_legends_loaded = "legends-loaded";
	private static String cache_request_state = "requeststate";
	private static String cache_legends_batch = "legends-batch";
	
	public static final String LOADED_PLAYERS_LIST = "loadedPlayers";

	public static final String BATCH_LIST = "batchList";
	
	private static final String localAdress = "local.ip";
	private static final String localPort = "local.port";
	private static final String localPortRange = "local.port.range";
	
	
	static {
	
		try(FileInputStream file = new FileInputStream(Init.propertiesPath)) {
		    
			configProperties.load(file);
		
			String igniteAddress = configProperties.getProperty(igniteIP);
			
			if(igniteAddress != null && !igniteAddress.isEmpty()) {
				
				// Preparing IgniteConfiguration using Java APIs
				IgniteConfiguration cfg = new IgniteConfiguration();
				
				String nodeName = configProperties.getProperty(igniteNodeName);
				
				// Node name
				cfg.setIgniteInstanceName(nodeName +"-"+DB.localTimeNow().toString().replace(":", "_"));
				
				// The node will be started as a client node.
				cfg.setClientMode(true);
				
				// Setting up an IP Finder to ensure the client can locate the servers.
				TcpDiscoveryMulticastIpFinder ipFinder = new TcpDiscoveryMulticastIpFinder();
				ipFinder.setAddresses(Collections.singletonList(igniteAddress));
				TcpDiscoverySpi tcpDiscoverySpi = new TcpDiscoverySpi().setIpFinder(ipFinder);
				
				String myAdress = configProperties.getProperty(localAdress);
				String myPort = configProperties.getProperty(localPort);
				String myPortRange = configProperties.getProperty(localPortRange);
				
				if(myAdress != null)
					tcpDiscoverySpi.setLocalAddress(myAdress);
				
				if(myPort != null) 
					tcpDiscoverySpi.setLocalPort(Integer.valueOf(myPort));
				
				if(myPortRange != null) 
					tcpDiscoverySpi.setLocalPortRange(Integer.valueOf(myPortRange));
				
				
				cfg.setDiscoverySpi(tcpDiscoverySpi);

				// Starting the node
				ignite = Ignition.getOrStart(cfg);
				
				ignite.cluster().state(ClusterState.ACTIVE);
			}
			
		} catch (IOException e) { logger.atError().log(e.getMessage()); }
	}
	
	public static IgniteCache<String, PlayerData> playersData(){
		return ignite.cache(cache_legends);
	}
	
	public static IgniteCache<String, PlayerInfo> playersInfo() {
    	return ignite.cache(cache_legends_info);
    }
	  
	public static IgniteCache<String, PlayerLog> playersLog() {
    	return ignite.cache(cache_legends_log);
    }
	  
	public static IgniteCache<String, RequestState> requestState() {
    	return ignite.cache(cache_request_state);
    }
	
	public static IgniteCache<String, Map<String, BatchParameters>> legendsBatch(){
		return ignite.cache(cache_legends_batch);
	}
	
	private static IgniteCache<String, List<String>> playersLoaded(){
		return ignite.cache(cache_legends_loaded);
	}
	
	public static List<String> playersLoadedList(){
		return playersLoaded().get(LOADED_PLAYERS_LIST);
	}
	
	public static void addLoadedPlayers(String tag) {
	
		List<String> list = playersLoadedList();
		list.add(tag);
		
		playersLoaded().put(LOADED_PLAYERS_LIST, list);
	}
	
	public static void initIfAbsentLoadedPlayers() {
		playersLoaded().putIfAbsent(LOADED_PLAYERS_LIST, new ArrayList<String>());
	}
	
	public static void resetLoadedPlayers() {
		playersLoaded().put(LOADED_PLAYERS_LIST, new ArrayList<String>());
	}
	
	public static IgniteEvents playersInfoEvents() {
		return ignite.events(ignite.cluster().forCacheNodes(cache_legends_info));
	}
	
	public static IgniteEvents batchListEvents() {
		return ignite.events(ignite.cluster().forCacheNodes(cache_legends_batch));
	}
	
	public static Map<String, BatchParameters> batchList(){
		return Cache.legendsBatch().get(BATCH_LIST);
	}
	
	public static void putBatchList(Map<String, BatchParameters> map) {
		Cache.legendsBatch().put(BATCH_LIST, map);
	}
	
	public static void initIfAbsentBatchList() {
		Cache.legendsBatch().putIfAbsent(BATCH_LIST, new HashMap<String, BatchParameters>());
	}

	public static Ignite ignite() {
		return ignite;
	}
}