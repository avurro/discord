package it.bitsplitters.legends.batch;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.clashapi.ClashAPI;
import it.bitsplitters.clashapi.ClashAPIAsync;
import it.bitsplitters.clashapi.ClashWrapper;
import it.bitsplitters.clashapi.CocApiAsyncConfig;

public enum COC {

	API;

	private final Logger logger = LogManager.getLogger();

	private ClashAPI clashAPI;
	private ClashAPIAsync batchAPIAsync;
	private CocApiAsyncConfig batchConfig;
	private String batchApiToken;
	
	/** where the configurations reside */
	private final Properties configProperties = new Properties();

	private final String batchApiTokenKey = "api.token.batch";
	
	private COC() {
		
		try(FileInputStream file = new FileInputStream("./batch.properties")) {
			configProperties.load(file);
			batchApiToken = configProperties.getProperty(batchApiTokenKey);

			batchConfig = new CocApiAsyncConfig() {
				ExecutorService service = Executors.newSingleThreadExecutor();
				@Override public String getToken() { return batchApiToken; }
				@Override public ExecutorService getService() { return service; }
				@Override public int timeout() { return 10; }
			};
			
			batchAPIAsync = ClashWrapper.getAPIInstanceAsync(batchConfig);
			clashAPI = ClashWrapper.getAPIInstance(batchApiToken);
		
		} catch (IOException e) { logger.atError().log(e.getMessage()); }
	}
	
	public ClashAPI get(){
		return this.clashAPI;
	}
	
	public ClashAPIAsync getBatchAsync() {
		return this.batchAPIAsync;
	}
}