package it.bitsplitters.legends.batch.commands;

import it.bitsplitters.legends.batch.Batch;
import it.bitsplitters.legends.batch.BatchParams;

public class CmdDayStart implements Command {

	@Override
	public String name() {
		return "hour";
	}

	@Override
	public void execute(String param) {
		
		System.out.println("Richiamato comando hour: "+param);
		
		Integer hour = Integer.valueOf(paramFiltered(param));
		
		Batch.nextLoad = Batch.nextLoad.withHour(hour).withMinute(0).withSecond(0).withNano(0);
		BatchParams.legendLeagueHour = hour;
		
		System.out.println("Ora impostata: "+hour+"\n Prossimo reset: "+ Batch.nextLoad);
	
	}

}
