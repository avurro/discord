package it.bitsplitters.legends.batch.commands;

import it.bitsplitters.legends.batch.BatchParams;
import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.DB;

public class CmdShutdown implements Command {

	@Override
	public String name() {
		return "shutdown";
	}

	@Override
	public void execute(String param) {

		Cache.ignite().close();

		DB.jooq().close();
		
		BatchParams.isShutdown = true;
		
	}

}
