package it.bitsplitters.legends.batch.logic;
import static it.bitsplitters.legends.batch.BatchParams.logEnabled;
import static it.bitsplitters.legends.batch.BatchParams.sleep;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.http.HttpTimeoutException;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.clashapi.Player;
import it.bitsplitters.clashapi.exception.MaintenanceException;
import it.bitsplitters.clashapi.exception.NotFoundException;
import it.bitsplitters.legends.batch.COC;
import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.models.PlayerData;
import it.bitsplitters.legendsdata.models.RequestState;

public class CycleManager {

	private static final Logger logger = LogManager.getLogger();
	
	public static boolean updateAttacksDefenses(Set<String> tags) {
		
		if(tags.isEmpty()) return true;
		
		for(String tag: tags) {
			
			RequestState state = Cache.requestState().get(tag);
			
			if(state == null || !RequestState.LOADED.equals(state)) {
				
				PlayerData playerData = Cache.playersData().get(tag);
				
				if(playerData == null) {
					Cache.requestState().remove(tag);
					continue;
				}
				
				Cache.requestState().put(tag, RequestState.STARTED);

				updatePlayer(playerData);
				
				try {
					Thread.sleep(sleep);
				} catch (InterruptedException e) {
					logger.atError().log(e.toString());
				}
				
			}
		}
		
		return false;
	}	
	
	private static CompletableFuture<Void> updatePlayer(PlayerData cachedPlayer) {
		
		try {
			
			return updatedPlayerAsync(cachedPlayer.getTag())
			.thenAccept(apiPlayer -> {
				
					Cache.requestState().replace(cachedPlayer.getTag(), RequestState.LOADED);
					
					Algorithm.updatePlayer(cachedPlayer, apiPlayer);
				}
			
			).whenComplete((result, ex) -> {
				
				if(ex != null) {
					
					Cache.requestState().replace(cachedPlayer.getTag(), RequestState.TIMEOUT);

					if(ex.getCause() instanceof HttpTimeoutException) {
						
							  if(logEnabled)
								logger.atError().log("Tag: {}, msg: {}", cachedPlayer.getTag(), ex.toString());
						
					}else if(ex.getCause() instanceof NotFoundException) {
					
						Cache.requestState().replace(cachedPlayer.getTag(), RequestState.LOADED);

						if(logEnabled)
						  logger.atError().log("Player non trovato. Nickname: {} Tag: {}", cachedPlayer.getNickname(), cachedPlayer.getTag());
						
					}else if(ex.getCause() instanceof MaintenanceException) {
					
						if(logEnabled)
							logger.atInfo().log("Manutenzione in corso..");
						
					}else
						logger.atError().log("Altro errore. Tag: {}, msg: {} ", cachedPlayer.getTag(), ex.toString());
				}
			});
			
		}catch (CompletionException e) {
			
			  if(logEnabled)
				logger.atError().log("{} Tag cercato: {}, messaggio: {}",e.getClass().getName(), cachedPlayer == null ? "NULLO" : cachedPlayer.getTag(), e.toString());
		
		} catch (Throwable e) {
			logger.atError().log("{} Tag cercato: {}, messaggio: {}",e.getClass().getName(), cachedPlayer == null ? "NULLO" : cachedPlayer.getTag(), e.toString());
		}
		
		return null;
	}
	
	private static CompletableFuture<Player> updatedPlayerAsync(String tag) throws CompletionException, URISyntaxException, UnsupportedEncodingException {
		return COC.API.getBatchAsync().requestPlayer(tag);
	}
}