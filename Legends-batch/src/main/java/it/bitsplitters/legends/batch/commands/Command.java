package it.bitsplitters.legends.batch.commands;

public interface Command {

	public String name();
	
	public void execute(String param);
	
	public default String paramFiltered(String param) {
		return param.substring(name().length()).trim();
	}
}
