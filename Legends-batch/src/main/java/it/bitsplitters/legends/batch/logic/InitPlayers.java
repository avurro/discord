package it.bitsplitters.legends.batch.logic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.cache.Cache.Entry;

import org.apache.ignite.IgniteCache;

import it.bitsplitters.legends.batch.service.PlayersService;
import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.models.GuildChannels;
import it.bitsplitters.legendsdata.models.PlayerData;
import it.bitsplitters.legendsdata.models.PlayerInfo;
import it.bitsplitters.legendsdata.service.LoadBalancer;
import it.bitsplitters.legendsdata.service.SrvGuild;
import it.bitsplitters.legendsdata.service.SrvPlayer;

public class InitPlayers {


	/**
	 * Check that all the players are loaded in the cache and that the servers connected to it are all there.
	 */
	public static void startup() {
		
		LoadBalancer.newBatch();
		
		IgniteCache<String, PlayerData> trophiesLog = Cache.playersData();
		IgniteCache<String,PlayerInfo> playersInfo = Cache.playersInfo();
		Cache.initIfAbsentLoadedPlayers();
		
		for(String tag: SrvPlayer.playersTrackedWithSubActive()) {

			boolean isCached = trophiesLog.containsKey(tag);
			boolean toSave = !isCached;
			
			PlayerData playerData = isCached ? trophiesLog.get(tag) : PlayersService.getPlayer(tag);
			
			List<GuildChannels> guildsChannels = SrvGuild.getGuilds(tag);
			
			if(!guildsChannels.isEmpty()) {
				
				List<GuildChannels> cachedGuildChannels = playerData.getGuildChannels();
				
				if(cachedGuildChannels.isEmpty()) {
					cachedGuildChannels.addAll(guildsChannels);
					toSave = true;
					
				}else 
					for (GuildChannels guildChannels : guildsChannels) 
						if(!cachedGuildChannels.contains(guildChannels)) {
							cachedGuildChannels.add(guildChannels);
							toSave = true;
						}
				
			}
			
			if(toSave) {
				trophiesLog.put(playerData.getTag(), playerData);
				playersInfo.put(playerData.getTag(), new PlayerInfo(playerData.getTag(), playerData.getNickname()));
			}
		}
	}
	
	public static void dailyReset() {
		
		Set<Long> activeGuildsID = SrvGuild.activeGuildsID();
		
		// Effettuo l'unlink dei players dai server non più abbonati
		Iterator<Entry<String, PlayerData>> iterator = Cache.playersData().iterator();
		
		List<Long> inactiveGuildIDs = new ArrayList<Long>();
		List<PlayerData> playersToUnlink = new ArrayList<PlayerData>();
		
		while(iterator.hasNext()) {
		
			Entry<String, PlayerData> entry = iterator.next();
			PlayerData player = entry.getValue();
			
			// verifico invece tra i players seguiti, quali guild hanno abbonamento non più attivo e quindi da disabilitare
			for(GuildChannels guild:player.getGuildChannels()) 
				if(!activeGuildsID.contains(guild.getGuildID())) {
					inactiveGuildIDs.add(guild.getGuildID());
					playersToUnlink.add(player);
				}
					
		}
		
		for (int i = 0; i < inactiveGuildIDs.size(); i++)
			PlayersService.unlinkCachedPlayerFromGuild(inactiveGuildIDs.get(i), playersToUnlink.get(i));
			
		// Sui players filtrati effettuo il reset
		iterator = Cache.playersData().iterator();
		while(iterator.hasNext()) {
			
			Entry<String, PlayerData> entry = iterator.next();
			Cache.playersData().replace(entry.getKey(), entry.getValue().reset());
			
			PlayerInfo playerInfo = Cache.playersInfo().get(entry.getKey());
			Cache.playersInfo().replace(entry.getKey(), playerInfo.reset());
		}
		
		Cache.resetLoadedPlayers();
	}
}