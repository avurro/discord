package it.bitsplitters.legends.batch;

public class BatchConstants {

	public static final String ATK = "<:atk:926414997301624902>";
	public static final String ATK_LOST = "<:atklost:944607359282585611>";
	public static final String ATK_WINLOST = "<:atkwinlost:944623178796392518>";
	public static final String ATK_AND = "<:atk_and:926415579030634496>";
	public static final String ATK_DEF = "<:atkdef:926414997242916914>";
	public static final String ATK_DEF_WHITE = "<:white_def:926415579093532732>";
	public static final String ATK_MORE_DEF = "<:atkdefs:926414997310042112>";
	public static final String BLANK = "<:blank:926415579001270282>";
	public static final String DEF = "<:def:926414997649776680>";
	public static final String DEF_WINS = "<:defwins:926415579009671169>";
	public static final String MORE_ATK = "<:atks:926414997268099133>";
	public static final String MORE_DEF = "<:defs:926414997393924096>";
	public static final String WIN_DEF = "<:wdef:926414997767213106>"; 
	
	public static final String START = "<:cupv:926414997704278036>";
	public static final String END = "<:cupr:926414997670723584>";
	public static final String DOWN_MIDDLE = "<:down2:926414997721079818>";
	public static final String UP_MIDDLE = "<:up1:926414997704282193>";
	public static final String EQ_MIDDLE = "<:eq:926414997393920020>";
	public static final String ATK_MIDDLE = "<:atkm:926418976957947944>"; 
	public static final String DEF_MIDDLE = "<:defm:926418977121517600>";

}
