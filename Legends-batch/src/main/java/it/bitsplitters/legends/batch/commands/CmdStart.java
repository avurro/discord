package it.bitsplitters.legends.batch.commands;

import static it.bitsplitters.legends.batch.BatchParams.isStarted;
import static it.bitsplitters.legends.batch.BatchParams.isStopped;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import it.bitsplitters.legends.batch.Batch;

public class CmdStart implements Command {

	@Override
	public String name() {
		return "start";
	}

	@Override
	public void execute(String param) {
		if(!isStarted) {
			isStopped = false;
			
			// Start tracking players
			ExecutorService executorService = Executors.newFixedThreadPool(1);
			executorService.execute(new Batch());
			
			System.out.println("Tracciamento avviato");
			
			isStarted = true;
		}
	}

}
