package it.bitsplitters.legends.batch.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.models.PlayerData;
import it.bitsplitters.legendsdata.models.PlayerInfo;

public class CmdState implements Command {

	private static final Logger logger = LogManager.getLogger();
	
	@Override
	public String name() {
		return "state";
	}

	@Override
	public void execute(String param) {
		
		String tag = paramFiltered(param);
		
		PlayerData playerData = Cache.playersData().get(tag);
		
		logger.atInfo().log(playerData != null ? playerData.toString() : "player data è null");

		PlayerInfo playerInfo = Cache.playersInfo().get(tag);
		
		logger.atInfo().log(playerInfo != null ? playerInfo.toString() : "player info è null");
		
	}

}
