package it.bitsplitters.legends.batch;

import static it.bitsplitters.legends.batch.BatchParams.isStopped;
import static it.bitsplitters.legends.batch.BatchParams.legendLeagueHour;
import static it.bitsplitters.legends.batch.BatchParams.logEnabled;
import static it.bitsplitters.legends.batch.BatchParams.timingEnabled;
import static it.bitsplitters.legends.batch.logic.CycleManager.updateAttacksDefenses;
import static it.bitsplitters.legends.batch.logic.InitPlayers.dailyReset;
import static it.bitsplitters.legends.batch.logic.InitPlayers.startup;
import static java.lang.Thread.sleep;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.DB;
import it.bitsplitters.legendsdata.models.BatchParameters;
import it.bitsplitters.legendsdata.models.RequestState;
import it.bitsplitters.legendsdata.service.LoadBalancer;

public class Batch implements Runnable {

	private static final Logger logger = LogManager.getLogger();
	
	/** durata in millisencodi di 10 secondi */
	private final long TEN_SECONDS = 1000 * 10;
	
	/** durata in millisencodi di 1 minuto */
	private final long ONE_MINUTE = 1000 * 60;
	
	public static LocalDateTime nextLoad;
	
	@Override
	public void run() {
		
		try {
			
			startup();
			
			BatchParameters batchParameters = Cache.batchList().get(Cache.ignite().name());
			LocalDateTime now = DB.localDateTimeNow();
			long startLoading = DB.instantNow().toEpochMilli();
			long totalTime = 0;
			int failedLoading = 0, prevFailedLoading = 0;
			Set<String> tags = null;
			
			nextLoad = now.withHour(legendLeagueHour).withMinute(0).withSecond(0).withNano(0);
			
			if(nextLoad.isBefore(now))
				nextLoad = nextLoad.plusDays(1);
			
			logger.atInfo().log("Prossimo azzeramento: {}",nextLoad);
			
			while(!isStopped) {

				if(DB.localDateTimeNow().isAfter(nextLoad)) {

					if(batchParameters.getHaveToReset()) {
						logger.atInfo().log("Reset in corso...");
						dailyReset();
					}
					
					nextLoad = nextLoad.plusDays(1).withHour(legendLeagueHour).withMinute(0).withSecond(0).withNano(0);
					
					logger.atInfo().log("Prossimo azzeramento: {}", nextLoad);	
				}
				
				tags = Set.copyOf(getTags());
				 
				boolean allPlayersLoaded = updateAttacksDefenses(tags);
				
				long secondToNextLoad = ChronoUnit.MILLIS.between(DB.localDateTimeNow(), nextLoad);
				
				if(allPlayersLoaded) {
					sleep(secondToNextLoad+1);
					continue;
				}
				
				secondToNextLoad = secondToNextLoad < 0 ? 0L : secondToNextLoad;
				
				sleep(secondToNextLoad < ONE_MINUTE ? secondToNextLoad : TEN_SECONDS);
				
				long loadingDuration = DB.instantNow().toEpochMilli() - startLoading;

				totalTime+= loadingDuration;
				
				if(prevFailedLoading == 0) 
					prevFailedLoading = tags.size();
				
				failedLoading = 0;
				
				for (Entry<String, RequestState> state : Cache.requestState().getAll(tags).entrySet()) 
					if(!RequestState.LOADED.equals(state.getValue()))
						failedLoading++;
					
				
				if((totalTime/ONE_MINUTE) >= 6) {
					
					logger.atInfo().log("reloading forzato. Non caricati {}", failedLoading);
					Cache.requestState().clearAll(tags);
					totalTime = 0;
					continue;
				}
				
				if(failedLoading == 0) {
					if(logEnabled)
						logger.atInfo().log("Accounts {} caricati in {}:{}s", tags.size(), (totalTime/ONE_MINUTE), ((totalTime%ONE_MINUTE/1000)));
					
					Cache.requestState().clearAll(tags);
					totalTime = 0;
					prevFailedLoading = 0;
				}else {
					if(timingEnabled) 
						logger.atInfo().log("Elaborazione:{}s, caricamenti falliti:{}/{}", (loadingDuration/1000), failedLoading, prevFailedLoading);

					prevFailedLoading = failedLoading;
				}
				
				startLoading = DB.instantNow().toEpochMilli();
			}

			if(tags != null && !tags.isEmpty())
				Cache.requestState().clearAll(tags);
			
			logger.atInfo().log("\n-------------------\nSTOP COMPLETATO\n-------------------");

		} catch (InterruptedException e) {
			logger.atError().log(e.getMessage());
		}
	}
	
	private static List<String> getTags(){
		
		List<String> playersToFind = LoadBalancer.playersToTrack();
		
		List<String> fullStats = Cache.playersLoadedList();
		
		if(!fullStats.isEmpty())
			playersToFind.removeAll(fullStats);
		
		return playersToFind;
	}
}


//TODO aggiungere cache di notifica e catturare evento dal bot dal quale poi propagare su canale discord admin
//String temperatura = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec("vcgencmd measure_temp").getInputStream())).lines().collect(Collectors.toList()).get(0).split("=")[1];
//
//if(Integer.valueOf(temperatura.subSequence(0, 2).toString()) >= Config.getTemperaturaLimite())
//		jda.getGuildById(784949259861884958L).getTextChannelById(787969826408103947L).sendMessageEmbeds(warningEmbedBuilder("**Il Raspberry ha raggiunto i "+temperatura+"!**").build())
//		.queue(); 