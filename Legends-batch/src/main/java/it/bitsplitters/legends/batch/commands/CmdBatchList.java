package it.bitsplitters.legends.batch.commands;

import java.util.Map;
import java.util.Map.Entry;

import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.models.BatchParameters;

public class CmdBatchList implements Command {

	@Override
	public String name() {
		return "resetBatchList";
	}

	@Override
	public void execute(String param) {
		
		System.out.println("Richiamato comando resetBatchList");
		
		Map<String, BatchParameters> batchList = Cache.batchList();
		for (Entry<String, BatchParameters> it : batchList.entrySet()) {
			System.out.println(it.getKey()+" "+it.getValue());
		}
		
		batchList.clear();
		
		Cache.putBatchList(batchList);
	}

}
