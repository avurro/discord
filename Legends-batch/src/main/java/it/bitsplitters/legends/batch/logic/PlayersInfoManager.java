package it.bitsplitters.legends.batch.logic;

import static it.bitsplitters.legends.batch.BatchConstants.*;

import static it.bitsplitters.legendsdata.service.SrvPrinter.*;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.models.PlayerData;
import it.bitsplitters.legendsdata.models.PlayerInfo;
import it.bitsplitters.legendsdata.models.PlayerLog;

public class PlayersInfoManager {

	public static void writeAtk(PlayerData playerData, String dailyAtk) {
		
		PlayerInfo playerInfo = Cache.playersInfo().containsKey(playerData.getTag()) 
								? Cache.playersInfo().get(playerData.getTag())
								: new PlayerInfo(playerData.getTag(), playerData.getNickname());
		
		playerInfo.setName(playerData.getNickname());
		playerInfo.addDailyAttacks(dailyAtk);
		playerInfo.setState(state(playerData, playerInfo));
		
		Cache.playersInfo().put(playerData.getTag(), playerInfo);
		Cache.playersLog().put(playerData.getTag(), new PlayerLog(playerData.getNickname(), playerData.getTag(), dailyAtk, Color.green));
	}
	
	public static void writeDef(PlayerData playerData, String dailyDef) {
		PlayerInfo playerInfo =  Cache.playersInfo().containsKey(playerData.getTag()) 
				? Cache.playersInfo().get(playerData.getTag())
				: new PlayerInfo(playerData.getTag(), playerData.getNickname());
				
		playerInfo.setName(playerData.getNickname());
		playerInfo.addDailyDefenses(dailyDef);
		playerInfo.setState(state(playerData, playerInfo));
		
		Cache.playersInfo().put(playerInfo.getTag(), playerInfo);
		Cache.playersLog().put(playerData.getTag(), new PlayerLog(playerData.getNickname(), playerData.getTag(), dailyDef, Color.red));
	}
		
	public static void writeMix(PlayerData playerData, String dailyMix) {
		PlayerInfo playerInfo = Cache.playersInfo().containsKey(playerData.getTag()) 
				? Cache.playersInfo().get(playerData.getTag())
				: new PlayerInfo(playerData.getTag(), playerData.getNickname());
				
		playerInfo.setName(playerData.getNickname());
		playerInfo.addDailyMix(dailyMix);
		playerInfo.setState(state(playerData, playerInfo));
		
		Cache.playersInfo().put(playerInfo.getTag(), playerInfo);
		Cache.playersLog().put(playerData.getTag(), new PlayerLog(playerData.getNickname(), playerData.getTag(), dailyMix, Color.red));
	}

	private static String state(PlayerData playerData, PlayerInfo playerInfo) {

		String embed = getOverview(playerData);
		
		List<String> playerDetails = getPlayerDetails(playerInfo);
		
		if(!playerDetails.isEmpty()) 
			embed = String.join("\n",embed, "\n__*Details*__",String.join("\n", playerDetails));
		
		return embed;
	}
	
	public static String getOverview(PlayerData player) {
		
		// calcolo la freccia da visualizzare
		int differenzaTrofei = player.getTrophies() - player.getStartTrophies();
		String trend = differenzaTrofei > 0 ? UP_MIDDLE : differenzaTrofei < 0 ? DOWN_MIDDLE : EQ_MIDDLE;
		
		String startTrophies = trophiesToEmoji(player.getStartTrophies());
		String currentTrophies = trophiesToEmoji(player.getTrophies());
		String cupDifference = centinaiaToEmoji(Math.abs(differenzaTrofei));
		String sumAtks = centinaiaToEmoji(player.sumAtks());
		String sumDefs = centinaiaToEmoji(player.sumDefs());
		String sumMixs = centinaiaToEmoji(player.sumMix());
		String totAtks = spAttacks(player.attacks());
		String totDefs = spDefenses(player.defenses());
		
		String mix = "";
		if(!player.getDailyMixTrophies().isEmpty())
			mix = String.join("",ATK_DEF, sumMixs);

		String result = String.join("","__*Overview*__"
		,"\n",START,startTrophies              ,  ATK_MIDDLE, sumAtks, mix
		,"\n",END  ,currentTrophies            ,  DEF_MIDDLE, sumDefs
		,"\n",BLANK, totAtks, totDefs," ",BLANK,trend, cupDifference);
		
		return result;
	}

	private static List<String> getPlayerDetails(PlayerInfo player)	{
		List<String> result = new ArrayList<>();
		
		List<String> dailyAttacks = player.getDailyAttacks();
		List<String> dailyDefenses = player.getDailyDefenses();
		List<String> dailyMix = player.getDailyMix();
		
		for (int i = 0; i < 8; i++) {
			
			if((dailyAttacks.size() <= i && dailyDefenses.size() <= i && dailyMix.size() <= i))
				break;
			
			result.add(String.join("",
					dailyAttacks.size()  > i ? dailyAttacks.get(i)  : fillSpace,
					dailyDefenses.size() > i ? dailyDefenses.get(i) : fillSpace,
					dailyMix.size()      > i ? dailyMix.get(i)      : ""));
		}
		
		return result;
	}
	
	private static final String fillSpace = BLANK + BLANK + BLANK + " ";

}
