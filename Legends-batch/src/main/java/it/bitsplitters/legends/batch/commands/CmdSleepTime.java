package it.bitsplitters.legends.batch.commands;

import it.bitsplitters.legends.batch.BatchParams;

public class CmdSleepTime implements Command {

	@Override
	public String name() {
		return "sleep";
	}

	@Override
	public void execute(String param) {

		Integer sleepTime = Integer.valueOf(paramFiltered(param));
		
		BatchParams.sleep = sleepTime;
		
		System.out.println("Impostato tempo di sleep: "+sleepTime+"ms");
		
	}

}
