package it.bitsplitters.legends.batch.commands;

import static it.bitsplitters.legends.batch.BatchParams.timingEnabled;

public class CmdTiming implements Command {

	@Override
	public String name() {
		return "timing";
	}

	@Override
	public void execute(String param) {
		
		timingEnabled = !timingEnabled;
		System.out.println("Log del timing di ogni ciclo "+ (timingEnabled ? "abilitato":"disabilitato"));
	
	}

}
