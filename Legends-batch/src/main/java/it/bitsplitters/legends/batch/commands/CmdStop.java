package it.bitsplitters.legends.batch.commands;

import java.util.Map;

import org.apache.ignite.configuration.DataRegionConfiguration;
import org.apache.ignite.configuration.DataStorageConfiguration;

import it.bitsplitters.legends.batch.BatchParams;
import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.models.BatchParameters;
import it.bitsplitters.legendsdata.service.LoadBalancer;

public class CmdStop implements Command {

	@Override
	public String name() {
		return "stop";
	}

	@Override
	public void execute(String param) {
		BatchParams.isStopped = true;
		BatchParams.isStarted = false;
		DataStorageConfiguration d;
		System.out.println("Stop programmato");
		
		String nodeName = Cache.ignite().name();
		
		System.out.println("Rimozione del batch "+nodeName+" dalla lista di batch attivi");
		
		Map<String, BatchParameters> batchList = Cache.batchList();
		
		batchList.remove(nodeName);
		Cache.putBatchList(batchList);
    	
		System.out.println("Ribilancio il carico");
		
		LoadBalancer.loadRebalance(batchList);
    	
		System.out.println("\n-------------------\nSTOP COMPLETATO\n-------------------");
		
	}

}
