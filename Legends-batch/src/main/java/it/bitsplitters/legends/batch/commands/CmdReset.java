package it.bitsplitters.legends.batch.commands;

import it.bitsplitters.legendsdata.Cache;

public class CmdReset implements Command {

	@Override
	public String name() {
		return "resetPlayers";
	}

	@Override
	public void execute(String param) {

		Cache.resetLoadedPlayers();
		System.out.println("Reset effetuato");
	}
	

}
