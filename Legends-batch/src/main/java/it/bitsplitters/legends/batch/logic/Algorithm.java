package it.bitsplitters.legends.batch.logic;

import static it.bitsplitters.legends.batch.BatchConstants.*;
import static it.bitsplitters.legendsdata.service.SrvPrinter.centinaiaToEmoji;
import static it.bitsplitters.legendsdata.service.SrvPrinter.unitaToEmoji;
import it.bitsplitters.clashapi.Player;
import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.models.PlayerData;

public class Algorithm {

	private enum LogType { ATK, ATK_LOST, ATK_WINLOST, ATK_AND, ATK_WHITEDEF, MORE_ATK, ATK_DEF, ATK_DEF_ZERO, ATK_MOREDEF, DEF, MORE_DEF, WINDEF, MOREWINDEF, NONE }
	
	private static final Integer NUMBER_ATTACKS = 8;
	
	public static void updatePlayer(PlayerData cachedPlayer, Player apiPlayer) {

		if(apiPlayer == null) return;
		
		LogType logType = LogType.NONE;
		int trophiesDifference = 0;
		
		boolean toUpdate = false;
		
		int actualTrophies = apiPlayer.getTrophies();
		
		Integer previousTrophies = cachedPlayer.getTrophies();
		
		cachedPlayer.setTrophies(actualTrophies);
		
		// --------------
		// ATTACKS
		// --------------
		if(actualTrophies > previousTrophies) {
			toUpdate = true;
			
			if(cachedPlayer.attacks() < NUMBER_ATTACKS) {
			
				cachedPlayer.addAttack();
				
				trophiesDifference = actualTrophies - previousTrophies;
				int atkWins = apiPlayer.getAttackWins() - cachedPlayer.getAttackWins();
				
				if(trophiesDifference > 0 && trophiesDifference < 5) {
					
					// se ho vinto l'attacco con trofei da 1 a 4, allora vuol dire che ho avuto difesa contemporanea
					if(atkWins > 0) {
						
						cachedPlayer.addDefense();
						logType = LogType.ATK_DEF;
						
					// altrimenti l'attacco è perso
					}else {
						logType = LogType.ATK_LOST;
					}
					
					// potentialDef
				}else if(trophiesDifference <= 10) {
					logType = LogType.ATK_WHITEDEF;
					
				// unknownFactor
				}else if(trophiesDifference > 32 && trophiesDifference < 36) {
					logType = LogType.ATK_AND;
					
				//more atks
				}else if(trophiesDifference > 35 && trophiesDifference < 40) {
					cachedPlayer.addAttacks(atkWins <= 1? atkWins : atkWins -1);
					if(atkWins > 1) {
						logType = LogType.MORE_ATK;
					}else {
						logType = LogType.ATK_WINLOST;
					}
				}else if(trophiesDifference > 40) {
					// nel caso in cui risultano piu di 40 trofei ma uno dei due attacchi non è vinto, allora la differenza
					// attacchi vinti tra prima e ora sara' di 1. Se invece risultano più attacchi vinti vuol dire che li ha vinti tutti
					// quindi prendiamo la diffenza - 1 che e' l'add fatto a inizio metodo
					cachedPlayer.addAttacks(atkWins <= 1? atkWins : atkWins -1);
					logType = LogType.MORE_ATK;
					
				// standard atk
				}else{
					logType = LogType.ATK;
				}
					
			}
			
			cachedPlayer.setAttackWins(apiPlayer.getAttackWins());
		}
		
		
		// --------------
		// DEFENCES
		// --------------
		if(actualTrophies < previousTrophies) {
			toUpdate = true;


			// gli attacchi e difese vinte sono minori rispetto agli attuali sono a seguito di un reset stagionale
			if(apiPlayer.getDefenseWins() < cachedPlayer.getDefenseWins() || apiPlayer.getAttackWins() < cachedPlayer.getAttackWins()) {
				cachedPlayer.setAttackWins(apiPlayer.getAttackWins());
				cachedPlayer.setDefenseWins(apiPlayer.getDefenseWins());
				logType = LogType.NONE;
			} 
			
			else if(cachedPlayer.defenses() < NUMBER_ATTACKS) {
		
				int numberDef = 1;
				boolean isWithAtk = false;
				int lostTrophies = previousTrophies - actualTrophies;
				
				cachedPlayer.addDefense();
				
				// se le coppe perse sono più di 40 segnare una difesa in piu
				if(lostTrophies> 40) {
					cachedPlayer.addDefense();
					numberDef++;
				}
				
				// se le coppe perse sono più di 80 segnare una difesa in piu
				if(lostTrophies> 80) {
					cachedPlayer.addDefense();
					numberDef++;
				}
				
				// se nella variazione a ribasso delle coppe, noto un attacco vinto
				// vuol dire che la difesa e' ridotta del numero di coppe fatte con un attacco
				// in contemporanea
				if(cachedPlayer.getAttackWins() != -1 && apiPlayer.getAttackWins() > cachedPlayer.getAttackWins() && cachedPlayer.attacks() < NUMBER_ATTACKS) {
					isWithAtk = true;
					cachedPlayer.addAttacks(apiPlayer.getAttackWins() - cachedPlayer.getAttackWins());
					cachedPlayer.setAttackWins(apiPlayer.getAttackWins());
				}
				
				// se le coppe perse sono tra le 33 e le 39 e non ho attacco contemporaneo allora ne ho subite due
				if(lostTrophies> 32 && lostTrophies < 40 && !isWithAtk) {
					cachedPlayer.addDefense();
					numberDef++;
				}
							
				if(!isWithAtk) {
					
					trophiesDifference = lostTrophies;
					logType = numberDef <= 1 ? LogType.DEF : LogType.MORE_DEF;
				
				}else {
					
					trophiesDifference = actualTrophies - previousTrophies;
					logType = numberDef <= 1 ? LogType.ATK_DEF : LogType.ATK_MOREDEF;
				}
			}
		}
		
		// --------------
		// PARKING
		// --------------
		if(actualTrophies == previousTrophies) {
			
			// Se non rilevo variazioni di coppe, controllo che non ci sia una difesa vinta
			if(cachedPlayer.getDefenseWins() != -1 && apiPlayer.getDefenseWins() > cachedPlayer.getDefenseWins()) {
				toUpdate = true;
				int defs = apiPlayer.getDefenseWins() - cachedPlayer.getDefenseWins();
				cachedPlayer.addDefenses(defs);
				cachedPlayer.setDefenseWins(apiPlayer.getDefenseWins());
				
				logType = defs > 1 ? LogType.MOREWINDEF : LogType.WINDEF;
				
			}
			
			//se risulta un attacco vinto vuol dire che ho allo stesso tempo subito una difesa
			//di pari coppe, quindi è un attacco e difesa valido
			if(cachedPlayer.getAttackWins() != -1 && apiPlayer.getAttackWins() > cachedPlayer.getAttackWins()){
				toUpdate = true;
				cachedPlayer.addAttack();
				cachedPlayer.addDefense();
				cachedPlayer.setAttackWins(apiPlayer.getAttackWins());

				logType = LogType.ATK_DEF_ZERO;
				
			}
		}
		
		if(toUpdate) {

			cachedPlayer.setNickname(apiPlayer.getName());
			
			switch(logType) {
				case ATK_AND: 		
				case ATK_WHITEDEF: 	
				case ATK:
				case ATK_LOST:
				case ATK_WINLOST:
				case MORE_ATK: 		cachedPlayer.addDailyAtkTrophies(trophiesDifference);  	break;
				
				case DEF:			
				case WINDEF:		
				case MORE_DEF:		
				case MOREWINDEF:	cachedPlayer.addDailyDefTrophies(trophiesDifference);	break;
				
				case ATK_DEF:		
				case ATK_DEF_ZERO:	
				case ATK_MOREDEF:	cachedPlayer.addDailyMixTrophies(trophiesDifference);	break;
				
				case NONE: break;
			}

			Cache.playersData().replace(cachedPlayer.getTag(), cachedPlayer);
						
			switch(logType) {
				case ATK_AND: 		writeAttackAnd(cachedPlayer, trophiesDifference); 	   	break;
				case ATK_WHITEDEF: 	writeAttackWhiteDef(cachedPlayer, trophiesDifference); 	break;
				case ATK: 			writeAttack(cachedPlayer, trophiesDifference);         	break;
				case ATK_LOST:		writeAttackLost(cachedPlayer, trophiesDifference);      break;
				case ATK_WINLOST:	writeAttackWinLost(cachedPlayer, trophiesDifference);   break;
				case MORE_ATK: 		writeMoreAttack(cachedPlayer, trophiesDifference);     	break;
				case DEF:			writeDefense(cachedPlayer, trophiesDifference);			break;
				case MORE_DEF:		writeMoreDefense(cachedPlayer, trophiesDifference);		break;
				case ATK_DEF:		writeAtkDef(cachedPlayer, trophiesDifference);			break;
				case ATK_DEF_ZERO:	writeAtkDef(cachedPlayer, 0);							break;
				case ATK_MOREDEF:	writeAtkMoreDef(cachedPlayer, trophiesDifference);		break;
				case MOREWINDEF:	writeWinMoreDefenses(cachedPlayer);						break;
				case WINDEF:		writeWinDefense(cachedPlayer);							break;
				case NONE: break;
			}
			
			if(cachedPlayer.isFullLoaded())
				Cache.addLoadedPlayers(cachedPlayer.getTag());
			
		}
	}
	
	private static void writeAttack(PlayerData plt, int atk) {
		writeAtk(plt, String.join("", ATK, centinaiaToEmoji(atk)),atk);
	}
	
	private static void writeAttackLost(PlayerData plt, int atk) {
		writeAtk(plt, String.join("", ATK_LOST, centinaiaToEmoji(atk)),atk);
	}
	
	private static void writeAttackWinLost(PlayerData plt, int atk) {
		writeAtk(plt, String.join("", ATK_WINLOST, centinaiaToEmoji(atk)),atk);
	}
	
	private static void writeAttackAnd(PlayerData plt, int atk) {
		writeAtk(plt, String.join("", ATK_AND, centinaiaToEmoji(atk)),atk);
	}
	
	private static void writeAttackWhiteDef(PlayerData plt, int atk) {
		writeAtk(plt, String.join("", ATK_DEF_WHITE, centinaiaToEmoji(atk)),atk);
	}
	
	private static void writeMoreAttack(PlayerData plt, int atk) {
		writeAtk(plt, String.join("", MORE_ATK, centinaiaToEmoji(atk)),atk);
	}
	
	private static void writeDefense(PlayerData plt, int def) {
		writeDef(plt, String.join("", DEF, centinaiaToEmoji(def)),def);
	}
	
	private static void writeWinDefense(PlayerData plt) {
		writeDef(plt, String.join("", WIN_DEF, BLANK + unitaToEmoji(0)),0);
	}
	
	private static void writeWinMoreDefenses(PlayerData plt) {
		writeDef(plt, String.join("", DEF_WINS, BLANK + unitaToEmoji(0)),0);
	}
	
	private static void writeMoreDefense(PlayerData plt, int def) {
		writeDef(plt, String.join("", MORE_DEF, centinaiaToEmoji(def)),def);
	}
	
	private static void writeAtkDef(PlayerData plt, int mix) {
		writeMix(plt, String.join("", ATK_DEF, centinaiaToEmoji(mix)),mix);
	}
	
	private static void writeAtkMoreDef(PlayerData plt, int mix) {
		writeMix(plt, String.join("", ATK_MORE_DEF, centinaiaToEmoji(mix)),mix);
	}
	
	private static void writeAtk(PlayerData plt, String dailyAtk, int trophies) {
		PlayersInfoManager.writeAtk(plt, dailyAtk);
	}
	
	private static void writeDef(PlayerData plt, String dailyDef, int trophies) {
		PlayersInfoManager.writeDef(plt, dailyDef);
	}
	
	private static void writeMix(PlayerData plt, String dailyMix, int trophies) {
		PlayersInfoManager.writeMix(plt, dailyMix);
	}
}