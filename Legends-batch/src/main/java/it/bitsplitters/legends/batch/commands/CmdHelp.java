package it.bitsplitters.legends.batch.commands;

import it.bitsplitters.legends.batch.BatchParams;

public class CmdHelp implements Command {

	@Override
	public String name() {
		return "help";
	}

	@Override
	public void execute(String param) {

		System.out.println(
				"\nstart		avvia il batch\n"+
				"stop		ferma il batch\n"+
				"resetPlayers 	reset della lista dei players completati\n"+
				"resetBatchList 	annulla la lista batch\n"+
				"log		abilita/disabilita il log degli errori (attuale: "+(BatchParams.logEnabled?"abilitato":"disabiltiato")+")\n"+
				"timing		abilita/disabilita il log del timing di ogni ciclo (attuale: "+(BatchParams.timingEnabled?"abilitato":"disabiltiato")+")\n"+
				"shutdown	arresta definitivamente il batch\n"+
				"hour <ora>	imposta l'ora di start del giorno di lega (attuale: "+BatchParams.legendLeagueHour+")\n"+
				"state <tag>	riporta le statistiche raccolte del player\n"+
				"sleep <ms>	imposta il ratelimit di call alle API in millisecondi (attuale: "+BatchParams.sleep+")");
	}

}
