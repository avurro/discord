package it.bitsplitters.legends.batch;

public class BatchParams {

	// COMMANDS Parameters
	public static boolean isStarted = false;
	public static boolean isStopped = false;
	public static boolean logEnabled = false;
	public static boolean timingEnabled = false;
	public static int legendLeagueHour = 7;	
	public static boolean isShutdown = false;
	public static long sleep = 60L;
	
}
