package it.bitsplitters.legends.batch;

import java.util.List;
import java.util.Scanner;

import it.bitsplitters.legends.batch.commands.CmdBatchList;
import it.bitsplitters.legends.batch.commands.CmdDayStart;
import it.bitsplitters.legends.batch.commands.CmdHelp;
import it.bitsplitters.legends.batch.commands.CmdLog;
import it.bitsplitters.legends.batch.commands.CmdReset;
import it.bitsplitters.legends.batch.commands.CmdShutdown;
import it.bitsplitters.legends.batch.commands.CmdSleepTime;
import it.bitsplitters.legends.batch.commands.CmdStart;
import it.bitsplitters.legends.batch.commands.CmdState;
import it.bitsplitters.legends.batch.commands.CmdStop;
import it.bitsplitters.legends.batch.commands.CmdTiming;
import it.bitsplitters.legends.batch.commands.Command;
import static it.bitsplitters.legends.batch.BatchParams.isShutdown;
public class Start {

	public static void main(String[] args) {

		List<Command> commands = List.of(
				
				new CmdHelp(), 
				new CmdLog(), 
				new CmdStart(), 
				new CmdState(), 
				new CmdStop(),
				new CmdTiming(),
				new CmdShutdown(),
				new CmdSleepTime(),
				new CmdBatchList(),
				new CmdDayStart(),
				new CmdReset());
		
		try(Scanner scanner = new Scanner(System.in)){
			
			while(!isShutdown) {
				String commandLine = scanner.nextLine();
				
				for (Command command : commands)
					if(commandLine.startsWith(command.name())) {
						
						try {
							
							command.execute(commandLine);
						
						}catch (Exception e) {
							e.printStackTrace();
						}
						
						break;
					}
			}
			
		}
	}
}