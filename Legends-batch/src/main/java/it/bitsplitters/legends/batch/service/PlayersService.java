package it.bitsplitters.legends.batch.service;

import static it.bitsplitters.legends.batch.BatchParams.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.clashapi.Player;
import it.bitsplitters.legends.batch.COC;
import it.bitsplitters.legendsdata.Cache;
import it.bitsplitters.legendsdata.jooq.models.tables.records.PlayersRecord;
import it.bitsplitters.legendsdata.models.PlayerData;
import it.bitsplitters.legendsdata.service.SrvPlayer;

public class PlayersService {

	private static final Logger logger = LogManager.getLogger();
	
	public static PlayerData getPlayer(String tag) {
		
		PlayersRecord dbPlayer = SrvPlayer.getDBPlayer(tag);
		
		int trophies = 5000;
		int defenseWins = -1;
		int attackWins = -1;
		
		try {
			
			Player player = COC.API.get().requestPlayer(tag);
			trophies = player.getTrophies();
			defenseWins = player.getDefenseWins();
			attackWins = player.getAttackWins();
			
		} catch (Exception e) {
			
			if(logEnabled)
				logger.atError().log("tag cercato: {}, nickname: {}, dettagli: {}",dbPlayer.getTag(), dbPlayer.getNickname(), e.getMessage());
		}
		
		return new PlayerData(dbPlayer.getNickname(), dbPlayer.getTag(), trophies, defenseWins, attackWins);
	}

	public static void unlinkCachedPlayerFromGuild(Long guildID, PlayerData player) {
		
		if(player == null) return;
		
		String tag = player.getTag();
		
		boolean isRemoved = player.getGuildChannels().removeIf(guildChannels -> guildChannels.getGuildID().equals(guildID));
		
		if(player.getGuildChannels().isEmpty()) {
			Cache.playersData().remove(tag);
			Cache.playersInfo().remove(tag);
			Cache.requestState().remove(tag);
		
		} else if(isRemoved)
				Cache.playersData().put(tag, player);
	}

	public static void unlinkCachedPlayerFromGuild(String tag, Long guildID) {
		unlinkCachedPlayerFromGuild(guildID, Cache.playersData().get(tag));
	}
}
