package it.bitsplitters.legends.batch.commands;

import static it.bitsplitters.legends.batch.BatchParams.logEnabled;
public class CmdLog implements Command {

	@Override
	public String name() {
		return "log";
	}

	@Override
	public void execute(String param) {
		logEnabled = !logEnabled;
		System.out.println("Log abilitato: "+ logEnabled);
	}

}
