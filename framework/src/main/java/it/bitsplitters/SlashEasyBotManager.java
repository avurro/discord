package it.bitsplitters;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.bitsplitters.command.SlashCommand;
import it.bitsplitters.command.exceptions.CommandNotCorrectlyHandledException;
import it.bitsplitters.command.exceptions.CommandPermissionException;
import it.bitsplitters.setting.SlashBotConfig;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import static it.bitsplitters.util.ResourceBundleUtil.getResourceBundle;
import static it.bitsplitters.util.ResourceBundleUtil.COMMAND_NOTHANDLED;
import static it.bitsplitters.util.ResourceBundleUtil.COMMAND_PERMISSION;
import static it.bitsplitters.util.BotUtil.errorEmbed;
import static it.bitsplitters.util.BotUtil.unauthorizedEmbed;

public class SlashEasyBotManager {
	private final Map<String, SlashCommand> slashCommandsMap;
	private final SlashBotConfig botConfig;
	private final SlashCommandsManager commandsManager;
	
	private final Logger logger = LoggerFactory.getLogger(SlashEasyBotManager.class);

	public SlashEasyBotManager(Map<String, SlashCommand> commandsMap, SlashCommandsManager commandsManager, SlashBotConfig botConfig) {
		this.botConfig = botConfig;
		this.commandsManager = commandsManager;
		this.slashCommandsMap = commandsMap;
	}
	
	public void findAndRunGuildCommand(SlashCommandEvent event) {
		
		if (!event.isFromGuild()) return;
		
		SlashCommand slashCommand = slashCommandsMap.get(event.getCommandPath());
		
		try {
			
			if(slashCommand == null) {
				event.deferReply(true).queue();
				throw new CommandNotCorrectlyHandledException(getResourceBundle(botConfig.defaultLocale()).getString(COMMAND_NOTHANDLED));
			}
			
			// True if the command was invoked by an admin bot
			boolean isBotAdminUser = botConfig.adminIDs().contains(event.getMember().getIdLong());
			
			if(!isBotAdminUser)
				if(slashCommand.permissionLevel().isBotAdmin() || !commandsManager.isCommandRunnable(slashCommand, event)) {
					event.deferReply(true).queue();
					throw new CommandPermissionException(getResourceBundle(botConfig.defaultLocale()).getString(COMMAND_PERMISSION));
				}
				
			
			event.deferReply(slashCommand.isEphemeral()).queue();
			
			logger.info("command executed: {} by: {} in server: {}", event.getCommandPath(), event.getMember().getEffectiveName(), event.getGuild().getId());
			
			slashCommand.execute(event);
		
		} catch (CommandPermissionException e) {
			unauthorizedEmbed(event, e.getMessage());
		
		} catch (CommandNotCorrectlyHandledException e) {
			errorEmbed(event, e.getMessage());
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
