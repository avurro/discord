package it.bitsplitters;

import java.util.Optional;

import it.bitsplitters.setting.GuildSettings;

/**
 * <b>Guilds Manager</b>
 * 
 * interface that defines guilds management.
 * 
 * @author AVurro
 *
 */
public interface GuildsManager {

	/**
	 * @param guildID
	 * @return
	 */
	Optional<GuildSettings> find(Long guildID);
	
	/**
	 * @param guildID
	 */
	void guildJoin(Long guildID);
	
	/**
	 * @param guildID
	 */
	void guildLeave(Long guildID);
	
	/**
	 * @param guildID
	 */
	GuildSettings register(Long guildID);
}