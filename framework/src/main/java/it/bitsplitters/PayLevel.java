package it.bitsplitters;

public enum PayLevel {
	FREE(0,"Free use"),
	PAYER(1,"subscribed");
	
	private int level;
	private String description;

	private PayLevel(int level, String description) {
		this.level = level;
		this.description = description;
	}
	
	public static boolean isFree(int level) {
		return FREE.level == level;
	}
	
	public String getDescription() {
		return this.description;
	}
}
