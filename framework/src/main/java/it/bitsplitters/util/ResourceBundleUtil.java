package it.bitsplitters.util;

import static java.util.ResourceBundle.getBundle;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class ResourceBundleUtil {

	public static final String TEST = "test";
	public static final String RESOURCE_FULLNAME = "discord-resource";
	public static final String COMMAND_SYNTAX = "command.syntax";
	public static final String COMMAND_PERMISSION = "command.permission";
	public static final String COMMAND_NOTHANDLED = "command.nothandled";
	public static final String COMMAND_HELP = "command.help";
	public static final String COMMAND_ADMIN = "command.admin";
	public static final String COMMAND_HELP_TITLE = "command.help.title";
	public static final String COMMAND_LEGEND = "command.legend";
	public static final String COMMAND_CHANNEL_GUILD = "command.channelenabledguild";
	public static final String COMMAND_CHANNEL_DMBOT = "command.channelenableddmbot";
	
	private static final String[] placeholders = new String[] {
		"{0}", "{1}", "{2}", "{3}", "{4}", "{5}", "{6}", "{7}", "{8}", "{9}", "{10}" 	
	};
	
	public static final ResourceBundle.Control customControl = new ResourceBundle.Control() {
		@Override
		public Locale getFallbackLocale(String baseName, Locale locale) {
			return Locale.ROOT;
		}
    };
    
	public static String replaceValues(String msg, String... values) {
		
		int pos = 0;
		for (String value : values) 
			msg = msg.replace(placeholders[pos++], value);
		
		return msg;
	}
	
	private static Map<Locale, ResourceBundle> guildsRB = new HashMap<>();
	
	public static ResourceBundle getResourceBundle(Locale locale) {
		return getResourceBundle(locale, RESOURCE_FULLNAME);
	}
	
	public static ResourceBundle getResourceBundle(Locale locale, String resourceName) {
		
		if(guildsRB.containsKey(locale))
			return guildsRB.get(locale);
		
		ResourceBundle bundle = getBundle(resourceName, locale, customControl);
		guildsRB.put(locale, bundle);
		return bundle;
	}
}