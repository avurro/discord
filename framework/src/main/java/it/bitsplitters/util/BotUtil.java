package it.bitsplitters.util;

import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;

import it.bitsplitters.GuildsManager;
import it.bitsplitters.UsersManager;
import it.bitsplitters.setting.Settings;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.GuildChannel;
import net.dv8tion.jda.api.events.interaction.SelectionMenuEvent;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

/**
 * Utility methods that are very useful in the common use of bots.
 * 
 * @author AVurro
 *
 */
public class BotUtil {

	private static final String[] charsToEscapeRegex = new String[] {"\\*","\\_","\\`","\\~~"};
	private static final String[] charsToEscapeReplacement = new String[] {"\\\\*","\\\\_","\\\\`","\\\\~~"};
	
	public static String escapeContent(String msg) {
		for (int i=0; i < charsToEscapeRegex.length; i++) 
			msg = msg.replaceAll(charsToEscapeRegex[i], charsToEscapeReplacement[i]);
		
		return msg;
	}
	
	public static Optional<GuildChannel> findChannel(List<GuildChannel> channels, String channelName) {
		
		if(channelName.startsWith("#")) {
			channelName = channelName.substring(1);
			for (GuildChannel guildChannel : channels) 
				if(guildChannel.getName().equals(channelName)) 
					return Optional.of(guildChannel);
		}
		
		return Optional.empty();
	}
	
	public static Optional<? extends Settings> getSettings(MessageReceivedEvent event, GuildsManager guildsManager, UsersManager usersManager){
		
		Optional<? extends Settings> optSettings = Optional.empty();
		
		if(event.getChannel().getType().isGuild()) {
			optSettings = guildsManager.find(event.getGuild().getIdLong());
			return !optSettings.isPresent() ? Optional.empty() : optSettings;
		}else {
			optSettings = usersManager.find(event.getAuthor().getIdLong());
			return !optSettings.isPresent() ? Optional.empty() : optSettings;
		}
	}
	
	public static InputStream toStream(String name) throws IOException {
		
		URL url = Thread.currentThread().getContextClassLoader().getResource(name);

	    // Could not find resource. Try with the classloader that loaded this class.
	    if (url == null) {
	      ClassLoader classLoader = BotUtil.class.getClassLoader();
	      if (classLoader != null) 
	        url = classLoader.getResource(name);
	    }

	    // Last ditch attempt searching classpath
	    if (url == null) 
	      url = ClassLoader.getSystemResource(name);
	    
	    // one more time
	    if (url == null && name.contains(":")) 
	        url = new URL(name);
	    
		return url.openStream();
	}
	
	public static Color getColor(String[] colors) {
		return new Color(Integer.valueOf(colors[0]), Integer.valueOf(colors[1]), Integer.valueOf(colors[2]));
	}
	
	public static boolean isEmpty(String str) {
		return str == null || str.isEmpty();
	}
	
	public static String caseInsensitive(String msg) {
		return msg.toLowerCase();
	}
	
	public static List<String> moreLinesToOne(List<String> lines, int linesNumber){
		List<String> result = new ArrayList<>();
		
		int count = 0;
		StringJoiner sj = new StringJoiner("\n");

		for (String string : lines) {
			
			sj.add(string);
			
			if(++count >= linesNumber) {
				result.add(sj.toString());
				sj = new StringJoiner("\n");
				count = 0;
			}
		}
		
		if(sj.length() > 0)
			result.add(sj.toString());
		
		return result;
	}
	
	public static void infoEmbed(SlashCommandEvent event, String msg) {
		infoEmbed(event, msg, new Color(254, 254, 254));
	}
	
	public static void infoEmbed(SlashCommandEvent event, String msg, Color color) {
		printEmbed(event, msg, "\uD83D\uDCF0 INFO", color);
	}
	
	public static void infoEmbed(MessageReceivedEvent event, String msg) {
		infoEmbed(event, msg, new Color(254, 254, 254));
	}
	
	public static void infoEmbed(MessageReceivedEvent event, String msg, Color color) {
		printEmbed(event, msg, "\uD83D\uDCF0 INFO", color);
	}
	
	public static void unauthorizedEmbed(MessageReceivedEvent event, String msg) {
		unauthorizedEmbed(event, msg, Color.red);
	}
	
	public static void unauthorizedEmbed(MessageReceivedEvent event, String msg, Color color) {
		printEmbed(event, msg, "\u26D4 UNAUTHORIZED", color);
	}
	
	public static void unauthorizedEmbed(SlashCommandEvent event, String msg) {
		unauthorizedEmbed(event, msg, Color.red);
	}
	
	public static void unauthorizedEmbed(SlashCommandEvent event, String msg, Color color) {
		printEmbed(event, msg, "\u26D4 UNAUTHORIZED", color);
	}
	
	public static void errorEmbed(SlashCommandEvent event, String msg) {
		errorEmbed(event, msg, Color.red);
	}
	
	public static void errorEmbed(SlashCommandEvent event, String msg, Color color) {
		printEmbed(event, msg, "\u2757 ERROR", color);
	}
	
	public static void errorEmbed(MessageReceivedEvent event, String msg) {
		errorEmbed(event, msg, Color.red);
	}
	
	public static void errorEmbed(MessageReceivedEvent event, String msg, Color color) {
		printEmbed(event, msg, "\u2757 ERROR", color);
	}
	
	public static void codeEmbed(SlashCommandEvent event, String msg) {
		codeEmbed(event, msg, Color.black);
	}
	public static void codeEmbed(SlashCommandEvent event, String msg, Color color) {
		printEmbed(event, "```"+msg+"```", "\uD83D\uDCDD COMMAND", color);
	}
	
	public static void codeEmbed(MessageReceivedEvent event, String msg) {
		codeEmbed(event, msg, Color.black);
	}
	public static void codeEmbed(MessageReceivedEvent event, String msg, Color color) {
		printEmbed(event, "```"+msg+"```", "\uD83D\uDCDD COMMAND", color);
	}
	
	public static void questionEmbed(MessageReceivedEvent event, String msg) {
		questionEmbed(event, msg, Color.white);
	}
	public static void questionEmbed(MessageReceivedEvent event, String msg, Color color) {
		printEmbed(event, msg, "\u2754 QUESTION", color);
	}
	
	public static void stopEmbed(SlashCommandEvent event, String msg) {
		stopEmbed(event, msg, Color.orange);
	}
	
	public static void stopEmbed(SlashCommandEvent event, String msg, Color color) {
		printEmbed(event, msg, "\u270B\uD83C\uDFFB STOP", color);
	}
	
	public static void stopEmbed(MessageReceivedEvent event, String msg) {
		stopEmbed(event, msg, Color.orange);
	}
	
	public static void stopEmbed(MessageReceivedEvent event, String msg, Color color) {
		printEmbed(event, msg, "\u270B\uD83C\uDFFB STOP", color);
	}
	
	public static void warningEmbed(SelectionMenuEvent event, String msg) {
		warningEmbed(event, msg, Color.yellow);
	}
	
	public static void warningEmbed(SlashCommandEvent event, String msg) {
		warningEmbed(event, msg, Color.yellow);
	}
	
	public static void warningEmbed(SelectionMenuEvent event, String msg, Color color) {
		printEmbed(event, msg, "\u26A0\uFE0F WARNING", color);
	}
	
	public static void warningEmbed(SlashCommandEvent event, String msg, Color color) {
		printEmbed(event, msg, "\u26A0\uFE0F WARNING", color);
	}
	
	public static void warningEmbed(MessageReceivedEvent event, String msg) {
		warningEmbed(event, msg, Color.yellow);
	}
	
	public static void warningEmbed(MessageReceivedEvent event, String msg, Color color) {
		printEmbed(event, msg, "\u26A0\uFE0F WARNING", color);
	}
	
	public static void okEmbed(SelectionMenuEvent event, String msg) {
		okEmbed(event, msg, Color.green);
	}
	
	public static void okEmbed(SlashCommandEvent event, String msg) {
		okEmbed(event, msg, Color.green);
	}
	
	public static void okEmbed(MessageReceivedEvent event, String msg) {
		okEmbed(event, msg, Color.green);
	}
	
	public static void okEmbed(SelectionMenuEvent event, String msg, Color color) {
		printEmbed(event, msg, "\u2705 DONE", color);
	}
	
	public static void okEmbed(SlashCommandEvent event, String msg, Color color) {
		printEmbed(event, msg, "\u2705 DONE", color);
	}
	
	public static void okEmbed(MessageReceivedEvent event, String msg, Color color) {
		printEmbed(event, msg, "\u2705 DONE", color);
	}
	
	public static EmbedBuilder infoEmbedBuilder(String msg) {
		return makeEmbed(msg, "\uD83D\uDCF0 INFO", new Color(254, 254, 254));
	}
	
	public static EmbedBuilder okEmbedBuilder(String msg) {
		return makeEmbed(msg, "\u2705 DONE", Color.green);
	}
	
	public static EmbedBuilder warningEmbedBuilder(String msg) {
		return makeEmbed(msg, "\u26A0\uFE0F WARNING", Color.yellow);
	}
	
	public static EmbedBuilder stopEmbedBuilder(String msg) {
		return makeEmbed(msg, "\u270B\uD83C\uDFFB STOP", Color.orange);
	}
	
	public static void printEmbed(SelectionMenuEvent event, String description, String author, Color color) {
		event.getHook().sendMessageEmbeds(makeEmbed(description, author, color).build()).queue();
	}
	
	public static void printEmbed(SlashCommandEvent event, String description, String author, Color color) {
		event.getHook().sendMessageEmbeds(makeEmbed(description, author, color).build()).queue();
	}
	
	public static void printEmbed(MessageReceivedEvent event, String description, String author, Color color) {
		event.getChannel().sendMessageEmbeds(makeEmbed(description, author, color).build()).queue();
	}
	
	private static EmbedBuilder makeEmbed(String description, String author, Color color) {
		EmbedBuilder eb = new EmbedBuilder();
		
		if(author != null)
			eb.setAuthor(author);
		
		if(color != null)
			eb.setColor(color);
		
		eb.setDescription(description);

		return eb;
	}
}