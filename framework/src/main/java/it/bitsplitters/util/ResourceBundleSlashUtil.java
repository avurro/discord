package it.bitsplitters.util;

import static java.util.ResourceBundle.getBundle;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class ResourceBundleSlashUtil {
	
	public static final String SLASH_COMMAND_RESOURCE_FULLNAME = "slash-resource";
	
	private static final String[] placeholders = new String[] {
		"{0}", "{1}", "{2}", "{3}", "{4}", "{5}", "{6}", "{7}", "{8}", "{9}", "{10}" 	
	};
	
	public static final ResourceBundle.Control customControl = new ResourceBundle.Control() {
		@Override
		public Locale getFallbackLocale(String baseName, Locale locale) {
			return Locale.ROOT;
		}
    };
    
	public static String replaceValues(String msg, String... values) {
		
		int pos = 0;
		for (String value : values) 
			msg = msg.replace(placeholders[pos++], value);
		
		return msg;
	}
	
	private static Map<Locale, ResourceBundle> guildsSlashRB = new HashMap<>();
	
	public static ResourceBundle getSlashResourceBundle(Locale locale) {
		return getSlashResourceBundle(locale, SLASH_COMMAND_RESOURCE_FULLNAME);
	}
	
	public static ResourceBundle getSlashResourceBundle(Locale locale, String resourceName) {
		
		if(guildsSlashRB.containsKey(locale))
			return guildsSlashRB.get(locale);
		
		ResourceBundle bundle = getBundle(resourceName, locale, customControl);
		guildsSlashRB.put(locale, bundle);
		return bundle;
	}
}
