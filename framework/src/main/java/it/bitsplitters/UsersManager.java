package it.bitsplitters;

import java.util.Optional;

import it.bitsplitters.setting.UserSettings;

/**
 * <b>Users Manager</b>
 * 
 * interface that defines users management.
 * 
 * @author AVurro
 *
 */
public interface UsersManager {

	/**
	 * @param userID
	 * @return
	 */
	Optional<UserSettings> find(Long userID);
	
	/**
	 * @param userID
	 */
	UserSettings register(Long userID);
	
}
