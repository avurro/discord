package it.bitsplitters.setting;

/**
 * <b>Guild settings</b>.<br><br>
 * Each guild can define a prefix and the locale.<br>
 * 
 * @author AVurro
 *
 */
public interface GuildSettings extends Settings{}
