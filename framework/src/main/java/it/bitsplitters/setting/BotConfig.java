package it.bitsplitters.setting;

import java.util.List;

/**
 * <b>Discord Bot configuration</b>.<br><br>
 * The ability to customize the bot passes through here.<br>
 * In this interface you specify whether to make everything case sensitive and which users are administrators of the bot.
 * 
 * @author AVurro
 *
 */
public interface BotConfig {

	/**
	 * <b>Commands setting</b>.<br>
	 * True if you want commands case sensitive, so that the upper and lower case are respected.
	 * 
	 * @return true if commands are case sensitive, false all other cases.
	 */
	public boolean isCaseSensitive();
	
	/**
	 * List of administrators of this BOT.
	 * 
	 * @return List of Admin IDs
	 */
	public List<Long> adminIDs();
	
	/**
	 * Resource bundle used by BOT.
	 * @return
	 */
	public String resourceBundle();
}
