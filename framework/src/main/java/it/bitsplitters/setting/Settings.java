package it.bitsplitters.setting;

import java.util.Locale;

/**
 * <b>General Settings</b>.<br><br>
 * Every entity must define a prefix and the locale.<br>
 * 
 * @author AVurro
 *
 */
public interface Settings {

	/**
	 * <b>ENTITY ID</b>.<br>
	 * This is an 18-digit number
	 * @return entity's ID
	 */
	public Long id();
	
	/**
	 * <b>Prefix definition</b>.<br>
	 * Definition of the character to be used as a <i>prefix</i> for each command provided by the bot.
	 * 
	 * @return Prefix to use 
	 */
	public String prefix();
	
	/**
	 * <b>Locale definition</b>.<br>
	 * Locale which identifies the language and possibly the geographical position of the discord server, 
	 * mainly used for i18n.
	 * @return Locale of specific Server
	 */
	public Locale locale();
}
