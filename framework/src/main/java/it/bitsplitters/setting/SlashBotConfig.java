package it.bitsplitters.setting;

import java.util.List;
import java.util.Locale;

@FunctionalInterface
public interface SlashBotConfig {

	/**
	 * List of administrators of this BOT.
	 * 
	 * @return List of Admin IDs
	 */
	public List<Long> adminIDs();
	
	public default Locale defaultLocale() {
		return Locale.ENGLISH;	
	}
}
