package it.bitsplitters.setting;

/**
 * <b>User settings</b>.<br><br>
 * Each user can define a prefix and the locale.<br>
 * 
 * @author AVurro
 *
 */
public interface UserSettings extends Settings{}