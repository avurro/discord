/**
 * Package containing the highest level configurations.
 * 
 * @author AVurro
 *
 */
package it.bitsplitters;