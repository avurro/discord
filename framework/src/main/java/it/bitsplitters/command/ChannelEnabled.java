package it.bitsplitters.command;

public enum ChannelEnabled {
	DMBOT("direct message to bot"),
	GUILD("guild"),
	ALL("all");
	
	private String descr;
	
	private ChannelEnabled(String descr) {
		this.descr = descr;
	}
	
	public boolean isExecutable(ChannelEnabled channel) {
		return this.equals(ALL) || this.equals(channel);
	}
	
	public String value() {
		return descr;
	}
}
