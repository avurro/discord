package it.bitsplitters.command;

import java.util.List;
import java.util.Locale;

import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public interface SlashCommand {

	
	/**
	 * @return command path
	 */
	public String path(Locale locale);
	
	/**
	 * @return command name
	 */
	public String name(Locale locale);
	
	/**
	 * @return command description
	 */
	public String description(Locale locale);
	
	/**
	 * @return command options
	 */
	public List<OptionData> options(Locale locale);
	
	/**
	 * @param  command options values
	 */
	public void execute(SlashCommandEvent event);
	
	/**
	 * @return Permission level
	 */
	PermissionLevel permissionLevel();
	
	/**
	 * @return channel enabled
	 */
	ChannelEnabled channelEnabled();
	
	/**
	 * @return true if the messages of this command are ephemeral
	 */
	boolean isEphemeral();
}
