package it.bitsplitters.command.exceptions;

@SuppressWarnings("serial")
public class CommandNotCorrectlyHandledException extends Exception {

	public CommandNotCorrectlyHandledException(String msg) {
		super(msg);
	}
}
