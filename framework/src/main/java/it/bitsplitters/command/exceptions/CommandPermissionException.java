package it.bitsplitters.command.exceptions;

/**
 * In case of the user isn't allowed to use a specific command.
 * 
 * @author AVurro
 *
 */
@SuppressWarnings("serial")
public class CommandPermissionException extends Exception{

	public CommandPermissionException(String msg) {
		super(msg);
	}
}
