/**
 * Commands exceptions.
 * 
 * @author AVurro
 *
 */
package it.bitsplitters.command.exceptions;