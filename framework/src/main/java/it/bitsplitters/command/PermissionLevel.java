package it.bitsplitters.command;

/**
 * Define the command permission level.<br>
 * @author AVurro
 *
 */
public enum PermissionLevel {

	PUBLIC(0), RESERVED(10), ADMIN(20), BOT_ADMIN(30);
	
	private int level;
	
	private PermissionLevel(int level) {
		this.level = level;
	}
	
	public int get() {
		return level;
	}
	
	public boolean isReserved() {
		return this.level == RESERVED.level;
	}
	
	public boolean isBotAdmin() {
		return this.level == BOT_ADMIN.level;
	}
	
	public boolean isAdmin() {
		return this.level == ADMIN.level;
	}

	public boolean isPublic() {
		return this.level == PUBLIC.level;
	}
	
	public static boolean isAdmin(Long role) {
		return role.equals(Long.valueOf(ADMIN.level));
	}
	
	public static boolean isPublic(Long role) {
		return role.equals(Long.valueOf(PUBLIC.level));
	}
}
