package it.bitsplitters.command;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

/**
 * <b>Command definition</b>.<br>
 * Interface that defines the basic behavior of each individual command.<br>
 * This interface is not directly usable, since the BotManager accepts only <code>GenericComand</code>.<br>
 * Each command will therefore have to extend it.<br>
 * For more info: {@link GeneralCommand}
 * 
 * @author AVurro
 */
public interface Command {

	/**
	 * Unique code that identifies the command.
	 * 
	 * @return Command key (prefix excluded)
	 */
	String key();

	/**
	 * @return Command name (prefix excluded)
	 */
	String name();

	/**
	 * @return Permission level
	 */
	PermissionLevel permissionLevel();
	
	/**
	 * @return channel enabled
	 */
	ChannelEnabled channelEnabled();
	
	/**
	 * Embed title used for help porpose.<br>
	 * By default is used <i>Command description</i> and similar for other languages.
	 * @return
	 */
	public String helpTitle();
	
	/**
	 * Embed content used for help porpose.<br>
	 * It is mandatory to define the content.
	 * @param event 
	 * @return
	 */
	String helpContent(MessageReceivedEvent event);
	
}