package it.bitsplitters.command;

import static it.bitsplitters.util.ResourceBundleSlashUtil.SLASH_COMMAND_RESOURCE_FULLNAME;
import static it.bitsplitters.util.ResourceBundleSlashUtil.getSlashResourceBundle;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public abstract class GeneralSlashCommand implements SlashCommand {

	public abstract String helpKey();
	
	public abstract String nameKey();
	
	public abstract List<String> pathKeys();
	
	@Override
	public String name(Locale locale) {
		return getMsg(nameKey(), locale);
	}
	
	@Override
	public String path(Locale locale) {
		List<String> pathPieces = new ArrayList<>(pathKeys().size()+1);
		
		for (String key : pathKeys()) 
			pathPieces.add(getMsg(key, locale));
		
		pathPieces.add(getMsg(nameKey(), locale));
		
		return String.join("/", pathPieces);
	}
	
	@Override
	public String description(Locale locale) {
		return getMsg(helpKey(), locale);
	}

	@Override
	public PermissionLevel permissionLevel() {
		return PermissionLevel.PUBLIC;
	}

	@Override
	public ChannelEnabled channelEnabled() {
		return ChannelEnabled.GUILD;
	}

	public String getMsg(String key, String locale) {
		return getMsg(key, Locale.forLanguageTag(locale));
	}
	
	public String getMsg(String key, Locale locale) {
		return getSlashResourceBundle(locale, SLASH_COMMAND_RESOURCE_FULLNAME).getString(key);
	}
}