/**
 * Signature exceptions.
 * 
 * @author AVurro
 *
 */
package it.bitsplitters.command.signature.exceptions;