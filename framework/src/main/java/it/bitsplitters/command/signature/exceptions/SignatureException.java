package it.bitsplitters.command.signature.exceptions;

import it.bitsplitters.command.Command;

/**
 * Exception that contains the error data relating to the signature of the command used.
 * 
 * @author AVurro
 *
 */
@SuppressWarnings("serial")
public class SignatureException extends Exception {

	private final Command command;
	
	public SignatureException(Command command, String message) {
		super(message);
		this.command = command;
	}

	public Command getCommand() {
		return command;
	}
}
