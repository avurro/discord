package it.bitsplitters;

import it.bitsplitters.command.SlashCommand;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
/**
 * <b>Slash Commands Manager</b><br><br>
 * It handles slash commands, mainly concerned with verifying that a command is executable or not.
 * 
 * @author AVurro
 */
@FunctionalInterface
public interface SlashCommandsManager {

	
	/**
	 * Verify that the slash command, for this event, is executable or not.
	 * 
	 * @param command the command invoked
	 * @param event event that includes all data related to the command
	 * @return true if it is executable, false otherwise
	 */
	boolean isCommandRunnable(SlashCommand command, SlashCommandEvent event);
}
