package it.bitsplitters;

import it.bitsplitters.command.Command;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

/**
 * <b>Commands Manager</b><br><br>
 * It handles commands, mainly concerned with verifying that a command is executable or not.
 * 
 * @author AVurro
 */
@FunctionalInterface
public interface CommandsManager {

	
	/**
	 * Verify that the command, for this event, is executable or not.
	 * 
	 * @param command the command invoked
	 * @param event event that includes all data related to the message
	 * @return true if it is executable, false otherwise
	 */
	boolean isCommandRunnable(Command command, MessageReceivedEvent event);
}